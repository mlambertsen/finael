/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUMATH_POLYGAMMA_DETAILS_PROVIDER_H_
#define FINAEL_CUMATH_POLYGAMMA_DETAILS_PROVIDER_H_

#include "../common/cudaHostDevice.h"

#include "constants.h"
#include "generalFunctions.h"


namespace finael {
namespace cuMath {



template<unsigned int derivative>
class PolyGammaDetailsProvider
{
	static_assert(sizeof(derivative) == 0, "PolyGamma not implemented for requested derivative");
};



template<> class PolyGammaDetailsProvider<0>
{
public:
	template<typename T>
	CUDA_HOST_DEVICE constexpr int selectImaginaryLimit() const
	{
		return (sizeof(T) == 4) ? 10 : 110;
	}



	template<template<typename> class Complex, typename T>
	CUDA_HOST_DEVICE Complex<T> approximateForLargeRealPart( const Complex<T>& argument ) const
	{
		return log( argument ) - 0.5 / argument - 1.0/12.0 / cuMath::Pow<Complex<T>, 2>::pow( argument ) + 1.0/120.0 / cuMath::Pow<Complex<T>, 4>::pow( argument ) - 1.0/252.0 / cuMath::Pow<Complex<T>, 6>::pow( argument );
	}



	template<template<typename> class Complex, typename T>
	CUDA_HOST_DEVICE Complex<T> reflect( const Complex<T>& argument ) const
	{
		return approximateForLargeRealPart( 1 - argument ) - cuMath::CONST<T>::PI * cuMath::cospi( argument ) / cuMath::sinpi( argument );
	}



	template<template<typename> class Complex, typename T>
	CUDA_HOST_DEVICE Complex<T> calculateRecurrenceContribution( const Complex<T>& argument ) const
	{
		return -1 / (argument);
	}
};



template<> class PolyGammaDetailsProvider<1>
{
public:
	template<typename T>
	CUDA_HOST_DEVICE constexpr int selectImaginaryLimit() const
	{
		return (sizeof(T) == 4) ? 20 : 220;
	}



	template<template<typename> class Complex, typename T>
	CUDA_HOST_DEVICE Complex<T> approximateForLargeRealPart( const Complex<T>& argument ) const
	{
		return 1 / argument + 0.5 / cuMath::Pow<Complex<T>, 2>::pow( argument ) + 1.0/6.0 / cuMath::Pow<Complex<T>, 3>::pow( argument ) - 1.0/30.0 / cuMath::Pow<Complex<T>, 5>::pow( argument ) + 1.0/42.0 / cuMath::Pow<Complex<T>, 7>::pow( argument ) - 1.0/30.0 / cuMath::Pow<Complex<T>, 9>::pow( argument );
	}



	template<template<typename> class Complex, typename T>
	CUDA_HOST_DEVICE Complex<T> reflect( const Complex<T>& argument ) const
	{
		return cuMath::Pow<Complex<T>, 2>::pow( cuMath::CONST<T>::PI / cuMath::sinpi( argument ) ) - approximateForLargeRealPart( static_cast<T>(1) - argument );
	}



	template<template<typename> class Complex, typename T>
	CUDA_HOST_DEVICE Complex<T> calculateRecurrenceContribution( const Complex<T>& argument ) const
	{
		return 1 / cuMath::Pow<Complex<T>, 2>::pow( argument );
	}
};



template<> class PolyGammaDetailsProvider<2>
{
public:
	template<typename T>
	CUDA_HOST_DEVICE constexpr int selectImaginaryLimit() const
	{
		return (sizeof(T) == 4) ? 20 : 220;
	}



	template<template<typename> class Complex, typename T>
	CUDA_HOST_DEVICE Complex<T> approximateForLargeRealPart( const Complex<T>& argument ) const
	{
		return -1 / cuMath::Pow<Complex<T>, 2>::pow( argument ) - 1 / cuMath::Pow<Complex<T>, 3>::pow( argument ) - 0.5 / cuMath::Pow<Complex<T>, 4>::pow( argument ) + 1.0/6.0 * (cuMath::Pow<Complex<T>, 2>::pow( argument ) - 1) / cuMath::Pow<Complex<T>, 8>::pow( argument ) + 3.0/10.0 / cuMath::Pow<Complex<T>, 10>::pow( argument );
	}



	template<template<typename> class Complex, typename T>
	CUDA_HOST_DEVICE Complex<T> reflect( const Complex<T>& argument ) const
	{
		return approximateForLargeRealPart( 1 - argument ) - 2 * cuMath::cospi( argument ) * cuMath::Pow<Complex<T>, 3>::pow( cuMath::CONST<T>::PI / cuMath::sinpi( argument ) );
	}



	template<template<typename> class Complex, typename T>
	CUDA_HOST_DEVICE Complex<T> calculateRecurrenceContribution( const Complex<T>& argument ) const
	{
		return -2 / cuMath::Pow<Complex<T>, 3>::pow( argument );
	}
};


} // namespace cuQcd
} // namespace finael



#endif // FINAEL_CUMATH_POLYGAMMA_DETAILS_PROVIDER_H_
