/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_QCD_PDFINTERPOLATION_CT_FORTRAN_CODE_SELECTOR_H_
#define FINAEL_QCD_PDFINTERPOLATION_CT_FORTRAN_CODE_SELECTOR_H_


#include <stdexcept>
#include <string>

#include "ct_settings.h"
#include "ct_fortran.h"


namespace finael {
namespace qcd {
namespace pdfInterpolation {



template<CtSet ctSet>
class CtFortranCodeSelector
{
public:
	constexpr static bool usesFortranCT12()
	{
		return ctSet == CtSet::CT10 || ctSet == CtSet::CT10N || ctSet == CtSet::CT10NN;
	}



	constexpr static bool usesFortranCT14()
	{
		return ctSet == CtSet::CT14;
	}



	static_assert( usesFortranCT12() || usesFortranCT14(), "Unexpected template spezialization in CtFortranCodeSelector" );



	static void set( const std::string& inputSetName )
	{
		if (usesFortranCT12())
		{
			fortran::setct12_( inputSetName.c_str() );
		}
		else if (usesFortranCT14())
		{
			fortran::setct14_( inputSetName.c_str() );
		}
	}



	static double call( const int flavour, const double x, const double scale )
	{
		if (usesFortranCT12())
		{
			return fortran::ct12pdf_( &flavour, &x, &scale );
		}
		else if (usesFortranCT14())
		{
			return fortran::ct14pdf_( &flavour, &x, &scale );
		}
		return 0.0;
	}
};



} // namespace pdfInterpolation
} // namespace qcd
} // namespace finael



#endif // FINAEL_QCD_PDFINTERPOLATION_CT_FORTRAN_CODE_SELECTOR_H_
