/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#define BOOST_TEST_MODULE "finael Particle Validator"
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "../../../include/qcd/ParticleValidator.h"



BOOST_AUTO_TEST_SUITE( particleValidator )



BOOST_AUTO_TEST_CASE( recognizesGluon )
{
	using namespace finael::cuQcd;
	BOOST_CHECK_EQUAL( ParticleValidator<PartonFlavour::G>::isGluon, true );
}



BOOST_AUTO_TEST_CASE( rejectsUpAsGluon )
{
	using namespace finael::cuQcd;
	BOOST_CHECK_EQUAL( ParticleValidator<PartonFlavour::U>::isGluon, false );
}



BOOST_AUTO_TEST_CASE( rejectsDownBarAsGluon )
{
	using namespace finael::cuQcd;
	BOOST_CHECK_EQUAL( ParticleValidator<PartonFlavour::DBAR>::isGluon, false );
}



BOOST_AUTO_TEST_CASE( recognizesUpAsParticle )
{
	using namespace finael::cuQcd;
	BOOST_CHECK_EQUAL( ParticleValidator<PartonFlavour::U>::isQuark, true );
}



BOOST_AUTO_TEST_CASE( rejectsUpAsAntiParticle )
{
	using namespace finael::cuQcd;
	BOOST_CHECK_EQUAL( ParticleValidator<PartonFlavour::U>::isAntiQuark, false );
}



BOOST_AUTO_TEST_CASE( recognizesCharmBarAsAntiParticle )
{
	using namespace finael::cuQcd;
	BOOST_CHECK_EQUAL( ParticleValidator<PartonFlavour::CBAR>::isAntiQuark, true );
}



BOOST_AUTO_TEST_CASE( rejectsCharmBarAsParticle )
{
	using namespace finael::cuQcd;
	BOOST_CHECK_EQUAL( ParticleValidator<PartonFlavour::CBAR>::isQuark, false );
}



BOOST_AUTO_TEST_CASE( recognizesCharmAsWeakIsospinUpAndCharmBarAsWeakIsospinDown )
{
	using namespace finael::cuQcd;
	BOOST_CHECK_EQUAL( ParticleValidator<PartonFlavour::C>::isWeakIsospinUp, true );
	BOOST_CHECK_EQUAL( ParticleValidator<PartonFlavour::CBAR>::isWeakIsospinDown, true );
}



BOOST_AUTO_TEST_CASE( rejectsGluonAsWeakIsospinUpAndDown )
{
	using namespace finael::cuQcd;
	BOOST_CHECK_EQUAL( ParticleValidator<PartonFlavour::G>::isWeakIsospinUp, false );
	BOOST_CHECK_EQUAL( ParticleValidator<PartonFlavour::G>::isWeakIsospinDown, false );
}



BOOST_AUTO_TEST_CASE( rejectsStrangeAsWeakIsospinUpAndStrangeBarAsWeakIsospinDown )
{
	using namespace finael::cuQcd;
	BOOST_CHECK_EQUAL( ParticleValidator<PartonFlavour::S>::isWeakIsospinUp, false );
	BOOST_CHECK_EQUAL( ParticleValidator<PartonFlavour::SBAR>::isWeakIsospinDown, false );
}



BOOST_AUTO_TEST_CASE( recognizesBottomAsWeakIsospinDownAndBottomBarAsWeakIsospinUp )
{
	using namespace finael::cuQcd;
	BOOST_CHECK_EQUAL( ParticleValidator<PartonFlavour::B>::isWeakIsospinDown, true );
	BOOST_CHECK_EQUAL( ParticleValidator<PartonFlavour::BBAR>::isWeakIsospinUp, true );
}



BOOST_AUTO_TEST_CASE( rejectsTopAsWeakIsospinDownAndTopBarAsWeakIsospinUp )
{
	using namespace finael::cuQcd;
	BOOST_CHECK_EQUAL( ParticleValidator<PartonFlavour::T>::isWeakIsospinDown, false );
	BOOST_CHECK_EQUAL( ParticleValidator<PartonFlavour::TBAR>::isWeakIsospinUp, false );
}





BOOST_AUTO_TEST_SUITE_END()
