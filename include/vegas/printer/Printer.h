/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_PRINTER_H_
#define FINAEL_CUVEGAS_PRINTER_H_

#include <cassert>
#include <cmath>
#include <ios>
#include <memory>
#include <ostream>
#include <string>

#include "../../common/cudaHostDevice.h"

#include "../increments/Increments.h"
#include "../increments/IncrementsContributions.h"
#include "../integrandTypes.h"
#include "../optionFlags.h"
#include "../ResultsConstructor.h"
#include "../sums/AccumulatedSums.h"
#include "../sums/EstimateSum.h"

#include "PrinterCache.h"


namespace finael {
namespace cuVegas {



template<unsigned int dim, typename T>
class Printer
{
public:
	static constexpr std::streamsize precisionDefault = 5;
	static constexpr VerbosityFlag verbosityDefault = VerbosityFlag::GENTLE;



	CUDA_HOST Printer()
		: stream_( &std::cout, [](std::ostream*){} )
		, precision_( precisionDefault )
		, verbosity_( verbosityDefault )
		, incrementContributions_()
	{}



	CUDA_HOST void printSettings( const Settings<T>& settings, const InternalSettings<dim>& internalSettings, PrngFlag prngFlag, const VolumeType<T, dim>& integrationLimits ) const
	{
		if (isQuiet()) return;

		PrinterCache userCache( *stream_, precision_ );

		auto strategy = determineStrategyText( settings, internalSettings );
		auto prng = determinePrngText( prngFlag );
		auto reduction = determineReductionText( internalSettings );
		auto verbosity = determineVerbosityText();
		auto samplesText = determineSamplesText( settings, internalSettings );

		stream_->unsetf( std::ios::floatfield );

		*stream_ << "Input parameters of Vegas:\n"
					<< "  dimension                =\t" << dim << "\n"
					<< "  iterations               =\t" << settings.numberIterations << "\n"
					<< "  samples                  =\t" << samplesText << "\n"
					<< "  increments per dimension =\t" << internalSettings.incrementsPerDim << "\n"
					<< "  adaption rate            =\t" << settings.adaptionRate << "\n"
					<< "  strategy                 =\t" << strategy << "\n"
					<< "  random number generator  =\t" << prng << "\n"
					<< "  reduction mode           =\t" << reduction << "\n"
					<< "  verbosity                =\t" << verbosity << "\n\n";

		printIntegrationLimits( integrationLimits );
	}



	CUDA_HOST void printIterationInfos( const AccumulatedSums<T>& accumulatedSums, const EstimateSum<T>& estimateSum, const Increments<dim, T, CudaHostDeviceType::HOST>& increments )
	{
		if (isQuiet()) return;

		PrinterCache userCache( *stream_, precision_ );

		printIterationResult( accumulatedSums, estimateSum );

		if (isVerbose())
		{
			incrementContributions_.normalize( estimateSum );
			printIncrements( increments );
		}
	}



	CUDA_HOST bool isVerbose() const
	{
		return verbosity_ == VerbosityFlag::VERBOSE;
	}



	CUDA_HOST bool isQuiet() const
	{
		return verbosity_ == VerbosityFlag::QUIET;
	}



	CUDA_HOST void setVerbosity( VerbosityFlag verbosity )
	{
		verbosity_ = verbosity;
	}



	CUDA_HOST void setPrecision( T precision )
	{
		precision_ = precision;
	}



	CUDA_HOST void setOutputStream( std::ostream& stream )
	{
		stream_.reset( &stream, [](std::ostream*){} );
	}



	template<class SamplesOrEstimates>
	CUDA_HOST void setIncrementContributions( const SamplesOrEstimates& samplesOrEstimates, unsigned int incrementsPerDim )
	{
		incrementContributions_.update( samplesOrEstimates, incrementsPerDim );
	}



private:
	std::shared_ptr<std::ostream> stream_;
	std::streamsize precision_;
	VerbosityFlag verbosity_;
	IncrementsContributions<dim, T> incrementContributions_;



	CUDA_HOST std::string determineStrategyText( const Settings<T>& settings, const InternalSettings<dim>& internalSettings ) const
	{
		if (settings.strategy == StrategyFlag::IMPORTANCE) return std::string( "Importance sampling" );
		else return std::string( "Free -> " ) + determineInternalStrategyText( internalSettings );
	}



	CUDA_HOST std::string determineInternalStrategyText( const InternalSettings<dim>& internalSettings ) const
	{
		if (internalSettings.strategy == InternalStrategy::MIXED) return std::string( "Importance + stratified sampling" );
		else return std::string( "Stratified sampling" );
	}



	CUDA_HOST std::string determinePrngText( PrngFlag prngFlag ) const
	{
		if (prngFlag == PrngFlag::LECUYER) return std::string( "Lecuyer" );
		else if (prngFlag == PrngFlag::CURAND) return std::string( "Curand" );
		return std::string( "Philox" );
	}



	CUDA_HOST std::string determineReductionText( const InternalSettings<dim>& internalSettings ) const
	{
		if (internalSettings.reduction == ReductionFlag::CLASSIC) return std::string( "Classic" );
		else return std::string( "Refined" );
	}



	CUDA_HOST std::string determineVerbosityText() const
	{
		if (verbosity_ == VerbosityFlag::GENTLE) return std::string( "Gentle" );
		else return std::string( "Verbose" );
	}



	CUDA_HOST std::string determineSamplesText( const Settings<T>& settings, const InternalSettings<dim>& internalSettings ) const
	{
		auto numberSamples = internalSettings.numberSamples();
		if (settings.numberSamples == numberSamples) return std::to_string( numberSamples );
		else
		{
			return std::to_string( settings.numberSamples ) + " -> " + std::to_string( numberSamples );
		}
	}



	CUDA_HOST void printIntegrationLimits( const VolumeType<T, dim>& integrationLimits ) const
	{
		*stream_ << "  integration limits:\n";

		stream_->setf( std::ios::scientific, std::ios::floatfield );
		for (auto dimIdx = 0u; dimIdx < dim; ++dimIdx)
		{
			*stream_ << "    dimension " << dimIdx << ": [" << integrationLimits[dimIdx].first << ", " << integrationLimits[dimIdx].second << "]\n";
		}
		*stream_ << "\n";
	}



	CUDA_HOST void printIterationResult( const AccumulatedSums<T>& accumulatedSums, const EstimateSum<T>& estimateSum ) const
	{
		auto allIterationResult = ResultsConstructor<T>::constructResults( accumulatedSums );

		*stream_ << "Iteration: " << accumulatedSums.cardinality << "\n"
					<< "  This iteration estimate =\t" << estimateSum.getEstimate() << " +/- " << std::sqrt( estimateSum.getVariance() ) << "\n"
					<< "  All iterations estimate =\t" << allIterationResult.integral << " +/- " << allIterationResult.standardDeviation << "\n"
					<< "          chi^2/iteration =\t" << allIterationResult.chiSquaredPerIteration << "\n\n";
	}



	CUDA_HOST void printIncrements( const Increments<dim, T, CudaHostDeviceType::HOST>& increments ) const
	{
		*stream_ << "  Scaled increments:\n\n";

		for (auto dimIdx = 0u; dimIdx < dim; ++dimIdx)
		{
			printIncrementAxis( dimIdx, increments );
		}
	}



	CUDA_HOST void printIncrementAxis( unsigned int dimIdx, const Increments<dim, T, CudaHostDeviceType::HOST>& increments ) const
	{
		auto incrementsPerDim = increments.sizePerDim();
		*stream_ << "    Axis " << dimIdx << "\n";
		for (auto incrementIdx = 0u; incrementIdx < incrementsPerDim; ++incrementIdx)
		{
			auto lowerLimit = (incrementIdx == 0) ? static_cast<T>( 0 ) : increments.at( dimIdx, incrementIdx - 1 );
			auto upperLimit = increments.at( dimIdx,  incrementIdx );
			*stream_ << "    [" << lowerLimit << ", " << upperLimit << "] = " << incrementContributions_.at( dimIdx, incrementIdx ) << "\n";
		}
		*stream_ << "\n";
	}
};



template<unsigned int dim, typename T>
constexpr std::streamsize Printer<dim, T>::precisionDefault;

template<unsigned int dim, typename T>
constexpr VerbosityFlag Printer<dim, T>::verbosityDefault;



} // namespace cuVegas
} // namespace finael


#endif // FINAEL_CUVEGAS_PRINTER_H_
