/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUQCD_ANTI_PARTICLE_H_
#define FINAEL_CUQCD_ANTI_PARTICLE_H_

#include "definitions.h"
#include "PartonFlavourConverter.h"
#include "PartonIndexConverter.h"


namespace finael {
namespace cuQcd {



template<PartonFlavour flavour>
class AntiParticle
{
public:
	static constexpr PartonFlavour FLAVOUR = PartonIndexConverter<-PartonFlavourConverter<flavour>::INDEX>::FLAVOUR;
};



template<PartonFlavour flavour>
constexpr PartonFlavour AntiParticle<flavour>::FLAVOUR;



} // namespace cuQcd
} // namespace finael



#endif // FINAEL_CUQCD_ANTI_PARTICLE_H_
