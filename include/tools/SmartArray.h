/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUTOOL_SMART_ARRAY_H_
#define FINAEL_CUTOOL_SMART_ARRAY_H_

#include <cstdlib>
#include <iterator>
#include <stdexcept>

#include "../common/cudaHostDevice.h"
#include "GpuMemoryHandler.h"



namespace finael {
namespace cuTool {



template<typename T, CudaHostDeviceType hostDeviceType = CudaHostDeviceType::HOST>
class SmartArray
{
public:
	using type = T;
	static constexpr CudaHostDeviceType hostOrDevice = hostDeviceType;



	friend class SmartArray<T, CudaHostDeviceType::HOST>;
	#ifdef __CUDACC__
	friend class SmartArray<T, CudaHostDeviceType::DEVICE>;
	#endif // __CUDACC__



	CUDA_HOST SmartArray() : SmartArray( 0u ) {}



	CUDA_HOST explicit SmartArray( const unsigned int sizeIn )
		: pointer_( nullptr )
		, referenceCounter_( nullptr )
		, size_( sizeIn )
	{
		newInstance();
	}



	template<class InputIter>
	CUDA_HOST SmartArray( InputIter first, InputIter last )
		: pointer_( nullptr )
		, referenceCounter_( nullptr )
		, size_( std::distance( first, last ) )
	{
		newCopy<CudaHostDeviceType::HOST>( first );
	}



	CUDA_HOST SmartArray( const SmartArray<T, hostDeviceType>& orig )
		: pointer_( orig.pointer_ )
		, referenceCounter_( orig.referenceCounter_ )
		, size_( orig.size_ )
	{
		if (size_ != 0u) ++(*referenceCounter_);
	}



	// specialized copy constructor for device->host and vice versa; gives ALWAYS new instance
	template<CudaHostDeviceType SourceType>
	CUDA_HOST SmartArray( const SmartArray<T, SourceType>& orig )
		: pointer_( nullptr )
		, referenceCounter_( nullptr )
		, size_( orig.size() )
	{
		newCopy<SourceType>( orig.data() );
	}



	CUDA_HOST ~SmartArray()
	{
		removeInstance();
	}



	CUDA_HOST SmartArray<T, hostDeviceType>& operator=( SmartArray<T, hostDeviceType> orig )
	{
		swap( orig );
		return *this;
	}



	// specialized assign operator for device->host and vice versa; gives ALWAYS new instance
	template<CudaHostDeviceType SourceType>
	CUDA_HOST SmartArray<T, hostDeviceType>& operator=( const SmartArray<T, SourceType>& orig )
	{
		newAssign<SourceType>( orig );
		return *this;
	}



	template<class Array>
	CUDA_HOST SmartArray<T, hostDeviceType>& operator=( const Array& orig )
	{
		newAssign<CudaHostDeviceType::HOST>( orig );
		return *this;
	}



	CUDA_HOST_DEVICE T& operator[]( const unsigned int index )
	{
		return pointer_[index];
	}



	CUDA_HOST_DEVICE T& at( const unsigned int index )
	{
		if(index >= size_)	printf( "WARNING: Cannot access array-index\n" );
		return pointer_[index];
	}



	CUDA_HOST_DEVICE const T& operator[]( const unsigned int index ) const
	{
		return pointer_[index];
	}



	CUDA_HOST_DEVICE const T& at( const unsigned int index ) const
	{
		if(index >= size_)	printf( "WARNING: Cannot access array-index\n" );
		return pointer_[index];
	}



	CUDA_HOST T getToHost( const unsigned int index ) const
	{
		if (hostDeviceType == CudaHostDeviceType::HOST)
		{
			return pointer_[index];
		}
		else // DEVICE
		{
			T value;
			GpuMemoryHandler::copy<CudaHostDeviceType::HOST, hostDeviceType>( &value, &pointer_[index], 1 );
			return value;
		}
	}



	CUDA_HOST T getToHostWithIndexCheck( const unsigned int index ) const
	{
		if(index >= size_) throw std::out_of_range( "Cannot access array-index" );
		return getToHost( index );
	}



	CUDA_HOST void newArray( const unsigned int newSize )
	{
		SmartArray<T, hostDeviceType> temp( newSize );
		swap( temp );
	}



	template<CudaHostDeviceType SourceType, class Array>
	CUDA_HOST void newAssign( const Array& orig )
	{
		newArray( orig.size() );
		if (size_ != 0u) GpuMemoryHandler::copy<hostDeviceType, SourceType>( pointer_, orig.data(), size_ );
	}



	CUDA_HOST const T* data() const
	{
		return pointer_;
	}



	CUDA_HOST_DEVICE unsigned int size() const
	{
		return size_;
	}



	CUDA_HOST unsigned int useCount() const
	{
		return (referenceCounter_ == nullptr) ? 0u : *referenceCounter_;
	}



	void swap( SmartArray<T, hostDeviceType>& orig )
	{
		using std::swap;
		swap( pointer_, orig.pointer_ );
		swap( referenceCounter_, orig.referenceCounter_ );
		swap( size_, orig.size_ );
	}



private:
	T* pointer_;
	unsigned int* referenceCounter_;
	unsigned int size_;



	CUDA_HOST void newInstance()
	{
		if (size_ != 0u)
		{
			referenceCounter_ = new unsigned int;
			*referenceCounter_ = 1u;
			GpuMemoryHandler::alloc<hostDeviceType>( pointer_, size_ );
		}
	}



	CUDA_HOST void removeInstance()
	{
		if (size_ != 0u)
		{
			--(*referenceCounter_);
			if (*referenceCounter_ == 0u) deleteInstance();
		}
	}



	CUDA_HOST void deleteInstance()
	{
		GpuMemoryHandler::free<hostDeviceType>( pointer_ );
		delete referenceCounter_;
	}



	template<CudaHostDeviceType SourceType, class InputIter>
	CUDA_HOST void newCopy( InputIter first )
	{
		newInstance();
		if (size_ != 0u) GpuMemoryHandler::copy<hostDeviceType, SourceType>( pointer_, &first[0], size_ );
	}
};




template<typename T, CudaHostDeviceType hostDeviceType>
constexpr CudaHostDeviceType SmartArray<T, hostDeviceType>::hostOrDevice;



} // namespace cuTool
} // namespace finael

#endif // FINAEL_CUTOOL_SMART_ARRAY_H_
