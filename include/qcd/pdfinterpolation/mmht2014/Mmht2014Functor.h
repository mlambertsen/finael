/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_QCD_PDFINTERPOLATION_MMHT2014_FUNCTOR_H_
#define FINAEL_QCD_PDFINTERPOLATION_MMHT2014_FUNCTOR_H_

#include <string>
#include <cstring>



namespace finael {
namespace qcd {
namespace pdfInterpolation {



class Mmht2014Functor
{
public:
	using value_type = double;



	Mmht2014Functor( const int flavour, const double scale, const int eigenvector, const std::string& filePrefix )
		: flavour_( flavour )
		, eigenvector_( eigenvector )
		, fileSize_( filePrefix.length() )
		, scale_( scale )
	{
		std::strcpy( filePrefix_, filePrefix.c_str() );
	}



	double operator()( const double x ) const
	{
		int flavour = flavour_;
		int eigenvector = eigenvector_;
		int fileSize = fileSize_;
		double scale = scale_;
		double xCopy = x;
		char filePrefix[40];

		std::strcpy( filePrefix, filePrefix_ );

		return fortran::mmht14pdf_getter_( &flavour, &eigenvector, filePrefix, &fileSize, &xCopy, &scale )/xCopy;
	}



	int flavour_;
	int eigenvector_;
	int fileSize_;
	double scale_;
	char filePrefix_[40];
};



} // namespace pdfInterpolation
} // namespace qcd
} // namespace finael



#endif // FINAEL_QCD_PDFINTERPOLATION_MMHT2014_FUNCTOR_H_
