/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_QCD_PDFINTERPOLATION_FORTRAN_CT_FORTRAN_H_
#define FINAEL_QCD_PDFINTERPOLATION_FORTRAN_CT_FORTRAN_H_



namespace finael {
namespace qcd {
namespace pdfInterpolation {
namespace fortran {



extern "C"
{
	void setct12_( const char tablefile[40] );
	double ct12pdf_( const int* parton, const double* x, const double* q );

	void setct14_( const char tablefile[40] );
	double ct14pdf_( const int* parton, const double* x, const double* q );
}



} // namespace fortran
} // namespace pdfInterpolation
} // namespace qcd
} // namespace finael



#endif // FINAEL_QCD_PDFINTERPOLATION_FORTRAN_CT_FORTRAN_H_
