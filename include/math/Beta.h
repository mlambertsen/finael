#ifndef FINAEL_CUMATH_BETA_H_
#define FINAEL_CUMATH_BETA_H_

#include "../common/cudaHostDevice.h"

#include "Complex.h"
#include "LogGamma.h"


namespace finael {
namespace cuMath {



class Beta
{
public:
	template<template<typename> class Complex, typename T>
	CUDA_HOST_DEVICE Complex<T> operator()( const Complex<T>& arg1, const Complex<T>& arg2 ) const
	{
		const LogGamma logGamma;
		return exp( logGamma.calcIgnoringLogBranch( arg1 ) + logGamma.calcIgnoringLogBranch( arg2 ) - logGamma.calcIgnoringLogBranch( arg1 + arg2 ) );
	}



	template<typename T>
	CUDA_HOST_DEVICE T operator()( T arg1, T arg2 ) const
	{
		return operator()( Complex<T>( arg1, 0 ), Complex<T>( arg2, 0 ) ).real();
	}
};



} // namesapce cuMath
} // namespace finael


#endif // FINAEL_CUMATH_BETA_H_
