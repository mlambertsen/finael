/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUQCD_EVOLUTIONKERNEL_CREATOR_H_
#define FINAEL_CUQCD_EVOLUTIONKERNEL_CREATOR_H_

#include "../common/cudaHostDevice.h"
#include "../math/Vector.h"
#include "../math/Matrix.h"

#include "constants.h"
#include "definitions.h"
#include "EvolutionKernelContainer.h"
#include "EvolutionKernelCreatorHelper.h"
#include "AnomalousDimension.h"



namespace finael {
namespace cuQcd {



template<unsigned short numberFlavours, PQcdOrder order, typename ScalarType>
class EvolutionKernelCreator
{
public:
	static_assert(sizeof(ScalarType) == 0, "ERROR: EvolutionKernelCreator not implemented for requested PQcdOrder!");
};





template<unsigned short numberFlavours, typename ScalarType>
class EvolutionKernelCreator<numberFlavours, PQcdOrder::LO, ScalarType>
{
public:
	static constexpr unsigned short NUMBERFLAVOURS = numberFlavours;
	static constexpr ScalarType BETA0 = CONSTANTS<ScalarType>::template BETA0<NUMBERFLAVOURS>();
	using AnomalousLo = AnomalousDimension<NUMBERFLAVOURS, PQcdOrder::LO, ScalarType>;



	template<typename ScaleType, class Complex>
	CUDA_HOST_DEVICE EvolutionKernelContainer<Complex> getEvolutionKernels( const ScaleType& oldAlphaS, const ScaleType& newAlphaS, const Complex& mellinMoment ) const
	{
		AnomalousLo anomalousLo;
		auto r0Matrix = anomalousLo.getDglapRelevant( mellinMoment ) / CONSTANTS<ScalarType>::template BETA0<NUMBERFLAVOURS>();

		EvolutionKernelCreatorHelper<NUMBERFLAVOURS, ScalarType> helper;
		auto eigenvalues = helper.calculateEigenvalues( r0Matrix );
		auto projectorPlus = helper.calculateProjector( r0Matrix, eigenvalues.first, eigenvalues.second );
		auto projectorMinus = helper.calculateProjector( r0Matrix, eigenvalues.second, eigenvalues.first );

		auto nonSingletKernel = pow( oldAlphaS/newAlphaS, r0Matrix[0][0] );
		auto singletKernel = pow( oldAlphaS/newAlphaS, eigenvalues.first ) * projectorPlus + pow( oldAlphaS/newAlphaS, eigenvalues.second ) * projectorMinus;

		return EvolutionKernelContainer<Complex>( singletKernel, nonSingletKernel, nonSingletKernel, nonSingletKernel );
	}
};





template<unsigned short numberFlavours, typename ScalarType>
constexpr unsigned short EvolutionKernelCreator<numberFlavours, PQcdOrder::LO, ScalarType>::NUMBERFLAVOURS;

template<unsigned short numberFlavours, typename ScalarType>
constexpr ScalarType EvolutionKernelCreator<numberFlavours, PQcdOrder::LO, ScalarType>::BETA0;





template<unsigned short numberFlavours, typename ScalarType>
class EvolutionKernelCreator<numberFlavours, PQcdOrder::NLO, ScalarType>
{
public:
	static constexpr unsigned short NUMBERFLAVOURS = numberFlavours;
	static constexpr ScalarType BETA0 = CONSTANTS<ScalarType>::template BETA0<NUMBERFLAVOURS>();
	static constexpr ScalarType BETA1 = CONSTANTS<ScalarType>::template BETA1<NUMBERFLAVOURS>();
	using AnomalousLo = AnomalousDimension<NUMBERFLAVOURS, PQcdOrder::LO, ScalarType>;
	using AnomalousNlo = AnomalousDimension<NUMBERFLAVOURS, PQcdOrder::NLO, ScalarType>;



	template<typename ScaleType, class Complex>
	CUDA_HOST_DEVICE EvolutionKernelContainer<Complex> getEvolutionKernels( const ScaleType& oldAlphaS, const ScaleType& newAlphaS, const Complex& mellinMoment ) const
	{
		AnomalousLo anomalousLo;
		auto r0Matrix = anomalousLo.getDglapRelevant( mellinMoment ) / CONSTANTS<ScalarType>::template BETA0<NUMBERFLAVOURS>();

		AnomalousNlo anomalousNlo;
		auto anomalousDimensionsNlo = anomalousNlo.getDglapRelevant( mellinMoment );

		// nonsinglet
		Complex r1NonSingletMinus = (anomalousDimensionsNlo.nonSingletMinus - BETA1* r0Matrix[0][0]) / BETA0;
		Complex nonSingletNonValenceMinusKernel = pow( oldAlphaS/newAlphaS, r0Matrix[0][0] ) * (1 + r1NonSingletMinus / (4 * cuMath::CONST<ScalarType>::PI) * (oldAlphaS - newAlphaS) );

		Complex r1NonSingletPlus = (anomalousDimensionsNlo.nonSingletPlus - BETA1 * r0Matrix[0][0]) / BETA0;
		Complex nonSingletNonValencePlusKernel = pow( oldAlphaS/newAlphaS, r0Matrix[0][0] ) * (1 + r1NonSingletPlus / (4 * cuMath::CONST<ScalarType>::PI) * (oldAlphaS - newAlphaS) );


		// singlet
		EvolutionKernelCreatorHelper<NUMBERFLAVOURS, ScalarType> helper;
		auto eigenvalues = helper.calculateEigenvalues( r0Matrix );
		auto projectorPlus = helper.calculateProjector( r0Matrix, eigenvalues.first, eigenvalues.second );
		auto projectorMinus = helper.calculateProjector( r0Matrix, eigenvalues.second, eigenvalues.first );

		auto r1Matrix = (anomalousDimensionsNlo.gamma1Matrix - CONSTANTS<ScalarType>::template BETA1<NUMBERFLAVOURS>() * r0Matrix) / CONSTANTS<ScalarType>::template BETA0<NUMBERFLAVOURS>();

		auto singletKernel = generateSingletEvolutionKernelPart( r1Matrix, projectorPlus, projectorMinus, eigenvalues.first, eigenvalues.second, oldAlphaS, newAlphaS ) + generateSingletEvolutionKernelPart( r1Matrix, projectorMinus, projectorPlus, eigenvalues.second, eigenvalues.first, oldAlphaS, newAlphaS );

		return EvolutionKernelContainer<Complex>( singletKernel, nonSingletNonValenceMinusKernel, nonSingletNonValencePlusKernel, nonSingletNonValenceMinusKernel);
	}



private:
	template<typename ScaleType, class Complex>
	CUDA_HOST_DEVICE cuMath::Matrix<2,2,Complex> generateSingletEvolutionKernelPart( const cuMath::Matrix<2,2,Complex>& R1, const cuMath::Matrix<2,2,Complex>& firstProjector, const cuMath::Matrix<2,2,Complex>& secondProjector, const Complex& firstEigenvalue, const Complex& secondEigenvalue, const ScaleType& oldAlphaS, const ScaleType& newAlphaS ) const
	{
		return (firstProjector + ((oldAlphaS - newAlphaS) * firstProjector * R1 * firstProjector + (newAlphaS * pow(newAlphaS/oldAlphaS, firstEigenvalue - secondEigenvalue) - oldAlphaS) / (secondEigenvalue - firstEigenvalue - 1) * firstProjector * R1 * secondProjector)/(4 * cuMath::CONST<ScalarType>::PI)) * pow(newAlphaS/oldAlphaS, -firstEigenvalue);
	}
};





template<unsigned short numberFlavours, typename ScalarType>
constexpr unsigned short EvolutionKernelCreator<numberFlavours, PQcdOrder::NLO, ScalarType>::NUMBERFLAVOURS;

template<unsigned short numberFlavours, typename ScalarType>
constexpr ScalarType EvolutionKernelCreator<numberFlavours, PQcdOrder::NLO, ScalarType>::BETA0;

template<unsigned short numberFlavours, typename ScalarType>
constexpr ScalarType EvolutionKernelCreator<numberFlavours, PQcdOrder::NLO, ScalarType>::BETA1;



} // namespace cuQcd
} // namespace finael


#endif // FINAEL_CUQCD_EVOLUTIONKERNEL_CREATOR_H_
