/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_INCREMENTS_INDICES_CONTAINER_H_
#define FINAEL_CUVEGAS_INCREMENTS_INDICES_CONTAINER_H_

#include "../../common/cudaHostDevice.h"
#include "../../tools/SmartArray.h"


namespace finael {
namespace cuVegas {



template<CudaHostDeviceType hostDeviceType, ReductionFlag reductionMode>
class IncrementsIndicesContainer;



template<CudaHostDeviceType hostDeviceType>
class IncrementsIndicesContainer<hostDeviceType, ReductionFlag::CLASSIC>
{
public:
	friend class IncrementsIndicesContainer<CudaHostDeviceType::HOST, ReductionFlag::CLASSIC>;
	#ifdef __CUDACC__
	friend class IncrementsIndicesContainer<CudaHostDeviceType::DEVICE, ReductionFlag::CLASSIC>;
	#endif // __CUDACC__



	CUDA_HOST IncrementsIndicesContainer( unsigned int size )
		: incrementIndices( size )
	{}



	template<CudaHostDeviceType otherDevice>
	CUDA_HOST IncrementsIndicesContainer( const IncrementsIndicesContainer<otherDevice, ReductionFlag::CLASSIC>& other )
		: incrementIndices ( other.incrementIndices )
	{}



	CUDA_HOST_DEVICE void setIncrementIdx( unsigned int arrayIdx, unsigned int incrementIdx )
	{
		incrementIndices[arrayIdx] = incrementIdx;
	}



	CUDA_HOST_DEVICE unsigned int getIncrementIdx( unsigned int arrayIdx ) const
	{
		return incrementIndices[arrayIdx];
	}



private:
	cuTool::SmartArray<unsigned int, hostDeviceType> incrementIndices;
};



template<CudaHostDeviceType hostDeviceType>
class IncrementsIndicesContainer<hostDeviceType, ReductionFlag::REFINED>
{
public:
	friend class IncrementsIndicesContainer<CudaHostDeviceType::HOST, ReductionFlag::CLASSIC>;
	#ifdef __CUDACC__
	friend class IncrementsIndicesContainer<CudaHostDeviceType::DEVICE, ReductionFlag::CLASSIC>;
	#endif // __CUDACC__



	CUDA_HOST IncrementsIndicesContainer( unsigned int /* size */ )
	{}



	template<CudaHostDeviceType otherDevice>
	CUDA_HOST IncrementsIndicesContainer( const IncrementsIndicesContainer<otherDevice, ReductionFlag::REFINED>& /* other */ )
	{}



	CUDA_HOST_DEVICE void setIncrementIdx( unsigned int /* arrayIdx */, unsigned int /* incrementIdx */ )
	{}
};



} // namespace cuVegas
} // namespace finael




#endif // FINAEL_CUVEGAS_INCREMENTS_INDICES_CONTAINER_H_
