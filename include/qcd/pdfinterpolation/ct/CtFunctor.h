/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_QCD_PDFINTERPOLATION_CT_FUNCTOR_H_
#define FINAEL_QCD_PDFINTERPOLATION_CT_FUNCTOR_H_

#include "CtFortranCodeSelector.h"



namespace finael {
namespace qcd {
namespace pdfInterpolation {



template<CtSet ctSet>
class CtFunctor
{
public:
	using value_type = double;



	CtFunctor( const int inputFlavour, const double inputScale, const std::string& inputSetName )
		: scale_( inputScale )
		, flavour_( inputFlavour )
	{
		CtFortranCodeSelector<ctSet>::set( inputSetName );
	}



	double operator()( const double x ) const
	{
		return CtFortranCodeSelector<ctSet>::call( flavour_, x, scale_ );
	}



	double scale_;
	int flavour_;
};



} // namespace pdfInterpolation
} // namespace qcd
} // namespace finael



#endif // FINAEL_QCD_PDFINTERPOLATION_CT_FUNCTOR_H_
