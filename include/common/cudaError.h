/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUDA_ERROR_H_
#define FINAEL_CUDA_ERROR_H_

#include <cstdlib>

#if defined __CUDACC__ && defined __CUDACC_DEBUG__
	#define CHECK_CUDA_ERROR( message )\
	{\
		cudaDeviceSynchronize();\
		cudaError_t error = cudaGetLastError();\
		if (error != cudaSuccess)\
		{\
			std::printf( " ERROR at line %i in %s\n %s: %s\n", __LINE__, __FILE__, message, cudaGetErrorString( error ) );\
			std::exit( EXIT_FAILURE );\
		}\
	}
#else
	#define CHECK_CUDA_ERROR( message )
#endif // __CUDACC__ && __CUDACC_DEBUG__

#endif // FINAEL_CUDA_ERROR_H_
