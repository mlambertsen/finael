/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_REDUCER_H_
#define FINAEL_CUVEGAS_REDUCER_H_

#include <cmath>
#include <utility>

#include "../../common/cudaHostDevice.h"
#include "../../tools/SmartArray.h"

#include "../increments/WeightsManager.h"
#include "../InternalSettings.h"
#include "../optionFlags.h"
#include "../printer/Printer.h"
#include "../Samples.h"
#include "../sums/Estimate.h"
#include "../sums/EstimateSum.h"

#include "reducerKernel.h"


namespace finael {
namespace cuVegas {


template<unsigned int dim, typename T, CudaHostDeviceType hostDeviceType, ReductionFlag reductionMode>
class Reducer
{
public:
	CUDA_HOST Reducer( const InternalSettings<dim>& settingsIn )
		: settings( settingsIn )
	{}



	CUDA_HOST std::pair<EstimateSum<T>, WeightsManager<dim, T>> operator()( const Samples<T, hostDeviceType, reductionMode>& samplesHostOrDevice, Printer<dim, T>& printer )
	{
		Samples<T, CudaHostDeviceType::HOST, reductionMode> samples( samplesHostOrDevice );

		if (printer.isVerbose()) printer.setIncrementContributions( samples, settings.incrementsPerDim );

		if (settings.strategy == InternalStrategy::STRATIFIED)
		{
			return reduce<InternalStrategy::STRATIFIED>( samples );
		}
		else // if (settings.strategy == InternalStrategy::IMPORTANCE || settings.strategy == InternalStrategy::MIXED)
		{
			return reduce<InternalStrategy::IMPORTANCE>( samples );
		}
	}



private:
	const InternalSettings<dim>& settings;



	template<InternalStrategy strategy>
	CUDA_HOST std::pair<EstimateSum<T>, WeightsManager<dim, T>> reduce( const Samples<T, CudaHostDeviceType::HOST, reductionMode>& samples )
	{
		auto estimateSumAndWeightManager = std::make_pair( EstimateSum<T>(), WeightsManager<dim, T>( settings.incrementsPerDim ) );

		for (auto binIdx = 0u; binIdx < settings.numberBins; ++binIdx)
		{
			FunctionSums<T> binSums;
			auto idxMin = binIdx * settings.binCapacity;
			for (auto sampleIdx = 0u; sampleIdx < settings.binCapacity; ++sampleIdx)
			{
				auto idx = idxMin + sampleIdx;
				FunctionSums<T> singleTerm = samples.getFunctionValue( idx );
				binSums += singleTerm;
				estimateSumAndWeightManager.second.template collect<strategy>( idx, samples, singleTerm );
			}
			Estimate<T> estimate( binSums );
			estimateSumAndWeightManager.first += estimate;
			estimateSumAndWeightManager.second.template collect<strategy>( idxMin, samples, estimate );
		}

		return estimateSumAndWeightManager;
	}
};





template<unsigned int dim, typename T, CudaHostDeviceType hostDeviceType>
class Reducer<dim, T, hostDeviceType, ReductionFlag::REFINED>
{
public:
	CUDA_HOST Reducer( const InternalSettings<dim>& settings )
		: warpSize( settings.warpSize() )
		, maxWarpsPerBlock( settings.maxBlockDim() / warpSize )
		, incrementsPerDim( settings.incrementsPerDim )
		, numberIncrements( std::pow( incrementsPerDim, dim ) )
		, binsPerIncrement( settings.numberBins / numberIncrements )
	{
		setInternalState( settings.numberBins );
	}



	CUDA_HOST std::pair<EstimateSum<T>, WeightsManager<dim, T>> operator()( const Samples<T, hostDeviceType, ReductionFlag::REFINED>& samples, Printer<dim, T>& printer )
	{
		cuTool::SmartArray<Estimate<T>, hostDeviceType> estimateSums( numberBlocks );

		reduceSamplesToEstimates( samples, estimateSums );

		if (numberBlocks != numberIncrements)
		{
			reduceIncrements( estimateSums );
		}

		cuTool::SmartArray<Estimate<T>, CudaHostDeviceType::HOST> estimateSumsHost( estimateSums );
		if (printer.isVerbose()) printer.setIncrementContributions( estimateSumsHost, incrementsPerDim );
		return reduce( estimateSumsHost );
	}



private:
	const unsigned int warpSize;
	const unsigned int maxWarpsPerBlock;
	const unsigned int incrementsPerDim;
	const unsigned int numberIncrements;
	const unsigned int binsPerIncrement;
	unsigned int elementsPerIncrement;
	unsigned int blocksPerIncrement;
	unsigned int numberBlocks;
	unsigned int blockDim;



	CUDA_HOST void setInternalState( const unsigned int numberElements )
	{
		elementsPerIncrement = numberElements/numberIncrements;

		auto warpsPerIncrement = (elementsPerIncrement - 1) / warpSize + 1;
		blocksPerIncrement = (warpsPerIncrement - 1) / maxWarpsPerBlock + 1;
		numberBlocks = blocksPerIncrement * numberIncrements;

		auto warpsPerBlock = (warpsPerIncrement - 1) / blocksPerIncrement + 1;
		blockDim = warpsPerBlock * warpSize;
	}



	CUDA_HOST std::pair<EstimateSum<T>, WeightsManager<dim, T>> reduce( const cuTool::SmartArray<Estimate<T>, CudaHostDeviceType::HOST>& estimateSums )
	{
		auto estimateSumAndWeightManager = std::make_pair( EstimateSum<T>(), WeightsManager<dim, T>( incrementsPerDim ) );

		for (auto incrementIdx = 0u; incrementIdx < numberIncrements; ++incrementIdx)
		{
			EstimateSum<T> estimateSum( estimateSums[incrementIdx], binsPerIncrement );
			estimateSumAndWeightManager.first += estimateSum;

			estimateSumAndWeightManager.second.collect( incrementIdx, estimateSum.getVariance() );
		}

		return estimateSumAndWeightManager;
	}



	CUDA_HOST void reduceSamplesToEstimates( const Samples<T, hostDeviceType, ReductionFlag::REFINED>& samples, cuTool::SmartArray<Estimate<T>, hostDeviceType>& estimateSums )
	{
		auto binCapacity = samples.size()/(elementsPerIncrement * numberIncrements);
		if (hostDeviceType == CudaHostDeviceType::HOST)
		{
			reducerKernel::reduceSamplesToEstimatesHost( numberBlocks, blockDim, samples, estimateSums, elementsPerIncrement, blocksPerIncrement, binCapacity );
		}
		#ifdef __CUDACC__
		else if (hostDeviceType == CudaHostDeviceType::DEVICE)
		{
			#if __CUDA_ARCH__ >= 300
				reducerKernel::reduceSamplesToEstimates<T><<<numberBlocks, blockDim>>>( samples, estimateSums, elementsPerIncrement, blocksPerIncrement, binCapacity );
			#else // __CUDA_ARCH__ >= 300
				reducerKernel::reduceSamplesToEstimates<T><<<numberBlocks, blockDim, blockDim*sizeof(T)>>>( samples, estimateSums, elementsPerIncrement, blocksPerIncrement, binCapacity );
			#endif // __CUDA_ARCH__ >= 300

			CHECK_CUDA_ERROR( "Error in reducerKernel::reduceSamplesToEstimates" );
		}
		#endif // __CUDACC__
	}


	CUDA_HOST void reduceIncrements( cuTool::SmartArray<Estimate<T>, hostDeviceType>& estimateSums )
	{
		setInternalState( numberBlocks );
		cuTool::SmartArray<Estimate<T>, hostDeviceType> tempEstimates( numberBlocks );

		callReductionKernel( estimateSums, tempEstimates );
		bool nextToTemp( false );
		while (numberBlocks != numberIncrements)
		{
			setInternalState( numberBlocks );
			if (nextToTemp)
			{
				callReductionKernel( estimateSums, tempEstimates );
				nextToTemp = true;
			}
			else
			{
				callReductionKernel( tempEstimates, estimateSums );
				nextToTemp = false;
			}
		}

		if (!nextToTemp) estimateSums = tempEstimates;
	}



	CUDA_HOST void callReductionKernel( const cuTool::SmartArray<Estimate<T>, hostDeviceType>& input, cuTool::SmartArray<Estimate<T>, hostDeviceType>& output )
	{
		if (hostDeviceType == CudaHostDeviceType::HOST)
		{
			reducerKernel::reduceIncrementsHost( numberBlocks, blockDim, input, output, elementsPerIncrement, blocksPerIncrement );
		}
		#ifdef __CUDACC__
		else
		{
			#if __CUDA_ARCH__ >= 300
				reducerKernel::reduceIncrements<T><<<numberBlocks, blockDim>>>( input, output, elementsPerIncrement, blocksPerIncrement );
			#else // __CUDA_ARCH__ >= 300
				reducerKernel::reduceIncrements<T><<<numberBlocks, blockDim, blockDim*sizeof(T)>>>( input, output, elementsPerIncrement, blocksPerIncrement );
			#endif // __CUDA_ARCH__ >= 300

			CHECK_CUDA_ERROR( "Error in reducerKernel::reduceIncrements" );
		}
		#endif // __CUDACC__
	}
};



} // namespace cuVegas
} // namespace finael


#endif // FINAEL_CUVEGAS_REDUCER_H_
