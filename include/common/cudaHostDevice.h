/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUDA_HOST_DEVICE_H_
#define FINAEL_CUDA_HOST_DEVICE_H_


namespace finael {



#ifdef __CUDACC__
	#define CUDA_HOST_DEVICE __host__ __device__
	#define CUDA_HOST __host__
	#define CUDA_DEVICE __device__
	enum class CudaHostDeviceType {HOST, DEVICE};
#else // __CUDACC__
	#define CUDA_HOST_DEVICE
	#define CUDA_HOST
	#define CUDA_DEVICE
	enum class CudaHostDeviceType {HOST};
#endif // __CUDACC__



} // namespace finael


#endif
