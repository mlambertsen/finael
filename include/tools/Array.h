/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUTOOL_ARRAY_H_
#define FINAEL_CUTOOL_ARRAY_H_

#include "../common/cudaHostDevice.h"



namespace finael {
namespace cuTool {



template<typename T, unsigned int size>
class Array
{
public:
	using value_type = T;
	static constexpr unsigned int SIZE = size;



	CUDA_HOST_DEVICE Array()
		: data_()
	{}



	explicit CUDA_HOST_DEVICE Array( const T& value )
	{
		set( value );
	}



	CUDA_HOST_DEVICE Array( const T (&data_in)[size] )
	{
		set( data_in );
	}



	CUDA_HOST_DEVICE void set( const unsigned int index, const T& value )
	{
		data_[index] = value;
	}



	CUDA_HOST_DEVICE void setAt( const unsigned int index, const T& value )
	{
		checkIndex( index );
		set( index, value );
	}



	CUDA_HOST_DEVICE void set( const T& value )
	{
		for (unsigned int index = 0; index < size; ++index) data_[index] = value;
	}



	CUDA_HOST_DEVICE void set( const T (&data_in)[size] )
	{
		for (unsigned int index = 0; index < size; ++index)
		{
			data_[index] = data_in[index];
		}
	}



	CUDA_HOST_DEVICE T& operator[]( const unsigned int index )
	{
		return data_[index];
	}



	CUDA_HOST_DEVICE T& at( const unsigned int index )
	{
		checkIndex( index );
		return data_[index];
	}



	CUDA_HOST_DEVICE const T& operator[]( const unsigned int index ) const
	{
		return data_[index];
	}



	CUDA_HOST_DEVICE const T& at( const unsigned int index ) const
	{
		checkIndex( index );
		return data_[index];
	}



private:
	value_type data_[SIZE];



	CUDA_HOST_DEVICE void checkIndex( const unsigned int index ) const
	{
		if(index >= SIZE) printf( "WARNING: Cannot access index\n" );
	}
};



template<typename T, unsigned int size>
constexpr unsigned int Array<T, size>::SIZE;



} // namespace cuTool
} // namespace finael



#endif // FINAEL_CUTOOL_ARRAY_H_
