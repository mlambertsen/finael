/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_TOOL_TYPE_TRAITS_H_
#define FINAEL_TOOL_TYPE_TRAITS_H_



namespace finael {
namespace cuTool {



template<typename T> using void_t = void;




template<typename T, typename = void>
struct ValueTypeOrInputType
{
	using type = T;
};

template<typename T>
struct ValueTypeOrInputType<T, void_t<typename T::value_type>>
{
	using type = typename T::value_type;
};

template<typename T, typename U = void>
using ValueTypeOrInputType_t = typename ValueTypeOrInputType<T,U>::type;



} // namespace cuTool
} // namespace finael



#endif // FINAEL_TOOL_TYPE_TRAITS_H_
