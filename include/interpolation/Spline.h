/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_INTERPOLATION_SPLINE_H_
#define FINAEL_INTERPOLATION_SPLINE_H_

#include <iterator>
#include <algorithm>
#include <cmath>
#include <vector>

#include "spline_settings.h"
#include "SplineError.h"
#include "SplinePiece.h"
#include "MellinSpline.h"



namespace finael {
namespace interpolation {



template<typename T>
class MellinSpline;



template<typename T>
class Spline
{
public:
	template<class PointIt, class Functor>
	Spline( PointIt firstPoint, PointIt lastPoint, const Functor& function )
		: Spline( firstPoint, lastPoint, function, SplineType::NATURAL )
	{}



	template<class PointIt, class Functor>
	Spline( PointIt firstPoint, PointIt lastPoint, const Functor& function, const SplineType type )
	{
		set( firstPoint, lastPoint, function, type );
	}



	explicit Spline( const MellinSpline<T>& mellinSpline )
	{
		set( mellinSpline );
	}



	template<class PointIt, class Functor>
	void set( PointIt firstPoint, PointIt lastPoint, const Functor& function, const SplineType type )
	{
		updatePoints( firstPoint, lastPoint );
		type_ = type;
		auto functionValues = computeFunctionValuesAtPoints( function );
		updatePieces( functionValues );
	}



	template<class PointIt, class Functor>
	void set( PointIt firstPoint, PointIt lastPoint, const Functor& function )
	{
		set( firstPoint, lastPoint, function, type_ );
	}



	template<class Functor>
	void set( const Functor& function, const SplineType type )
	{
		set( points_.begin(), points_.end(), function, type );
	}



	template<class Functor>
	void set( const Functor& function )
	{
		set( points_.begin(), points_.end(), function, type_ );
	}



	void set ( const MellinSpline<T>& mellinSpline )
	{
		type_ = mellinSpline.getType();
		extractPoints( mellinSpline );
		extractPieces( mellinSpline );
	}



	std::size_t numberPoints() const
	{
		return points_.size();
	}



	T getMinimalPoint() const
	{
		return getPointAt( 0 );
	}



	T getMaximalPoint() const
	{
		return getPointAt( numberPoints() - 1 );
	}



	T getPointAt( const std::size_t index ) const
	{
		return points_.at( index );
	}



	bool isInRange( const T& point ) const
	{
		return point >= getMinimalPoint() && point <= getMaximalPoint();
	}



	SplineType getType() const
	{
		return type_;
	}



	SplinePiece<T> getPiece( const std::size_t index ) const
	{
		return pieces_.at( index );
	}



	T operator()( const T& point ) const
	{
		if (!isInRange( point )) throw SplineError( "Point out of range." );
		auto pieceIndex = findPieceIndex( point );
		return pieces_.at( pieceIndex )( point );
	}



private:
	std::vector<T> points_;
	std::vector<SplinePiece<T>> pieces_;
	SplineType type_;



	template<class PointIt>
	void updatePoints( PointIt firstPoint, PointIt lastPoint )
	{
		if (std::distance( firstPoint, lastPoint ) < 3) throw SplineError( "Too litte points." );
		points_.assign( firstPoint, lastPoint );
		std::sort( points_.begin(), points_.end() );
	}



	void updatePieces( const std::vector<T>& functionValues )
	{
		SplineSleSolver solver( *this, functionValues );
		auto parameters = solver.solve();
		pieces_.resize( numberPoints() - 1 );
		calculatePieces( parameters, functionValues );
	}



	void extractPoints( const MellinSpline<T>& mellinSpline )
	{
		points_.resize( mellinSpline.numberPoints() );
		for (auto index = 0u; index < mellinSpline.numberPoints(); ++index)
		{
			points_.at( index ) = mellinSpline.getPointAt( index );
		}
	}


	void extractPieces( const MellinSpline<T>& mellinSpline )
	{
		pieces_.resize( numberPoints() - 1 );

		auto indexOfZero = static_cast<std::size_t>( std::find( points_.begin(), points_.end(), 0 ) - points_.begin() );

		for (auto index = 0u; index != indexOfZero; ++index)
		{
			auto piece = extractPieceAt( mellinSpline, index );
			pieces_.at( index ) = ((index == 0u) ? SplinePiece<T>() : pieces_.at( index-1 )) - piece;
		}

		for (auto index = numberPoints() - 1; index != indexOfZero; --index)
		{
			auto piece = extractPieceAt( mellinSpline, index );
			pieces_.at( index-1 ) = ((index == numberPoints() - 1) ? SplinePiece<T>() : pieces_.at( index )) + piece;
		}
	}


	SplinePiece<T> extractPieceAt( const MellinSpline<T>& mellinSpline, const std::size_t index ) const
	{
		using std::pow;

		SplinePiece<T> piece;
		auto coefficients = mellinSpline.getCoefficientsAt( index );
		piece.constant = coefficients.at( 0 );
		piece.linear = coefficients.at( 1 )/getPointAt( index );
		piece.square = coefficients.at( 2 )/pow( getPointAt( index ), 2 );
		piece.cubic = coefficients.at( 3 )/pow( getPointAt( index ), 3);

		return piece;
	}



	template<class Functor>
	std::vector<T> computeFunctionValuesAtPoints( const Functor& function )
	{
		std::vector<T> functionValues( numberPoints() );
		std::transform( points_.begin(), points_.end(), functionValues.begin(), function );
		return functionValues;
	}



	std::size_t findPieceIndex( const T& point ) const
	{
		auto startIt = ++points_.begin();
		auto it = std::find_if(startIt, points_.end(), [&point]( const T& gridPoint ){return gridPoint >= point;});
		return it - startIt;
	}



	void calculatePieces( const std::vector<T>& parameters, const std::vector<T>& functionValues )
	{
		using std::pow;
		for (auto index = 1u; index <= pieces_.size(); ++index)
		{
			pieces_.at( index - 1 ).cubic = (parameters.at( index ) - parameters.at( index - 1 )) / (getPointAt( index ) - getPointAt( index - 1 )) / 6;
			pieces_.at( index - 1 ).square = (parameters.at( index - 1 ) * getPointAt( index ) - parameters.at( index ) * getPointAt( index - 1 )) / (getPointAt( index ) - getPointAt( index - 1 )) / 2;
			pieces_.at( index - 1 ).linear = (functionValues.at( index ) - functionValues.at( index - 1 ))/(getPointAt( index ) - getPointAt( index - 1 )) + (getPointAt( index ) - getPointAt( index - 1 )) * (parameters.at( index - 1 ) - parameters.at( index )) / 6 + (parameters.at( index ) * pow( getPointAt( index - 1 ), 2 ) - parameters.at( index - 1 ) * pow( getPointAt( index ), 2 )) / (getPointAt( index ) - getPointAt( index - 1 )) / 2;
			pieces_.at( index - 1 ).constant = (functionValues.at( index - 1 ) * getPointAt( index ) - functionValues.at( index ) * getPointAt( index - 1 )) / (getPointAt( index ) - getPointAt( index - 1 )) + ((getPointAt( index ) - getPointAt( index - 1 )) * (parameters.at( index ) * getPointAt( index - 1 ) - parameters.at( index - 1 ) * getPointAt( index )) + (parameters.at( index - 1 ) * pow( getPointAt( index ), 3 ) - parameters.at( index ) * pow( getPointAt( index - 1 ), 3 )) / (getPointAt( index ) - getPointAt( index - 1 ))) / 6;
		}
	}





	class SplineSleSolver
	{
	public:
		SplineSleSolver( const Spline<T>& spline, const std::vector<T>& functionValues )
			: spline_( spline )
			, functionValues_( functionValues )
			, size_( spline.numberPoints() )
		{}



		std::vector<T> solve() const
		{
			std::vector<T> result( size_ );
			std::vector<T> uMatrixDiag( size_ );

			uMatrixDiag.at( 0 ) = 1;
			result.at( 0 ) = inhomogeneityAt( 0 );

			for (auto column = 1u; column < size_; ++column)
			{
				T minorDiagonalOfL = matrixAt( column, column - 1 )/uMatrixDiag.at( column - 1 );
				uMatrixDiag.at( column ) = matrixAt( column, column ) - minorDiagonalOfL*matrixAt( column - 1, column );
				result.at( column ) = inhomogeneityAt( column ) - minorDiagonalOfL*result.at( column - 1 );
			}

			result.at( size_ - 1 ) /= uMatrixDiag.at( size_ - 1 );
			for (auto index = 2u; index <= size_; ++index)
			{
				auto column = size_ - index;
				result.at( column ) = ( result.at( column ) - matrixAt( column, column + 1 )*result.at( column + 1 )) / uMatrixDiag.at( column );
			}

			return result;
		}
	private:
		const Spline<T>& spline_;
		const std::vector<T>& functionValues_;
		std::size_t size_;



		enum class MinorDiagonal {LOWER, UPPER};



		T inhomogeneityAt( const std::size_t row ) const
		{
			if (row == 0 || row == size_ - 1) return 0;
			else return (functionValues_.at( row + 1 ) - functionValues_.at( row ))/(spline_.getPointAt( row + 1) - spline_.getPointAt( row )) - (functionValues_.at( row ) - functionValues_.at( row - 1 ))/(spline_.getPointAt( row ) - spline_.getPointAt( row - 1 ));
		}


		T matrixAt( const std::size_t row, const std::size_t column ) const
		{
			if (row == column)
			{
				return matrixDiagonalAt( row );
			}
			else if (row == column + 1)
			{
				return matrixMinorDiagonalAt<MinorDiagonal::LOWER>( row );
			}
			else if (row + 1 == column)
			{
				return matrixMinorDiagonalAt<MinorDiagonal::UPPER>( row );
			}
			else throw SplineError( "Internal Spline error: Unintended element request during solving linear equations." );
		}



		T matrixDiagonalAt( const std::size_t row ) const
		{
			if (row == 0u || row == size_ - 1u) return 1;
			else return (spline_.getPointAt( row + 1 ) - spline_.getPointAt( row - 1 ))/static_cast<T>( 3 );
		}



		template<MinorDiagonal minor>
		T matrixMinorDiagonalAt( const std::size_t row ) const
		{
			auto border = (minor == MinorDiagonal::UPPER) ? 0u : size_ - 1u;
			auto index = (minor == MinorDiagonal::UPPER) ? row : row - 1u;

			if (row == border)
			{
				if (spline_.type_ == SplineType::NATURAL) return 0;
				else if (spline_.type_ == SplineType::FIXED) return 0.5;
				else throw SplineError( "SplineType not known in SplineSleSolver" );
			}
			else return (spline_.getPointAt( index + 1 ) - spline_.getPointAt( index ))/static_cast<T>( 6 );
		}
	};
};



} // namespace interpolation
} // namespace finael


#endif // FINAEL_INTERPOLATION_SPLINE_H_
