/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUQCD_EVOLUTIONKERNEL_CREATOR_HELPER_H_
#define FINAEL_CUQCD_EVOLUTIONKERNEL_CREATOR_HELPER_H_

#include "../common/cudaHostDevice.h"
#include "../math/generalFunctions.h"
#include "../math/Matrix.h"
#include "../tools/Pair.h"

#include "constants.h"


namespace finael {
namespace cuQcd {



template<unsigned short numberFlavours, typename ScalarType>
class EvolutionKernelCreatorHelper
{
public:
	static constexpr unsigned short NUMBERFLAVOURS = numberFlavours;
	static constexpr ScalarType BETA0 = CONSTANTS<ScalarType>::template BETA0<NUMBERFLAVOURS>();



	template<class Complex>
	CUDA_HOST_DEVICE cuTool::Pair<Complex, Complex> calculateEigenvalues( const cuMath::Matrix<2,2,Complex>& matrix ) const
	{
		cuTool::Pair<Complex, Complex> eigenValues;

		auto discriminant = pow( cuMath::Pow<Complex,2>::pow( matrix[0][0] - matrix[1][1]) + 4 * matrix[0][1] * matrix[1][0], static_cast<ScalarType>( 0.5 ) );

		eigenValues.first = 0.5 * (matrix[0][0] + matrix[1][1] + discriminant);
		eigenValues.second = 0.5 * (matrix[0][0] + matrix[1][1] - discriminant);

		return eigenValues;
	}



	template<class Complex>
	CUDA_HOST_DEVICE cuMath::Matrix<2,2,Complex> calculateProjector( const cuMath::Matrix<2,2,Complex>& r0Matrix, const Complex& ownEigenvalue, const Complex& otherEigenvalue ) const
	{
		cuMath::Matrix<2,2,Complex> projector( r0Matrix );
		projector[0][0] -= otherEigenvalue;
		projector[1][1] -= otherEigenvalue;

		projector /= ownEigenvalue - otherEigenvalue;

		return projector;
	}
};





template<unsigned short numberFlavours, typename ScalarType>
constexpr unsigned short EvolutionKernelCreatorHelper<numberFlavours, ScalarType>::NUMBERFLAVOURS;

template<unsigned short numberFlavours, typename ScalarType>
constexpr ScalarType EvolutionKernelCreatorHelper<numberFlavours, ScalarType>::BETA0;



} // namespace cuQcd
} // namespace finael


#endif // FINAEL_CUQCD_EVOLUTIONKERNEL_CREATOR_HELPER_H_
