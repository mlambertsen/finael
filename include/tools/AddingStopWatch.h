/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_TOOL_ADDING_STOP_WATCH_H_
#define FINAEL_TOOL_ADDING_STOP_WATCH_H_

#include <chrono>
#include <iostream>


namespace finael {
namespace tool {



template<class stdChronoDuration>
class AddingStopWatch
{
public:
	class SingleWatch
	{
	public:
		using clock = std::chrono::steady_clock;


		SingleWatch( AddingStopWatch& parentIn )
			: parent( &parentIn )
			, start( clock::now() )
		{}


		~SingleWatch()
		{
			auto end = clock::now();
			auto duration = std::chrono::duration_cast<stdChronoDuration>(end - start);
			parent->add( duration );
		}


	private:
		AddingStopWatch* parent;
		clock::time_point start;


		static_assert( clock::is_steady, "No reliable measurements possible." );
	};



	AddingStopWatch()
		: total( stdChronoDuration::zero() )
	{}



	SingleWatch createSingleWatch()
	{
		return SingleWatch( *this );
	}



	void add( stdChronoDuration& duration )
	{
		total += duration;
	}



	void time() const
	{
		std::cout << total.count() << std::endl;
	}



	void reset()
	{
		total = stdChronoDuration::zero();
	}



private:
	stdChronoDuration total;
};



} // namespace tool
} // namespace finael




#endif // FINAEL_TOOL_ADDING_STOP_WATCH_H_
