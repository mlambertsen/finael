/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_WEIGHTS_MANAGER_H_
#define FINAEL_CUVEGAS_WEIGHTS_MANAGER_H_

#include <algorithm>
#include <cmath>
#include <iterator>
#include <numeric>
#include <utility>
#include <vector>

#include "../../common/cudaHostDevice.h"

#include "../InternalSettings.h"
#include "../Samples.h"
#include "../sums/Estimate.h"
#include "../sums/EstimateSum.h"
#include "../sums/FunctionSums.h"


namespace finael {
namespace cuVegas {



template<unsigned int dim, typename T>
class WeightsManager
{
public:
	CUDA_HOST WeightsManager( unsigned int incrementsPerDim )
		: weights( dim * incrementsPerDim, 0 )
	{}



	template<InternalStrategy strategy, ReductionFlag reductionMode>
	CUDA_HOST void collect( unsigned int idx, const Samples<T, CudaHostDeviceType::HOST, reductionMode>& samples, const FunctionSums<T>& singleTerm )
	{
		if (strategy == InternalStrategy::IMPORTANCE)
		{
			addWeight( idx, singleTerm.squaredSum, samples );
		}
	}



	template<InternalStrategy strategy, ReductionFlag reductionMode>
	CUDA_HOST void collect( unsigned int idxMin, const Samples<T, CudaHostDeviceType::HOST, reductionMode>& samples, const Estimate<T>& estimate )
	{
		if (strategy == InternalStrategy::STRATIFIED)
		{
			addWeight( idxMin, estimate.variance, samples );
		}
	}



	CUDA_HOST void collect( unsigned int incrementTotalIdx, T weight )
	{
		const auto incrementsPerDim = weights.size() / dim;
		for (auto dimIdx = 0u; dimIdx < dim; ++dimIdx)
		{
			auto incrementIdx = (incrementTotalIdx/static_cast<unsigned int>( std::pow( incrementsPerDim, dimIdx ) )) % incrementsPerDim;
			weights[dimIdx * incrementsPerDim + incrementIdx] += weight;
		}
	}



	CUDA_HOST std::pair<typename std::vector<T>::iterator, typename std::vector<T>::iterator> getWeights( unsigned int dimIdx )
	{
		auto incrementsPerDim = weights.size() / dim;
		auto first = std::next( weights.begin(), incrementsPerDim * dimIdx );
		auto last = std::next( first, incrementsPerDim );
		auto pair = std::make_pair( first, last );
		return pair;
	}



	CUDA_HOST void smoothen()
	{
		for (auto dimIdx = 0u; dimIdx < dim; ++dimIdx)
		{
			auto itPair = getWeights( dimIdx );
			smoothen( itPair.first, itPair.second );
		}
	}



private:
	std::vector<T> weights;



	template<ReductionFlag reductionMode>
	CUDA_HOST void addWeight( unsigned int idx, T weight, const Samples<T, CudaHostDeviceType::HOST, reductionMode>& samples )
	{
		auto incrementsPerDim = weights.size() / dim;
		for (auto dimIdx = 0u; dimIdx < dim; ++dimIdx)
		{
			auto incrementIdx = samples.getIncrementIdx( idx, dimIdx );
			weights[dimIdx * incrementsPerDim + incrementIdx] += weight;
		}
	}



	template<class InputIt>
	CUDA_HOST void smoothen( InputIt firstOfDim, InputIt lastOfDim )
	{
		std::vector<T> newWeights( std::distance( firstOfDim, lastOfDim ) );
		auto newIt = newWeights.begin();
		for (auto it = firstOfDim; it != lastOfDim; ++it, ++newIt)
		{
			auto first = (it == firstOfDim) ? it : std::prev( it, 1 );
			auto last = (it == std::prev( lastOfDim, 1)) ? std::next( it, 1 ) : std::next( it, 2 );
			*newIt = average( first, last );
		}
		std::copy( newWeights.begin(), newWeights.end(), firstOfDim );
	}



	template<class InputIt>
	CUDA_HOST T average( InputIt first, InputIt last ) const
	{
		return std::accumulate( first, last, static_cast<T>( 0 ) )/(std::distance( first, last ));
	}
};



} // namespace cuVegas
} // namespace finael




#endif // FINAEL_CUVEGAS_WEIGHTS_MANAGER_H_
