/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_PRNG_RANDOM123_PHILOX_H_
#define FINAEL_PRNG_RANDOM123_PHILOX_H_

#if defined(__CUDACC__)
#pragma diag_suppress code_is_unreachable
#include "../external/Random123/philox.h"
#pragma diag_warning code_is_unreachable

#elif defined(__GNUC__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-cast"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#include "../external/Random123/philox.h"
#pragma GCC diagnostic pop

#else
#include "../external/Random123/philox.h"
#endif

#endif // FINAEL_PRNG_RANDOM123_PHILOX_H_
