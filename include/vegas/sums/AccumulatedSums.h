/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_ACCUMULATED_SUMS_H_
#define FINAEL_CUVEGAS_ACCUMULATED_SUMS_H_

#include "../../common/cudaHostDevice.h"

#include "EstimateSum.h"


namespace finael {
namespace cuVegas {



template<typename T>
struct AccumulatedSums
{
	unsigned int cardinality;
	T estimateOverVariance;
	T oneOverVariance;
	T estimateSquaredOverVariance;



	CUDA_HOST AccumulatedSums()
		: cardinality( 0u )
		, estimateOverVariance( 0 )
		, oneOverVariance( 0 )
		, estimateSquaredOverVariance( 0 )
	{}



	CUDA_HOST void add( const EstimateSum<T>& estimateSum )
	{
		T estimate = estimateSum.getEstimate();
		T variance = estimateSum.getVariance();
		estimateOverVariance += estimate/variance;
		oneOverVariance += 1 / variance;
		estimateSquaredOverVariance += estimate * estimate / variance;
		++cardinality;
	}



	CUDA_HOST void reset()
	{
		AccumulatedSums newInstance;
		*this = newInstance;
	}
};



} // namespace cuVegas
} // namespace finael



#endif // FINAEL_CUVEGAS_ACCUMULATED_SUMS_H_
