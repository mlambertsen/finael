/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_SETTINGS_H_
#define FINAEL_CUVEGAS_SETTINGS_H_

#include "../common/cudaHostDevice.h"
#include "../tools/DeviceInfo.h"

#include "optionFlags.h"


namespace finael {
namespace cuVegas {



template<typename T>
struct Settings
{
public:
	static constexpr unsigned int numberSamplesDefault = 10000;
	static constexpr unsigned int numberIterationsDefault = 5;
	static constexpr StrategyFlag strategyDefault = StrategyFlag::FREE;
	static constexpr ReductionFlag reductionDefault = ReductionFlag::CLASSIC;
	static constexpr T adaptionRateDefault = static_cast<T>( 1.5 );



	unsigned int numberSamples;
	unsigned int numberIterations;
	StrategyFlag strategy;
	ReductionFlag reduction;
	unsigned int blockDim;
	T adaptionRate;



	CUDA_HOST Settings()
		: numberSamples( numberSamplesDefault )
		, numberIterations( numberIterationsDefault )
		, strategy( strategyDefault )
		, reduction( reductionDefault )
		, blockDim( cuTool::DeviceInfo::hostWarpSize )
		, adaptionRate( adaptionRateDefault )
	{}
};



template<typename T>
constexpr unsigned int Settings<T>::numberSamplesDefault;

template<typename T>
constexpr unsigned int Settings<T>::numberIterationsDefault;

template<typename T>
constexpr StrategyFlag Settings<T>::strategyDefault;

template<typename T>
constexpr ReductionFlag Settings<T>::reductionDefault;

template<typename T>
constexpr T Settings<T>::adaptionRateDefault;



} // namespace cuVegas
} // namespace finael

#endif // FINAEL_CUVEGAS_SETTINGS_H_
