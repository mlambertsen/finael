/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_INTEGRAND_TYPES_H_
#define FINAEL_CUVEGAS_INTEGRAND_TYPES_H_

#include "../tools/Array.h"
#include "../tools/Pair.h"


namespace finael {
namespace cuVegas {


template<typename T, unsigned int dim>
using VolumeType = cuTool::Array<cuTool::Pair<T,T>, dim>;



template<typename T, unsigned int dim>
using IntegrandPointType = cuTool::Array<T, dim>;


} // namespace cuVegas
} // namespace finael



#endif // FINAEL_CUVEGAS_INTEGRAND_TYPES_H_
