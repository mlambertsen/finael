/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#define BOOST_TEST_MODULE "finael Spline"
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include <array>
#include <vector>

#include "../../../include/interpolation/Spline.h"



namespace finael {
namespace interpolation {
namespace splineTest {



auto function = []( double point )
{
	return 10.0/(1.0 + point*point);
};

auto firstSplinePiece = []( double point )
{
	return 9.0/7.0*point*point*point + 54.0/7.0*point*point + 120.0/7.0*point + 110.0/7.0;
};

auto thirdSplinePiece = []( double point )
{
	return 31.0/7.0*point*point*point - 66.0/7.0*point*point + 10.0;
};

auto lastSplinePiece = []( double point )
{
	return -9.0/7.0*point*point*point + 54.0/7.0*point*point - 120.0/7.0*point + 110.0/7.0;
};



struct BoostTestSplineFixture
{
	BoostTestSplineFixture()
		: points {-2.0, -1.0, 0.0, 1.0, 2.0}
		, largestIndex ( points.size() - 1 )
		, spline( points.begin(), points.end(), function )
	{}



	std::vector<double> points;
	std::size_t largestIndex;
	Spline<double> spline;
};



BOOST_FIXTURE_TEST_SUITE( spline, BoostTestSplineFixture )



BOOST_AUTO_TEST_CASE( needsMinimumThreePoints )
{
	std::array<double, 2> tooSmall;
	BOOST_CHECK_THROW( Spline<double>( tooSmall.begin(), tooSmall.end(), function ), SplineError );
}



BOOST_AUTO_TEST_CASE( sortsUnsortedInput )
{
	std::vector<double> unsortedPoints {1.0, 2.0, 0.0};

	Spline<double> aSpline( unsortedPoints.begin(), unsortedPoints.end(), function );

	BOOST_CHECK_EQUAL( aSpline.getPointAt( 0 ), 0.0 );
	BOOST_CHECK_EQUAL( aSpline.getPointAt( 1 ), 1.0 );
	BOOST_CHECK_EQUAL( aSpline.getPointAt( 2 ), 2.0 );
}



BOOST_AUTO_TEST_CASE( defaultsToNaturalSpline )
{
	Spline<double> aSpline( points.begin(), points.end(), function );

	BOOST_CHECK( aSpline.getType() == SplineType::NATURAL );
}



BOOST_AUTO_TEST_CASE( respondsPassedType )
{
	Spline<double> aSpline( points.begin(), points.end(), function, SplineType::FIXED );

	BOOST_CHECK( aSpline.getType() == SplineType::FIXED );
}



BOOST_AUTO_TEST_CASE( answersFunctionValuesOnMiddleGridValue )
{
	auto point = points.at( 1 );

	BOOST_CHECK_CLOSE( spline( point ), function( point ), 1.e-12 );
}



BOOST_AUTO_TEST_CASE( answersFunctionValuesOnLowestGridValue )
{
	auto point = points.at( 0 );

	BOOST_CHECK_CLOSE( spline( point ), function( point ), 1.e-12 );
}



BOOST_AUTO_TEST_CASE( answersFunctionValuesOnHighestGridValue )
{
	auto point = points.at( largestIndex );

	BOOST_CHECK_CLOSE( spline( point ), function( point ), 1.e-12 );
}



BOOST_AUTO_TEST_CASE( throwsOnInputBelowLowestGridPoint )
{
	auto belowGrid = points.at( 0 ) - 1.0;

	BOOST_CHECK_THROW( spline( belowGrid ), SplineError );
}



BOOST_AUTO_TEST_CASE( throwsOnInputAboveLargestGridPoint )
{
	auto aboveGrid = points.at( largestIndex ) + 1.0;

	BOOST_CHECK_THROW( spline( aboveGrid ), SplineError );
}



BOOST_AUTO_TEST_CASE( adaptsNewSize )
{
	auto newSize = points.size() + 1;
	std::vector<double> newPoints( newSize );

	spline.set( newPoints.begin(), newPoints.end(), function );

	BOOST_CHECK_EQUAL( spline.numberPoints(), newSize );
}



BOOST_AUTO_TEST_CASE( offersSplinePieceToCorrespondingIndex )
{
	auto piece = spline.getPiece( 2 );

	BOOST_CHECK_CLOSE( piece.cubic, 31.0/7.0, 1.e-12 );
	BOOST_CHECK_CLOSE( piece.square, -66.0/7.0, 1.e-12 );
	BOOST_CHECK_EQUAL( piece.linear, 0.0 );
	BOOST_CHECK_EQUAL( piece.constant, 10.0 );
}



BOOST_AUTO_TEST_CASE( offersSplinePieceToFirstIndex )
{
	auto piece = spline.getPiece( 0 );

	BOOST_CHECK_CLOSE( piece.cubic, 9.0/7.0, 1.e-12 );
	BOOST_CHECK_CLOSE( piece.square, 54.0/7.0, 1.e-12 );
	BOOST_CHECK_CLOSE( piece.linear, 120.0/7.0, 1.e-12 );
	BOOST_CHECK_CLOSE( piece.constant, 110.0/7.0, 1.e-12 );
}



BOOST_AUTO_TEST_CASE( calculatesValueOfSplineBetweenTwoNonBorderPoints )
{
	BOOST_CHECK_CLOSE( spline( 0.5 ) , thirdSplinePiece( 0.5 ), 1.e-12 );
}



BOOST_AUTO_TEST_CASE( calculatesValueOfSplineInsideFirstSplinePiece )
{
	BOOST_CHECK_CLOSE( spline( -1.5 ) , firstSplinePiece( -1.5 ), 1.e-12 );
}



BOOST_AUTO_TEST_CASE( calculatesValueOfSplineInsideLastSplinePiece )
{
	BOOST_CHECK_CLOSE( spline( 1.5 ) , lastSplinePiece( 1.5 ), 1.e-12 );
}



BOOST_AUTO_TEST_CASE( canBeSetWithAMellinSpline )
{
	MellinSpline<double> mellinSpline( spline );

	Spline<double> newSpline( mellinSpline );

	double point {-1.5};
	BOOST_CHECK_CLOSE( spline( point ), newSpline( point ), 1.e-12 );
	point = -0.5;
	BOOST_CHECK_CLOSE( spline( point ), newSpline( point ), 1.e-12 );
	point = 0.5;
	BOOST_CHECK_CLOSE( spline( point ), newSpline( point ), 1.e-12 );
	point = 1.5;
	BOOST_CHECK_CLOSE( spline( point ), newSpline( point ), 1.e-12 );
}



BOOST_AUTO_TEST_SUITE_END()


} // namespace splineTest
} // namespace interpolation
} // namespace finael
