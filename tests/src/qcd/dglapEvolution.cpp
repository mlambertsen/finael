/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#define BOOST_TEST_MODULE "finael Dglap Evolution"
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "../../../include/qcd/DglapEvolution.h"
#include "../../../include/math/Complex.h"


template<unsigned short numberFlavours, finael::cuQcd::PQcdOrder order, typename T> class AlphaSMock
{
public:
	using ScaleType = T;
	static constexpr unsigned short NUMBERFLAVOURS = numberFlavours;
	static constexpr finael::cuQcd::PQcdOrder ORDER = order;


	T operator()( T scale ) const
	{
		return scale;
	}
};



template<unsigned short numberFlavours, finael::cuQcd::PQcdOrder order, typename T> constexpr unsigned short AlphaSMock<numberFlavours, order, T>::NUMBERFLAVOURS;
template<unsigned short numberFlavours, finael::cuQcd::PQcdOrder order, typename T> constexpr finael::cuQcd::PQcdOrder AlphaSMock<numberFlavours, order, T>::ORDER;



BOOST_AUTO_TEST_SUITE( aDglapEvolution )



BOOST_AUTO_TEST_CASE( evolvesAtLo )
{
	using namespace finael::cuQcd;
	using namespace finael::cuMath;
	using namespace finael::cuTool;
	using CD = Complex<double>;

	double startScale = 3.0;
	double endScale = 5.0;
	CD mellin( -2.0, 3.0 );
	Array<CD, 7> rawPartons = {{ CD( 1.0 ), CD( 2.0 ), CD( 3.0 ), CD( 5.0 ), CD( 7.0 ), CD( 11.0 ), CD( 11.0 ) }};
	PartonDistribution<3, CD, double> pdfs( rawPartons, startScale );
	DglapEvolution<AlphaSMock<3, PQcdOrder::LO, double>, double> dglap;

	dglap.evolve( pdfs, endScale, mellin );

	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::G>().real(), -0.22194340554468883, 1.e-12 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::G>().imag(), 4.1314926567740038, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::U>().real(), 2.2242828711145801, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::U>().imag(), 1.5774541905110453, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::UBAR>().real(), 3.3576825986168637, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::UBAR>().imag(), 2.3633573972454696, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::D>().real(), 5.6244820536214313, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::D>().imag(), 3.9351638107143185, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::DBAR>().real(), 7.8912815086259975, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::DBAR>().imag(), 5.5069702241831671, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::S>().real(), 12.424880418635132, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::S>().imag(), 8.6505830511208632, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::SBAR>().real(), 12.424880418635132, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::SBAR>().imag(), 8.6505830511208632, 1.e-13 );
}



BOOST_AUTO_TEST_CASE( evolvesAtNlo )
{
	using namespace finael::cuQcd;
	using namespace finael::cuMath;
	using namespace finael::cuTool;
	using CD = Complex<double>;

	double startScale = 3.0;
	double endScale = 5.0;
	CD mellin( -2.0, 3.0 );
	Array<CD, 7> rawPartons = {{ CD( 1.0 ), CD( 2.0 ), CD( 3.0 ), CD( 5.0 ), CD( 7.0 ), CD( 11.0 ), CD( 11.0 ) }};
	PartonDistribution<3, CD, double> pdfs( rawPartons, startScale );
	DglapEvolution<AlphaSMock<3, PQcdOrder::NLO, double>, double> dglap;

	dglap.evolve( pdfs, endScale, mellin );

	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::G>().real(), 6.4520728888686776, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::G>().imag(), 6.8787998085848763, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::U>().real(), 2.9669248207773942, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::U>().imag(), 1.9021463786135417, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::UBAR>().real(), 4.3877541991937292, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::UBAR>().imag(), 2.5997399220107096, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::D>().real(), 7.2334868545094935, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::D>().imag(), 4.0004226786750738, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::DBAR>().real(), 10.075145611342164, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::DBAR>().imag(), 5.3956097654694100, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::S>().real(), 15.764282979983349, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::S>().imag(), 8.1938348960152663, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::SBAR>().real(), 15.764282979983349, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::SBAR>().imag(), 8.1938348960152663, 1.e-13 );
}



BOOST_AUTO_TEST_SUITE_END()
