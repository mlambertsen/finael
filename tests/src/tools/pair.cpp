/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#define BOOST_TEST_MODULE "finael Pair"
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "../../../include/tools/Pair.h"
#include "../../../include/tools/Array.h"



BOOST_AUTO_TEST_SUITE( pair )



BOOST_AUTO_TEST_CASE( defaultConstructable )
{
	using namespace finael::cuTool;
	Pair<Array<int, 1>, Array<double, 1>> pair;

	BOOST_CHECK_EQUAL( pair.first[0], 0 );
	BOOST_CHECK_EQUAL( pair.second[0], 0.0 );
}



BOOST_AUTO_TEST_CASE( defaultConstructsWithZeros )
{
	using namespace finael::cuTool;
	Pair<int, double> pair( 1, 2.0 );

	BOOST_CHECK_EQUAL( pair.first, 1 );
	BOOST_CHECK_EQUAL( pair.second, 2.0 );
}



BOOST_AUTO_TEST_SUITE_END()
