/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_QCD_PDF_INTERPOLATION_DSS_FUNCTOR_H_
#define FINAEL_QCD_PDF_INTERPOLATION_DSS_FUNCTOR_H_

#include "dss_fortran.h"



namespace finael {
namespace qcd {
namespace pdfInterpolation {



class DssFunctor
{
public:
	using value_type = double;



	DssFunctor( const int setVersion, const int order, const int flavour, const int hadronType, const int hadronCharge, const double scale )
		: scale_( scale )
		, setVersion_( setVersion )
		, order_( order )
		, flavour_( flavour )
		, hadronType_( hadronType )
		, hadronCharge_( hadronCharge )
	{
		fortran::fragini_.fini = 0;
	}



	double operator()( const double x ) const
	{
		double temp[9];
		int setVersion = setVersion_;
		int order = order_;
		double xCopy = x;
		double scaleSquared = scale_*scale_;
		int hadronType = hadronType_;
		int hadronCharge = hadronCharge_;

		fortran::fdss_( &setVersion, &hadronType, &hadronCharge, &order, &xCopy, &scaleSquared, &temp[0], &temp[1], &temp[2], &temp[3], &temp[4], &temp[5], &temp[6], &temp[7], &temp[8] );

		return temp[flavour_]/xCopy;
	}



	double scale_;
	int setVersion_;
	int order_;
	int flavour_;
	int hadronType_;
	int hadronCharge_;
};



} // namespace pdfInterpolation
} // namespace qcd
} // namespace finael



#endif // FINAEL_QCD_PDF_INTERPOLATION_DSS_FUNCTOR_H_
