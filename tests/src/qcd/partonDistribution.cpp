/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#define BOOST_TEST_MODULE "finael Parton Distribution"
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "../../../include/qcd/PartonDistribution.h"



BOOST_AUTO_TEST_SUITE( aPartonDistribution )



BOOST_AUTO_TEST_CASE( offersDisentangledPartonDensities )
{
	using namespace finael::cuQcd;
	using namespace finael::cuTool;

	Array<double, 7> rawPartons = {{ 0.0, 1.0, 2.0, 3.0, 5.0, 7.0, 11.0 }};
	PartonDistribution<3, double, int> pdfs( rawPartons, 50 );

	BOOST_CHECK_EQUAL( pdfs.get<PartonFlavour::G>(), 0.0 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::U>(), 1.0, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::UBAR>(), 2.0, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::D>(), 3.0, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::DBAR>(), 5.0, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::S>(), 7.0, 1.e-13 );
	BOOST_CHECK_CLOSE( pdfs.get<PartonFlavour::SBAR>(), 11.0, 1.e-13 );
}




BOOST_AUTO_TEST_SUITE_END()
