/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_SUBINCREMENTS_H_
#define FINAEL_CUVEGAS_SUBINCREMENTS_H_

#include <cmath>
#include <iterator>
#include <numeric>
#include <utility>
#include <vector>

#include "../../common/cudaHostDevice.h"

#include "../Parameters.h"


namespace finael {
namespace cuVegas {



enum class SubdividingMode {IMPROVE, STRETCH};




template<typename T, SubdividingMode mode>
class Subincrements;




template<typename T>
class Subincrements<T, SubdividingMode::IMPROVE>
{
public:
	template<class Pair>
	CUDA_HOST Subincrements( const Pair& weights, T adaptionRate )
		: numbers_( std::distance( weights.first, weights.second ), 0 )
	{
		const T sumWeights = std::accumulate( weights.first, weights.second, static_cast<T>( 0 ) );

		auto weightsIt = weights.first;
		for (auto it = numbers_.begin(); it != numbers_.end(); ++it, ++weightsIt)
		{
			T weight = ensurePositivity( *weightsIt );
			*it = std::pow((weight/sumWeights - 1)/(std::log(weight/sumWeights)), adaptionRate);
		}
	}



	CUDA_HOST const T& operator[]( unsigned int index ) const
	{
		return numbers_[index];
	}



	CUDA_HOST unsigned int size() const
	{
		return numbers_.size();
	}



	CUDA_HOST T average() const
	{
		return std::accumulate( numbers_.begin(), numbers_.end(), static_cast<T>( 0 ) )/numbers_.size();
	}



private:
	std::vector<T> numbers_;



	CUDA_HOST T ensurePositivity( const T value ) const
	{
		static const T EPSILON = std::nextafter( static_cast<T>( 0 ), static_cast<T>( 1 ));
		return (value < EPSILON) ? EPSILON : value;
	}
};





template<typename T>
class Subincrements<T, SubdividingMode::STRETCH>
{
public:
	CUDA_HOST Subincrements( unsigned int oldNumberIncrementsIn, unsigned int newNumberIncrementsIn )
		: oldNumberIncrements( oldNumberIncrementsIn )
		, newNumberIncrements( newNumberIncrementsIn )
	{}



	CUDA_HOST unsigned int operator[]( unsigned int /* index */ ) const
	{
		return 1u;
	}



	CUDA_HOST unsigned int size() const
	{
		return newNumberIncrements;
	}



	CUDA_HOST T average() const
	{
		return static_cast<T>( oldNumberIncrements ) / newNumberIncrements;
	}



private:
	unsigned int oldNumberIncrements;
	unsigned int newNumberIncrements;
};



} // namespace cuVegas
} // namespace finael


#endif // FINAEL_CUVEGAS_SUBINCREMENTS_H_
