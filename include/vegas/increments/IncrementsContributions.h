/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_INCREMENTS_CONTRIBUTIONS_H_
#define FINAEL_CUVEGAS_INCREMENTS_CONTRIBUTIONS_H_

#include "../../common/cudaHostDevice.h"
#include "../../tools/SmartArray.h"

#include "../Samples.h"
#include "../sums/Estimate.h"
#include "../sums/EstimateSum.h"


namespace finael {
namespace cuVegas {



template<unsigned int dim, typename T>
class IncrementsContributions
{
public:
	template<class SamplesOrEstimates>
	CUDA_HOST void update( const SamplesOrEstimates& samplesOrEstimates, unsigned int incrementsPerDim )
	{
		reset( incrementsPerDim );
		collect( samplesOrEstimates );
	}



	CUDA_HOST void normalize( const EstimateSum<T>& estimateSum )
	{
		T normalizationFactor = estimateSum.getEstimate() / getUnnormalizedSum();

		for (auto i = 0u; i < contributions_.size(); ++i)
		{
			contributions_[i] *= normalizationFactor;
		}
	}



	CUDA_HOST T at( unsigned int dimIdx, unsigned int incrementIdx ) const
	{
		return contributions_[dimIdx*incrementsPerDim_ + incrementIdx];
	}



private:
	unsigned int incrementsPerDim_;
	cuTool::SmartArray<T, CudaHostDeviceType::HOST> contributions_;



	CUDA_HOST T getUnnormalizedSum()
	{
		T sum = 0;
		for (auto incrementIdx = 0u; incrementIdx < incrementsPerDim_; ++incrementIdx)
		{
			sum += contributions_[incrementIdx];
		}
		return sum;
	}



	CUDA_HOST void reset( unsigned int incrementsPerDim )
	{
		incrementsPerDim_ = incrementsPerDim;
		if (contributions_.size() != dim*incrementsPerDim_) contributions_.newArray( dim*incrementsPerDim_ );

		for (auto idx = 0u; idx < contributions_.size(); ++idx)
		{
			contributions_[idx] = 0;
		}
	}



	template<ReductionFlag reductionMode>
	CUDA_HOST void collect( const Samples<T, CudaHostDeviceType::HOST, reductionMode>& samples )
	{
		for (auto sampleIdx = 0u; sampleIdx < samples.size(); ++sampleIdx)
		{
			auto functionValue = samples.getFunctionValue( sampleIdx );
			for (auto dimIdx = 0u; dimIdx < dim; ++dimIdx)
			{
				auto incrementIdx = samples.getIncrementIdx( sampleIdx, dimIdx );
				contributions_[dimIdx * incrementsPerDim_ + incrementIdx] += functionValue;
			}
		}
	}



	CUDA_HOST void collect( const cuTool::SmartArray<Estimate<T>, CudaHostDeviceType::HOST>& estimateSums )
	{
		for (auto incrementTotalIdx = 0u; incrementTotalIdx < estimateSums.size(); ++incrementTotalIdx)
		{
			auto contribution = estimateSums[incrementTotalIdx].estimate;
			for (auto dimIdx = 0u; dimIdx < dim; ++dimIdx)
			{
				auto incrementIdx = (incrementTotalIdx/static_cast<unsigned int>( std::pow( incrementsPerDim_, dimIdx ) )) % incrementsPerDim_;
				contributions_[dimIdx * incrementsPerDim_ + incrementIdx] += contribution;
			}
		}
	}
};



} // namespace cuVegas
} // namespace finael



#endif // FINAEL_CUVEGAS_INCREMENTS_CONTRIBUTIONS_H_
