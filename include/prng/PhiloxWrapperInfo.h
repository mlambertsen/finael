/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_PRNG_PHILOX_WRAPPER_INFO_H_
#define FINAEL_PRNG_PHILOX_WRAPPER_INFO_H_



namespace finael {
namespace prng {



template<typename T>
class PhiloxWrapperInfo;



template<>
class PhiloxWrapperInfo<float>
{
public:
	static constexpr int NumberInts = 4;
	using IntType = uint32_t;
};

constexpr int PhiloxWrapperInfo<float>::NumberInts;



template<>
class PhiloxWrapperInfo<double>
{
public:
	static constexpr int NumberInts = 2;
	using IntType = uint64_t;
};

constexpr int PhiloxWrapperInfo<double>::NumberInts;



} // namespace prng
} // namespace finael

#endif // FINAEL_PRNG_PHILOX_WRAPPER_INFO_H_
