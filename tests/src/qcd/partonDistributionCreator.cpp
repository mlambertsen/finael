/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#define BOOST_TEST_MODULE "finael Parton Distribution Creator"
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "../../../include/qcd/PartonDistributionCreator.h"



BOOST_AUTO_TEST_SUITE( aPartonDistributionCreator )



BOOST_AUTO_TEST_CASE( createsSingletThreeFlavours )
{
	using namespace finael::cuQcd;
	using namespace finael::cuTool;

	Array<int, 7> rawPartons = {{ 1, 2, 3, 4, 5, 6, 7 }};
	PartonDistributionCreator<3, int> creator;

	auto singlet = creator.createSinglet( rawPartons );

	BOOST_CHECK_EQUAL( singlet[0], 27 );
	BOOST_CHECK_EQUAL( singlet[1], 1 );
}



BOOST_AUTO_TEST_CASE( createsNonSingletValenceThreeFlavours )
{
	using namespace finael::cuQcd;
	using namespace finael::cuTool;

	Array<int, 7> rawPartons = {{ 1, 2, 3, 4, 5, 6, 7 }};
	PartonDistributionCreator<3, int> creator;

	auto nonSingletValence = creator.createNonSingletValence( rawPartons );

	BOOST_CHECK_EQUAL( nonSingletValence, -3 );
}



BOOST_AUTO_TEST_CASE( createsNonSingletNonValencePlusThreeFlavours )
{
	using namespace finael::cuQcd;
	using namespace finael::cuTool;

	Array<int, 7> rawPartons = {{ 1, 2, 3, 4, 5, 6, 7 }};
	PartonDistributionCreator<3, int> creator;

	auto nonSingletNonValencePlus = creator.createNonSingletNonValence<finael::cuMath::Sign::PLUS>( rawPartons );

	BOOST_CHECK_EQUAL( nonSingletNonValencePlus[0], -4 );
	BOOST_CHECK_EQUAL( nonSingletNonValencePlus[1], -12 );
}



BOOST_AUTO_TEST_CASE( createsNonSingletNonValenceMinusThreeFlavours )
{
	using namespace finael::cuQcd;
	using namespace finael::cuTool;

	Array<int, 7> rawPartons = {{ 1, 2, 3, 4, 5, 6, 8 }};
	PartonDistributionCreator<3, int> creator;

	auto nonSingletNonValenceMinus = creator.createNonSingletNonValence<finael::cuMath::Sign::MINUS>( rawPartons );

	BOOST_CHECK_EQUAL( nonSingletNonValenceMinus[0], 0 );
	BOOST_CHECK_EQUAL( nonSingletNonValenceMinus[1], 2 );
}




BOOST_AUTO_TEST_SUITE_END()
