/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_REDUCER_KERNEL_H_
#define FINAEL_CUVEGAS_REDUCER_KERNEL_H_

#include "../../common/cudaHostDevice.h"
#include "../../tools/SmartArray.h"

#include "../Samples.h"
#include "../sums/Estimate.h"

#include "BlockReducer.h"
#include "RefinedEstimateCalculator.h"


namespace finael {
namespace cuVegas {
namespace reducerKernel {



#ifdef __CUDACC__
template<typename T>
__global__ void reduceSamplesToEstimates( const Samples<T, CudaHostDeviceType::DEVICE, ReductionFlag::REFINED> samples, cuTool::SmartArray<Estimate<T>, CudaHostDeviceType::DEVICE> estimates, unsigned int elementsPerIncrement, unsigned int blocksPerIncrement, unsigned int binCapacity )
{
	auto idx = blockIdx.x * blockDim.x + threadIdx.x;
	auto incrementIdx = blockIdx.x / blocksPerIncrement;
	auto binInIncrementIdx = idx % (blocksPerIncrement * blockDim.x);

	auto estimate = RefinedEstimateCalculator::calculate( samples, incrementIdx, elementsPerIncrement, binInIncrementIdx, binCapacity );

	BlockReducer<T, CudaHostDeviceType::DEVICE> blockReducer;
	estimate.estimate = blockReducer( estimate.estimate );
	estimate.variance = blockReducer( estimate.variance );

	if (threadIdx.x == 0) estimates[blockIdx.x] = estimate;
}



template<typename T>
__global__ void reduceIncrements( const cuTool::SmartArray<Estimate<T>, CudaHostDeviceType::DEVICE> input, cuTool::SmartArray<Estimate<T>, CudaHostDeviceType::DEVICE> output, unsigned int elementsPerIncrement, unsigned int blocksPerIncrement )
{
	auto idx = blockIdx.x * blockDim.x + threadIdx.x;
	auto incrementIdx = blockIdx.x / blocksPerIncrement;
	auto elementInIncrementIdx = idx % (blocksPerIncrement * blockDim.x);

	auto inputIdx = elementsPerIncrement * incrementIdx + elementInIncrementIdx;

	Estimate<T> estimate = (elementInIncrementIdx < elementsPerIncrement) ? input[inputIdx] : Estimate<T>();

	BlockReducer<T, CudaHostDeviceType::DEVICE> blockReducer;
	estimate.estimate = blockReducer( estimate.estimate );
	estimate.variance = blockReducer( estimate.variance );

	if (threadIdx.x == 0) output[blockIdx.x] = estimate;
}
#endif // __CUDACC__





template<class Samples, class Estimates>
CUDA_HOST void reduceSamplesToEstimatesHost( unsigned int numberBlocks, unsigned int blockDim, const Samples& samples, Estimates& estimates, unsigned int elementsPerIncrement, unsigned int blocksPerIncrement, unsigned int binCapacity )
{
	using T = typename Estimates::type::type;

	for (auto blockIdx = 0u; blockIdx < numberBlocks; ++blockIdx)
	{
		cuTool::SmartArray<Estimate<T>, CudaHostDeviceType::HOST> estimateInLocalThreadRegister( blockDim );
		for (auto threadIdx = 0u; threadIdx < blockDim; ++threadIdx)
		{
			auto idx = blockIdx * blockDim + threadIdx;
			auto incrementIdx = blockIdx / blocksPerIncrement;
			auto binInIncrementIdx = idx % (blocksPerIncrement * blockDim);

			estimateInLocalThreadRegister[threadIdx] = RefinedEstimateCalculator::calculate( samples, incrementIdx, elementsPerIncrement, binInIncrementIdx, binCapacity );
		}

		BlockReducer<Estimate<T>, CudaHostDeviceType::HOST> blockReducer;
		estimates[blockIdx] = blockReducer( estimateInLocalThreadRegister, blockDim );
	}
}



template<class Estimates>
CUDA_HOST void reduceIncrementsHost( unsigned int numberBlocks, unsigned int blockDim, const Estimates input, Estimates output, unsigned int elementsPerIncrement, unsigned int blocksPerIncrement )
{
	using T = typename Estimates::type::type;

	for (auto blockIdx = 0u; blockIdx < numberBlocks; ++blockIdx)
	{
		cuTool::SmartArray<Estimate<T>, CudaHostDeviceType::HOST> estimateInLocalThreadRegister( blockDim );
		for (auto threadIdx = 0u; threadIdx < blockDim; ++threadIdx)
		{
			auto idx = blockIdx * blockDim + threadIdx;
			auto incrementIdx = blockIdx / blocksPerIncrement;
			auto elementInIncrementIdx = idx % (blocksPerIncrement * blockDim);

			auto inputIdx = elementsPerIncrement * incrementIdx + elementInIncrementIdx;
			estimateInLocalThreadRegister[threadIdx] = (elementInIncrementIdx < elementsPerIncrement) ? input[inputIdx] : Estimate<T>();
		}

		BlockReducer<Estimate<T>, CudaHostDeviceType::HOST> blockReducer;
		output[blockIdx] = blockReducer( estimateInLocalThreadRegister, blockDim );
	}
}



} // namespace reducerKernel
} // namespace cuVegas
} // namespace finael


#endif // FINAEL_CUVEGAS_REDUCER_KERNEL_H_
