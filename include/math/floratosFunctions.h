/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUMATH_FLORATOS_FUNC_H_
#define FINAEL_CUMATH_FLORATOS_FUNC_H_

#include "../common/cudaHostDevice.h"

#include "constants.h"
#include "definitions.h"
#include "generalFunctions.h"
#include "PolyGamma.h"


namespace finael {
namespace cuMath {



//------------------------------------------
// Function usually defined in DGLAP evolution in Mellin space
// Definitions in Floratos et. al., Nucl. Phys. B192, 417-462 (1981) and Glück et. al. Z.Phys. C48, 471-482 (1990)
//------------------------------------------
template<unsigned int index, template<typename> class Complex, typename T>
CUDA_HOST_DEVICE Complex<T> sFloratos( const Complex<T>& argument )
{
	PolyGamma<index - 1> polyGamma;
	if (index == 1)	return polyGamma( argument + 1 ) + CONST<T>::EULERGAMMA;
	else if (index == 2) return CONST<T>::ZETA2 - polyGamma( argument + 1 );
	else if (index == 3) return 0.5 * polyGamma( argument + 1 ) + CONST<T>::ZETA3;
	else
	{
		printf( "sFloratos not implemented for index %u\n", index );
		return Complex<T>( 0 );
	}
}



template<unsigned int index, Sign sign, template<typename> class Complex, typename T>
CUDA_HOST_DEVICE inline Complex<T> sPrimeFloratos( const Complex<T>& argument )
{
	return (sign == Sign::PLUS) ? sFloratos<index>( argument ) : sFloratos<index>( argument - 0.5 );
}



// Use still Floratos in the name, although the definition seems to be from Glück et. al..
// For large norm of the argument the series given in Gonzalez-Arroyo et. al. Nucl.Phys. B153, 161-186 (1979) should be sufficient and much faster as the parameterization given by Glück et. al. Z.Phys. C48, 471-482 (1990)
// The limit to change the parameterization is taken from Weinzierl Comp.Phys.Comm. 148, 314-326 (2002)
// Weinzierl and also Vogt in his Pegasus package claim that below equation (A.5) in Glück et. al. Z.Phys. C48, 471-482 (1990) should be G(N) = psi((N+1)/2) - psi(N/2) instead of G(N) = psi(N+1/2) - psi(N/2)
template<Sign sign, template<typename> class Complex, typename T>
CUDA_HOST_DEVICE Complex<T> sTildeFloratos( const Complex<T>& argument )
{
	Complex<T> result( static_cast<T>( -0.625) * CONST<T>::ZETA3);
	Complex<T> second_term;

	if (norm( argument ) > 24) second_term = 0.5 * (log( argument ) * (argument - 1) / Pow<Complex<T>, 3>::pow( argument ) + CONST<T>::EULERGAMMA / Pow<Complex<T>, 2>::pow( argument ) + ( 1 - CONST<T>::EULERGAMMA ) / Pow<Complex<T>, 3>::pow( argument ) - 5.0/6.0 / Pow<Complex<T>, 4>::pow( argument ));
	else
	{
		PolyGamma<0> diGamma;
		second_term = sFloratos<1>( argument ) / Pow<Complex<T>, 2>::pow( argument ) - CONST<T>::ZETA2 * 0.5 * (diGamma( argument / 2 + 0.5) - diGamma( argument / 2 )) + (1.010 / (argument + 1) - 0.846 / (argument + 2) + 1.155 / (argument + 3)  - 1.074 / (argument + 4) + 0.550 / (argument + 5));
	}

	if (sign == Sign::PLUS) result += second_term;
	else if (sign == Sign::MINUS) result -= second_term;

	return result;
}



} // namespace cuMath
} // namespace finael


#endif // FINAEL_CUMATH_FLORATOS_FUNC_H_
