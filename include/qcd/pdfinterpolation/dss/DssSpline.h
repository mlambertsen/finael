/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_QCD_PDF_INTERPOLATION_DSS_SPLINE_H_
#define FINAEL_QCD_PDF_INTERPOLATION_DSS_SPLINE_H_

#include <stdexcept>

#include "../../definitions.h"

#include "dss_settings.h"
#include "DssDefaultGrid.h"
#include "DssFunctor.h"
#include "../PdfSplineBase.h"



namespace finael {
namespace qcd {
namespace pdfInterpolation {



// template just for interface reasons, to fit with other classes. In principle only T = double makes sense, because DSS provides only double
template<typename T = double>
class DssSpline : public PdfSplineBase<DssFunctor, int, int, int, int, int, double>
{
private:
	using PdfSpline = PdfSplineBase<DssFunctor, int, int, int, int, int, double>;



public:
	using SplineType = typename PdfSpline::SplineType;



	template<class PointIt>
	DssSpline( PointIt firstPoint, PointIt lastPoint, const SplineType type, const DssSet dssSet, const PQcdOrder order, const PartonFlavour flavour, const T scale, const Hadron hadronType, const int hadronCharge )
		: PdfSpline( firstPoint, lastPoint, type, convertVersion( dssSet ), convertOrder( order ), convertPartonFlavour( flavour ), convertHadronType( hadronType ), hadronCharge, static_cast<double>( scale ) )
	{}



	template<class PointIt>
	DssSpline( PointIt firstPoint, PointIt lastPoint, const DssSet dssSet, const PQcdOrder order, const PartonFlavour flavour, const T scale, const Hadron hadronType, const int hadronCharge )
		: DssSpline( firstPoint, lastPoint, SplineType::NATURAL, dssSet, order, flavour, scale, hadronType, hadronCharge )
	{}



	DssSpline( const SplineType type, const DssSet dssSet, const PQcdOrder order, const PartonFlavour flavour, const T scale, const Hadron hadronType, const int hadronCharge )
		: DssSpline( DssDefaultGrid<T>::POINTS.begin(), DssDefaultGrid<T>::POINTS.end(), type, dssSet, order, flavour, scale, hadronType, hadronCharge )
	{}



	DssSpline( const DssSet dssSet, const PQcdOrder order, const PartonFlavour flavour, const T scale, const Hadron hadronType, const int hadronCharge )
		: DssSpline( SplineType::NATURAL, dssSet, order, flavour, scale, hadronType, hadronCharge )
	{}



	void setDefaultPoints()
	{
		PdfSpline::set( DssDefaultGrid<T>::POINTS.begin(), DssDefaultGrid<T>::POINTS.end() );
	}



	void setScale( const T scale )
	{
		double newScale = static_cast<double>( scale );
		if (newScale != PdfSpline::function_.scale_)
		{
			PdfSpline::function_.scale_ = newScale;
			update();
		}
	}



	void setFlavour( const PartonFlavour flavour )
	{
		int newFlavour = convertPartonFlavour( flavour );
		if (newFlavour != PdfSpline::function_.flavour_)
		{
			PdfSpline::function_.flavour_ = newFlavour;
			PdfSpline::update();
		}
	}



	void setHadronType( const Hadron& hadron )
	{
		int newHadron = convertHadronType( hadron );
		if(newHadron != PdfSpline::function_.hadronType_)
		{
			PdfSpline::function_.hadronType_ = newHadron;
			update();
		}
	}



	void setHadronCharge( const int hadronCharge )
	{
		if(hadronCharge != PdfSpline::function_.hadronCharge_)
		{
			PdfSpline::function_.hadronCharge_ = hadronCharge;
			update();
		}
	}



	void set_version( const DssSet dssSet )
	{
		int version = convertVersion( dssSet );
		if(version != PdfSpline::function_.setVersion_)
		{
			PdfSpline::function_.setVersion_ = version;
			update();
		}
	}



	void setOrder( const PQcdOrder order )
	{
		int newOrder = convertOrder( order );
		if(newOrder != PdfSpline::function_.order_)
		{
			PdfSpline::function_.order_ = newOrder;
			update();
		}
	}



private:
	void update()
	{
		fortran::fragini_.fini = 0;
		PdfSpline::update();
	}



	int convertPartonFlavour( const PartonFlavour flavour ) const
	{
		switch (flavour)
		{
			case PartonFlavour::U:
				return 0;
			case PartonFlavour::UBAR:
				return 1;
			case PartonFlavour::D:
				return 2;
			case PartonFlavour::DBAR:
				return 3;
			case PartonFlavour::S:
				return 4;
			case PartonFlavour::SBAR:
				return 5;
			case PartonFlavour::C:
				return 6;
			case PartonFlavour::B:
				return 7;
			case PartonFlavour::G:
				return 8;
			default:
				throw std::invalid_argument( "Required PartonFlavour is not availible for DSS." );
		}
	}



	int convertHadronType( const Hadron hadron ) const
	{
		switch (hadron)
		{
			case Hadron::PION:
				return 1;
			case Hadron::KAON:
				return 2;
			case Hadron::PROTON:
				return 3;
			case Hadron::ALL:
				return 4;
			default:
				throw std::invalid_argument( "Required Hadron is not availible for DSS." );
		}
	}



	int convertVersion( const DssSet dssSet )
	{
		switch (dssSet)
		{
			case DssSet::V07:
				return 1;
			case DssSet::V14:
				return 2;
			default:
				throw std::invalid_argument( "Required Version is not availible for DSS." );
		}
	}



	int convertOrder( const PQcdOrder order )
	{
		switch (order)
		{
			case PQcdOrder::LO:
				return 0;
			case PQcdOrder::NLO:
				return 1;
			default:
				throw std::invalid_argument( "Required PQcdOrder is not availible for DSS." );
		}
	}
};



} // namespace pdfInterpolation
} // namespace qcd
} // namespace finael



#endif // FINAEL_QCD_PDF_INTERPOLATION_DSS_SPLINE_H_
