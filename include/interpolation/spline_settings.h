/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_INTERPOLATION_SPLINE_SETTINGS_H_
#define FINAEL_INTERPOLATION_SPLINE_SETTINGS_H_


namespace finael {
namespace interpolation {


enum class SplineType {NATURAL, FIXED};



} // namespace interpolation
} // namespace finael


#endif // FINAEL_INTERPOLATION_SPLINE_SETTINGS_H_
