/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUTOOL_DEVICE_INFO_H_
#define FINAEL_CUTOOL_DEVICE_INFO_H_

#include <iostream>

#include "../common/cudaHostDevice.h"


namespace finael {
namespace cuTool {



class DeviceInfo
{
public:
	static constexpr unsigned int hostMaxBlockDim = 1024;
	static constexpr unsigned int hostWarpSize = 32;



	CUDA_HOST DeviceInfo()
	#ifdef __CUDACC__
		: deviceNumber( -1 )
	#endif // __CUDACC__
	{
		update();
	}



	CUDA_HOST void update()
	{
		#ifdef __CUDACC__
			int newDeviceNumber;
			cudaGetDevice( &newDeviceNumber );
			if (newDeviceNumber != deviceNumber)
			{
				deviceNumber = newDeviceNumber;
				cudaGetDeviceProperties( &properties, deviceNumber );
			}
		#endif // __CUDACC__
	}



	CUDA_HOST unsigned int getMaxBlockDim() const
	{
		#ifdef __CUDACC__
			return properties.maxThreadsPerBlock;
		#else // __CUDACC__
			return hostMaxBlockDim;
		#endif // __CUDACC__
	}



	CUDA_HOST unsigned int getWarpSize() const
	{
		#ifdef __CUDACC__
			return properties.warpSize;
		#else // __CUDACC__
			return hostWarpSize;
		#endif // __CUDACC__
	}



	CUDA_HOST void print() const
	{
		#ifdef __CUDACC__
			std::cout << "--- DeviceInfo for \e[1m" << properties.name << "\e[0m\n"
				<< "                   Compute capability = " << properties.major << "." << properties.minor << "\n"
				<< "                             # of SMs = " << properties.multiProcessorCount << "\n"
				<< "                           Clock rate = " << properties.clockRate << " kHz\n"
				<< "                        Global memory = " << properties.totalGlobalMem << " bytes\n"
				<< "                      Constant memory = " << properties.totalConstMem << " bytes\n"
				<< "                             L2 cache = " << properties.l2CacheSize << " bytes\n"
				<< "              Shared memory per block = " << properties.sharedMemPerBlock << " bytes\n"
				<< "                 Shared memory per SM = " << properties.sharedMemPerMultiprocessor << " bytes\n"
				<< "                 Max thread per block = " << properties.maxThreadsPerBlock << "\n"
				<< "                    Max thread per SM = " << properties.maxThreadsPerMultiProcessor << "\n"
				<< "                       Regs per block = " << properties.regsPerBlock << "\n"
				<< "                          Regs per SM = " << properties.regsPerMultiprocessor << "\n"
				<< "                            Warp size = " << properties.warpSize << "\n"
			<< "-------------------" << std::endl;
		#else // __CUDACC__
			std::cout << "Host compiler, no device accessible." << std::endl;
		#endif // __CUDACC__
	}



private:
	#ifdef __CUDACC__
		int deviceNumber;
		cudaDeviceProp properties;
	#endif // __CUDACC__
};



constexpr unsigned int DeviceInfo::hostMaxBlockDim;
constexpr unsigned int DeviceInfo::hostWarpSize;



} // namespace cuTool
} // namespace finael


#endif // FINAEL_CUTOOL_DEVICE_INFO_H_
