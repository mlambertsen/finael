/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#define BOOST_TEST_MODULE "finael Anomalous Dimension"
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "../../../include/qcd/AnomalousDimension.h"
#include "../../../include/math/Complex.h"


BOOST_AUTO_TEST_SUITE( anomalousDimension )



BOOST_AUTO_TEST_CASE( loQQ )
{
	using namespace finael::cuQcd;
	using namespace finael::cuMath;

	AnomalousDimension<3, PQcdOrder::LO, double> anom;

	Complex<double> arg( 3 );
	Complex<double> expectedResult( -5.5555555553372660 );

	BOOST_CHECK_CLOSE( anom.getQQ( arg ).real(), expectedResult.real(), 1.e-14 );
	BOOST_CHECK_CLOSE( anom.getQQ( arg ).imag(), expectedResult.imag(), 1.e-14 );


	arg = Complex<double>( -3, 2 );
	expectedResult = Complex<double>( -5.2383562435210180, -12.921228584535259 );

	BOOST_CHECK_CLOSE( anom.getQQ( arg ).real(), expectedResult.real(), 1.e-14 );
	BOOST_CHECK_CLOSE( anom.getQQ( arg ).imag(), expectedResult.imag(), 1.e-14 );
}



BOOST_AUTO_TEST_CASE( loQG )
{
	using namespace finael::cuQcd;
	using namespace finael::cuMath;

	AnomalousDimension<3, PQcdOrder::LO, double> anom;

	Complex<double> arg( 3 );
	Complex<double> expectedResult( 1.3999999999999999 );

	BOOST_CHECK_CLOSE( anom.getQG( arg ).real(), expectedResult.real(), 1.e-14 );
	BOOST_CHECK_CLOSE( anom.getQG( arg ).imag(), expectedResult.imag(), 1.e-14 );


	arg = Complex<double>( -3, 2 );
	expectedResult = Complex<double>( -0.78461538461538471, -2.7230769230769232 );

	BOOST_CHECK_CLOSE( anom.getQG( arg ).real(), expectedResult.real(), 1.e-13 );
	BOOST_CHECK_CLOSE( anom.getQG( arg ).imag(), expectedResult.imag(), 1.e-14 );
}



BOOST_AUTO_TEST_CASE( loGQ )
{
	using namespace finael::cuQcd;
	using namespace finael::cuMath;

	AnomalousDimension<3, PQcdOrder::LO, double> anom;

	Complex<double> arg( 3 );
	Complex<double> expectedResult( 1.5555555555555554 );

	BOOST_CHECK_CLOSE( anom.getGQ( arg ).real(), expectedResult.real(), 1.e-14 );
	BOOST_CHECK_CLOSE( anom.getGQ( arg ).imag(), expectedResult.imag(), 1.e-14 );


	arg = Complex<double>( -3, 2 );
	expectedResult = Complex<double>( -0.50256410256410244, -0.37948717948717947 );

	BOOST_CHECK_CLOSE( anom.getGQ( arg ).real(), expectedResult.real(), 1.e-13 );
	BOOST_CHECK_CLOSE( anom.getGQ( arg ).imag(), expectedResult.imag(), 1.e-14 );
}



BOOST_AUTO_TEST_CASE( loGG )
{
	using namespace finael::cuQcd;
	using namespace finael::cuMath;

	AnomalousDimension<3, PQcdOrder::LO, double> anom;

	Complex<double> arg( 3 );
	Complex<double> expectedResult( -10.399999999508848 );

	BOOST_CHECK_CLOSE( anom.getGG( arg ).real(), expectedResult.real(), 1.e-13 );
	BOOST_CHECK_CLOSE( anom.getGG( arg ).imag(), expectedResult.imag(), 1.e-14 );


	arg = Complex<double>( -3, 2 );
	expectedResult = Complex<double>( -12.132455394076135, -27.203533545973563 );

	BOOST_CHECK_CLOSE( anom.getGG( arg ).real(), expectedResult.real(), 1.e-14 );
	BOOST_CHECK_CLOSE( anom.getGG( arg ).imag(), expectedResult.imag(), 1.e-13 );
}



BOOST_AUTO_TEST_CASE( loPS )
{
	using namespace finael::cuQcd;
	using namespace finael::cuMath;

	AnomalousDimension<3, PQcdOrder::LO, double> anom;

	Complex<double> arg( 3 );
	Complex<double> expectedResult( 0 );

	BOOST_CHECK_EQUAL( anom.getPS( arg ), expectedResult );
}



BOOST_AUTO_TEST_CASE( loNS )
{
	using namespace finael::cuQcd;
	using namespace finael::cuMath;

	AnomalousDimension<3, PQcdOrder::LO, double> anom;

	Complex<double> arg( 3 );

	BOOST_CHECK_EQUAL( anom.getNS<Sign::PLUS>( arg ), anom.getQQ( arg ) );
	BOOST_CHECK_EQUAL( anom.getNS<Sign::MINUS>( arg ), anom.getQQ( arg ) );
}



BOOST_AUTO_TEST_CASE( loNSSea )
{
	using namespace finael::cuQcd;
	using namespace finael::cuMath;

	AnomalousDimension<3, PQcdOrder::LO, double> anom;

	Complex<double> arg( 3 );
	Complex<double> expectedResult( 0 );

	BOOST_CHECK_EQUAL( anom.getNSSea( arg ), expectedResult );
}



BOOST_AUTO_TEST_CASE( nloNSPlus )
{
	using namespace finael::cuQcd;
	using namespace finael::cuMath;

	AnomalousDimension<3, PQcdOrder::NLO, double> anom;

	Complex<double> arg( 3 );
	Complex<double> expectedResult( -55.526306389372380 );

	BOOST_CHECK_CLOSE( anom.getNS<Sign::PLUS>( arg ).real(), expectedResult.real(), 1.e-13 );
	BOOST_CHECK_CLOSE( anom.getNS<Sign::PLUS>( arg ).imag(), expectedResult.imag(), 1.e-13 );


	arg = Complex<double>( -3, 2 );
	expectedResult = Complex<double>( -30.355865521568116, -78.852875425490339 );

	BOOST_CHECK_CLOSE( anom.getNS<Sign::PLUS>( arg ).real(), expectedResult.real(), 1.e-13 );
	BOOST_CHECK_CLOSE( anom.getNS<Sign::PLUS>( arg ).imag(), expectedResult.imag(), 1.e-13 );
}



BOOST_AUTO_TEST_CASE( nloNSMinus )
{
	using namespace finael::cuQcd;
	using namespace finael::cuMath;

	AnomalousDimension<3, PQcdOrder::NLO, double> anom;

	Complex<double> arg( 3 );
	Complex<double> expectedResult( -55.512217446819122 );

	BOOST_CHECK_CLOSE( anom.getNS<Sign::MINUS>( arg ).real(), expectedResult.real(), 1.e-13 );
	BOOST_CHECK_CLOSE( anom.getNS<Sign::MINUS>( arg ).imag(), expectedResult.imag(), 1.e-14 );


	arg = Complex<double>( -3, 2 );
	expectedResult = Complex<double>( -31.009737958454732, -79.280091855300327 );

	BOOST_CHECK_CLOSE( anom.getNS<Sign::MINUS>( arg ).real(), expectedResult.real(), 1.e-13 );
	BOOST_CHECK_CLOSE( anom.getNS<Sign::MINUS>( arg ).imag(), expectedResult.imag(), 1.e-13 );
}



BOOST_AUTO_TEST_CASE( nloPS )
{
	using namespace finael::cuQcd;
	using namespace finael::cuMath;

	AnomalousDimension<3, PQcdOrder::NLO, double> anom;

	Complex<double> arg( 3 );
	Complex<double> expectedResult( 1.0303703703703704 );

	BOOST_CHECK_CLOSE( anom.getPS( arg ).real(), expectedResult.real(), 1.e-14 );
	BOOST_CHECK_CLOSE( anom.getPS( arg ).imag(), expectedResult.imag(), 1.e-14 );


	arg = Complex<double>( -3, 2 );
	expectedResult = Complex<double>( -0.29984524351388253, 0.89698680018206645 );

	BOOST_CHECK_CLOSE( anom.getPS( arg ).real(), expectedResult.real(), 1.e-13 );
	BOOST_CHECK_CLOSE( anom.getPS( arg ).imag(), expectedResult.imag(), 1.e-14 );
}



BOOST_AUTO_TEST_CASE( nloQQ )
{
	using namespace finael::cuQcd;
	using namespace finael::cuMath;

	AnomalousDimension<3, PQcdOrder::NLO, double> anom;

	Complex<double> arg( 3 );
	Complex<double> expectedResult( -54.495936019002009 );

	BOOST_CHECK_CLOSE( anom.getQQ( arg ).real(), expectedResult.real(), 1.e-13 );
	BOOST_CHECK_CLOSE( anom.getQQ( arg ).imag(), expectedResult.imag(), 1.e-14 );


	arg = Complex<double>( -3, 2 );
	expectedResult = Complex<double>( -30.655710765081999, -77.955888625308276 );

	BOOST_CHECK_CLOSE( anom.getQQ( arg ).real(), expectedResult.real(), 1.e-13 );
	BOOST_CHECK_CLOSE( anom.getQQ( arg ).imag(), expectedResult.imag(), 1.e-13 );
}



BOOST_AUTO_TEST_CASE( nloQG )
{
	using namespace finael::cuQcd;
	using namespace finael::cuMath;

	AnomalousDimension<3, PQcdOrder::NLO, double> anom;

	Complex<double> arg( 3 );
	Complex<double> expectedResult( 2.2082997311706851 );

	BOOST_CHECK_CLOSE( anom.getQG( arg ).real(), expectedResult.real(), 1.e-14 );
	BOOST_CHECK_CLOSE( anom.getQG( arg ).imag(), expectedResult.imag(), 1.e-14 );


	arg = Complex<double>( -3, 2 );
	expectedResult = Complex<double>( -46.919669537754501, 0.33081227131722457 );

	BOOST_CHECK_CLOSE( anom.getQG( arg ).real(), expectedResult.real(), 1.e-13 );
	BOOST_CHECK_CLOSE( anom.getQG( arg ).imag(), expectedResult.imag(), 1.e-11 );
}



BOOST_AUTO_TEST_CASE( nloGQ )
{
	using namespace finael::cuQcd;
	using namespace finael::cuMath;

	AnomalousDimension<3, PQcdOrder::NLO, double> anom;

	Complex<double> arg( 3 );
	Complex<double> expectedResult( 20.107987355512417 );

	BOOST_CHECK_CLOSE( anom.getGQ( arg ).real(), expectedResult.real(), 1.e-13 );
	BOOST_CHECK_CLOSE( anom.getGQ( arg ).imag(), expectedResult.imag(), 1.e-14 );


	arg = Complex<double>( -3, 2 );
	expectedResult = Complex<double>( -7.1509372610979636, 3.3279466185251141 );

	BOOST_CHECK_CLOSE( anom.getGQ( arg ).real(), expectedResult.real(), 1.e-14 );
	BOOST_CHECK_CLOSE( anom.getGQ( arg ).imag(), expectedResult.imag(), 1.e-12 );
}



BOOST_AUTO_TEST_CASE( nloGG )
{
	using namespace finael::cuQcd;
	using namespace finael::cuMath;

	AnomalousDimension<3, PQcdOrder::NLO, double> anom;

	Complex<double> arg( 3 );
	Complex<double> expectedResult( -61.042808926417131 );

	BOOST_CHECK_CLOSE( anom.getGG( arg ).real(), expectedResult.real(), 1.e-14 );
	BOOST_CHECK_CLOSE( anom.getGG( arg ).imag(), expectedResult.imag(), 1.e-14 );


	arg = Complex<double>( -3, 2 );
	expectedResult = Complex<double>( -61.455989896009974, -119.76679439198783 );

	BOOST_CHECK_CLOSE( anom.getGG( arg ).real(), expectedResult.real(), 1.e-12 );
	BOOST_CHECK_CLOSE( anom.getGG( arg ).imag(), expectedResult.imag(), 1.e-13 );
}






BOOST_AUTO_TEST_SUITE_END()
