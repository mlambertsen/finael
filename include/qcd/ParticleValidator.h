/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUQCD_PARTICLE_VALIDATOR
#define FINAEL_CUQCD_PARTICLE_VALIDATOR


#include "definitions.h"



namespace finael {
namespace cuQcd {



template<PartonFlavour flavour>
class ParticleValidator
{
public:
	static constexpr bool isGluon = (flavour == PartonFlavour::G);
	static constexpr bool isQuark = (flavour == PartonFlavour::U || flavour == PartonFlavour::D || flavour == PartonFlavour::S || flavour == PartonFlavour::C || flavour == PartonFlavour::B || flavour == PartonFlavour::T);
	static constexpr bool isAntiQuark = !(isGluon || isQuark);
	static constexpr bool isWeakIsospinUp = (flavour == PartonFlavour::U || flavour == PartonFlavour::C || flavour == PartonFlavour::T || flavour == PartonFlavour::DBAR || flavour == PartonFlavour::SBAR || flavour == PartonFlavour::BBAR);
	static constexpr bool isWeakIsospinDown = !(isGluon || isWeakIsospinUp);
};



template<PartonFlavour flavour>
constexpr bool ParticleValidator<flavour>::isGluon;

template<PartonFlavour flavour>
constexpr bool ParticleValidator<flavour>::isQuark;

template<PartonFlavour flavour>
constexpr bool ParticleValidator<flavour>::isAntiQuark;

template<PartonFlavour flavour>
constexpr bool ParticleValidator<flavour>::isWeakIsospinUp;

template<PartonFlavour flavour>
constexpr bool ParticleValidator<flavour>::isWeakIsospinDown;



} // namespace cuQcd
} // namespace finael



#endif // FINAEL_CUQCD_PARTICLE_VALIDATOR
