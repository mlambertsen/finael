/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUQCD_PARTON_FLAVOUR_CONVERTER_H_
#define FINAEL_CUQCD_PARTON_FLAVOUR_CONVERTER_H_

#include "definitions.h"



namespace finael {
namespace cuQcd {



template<PartonFlavour flavour>
class PartonFlavourConverter
{
public:
	static_assert(flavour == PartonFlavour::G || flavour == PartonFlavour::U || flavour == PartonFlavour::UBAR || flavour == PartonFlavour::D || flavour == PartonFlavour::DBAR || flavour == PartonFlavour::S || flavour == PartonFlavour::SBAR || flavour == PartonFlavour::C || flavour == PartonFlavour::CBAR || flavour == PartonFlavour::B || flavour == PartonFlavour::BBAR || flavour == PartonFlavour::T || flavour == PartonFlavour::TBAR, "ERROR: Given PartonFlavour not implemented in PartonFlavourConverter!");

	static constexpr short INDEX = (flavour == PartonFlavour::G) ? 0
								: (flavour == PartonFlavour::U) ? 1
								: (flavour == PartonFlavour::UBAR) ? -1
								: (flavour == PartonFlavour::D) ? 2
								: (flavour == PartonFlavour::DBAR) ? -2
								: (flavour == PartonFlavour::S) ? 3
								: (flavour == PartonFlavour::SBAR) ? -3
								: (flavour == PartonFlavour::C) ? 4
								: (flavour == PartonFlavour::CBAR) ? -4
								: (flavour == PartonFlavour::B) ? 5
								: (flavour == PartonFlavour::BBAR) ? -5
								: (flavour == PartonFlavour::T) ? 6
								: (flavour == PartonFlavour::TBAR) ? -6 : 42;
};





template<PartonFlavour flavour>
constexpr short PartonFlavourConverter<flavour>::INDEX;



} // namespace cuQcd
} // namespace finael



#endif // FINAEL_CUQCD_PARTON_FLAVOUR_CONVERTER_H_
