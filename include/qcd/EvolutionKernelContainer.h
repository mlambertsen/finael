/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUQCD_EVOLUTIONKERNEL_CONTAINER_H_
#define FINAEL_CUQCD_EVOLUTIONKERNEL_CONTAINER_H_

#include "../common/cudaHostDevice.h"
#include "../math/Complex.h"
#include "../math/Matrix.h"



namespace finael {
namespace cuQcd {



template<class Complex>
class EvolutionKernelContainer
{
public:
	cuMath::Matrix<2, 2, Complex> singletKernel;
	Complex nonSingletValenceKernel;
	Complex nonSingletNonValencePlusKernel;
	Complex nonSingletNonValenceMinusKernel;



	CUDA_HOST_DEVICE EvolutionKernelContainer( const cuMath::Matrix<2,2,Complex>& singlet, const Complex& nonSingletValence, const Complex& nonSingletNonValencePlus, const Complex& nonSingletNonValenceMinus )
		: singletKernel( singlet )
		, nonSingletValenceKernel( nonSingletValence )
		, nonSingletNonValencePlusKernel( nonSingletNonValencePlus )
		, nonSingletNonValenceMinusKernel( nonSingletNonValenceMinus )
	{}
};



} // namespace cuQcd
} // namespace finael




#endif // FINAEL_CUQCD_EVOLUTIONKERNEL_CONTAINER_H_
