/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_INTERPOLATION_MELLIN_SPLINE_H_
#define FINAEL_INTERPOLATION_MELLIN_SPLINE_H_

#include "Spline.h"



namespace finael {
namespace interpolation {



template<typename T>
class Spline;



template<typename T>
class MellinSpline
{
public:
	static constexpr std::size_t coefficientsPerPoint = 4;



	template<class PointIt, class Functor>
	MellinSpline( PointIt firstPoint, PointIt lastPoint, const Functor& function )
		: MellinSpline( firstPoint, lastPoint, function, SplineType::NATURAL )
	{}



	template<class PointIt, class Functor>
	MellinSpline( PointIt firstPoint, PointIt lastPoint, const Functor& function, const SplineType type )
		: MellinSpline( Spline<T>( firstPoint, lastPoint, function, type ) )
	{}



	explicit MellinSpline( const Spline<T>& spline )
	{
		set( spline );
	}



	template<class PointIt, class Functor>
	void set( PointIt firstPoint, PointIt lastPoint, const Functor& function )
	{
		set( Spline<T>( firstPoint, lastPoint, function ) );
	}



	template<class PointIt, class Functor>
	void set( PointIt firstPoint, PointIt lastPoint, const Functor& function, const SplineType type )
	{
		set( Spline<T>( firstPoint, lastPoint, function, type ) );
	}



	template<class Functor>
	void set( const Functor& function, const SplineType type )
	{
		set( Spline<T>( points_.begin(), points_.end(), function, type ) );
	}



	template<class Functor>
	void set( const Functor& function )
	{
		set( Spline<T>( points_.begin(), points_.end(), function, type_ ) );
	}



	void set( const Spline<T>& spline )
	{
		type_ = spline.getType();
		extractPoints( spline );
		calculateCoefficients( spline );
	}



	SplineType getType() const
	{
		return type_;
	}



	std::size_t numberPoints() const
	{
		return points_.size();
	}



	T getMinimalPoint() const
	{
		return getPointAt( 0 );
	}



	T getMaximalPoint() const
	{
		return getPointAt( numberPoints() - 1 );
	}



	T getPointAt( const std::size_t index ) const
	{
		return points_.at( index );
	}



	std::vector<T> getCoefficientsAt( const std::size_t index ) const
	{
		if (index >= numberPoints()) throw SplineError( "Index out of range." );
		auto offset = index * coefficientsPerPoint;
		auto begin = coefficients_.begin() + offset;
		auto end = begin + coefficientsPerPoint;
		return std::vector<T>( begin, end );
	}



	template<template<typename> class Complex>
	Complex<T> operator()( const Complex<T>& mellinVariable ) const
	{
		Complex<T> result;

		for (auto pointIndex = 0u; pointIndex < numberPoints(); ++pointIndex)
		{
			for (auto degree = 0u; degree < coefficientsPerPoint; ++degree)
			{
				result += pow( points_.at( pointIndex ), mellinVariable ) * coefficients_.at( coefficientsPerPoint*pointIndex + degree ) / (mellinVariable + degree);
			}
		}

		return result;
	}



private:
	SplineType type_;
	std::vector<T> points_;
	std::vector<T> coefficients_;



	void extractPoints( const Spline<T>& spline )
	{
		points_.resize( spline.numberPoints() );
		for (auto knotIndex = 0u; knotIndex < spline.numberPoints(); ++knotIndex)
		{
			points_.at( knotIndex ) = spline.getPointAt( knotIndex );
		}
	}



	void calculateCoefficients( const Spline<T>& spline )
	{
		coefficients_.resize( coefficientsPerPoint*spline.numberPoints() );

		for (auto pointIndex = 0u; pointIndex < numberPoints(); ++pointIndex)
		{
			auto point = points_.at( pointIndex );

			auto minuend = (pointIndex == 0) ? SplinePiece<T>() : spline.getPiece( pointIndex - 1 );
			auto subtrahend = (pointIndex == numberPoints() - 1) ? SplinePiece<T>() : spline.getPiece( pointIndex );
			auto pieceDifference = minuend - subtrahend;

			coefficients_.at( coefficientsPerPoint*pointIndex ) = pieceDifference.constant;
			coefficients_.at( coefficientsPerPoint*pointIndex + 1 ) = pieceDifference.linear * point;
			coefficients_.at( coefficientsPerPoint*pointIndex + 2 ) = pieceDifference.square * point * point;
			coefficients_.at( coefficientsPerPoint*pointIndex + 3 ) = pieceDifference.cubic * point * point * point;
		}
	}
};



template<typename T>
constexpr std::size_t MellinSpline<T>::coefficientsPerPoint;



} // interpolation
} // finael



#endif // FINAEL_INTERPOLATION_MELLIN_SPLINE_H_
