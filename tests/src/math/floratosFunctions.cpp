/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#define BOOST_TEST_MODULE "finael Floratos Functions"
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "../../../include/math/floratosFunctions.h"
#include "../../../include/math/Complex.h"



BOOST_AUTO_TEST_SUITE( floratosFunc )



BOOST_AUTO_TEST_CASE( sPrimeFloratorTwoPlus )
{
	using namespace finael::cuMath;

	Complex<double> arg( 1.5 );
	Complex<double> expectedResult( 1.1545763107484217 );

	auto result = sPrimeFloratos<2,Sign::PLUS>( arg );
	BOOST_CHECK_CLOSE( result.real(), expectedResult.real(), 1.e-14 );
	BOOST_CHECK_EQUAL( result.imag(), expectedResult.imag() );


	arg = Complex<double>(-1.5, 1.0);
	expectedResult = Complex<double>( 2.0882095149074198, 0.47706865782924718 );

	result = sPrimeFloratos<2,Sign::PLUS>( arg );
	BOOST_CHECK_CLOSE( result.real(), expectedResult.real(), 1.e-14 );
	BOOST_CHECK_CLOSE( result.imag(), expectedResult.imag(), 1.e-14 );
}



BOOST_AUTO_TEST_CASE( sPrimeFloratorTwoMinus )
{
	using namespace finael::cuMath;

	Complex<double> arg( 1.5 );
	Complex<double> expectedResult( 1.0000000000007332 );

	auto result = sPrimeFloratos<2,Sign::MINUS>( arg );
	BOOST_CHECK_CLOSE( result.real(), expectedResult.real(), 1.e-14 );
	BOOST_CHECK_EQUAL( result.imag(), expectedResult.imag() );

	arg = Complex<double>(-1.5, 1.0);
	expectedResult = Complex<double>( 2.1819339702257841, 0.29423354275870267 );

	result = sPrimeFloratos<2,Sign::MINUS>( arg );
	BOOST_CHECK_CLOSE( result.real(), expectedResult.real(), 1.e-14 );
	BOOST_CHECK_CLOSE( result.imag(), expectedResult.imag(), 1.e-14 );
}



BOOST_AUTO_TEST_CASE( sPrimeFloratorThreePlus )
{
	using namespace finael::cuMath;

	Complex<double> arg( 1.5 );
	Complex<double> expectedResult( 1.0839548773389547 );

	auto result = sPrimeFloratos<3,Sign::PLUS>( arg );
	BOOST_CHECK_CLOSE( result.real(), expectedResult.real(), 1.e-14 );
	BOOST_CHECK_EQUAL( result.imag(), expectedResult.imag() );

	arg = Complex<double>(-1.5, 1.0);
	expectedResult = Complex<double>( 1.1738778535803147, -1.3056521337937511e-2 );

	result = sPrimeFloratos<3,Sign::PLUS>( arg );
	BOOST_CHECK_CLOSE( result.real(), expectedResult.real(), 1.e-14 );
	BOOST_CHECK_CLOSE( result.imag(), expectedResult.imag(), 1.e-14 );
}



BOOST_AUTO_TEST_CASE( sPrimeFloratorThreeMinus )
{
	using namespace finael::cuMath;

	Complex<double> arg( 1.5 );
	Complex<double> expectedResult( 1.0000000000004010 );

	auto result = sPrimeFloratos<3,Sign::MINUS>( arg );
	BOOST_CHECK_CLOSE( result.real(), expectedResult.real(), 1.e-14 );
	BOOST_CHECK_EQUAL( result.imag(), expectedResult.imag() );

	arg = Complex<double>(-1.5, 1.0);
	expectedResult = Complex<double>( 1.1363333689537027, -0.36667357482781782 );

	result = sPrimeFloratos<3,Sign::MINUS>( arg );
	BOOST_CHECK_CLOSE( result.real(), expectedResult.real(), 1.e-14 );
	BOOST_CHECK_CLOSE( result.imag(), expectedResult.imag(), 1.e-13 );
}



BOOST_AUTO_TEST_CASE( sTildeFloratorPlus )
{
	using namespace finael::cuMath;

	Complex<double> arg( 3 );
	Complex<double> expectedResult( -0.67417480941206043 );

	auto result = sTildeFloratos<Sign::PLUS>( arg );
	BOOST_CHECK_CLOSE( result.real(), expectedResult.real(), 1.e-13 );
	BOOST_CHECK_EQUAL( result.imag(), expectedResult.imag() );


	arg = Complex<double>( -3, 2 );
	expectedResult = Complex<double>( -0.85886029890996773, 7.6202355028004709e-2 );

	result = sTildeFloratos<Sign::PLUS>( arg );
	BOOST_CHECK_CLOSE( result.real(), expectedResult.real(), 1.e-13 );
	BOOST_CHECK_CLOSE( result.imag(), expectedResult.imag(), 1.e-12 );
}



BOOST_AUTO_TEST_CASE( sTildeFloratorMinus )
{
	using namespace finael::cuMath;

	Complex<double> arg( 3 );
	Complex<double> expectedResult( -0.82839631953743231 );

	auto result = sTildeFloratos<Sign::MINUS>( arg );
	BOOST_CHECK_CLOSE( result.real(), expectedResult.real(), 1.e-13 );
	BOOST_CHECK_EQUAL( result.imag(), expectedResult.imag() );

	arg = Complex<double>( -3, 2 );
	expectedResult = Complex<double>( -0.64371083003952501, -7.6202355028004709e-2 );

	result = sTildeFloratos<Sign::MINUS>( arg );
	BOOST_CHECK_CLOSE( result.real(), expectedResult.real(), 1.e-12 );
	BOOST_CHECK_CLOSE( result.imag(), expectedResult.imag(), 1.e-12 );
}





BOOST_AUTO_TEST_SUITE_END()
