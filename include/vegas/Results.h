/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINEAL_CUVEGAS_RESULTS_H_
#define FINEAL_CUVEGAS_RESULTS_H_

#include "../common/cudaHostDevice.h"

#include "sums/AccumulatedSums.h"


namespace finael {
namespace cuVegas {



template<typename T>
struct Results
{
public:
	T integral;
	T standardDeviation;
	T chiSquaredPerIteration;



	CUDA_HOST Results( T integralIn, T standardDeviationIn, T chiSquaredPerIterationIn )
		: integral( integralIn )
		, standardDeviation( standardDeviationIn )
		, chiSquaredPerIteration( chiSquaredPerIterationIn )
	{}
};



} // namespace cuVegas
} // namespace finael

#endif // FINEAL_CUVEGAS_RESULTS_H_
