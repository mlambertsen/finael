/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUMATH_POLYGAMMA_H_
#define FINAEL_CUMATH_POLYGAMMA_H_

#include "../common/cudaHostDevice.h"
#include "PolyGammaDetailsProvider.h"
#include "Complex.h"


namespace finael {
namespace cuMath {



//------------------------------------------
// Polygamma functions (derivatives of logGamma)
// Use equations 6.4.6, 6.4.7 and 6.3.18/6.4.12/6.4.13/6.4.14 in Abramowitz, Stegun (Handbook of Mathematical Functions)
//------------------------------------------
template<unsigned int derivative>
class PolyGamma
{
public:
	static constexpr int approximationLimit = 10;



	template<template<typename> class Complex, typename T>
	CUDA_HOST_DEVICE Complex<T> operator()( const Complex<T>& argument ) const
	{
		if (argument.real() < (1 - approximationLimit) && isReflectable( argument )) return details.reflect( argument );
		if (argument.real() >= approximationLimit) return details.approximateForLargeRealPart( argument );

		return calculateViaRecurrence( argument );
	}



	template<typename T>
	CUDA_HOST_DEVICE T operator()( const T& argument ) const
	{
		return operator()( Complex<T>( argument, 0 ) ).real();
	}



private:
	static constexpr PolyGammaDetailsProvider<derivative> details{};



	template<template<typename> class Complex, typename T>
	CUDA_HOST_DEVICE bool isReflectable( const Complex<T>& argument ) const
	{
		return ::abs(argument.imag()) < details.template selectImaginaryLimit<T>();
	}



	template<template<typename> class Complex, typename T>
	CUDA_HOST_DEVICE Complex<T> calculateViaRecurrence( const Complex<T>& argument ) const
	{
		auto recurrenceSteps = approximationLimit - static_cast<unsigned int>( floor( argument.real() ) );
		return details.approximateForLargeRealPart( argument + recurrenceSteps ) + accumulateRecurrenceSum( argument, recurrenceSteps );
	}



	template<template<typename> class Complex, typename T>
	CUDA_HOST_DEVICE Complex<T> accumulateRecurrenceSum( const Complex<T>& argument, unsigned int recurrenceSteps ) const
	{
		Complex<T> recurrenceSum;
		for (auto step = 0u; step < recurrenceSteps; ++step)
		{
			recurrenceSum += details.calculateRecurrenceContribution( argument + step );
		}
		return recurrenceSum;
	}
};



template<unsigned int derivative>
constexpr int PolyGamma<derivative>::approximationLimit;

template<unsigned int derivative>
constexpr PolyGammaDetailsProvider<derivative> PolyGamma<derivative>::details;



} // namespace cuMath
} // namespace finael



#endif // FINAEL_CUMATH_POLYGAMMA_H_
