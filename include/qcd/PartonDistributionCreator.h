/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUQCD_PARTON_DISTRIBUTION_CREATOR_H_
#define FINAEL_CUQCD_PARTON_DISTRIBUTION_CREATOR_H_

#include "../common/cudaHostDevice.h"
#include "../math/definitions.h"
#include "../math/generalFunctions.h"
#include "../math/Vector.h"
#include "../tools/Array.h"



namespace finael {
namespace cuQcd {



// assume following order: [Gluon, Up, UpBar, Down, DownBar, Strange, StrangeBar, Charm, CharmBar, Bottom, BottomBar, Top, TopBar]
template<unsigned short numFlavours, typename T>
class PartonDistributionCreator
{
public:
	static constexpr unsigned short NUMBERFLAVOURS = numFlavours;
	using value_type = T;
	using Singlet = cuMath::Vector<2, value_type>;
	using NonSingletNonValence = cuMath::Vector<numFlavours - 1, value_type>;



	CUDA_HOST_DEVICE static Singlet createSinglet( const cuTool::Array<value_type, 2*numFlavours + 1>& inputData )
	{
		Singlet singlet;
		singlet[1] = inputData[0];
		for (unsigned short flavourIndex = 0; flavourIndex < numFlavours; ++flavourIndex)
		{
			singlet[0] += inputData[2 * flavourIndex + 1] + inputData[2 * flavourIndex + 2];
		}
		return singlet;
	}



	CUDA_HOST_DEVICE static value_type createNonSingletValence( const cuTool::Array<value_type, 2*numFlavours + 1>& inputData )
	{
		value_type nonSingletValence = static_cast<value_type>( 0 );

		for (unsigned short flavourIndex = 0; flavourIndex < numFlavours; ++flavourIndex)
		{
			nonSingletValence += inputData[2*flavourIndex + 1] - inputData[2*flavourIndex + 2];
		}

		return nonSingletValence;
	}



	template<cuMath::Sign sign>
	CUDA_HOST_DEVICE static NonSingletNonValence createNonSingletNonValence( const cuTool::Array<value_type, 2*numFlavours + 1>& inputData )
	{
		NonSingletNonValence nonSingletNonValence;

		for (unsigned short index = 1; index < numFlavours; ++index)
		{
			if (sign == cuMath::Sign::PLUS)	nonSingletNonValence[index - 1] = -static_cast<value_type>( index ) * (inputData[2*index + 1] + inputData[2*index + 2]);
			else nonSingletNonValence[index - 1] = -static_cast<value_type>( index ) * (inputData[2*index + 1] - inputData[2*index + 2]);

			for (unsigned short sumindex = 0; sumindex < index; ++sumindex)
			{
				if (sign == cuMath::Sign::PLUS) nonSingletNonValence[index - 1] += inputData[2*sumindex + 1] + inputData[2*sumindex + 2];
				else nonSingletNonValence[index - 1] += inputData[2*sumindex + 1] - inputData[2*sumindex + 2];
			}
		}

		return nonSingletNonValence;
	}
};


template<unsigned short numFlavours, typename T>
constexpr unsigned short PartonDistributionCreator<numFlavours, T>::NUMBERFLAVOURS;



} // namespace cuQcd
} // namespace finael


#endif // FINAEL_CUQCD_PARTON_DISTRIBUTION_CREATOR_H_
