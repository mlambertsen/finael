/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_INTERNAL_SETTINGS_H_
#define FINAEL_CUVEGAS_INTERNAL_SETTINGS_H_

#include <cmath>

#include "../common/cudaHostDevice.h"
#include "../tools/DeviceInfo.h"

#include "integrandTypes.h"
#include "optionFlags.h"
#include "Parameters.h"
#include "Settings.h"


namespace finael {
namespace cuVegas {



enum class InternalStrategy {IMPORTANCE, MIXED, STRATIFIED};



template<unsigned int dim>
struct InternalSettings
{
public:
	bool isInitialized = false;
	unsigned int incrementsPerDim;
	unsigned int binsPerDim;
	unsigned int binCapacity;
	InternalStrategy strategy;
	ReductionFlag reduction;
	unsigned int blockDim;

	unsigned int numberBins;

	unsigned int numberRequestedSamples;
	StrategyFlag requestedStrategy;



	template<typename T>
	CUDA_HOST void update( const Settings<T>& settings )
	{
		isInitialized = true;
		determineBlockDim( settings );
		if (isUpToDate( settings )) return;
		determineInternalSettings( settings );
	}



	CUDA_HOST unsigned int numberSamples() const
	{
		return numberBins*binCapacity;
	}



	CUDA_HOST unsigned int maxBlockDim() const
	{
		return deviceInfo.getMaxBlockDim();
	}



	CUDA_HOST unsigned int warpSize() const
	{
		return deviceInfo.getWarpSize();
	}



	template<typename T>
	CUDA_HOST T calculateJacobian( const VolumeType<T, dim>& limits ) const
	{
		T jacobian = std::pow( static_cast<double>( incrementsPerDim ), dim );
		for (auto dimIdx = 0u; dimIdx < dim; ++dimIdx)
		{
			jacobian *= (limits[dimIdx].second - limits[dimIdx].first);
		}
		return jacobian;
	}



private:
	cuTool::DeviceInfo deviceInfo;



	template<typename T>
	CUDA_HOST void determineBlockDim( const Settings<T>& settings )
	{
		deviceInfo.update();
		blockDim = std::max( deviceInfo.getWarpSize(), std::min( settings.blockDim, deviceInfo.getMaxBlockDim() ) );
	}



	template<typename T>
	CUDA_HOST bool isUpToDate( const Settings<T>& settings ) const
	{
		if (numberRequestedSamples == settings.numberSamples && requestedStrategy == settings.strategy) return true;
		else return false;
	}



	template<typename T>
	CUDA_HOST void determineInternalSettings( const Settings<T>& settings )
	{
		numberRequestedSamples = settings.numberSamples;
		requestedStrategy = settings.strategy;

		determineStrategyDependentSettings();
		determineAddictedSettings( settings );
	}



	template<typename T>
	CUDA_HOST void determineAddictedSettings( const Settings<T>& settings )
	{
		numberBins = std::pow( binsPerDim, dim );
		binCapacity = std::max( 2u, numberRequestedSamples / numberBins );
		setReductionMode( settings );
	}



	template<typename T>
	CUDA_HOST void setReductionMode( const Settings<T>& settings )
	{
		if (strategy == InternalStrategy::STRATIFIED && settings.reduction == ReductionFlag::REFINED) reduction = ReductionFlag::REFINED;
		else reduction = ReductionFlag::CLASSIC;
	}



	CUDA_HOST void determineStrategyDependentSettings()
	{
		if (requestedStrategy == StrategyFlag::IMPORTANCE)
		{
			strategy = InternalStrategy::IMPORTANCE;
			setUnalignedBinsAndIncrements( 1u );
		}
		else if (requestedStrategy == StrategyFlag::FREE)
		{
			constexpr auto roundingCorrection = 1.e-10;
			auto binsPerDimProposal = static_cast<unsigned int>( std::pow( std::max( 1.0, numberRequestedSamples / 2.0 ), 1.0/dim ) + roundingCorrection );
			strategy = stratifiedIsPossible( binsPerDimProposal ) ? InternalStrategy::STRATIFIED : InternalStrategy::MIXED;

			if (strategy == InternalStrategy::STRATIFIED) alignBinsAndIncrements( binsPerDimProposal );
			else setUnalignedBinsAndIncrements( binsPerDimProposal );
		}
	}



	CUDA_HOST void setUnalignedBinsAndIncrements( unsigned int binsPerDimProposal )
	{
		binsPerDim = binsPerDimProposal;
		incrementsPerDim = Parameters::MAXIMUMNUMBERINCREMENTS;
	}



	CUDA_HOST void alignBinsAndIncrements( unsigned int binsPerDimProposal )
	{
		auto binsPerIncrementPerDim = std::max( binsPerDimProposal / Parameters::MAXIMUMNUMBERINCREMENTS, 1u );
		incrementsPerDim = std::min( binsPerDimProposal / binsPerIncrementPerDim, Parameters::MAXIMUMNUMBERINCREMENTS );
		binsPerDim = binsPerIncrementPerDim * incrementsPerDim;
	}



	CUDA_HOST bool stratifiedIsPossible( unsigned int binsPerDimProposal ) const
	{
		return (2*binsPerDimProposal >= Parameters::MAXIMUMNUMBERINCREMENTS);
	}
};



} // cuVegas
} // finael



#endif // FINAEL_CUVEGAS_INTERNAL_SETTINGS_H_
