/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_BLOCK_REDUCER_H_
#define FINAEL_CUVEGAS_BLOCK_REDUCER_H_

#include "../../common/cudaHostDevice.h"
#include "../../tools/DeviceInfo.h"
#include "../../tools/SmartArray.h"

#include "../sums/EstimateSum.h"


namespace finael {
namespace cuVegas {


template<typename T, CudaHostDeviceType hostDeviceType>
class BlockReducer;





#ifdef __CUDACC__
template<typename T>
class BlockReducer<T, CudaHostDeviceType::DEVICE>
{


#if __CUDA_ARCH__ >= 300


public:
	CUDA_DEVICE inline T operator()( T value )
	{
		static __shared__ T sharedData[warpSize];
		auto laneIdx = threadIdx.x % warpSize;
		auto warpIdx = threadIdx.x / warpSize;

		if (warpIdx == 0) sharedData[laneIdx] = T();
		__syncthreads();

		value = reduceWarp( value );

		if (laneIdx == 0) sharedData[warpIdx] = value;
		__syncthreads();

		value = (threadIdx.x < blockDim.x / warpSize) ? sharedData[laneIdx] : T();
		if (warpIdx == 0) value = reduceWarp( value );

		return value;
	}



private:
	static constexpr unsigned int warpSize = cuTool::DeviceInfo::hostWarpSize;


	
	CUDA_DEVICE inline T reduceWarp( T value )
	{
		for (auto offset = warpSize/2u; offset > 0u; offset /= 2u)
		{
			value += __shfl_down( value, offset );
		}
		return value;
	}


# else // __CUDA_ARCH__ >= 300


public:
	CUDA_DEVICE inline T operator()( T value )
	{
		extern __shared__ T sharedData[]; // has blockDim elements

		auto tid = threadIdx.x;
		sharedData[tid] = value;
		__syncthreads();

		for (auto activeThreads = blockDim.x/2u; activeThreads > 0u; activeThreads /= 2u)
		{
			if (tid < activeThreads) sharedData[tid] += sharedData[tid + activeThreads];
			__syncthreads();
		}

		return sharedData[tid];
	}


#endif // __CUDA_ARCH__ >= 300
};
#endif // __CUDACC__





template<typename T>
class BlockReducer<T, CudaHostDeviceType::HOST>
{
public:
	CUDA_HOST inline T operator()( cuTool::SmartArray<T, CudaHostDeviceType::HOST>& value, unsigned int blockDim )
	{
		cuTool::SmartArray<T, CudaHostDeviceType::HOST> sharedData( warpSize );
		for (auto i = 0u; i < warpSize; ++i)
		{
			sharedData[i] = T();
		}

		const auto warpsPerBlock = blockDim / warpSize;
		for (auto warpIdx = 0u; warpIdx < warpsPerBlock; ++warpIdx)
		{
			sharedData[warpIdx] = reduceWarp( value, warpIdx );
			// std::cout << sharedData[warpIdx].estimate << std::endl;
		}

		return reduceWarp( sharedData, 0u );
	}



private:
	static constexpr unsigned int warpSize = cuTool::DeviceInfo::hostWarpSize;



	CUDA_HOST inline T reduceWarp( cuTool::SmartArray<T, CudaHostDeviceType::HOST>& value, unsigned int warpIdx )
	{
		for (auto numberActiveThreads = warpSize/2u; numberActiveThreads >= 1; numberActiveThreads /= 2u)
		{
			for (auto threadIdx = 0u; threadIdx < numberActiveThreads; ++threadIdx)
			{
				auto targetIdx = warpIdx * warpSize + threadIdx;
				value[targetIdx] += value[targetIdx + numberActiveThreads];
			}
		}

		return value[warpIdx * warpSize];
	}
};



} // namespace finael
} // namespace cuVegas




#endif // FINAEL_CUVEGAS_BLOCK_REDUCER_H_
