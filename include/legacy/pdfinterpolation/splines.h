#ifndef PDF_INTERPOLATION_SPLINES_H_
#define PDF_INTERPOLATION_SPLINES_H_

#include <string>
#include <vector>
#include <cstring>


namespace pdfinterpolation {
namespace fortran {


extern"C"
{
	void fort_get_spline_mell_(int *n, int *iparton, int *st, int *iset, int *mmhtih, int *dssih, int *dssic, int *dssio, double *q2, char *file, int *length_file, bool *larray, double *xarray, double *xdat, double *paramellin);
	void fort_get_spline_x_(int *iparton, double *x, double *pdf);

	void fort_get_ct12pdf_(int *iparton, double *x, double *q2, double *pdf);
	void fort_get_mmht14pdf_(int *iparton, int *iset, char *file, int *length_file, double *x, double *q2, double *pdf);
	void fort_getdss07ff_(int *iparton, int *ih, int *ic, int *io, double *x, double *q2, double *ff);
}



} // namespace fortran



class lvpdff_spline
{
	public:
		void set_parton(int iparton)
		{
			m_iparton = iparton;
			mellin_moments();
		}

		void set_q2(double q2)
		{
			m_q2 = q2;
			mellin_moments();
		}

		void set_grid(std::vector<double> grid)
		{
			m_number_nodes = grid.size();
			m_xarray = grid;
			m_xdat.resize(m_number_nodes);
			m_paramellin.resize(4*m_number_nodes);
			m_array_given = true;
			mellin_moments();
		}

		void set_spline_type(int st)
		{
			m_st = st;
			mellin_moments();
		}

		std::vector<double> get_x_grid() const
		{
			std::vector<double> result;
			for (int i = 0; i < m_number_nodes; ++i)
			{
				result.push_back(m_xdat[i]);
			}
			return result;
		}

		std::vector<double> get_mellin_para() const
		{
			std::vector<double> result;
			for (int i = 0; i < 4*m_number_nodes; ++i)
			{
				result.push_back(m_paramellin[i]);
			}
			return result;
		}

		// std::vector<float> get_x_grid() const
		// {
		// 	std::vector<float> result;
		// 	for (int i = 0; i < m_number_nodes; ++i)
		// 	{
		// 		result.push_back((float) m_xdat[i]);
		// 	}
		// 	return result;
		// }

		// std::vector<float> get_mellin_para() const
		// {
		// 	std::vector<float> result;
		// 	for (int i = 0; i < 4*m_number_nodes; ++i)
		// 	{
		// 		result.push_back((float) m_paramellin[i]);
		// 	}
		// 	return result;
		// }

		int get_size() const
		{
			return m_number_nodes;
		}

		double xpdf(double x)
		{
			mellin_moments();
			double result;
			fortran::fort_get_spline_x_(&m_iparton, &x, &result);
			return result;
		}

	protected:
		lvpdff_spline(int iparton, double q2)
		{
			m_q2 = q2;
			m_iparton = iparton;
			m_array_given = false;
			// m_xdat and m_paramellin are constructed in the subclasses
		}

		lvpdff_spline(int iparton, double q2, std::vector<double> xarray)
		{
			m_number_nodes = xarray.size();
			m_xarray = xarray;
			m_xdat.resize(m_number_nodes);
			m_paramellin.resize(4*m_number_nodes);
			m_q2 = q2;
			m_iparton = iparton;
			m_array_given = true;
		}

		virtual void mellin_moments(){}


		// variables
		double m_q2;
		int m_iparton;
		int m_number_nodes;
		int m_st = 0;
		int m_null = 0;

		bool m_array_given = false;

		std::vector<double> m_xarray;
		std::vector<double> m_xdat;
		std::vector<double> m_paramellin;
};


class cteq_spline : public lvpdff_spline
{
	public:
		cteq_spline(int iparton, double q2, std::string filename) : lvpdff_spline(iparton, q2)
		{
			m_filename = filename;
			m_number_nodes = 150;
			m_xdat.resize(m_number_nodes);
			m_paramellin.resize(4*m_number_nodes);
			mellin_moments();
		}

		cteq_spline(int iparton, double q2, std::vector<double> xarray, std::string filename) : lvpdff_spline(iparton, q2, xarray)
		{
			m_filename = filename;
			mellin_moments();
		}

		void set_file(std::string filename)
		{
			m_filename = filename;
			mellin_moments();
		}

		double cteq_pdf(double x)
		{
			double result;
			fortran::fort_get_ct12pdf_(&m_iparton, &x, &m_q2, &result);
			return result;
		}

	private:
		int m_iset = 1;
		std::string m_filename;


		void mellin_moments(void)
		{
			int temp_filename_size = m_filename.size();
			char *temp_filename = new char[temp_filename_size + 1];
			std::copy(m_filename.begin(), m_filename.end(), temp_filename);
			temp_filename[temp_filename_size] = '\0';

			double *temp_xarray = new double[m_number_nodes];
			std::copy(m_xarray.begin(), m_xarray.end(), temp_xarray);
			double *temp_xdat = new double[m_number_nodes];
			double *temp_paramellin = new double[4*m_number_nodes];

			fortran::fort_get_spline_mell_(&m_number_nodes, &m_iparton, &m_st, &m_iset, &m_null, &m_null, &m_null, &m_null, &m_q2, temp_filename, &temp_filename_size, &m_array_given, temp_xarray, temp_xdat, temp_paramellin);

			m_xdat.assign(temp_xdat, temp_xdat+m_number_nodes);
			m_paramellin.assign(temp_paramellin, temp_paramellin+4*m_number_nodes);
			delete[] temp_xdat;
			delete[] temp_paramellin;
			delete[] temp_filename;
			delete[] temp_xarray;
		}
};

class mmht_spline : public lvpdff_spline
{
	public:
		mmht_spline(int iparton, double q2, std::string filename, int ih) : lvpdff_spline(iparton, q2)
		{
			m_filename = filename;
			m_ih = ih;
			m_number_nodes = 64;
			m_xdat.resize(m_number_nodes);
			m_paramellin.resize(4*m_number_nodes);
			mellin_moments();
		}

		mmht_spline(int iparton, double q2, std::vector<double> xarray, std::string filename, int ih) : lvpdff_spline(iparton, q2, xarray)
		{
			m_filename = filename;
			m_ih = ih;
			mellin_moments();
		}

		void set_file(std::string filename)
		{
			m_filename = filename;
			mellin_moments();
		}

		void set_ih(int ih)
		{
			m_ih = ih;
			mellin_moments();
		}

		double mmht_pdf(double x)
		{
			int temp_filename_size = m_filename.size();
			char *temp_filename = new char[temp_filename_size + 1];
			std::copy(m_filename.begin(), m_filename.end(), temp_filename);
			temp_filename[temp_filename_size] = '\0';

			double result;
			fortran::fort_get_mmht14pdf_(&m_iparton, &m_iset, temp_filename, &temp_filename_size, &x, &m_q2, &result);
			return result;
		}

	private:
		int m_iset = 2;
		std::string m_filename;
		int m_ih;

		void mellin_moments(void)
		{
			int temp_filename_size = m_filename.size();
			char *temp_filename = new char[temp_filename_size + 1];
			std::copy(m_filename.begin(), m_filename.end(), temp_filename);
			temp_filename[temp_filename_size] = '\0';

			double *temp_xarray = new double[m_number_nodes];
			std::copy(m_xarray.begin(), m_xarray.end(), temp_xarray);
			double *temp_xdat = new double[m_number_nodes];
			double *temp_paramellin = new double[4*m_number_nodes];

			fortran::fort_get_spline_mell_(&m_number_nodes, &m_iparton, &m_st, &m_iset, &m_ih, &m_null, &m_null, &m_null, &m_q2, temp_filename, &temp_filename_size, &m_array_given, temp_xarray, temp_xdat, temp_paramellin);

			m_xdat.assign(temp_xdat, temp_xdat+m_number_nodes);
			m_paramellin.assign(temp_paramellin, temp_paramellin+4*m_number_nodes);
			delete[] temp_xdat;
			delete[] temp_paramellin;
			delete[] temp_filename;
			delete[] temp_xarray;
		}
};

class dss_spline : public lvpdff_spline
{
	public:
		dss_spline(int iparton, double q2, int ih, int ic, int io) : lvpdff_spline(iparton, q2)
		{
			m_hadron = ih;
			m_charge = ic;
			m_order = io;
			m_number_nodes = 35;
			m_xdat.resize(m_number_nodes);
			m_paramellin.resize(4*m_number_nodes);
			mellin_moments();
		}

		dss_spline(int iparton, double q2, std::vector<double> xarray, int ih, int ic, int io) : lvpdff_spline(iparton, q2, xarray)
		{
			m_hadron = ih;
			m_charge = ic;
			m_order = io;
			mellin_moments();
		}

		void set_ih(int ih)
		{
			m_hadron = ih;
			mellin_moments();
		}

		void set_ic(int ic)
		{
			m_charge = ic;
			mellin_moments();
		}

		void set_io(int io)
		{
			m_order = io;
			mellin_moments();
		}

		double dss_ff(double x)
		{
			double result;
			fortran::fort_getdss07ff_(&m_iparton, &m_hadron, &m_charge, &m_order, &x, &m_q2, &result);
			return result;
		}

	private:
		int m_iset = 3;
		int m_hadron;
		int m_charge;
		int m_order;

		void mellin_moments(void)
		{
			double *temp_xarray = new double[m_number_nodes];
			std::copy(m_xarray.begin(), m_xarray.end(), temp_xarray);
			double *temp_xdat = new double[m_number_nodes];
			double *temp_paramellin = new double[4*m_number_nodes];
			char* temp_filename = new char[1];
			int temp_filename_size = 1;

			fortran::fort_get_spline_mell_(&m_number_nodes, &m_iparton, &m_st, &m_iset, &m_null, &m_hadron, &m_charge, &m_order, &m_q2, temp_filename, &temp_filename_size, &m_array_given, temp_xarray, temp_xdat, temp_paramellin);

			m_xdat.assign(temp_xdat, temp_xdat+m_number_nodes);
			m_paramellin.assign(temp_paramellin, temp_paramellin+4*m_number_nodes);
			delete[] temp_filename;
			delete[] temp_xarray;
			delete[] temp_xdat;
			delete[] temp_paramellin;
		}
};


} // namespace pdfinterpolation

#endif // PDF_INTERPOLATION_SPLINES_H_