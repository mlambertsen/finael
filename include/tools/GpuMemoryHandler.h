/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUTOOL_GPU_MEMORY_HANDLER_H_
#define FINAEL_CUTOOL_GPU_MEMORY_HANDLER_H_

#include <cstring>
#include "../common/cudaHostDevice.h"
#include "../common/cudaError.h"



namespace finael {
namespace cuTool {



class GpuMemoryHandler
{
public:
	template<CudaHostDeviceType hostDeviceType, typename T>
	CUDA_HOST static void alloc( T* &ptr, const unsigned int size )
	{
		if (hostDeviceType == CudaHostDeviceType::HOST)
		{
			ptr = new T[size];
		}
		#ifdef __CUDACC__
		else if (hostDeviceType == CudaHostDeviceType::DEVICE)
		{
			cudaMalloc( &ptr, size * sizeof(T) );
			CHECK_CUDA_ERROR( "Cannot allocate memory" );
		}
		#endif
		return;
	}



	template<CudaHostDeviceType hostDeviceType, typename T>
	CUDA_HOST static void free( T* &ptr )
	{
		if (hostDeviceType == CudaHostDeviceType::HOST)
		{
			delete[] ptr;
		}
		#ifdef __CUDACC__
		else if (hostDeviceType == CudaHostDeviceType::DEVICE)
		{
			cudaFree( ptr );
			CHECK_CUDA_ERROR( "Cannot free memory" );
		}
		#endif
		return;
	}



	template<CudaHostDeviceType hostDeviceType, typename T>
	CUDA_HOST static void copy( T* const &target, const T* const &source, const unsigned int size )
	{
		if (hostDeviceType == CudaHostDeviceType::HOST)
		{
			memcpy( target, source, size*sizeof(T) );
		}
		#ifdef __CUDACC__
		else if (hostDeviceType == CudaHostDeviceType::DEVICE)
		{
			cudaMemcpy( target, source, size*sizeof(T), cudaMemcpyDeviceToDevice );
			CHECK_CUDA_ERROR( "Cannot copy data from Device to Device" );
		}
		#endif
		return;
	}



	template<CudaHostDeviceType TargetType, CudaHostDeviceType SourceType, typename T>
	CUDA_HOST static void copy( T* const &target, const T* const &source, const unsigned int size )
	{
		if (TargetType == SourceType)
		{
			copy<SourceType>( target, source, size );
		}
		#ifdef __CUDACC__
		else if (SourceType == CudaHostDeviceType::HOST)
		{
			cudaMemcpy( target, source, size*sizeof(T), cudaMemcpyHostToDevice );
			CHECK_CUDA_ERROR( "Cannot copy data from Host to Device" );
		}
		else if (SourceType == CudaHostDeviceType::DEVICE)
		{
			cudaMemcpy( target, source, size*sizeof(T), cudaMemcpyDeviceToHost );
			CHECK_CUDA_ERROR( "Cannot copy data from Device to Host" );
		}
		#endif // __CUDACC__
		return;
	}



private:
	GpuMemoryHandler();
};



} // namespace cuTool
} // namespace finael

#endif // FINAEL_CUTOOL_GPU_MEMORY_HANDLER_H_
