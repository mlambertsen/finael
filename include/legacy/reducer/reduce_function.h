#ifndef REDUCE_FUNCTION_H_
#define REDUCE_FUNCTION_H_

#include "../../common/cudaHostDevice.h"
#include "../../tools/SmartArray.h"



namespace finael {
namespace reducer {



template<typename Operation> class Sum
{
public:
	template<unsigned int valuesPerThread, CudaHostDeviceType hostDeviceType, typename T> CUDA_HOST_DEVICE static inline T calcMyContribution( const cuTool::SmartArray<T, hostDeviceType>& inputArray, const unsigned int size, const unsigned int offset, const unsigned int idx )
	{
		T mySum = (idx * valuesPerThread < size) ? calc( inputArray[offset + idx * valuesPerThread] ) : 0;

		// if (valuesPerThread > 9)
		// {
		// 	if (idx * valuesPerThread + 9 < size) mySum += calc( inputArray[offset + idx * valuesPerThread + 9] );
		// }

		// if (valuesPerThread > 8)
		// {
		// 	if (idx * valuesPerThread + 8 < size) mySum += calc( inputArray[offset + idx * valuesPerThread + 8] );
		// }

		// if (valuesPerThread > 7)
		// {
		// 	if (idx * valuesPerThread + 7 < size) mySum += calc( inputArray[offset + idx * valuesPerThread + 7] );
		// }

		// if (valuesPerThread > 6)
		// {
		// 	if (idx * valuesPerThread + 6 < size) mySum += calc( inputArray[offset + idx * valuesPerThread + 6] );
		// }

		// if (valuesPerThread > 5)
		// {
		// 	if (idx * valuesPerThread + 5 < size) mySum += calc( inputArray[offset + idx * valuesPerThread + 5] );
		// }

		if (valuesPerThread > 4)
		{
			if (idx * valuesPerThread + 4 < size) mySum += calc( inputArray[offset + idx * valuesPerThread + 4] );
		}

		if (valuesPerThread > 3)
		{
			if (idx * valuesPerThread + 3 < size) mySum += calc( inputArray[offset + idx * valuesPerThread + 3] );
		}

		if (valuesPerThread > 2)
		{
			if (idx * valuesPerThread + 2 < size) mySum += calc( inputArray[offset + idx * valuesPerThread + 2] );
		}

		if (valuesPerThread > 1)
		{
			if (idx * valuesPerThread + 1 < size) mySum += calc( inputArray[offset + idx * valuesPerThread + 1] );
		}

		return mySum;
	}



	template<typename T> CUDA_HOST_DEVICE static inline T calc( const T value )
	{
		return Operation::calc( value );
	}



private:
	Sum( void );
};




namespace helperfunction
{

class Simple
{
public:
	template<typename T> CUDA_HOST_DEVICE static inline T calc( const T value )
	{
		return value;
	}



private:
	Simple( void );
};




class Square
{
public:
	template<typename T> CUDA_HOST_DEVICE static inline T calc( const T value )
	{
		return value * value;
	}



private:
	Square( void );
};

} // namespace helperfunction



typedef Sum<helperfunction::Simple> Simple;
typedef Sum<helperfunction::Square> Square;



class Variance
{
public:
	template<unsigned int valuesPerThread, CudaHostDeviceType hostDeviceType, typename T> CUDA_HOST_DEVICE static inline T calcMyContribution( const cuTool::SmartArray<T, hostDeviceType>& inputArray, const unsigned int size, const unsigned int offset, const unsigned int idx )
	{
		T mySum = Simple::template calcMyContribution<valuesPerThread>( inputArray, size, offset, idx );
		T mySquareSum = Square::template calcMyContribution<valuesPerThread>( inputArray, size, offset, idx );
		// Two Versions. Version one has lower computational cost, version two avoids subtracting two large numbers with possible tiny result
		// --- Version 1 -----------------------------------------------------------------
		// T myVariance = mySquareSum * valuesPerThread - mySum*mySum;
		// -------------------------------------------------------------------------------
		// --- Version 2 -----------------------------------------------------------------
		T myVariance = sqrt( mySquareSum * valuesPerThread );
		myVariance = (myVariance - mySum) * (myVariance + mySum);
		// -------------------------------------------------------------------------------

		if (myVariance <= static_cast<T>( 0 )) myVariance = static_cast<T>( 1.0e-30 );

		return myVariance;
	}



private:
	Variance( void );
};



} // namespace reducer
} // namespace finael


#endif // REDUCE_FUNCTION_H_