/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#define BOOST_TEST_MODULE "finael Array"
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "../../../include/tools/Array.h"



BOOST_AUTO_TEST_SUITE( array )



BOOST_AUTO_TEST_CASE( offersSize )
{
	using namespace finael::cuTool;
	Array<int, 3> array;

	BOOST_CHECK_EQUAL( array.SIZE, 3 );
}


BOOST_AUTO_TEST_CASE( defaultConstructsWithZeros )
{
	using namespace finael::cuTool;

	Array<int, 3> array;

	BOOST_CHECK_EQUAL( array[1], 0 );
}



BOOST_AUTO_TEST_CASE( constructsAllMemberIfGivenOneValue )
{
	using namespace finael::cuTool;

	Array<int, 3> array(5);

	BOOST_CHECK_EQUAL( array[1], 5 );
}



BOOST_AUTO_TEST_CASE( constructsFromRawArray )
{
	using namespace finael::cuTool;

	Array<int, 3> array( {0,1,2} );

	BOOST_CHECK_EQUAL( array[1], 1 );
}



BOOST_AUTO_TEST_CASE( performsDeepCopy )
{
	using namespace finael::cuTool;
	Array<int, 3> array( {0,1,2} );

	auto copy( array );
	array[1] = 2;

	BOOST_CHECK_EQUAL( copy[1], 1 );
}



BOOST_AUTO_TEST_CASE( performsDeepAssigment )
{
	using namespace finael::cuTool;
	Array<int, 3> array( {0,1,2} );

	Array<int, 3> copy;
	copy = array;
	array[1] = 2;

	BOOST_CHECK_EQUAL( copy[1], 1 );
}



BOOST_AUTO_TEST_CASE( canSetOneValue )
{
	using namespace finael::cuTool;

	Array<int, 3> array( 5 );
	array.set( 1, 3 );

	BOOST_CHECK_EQUAL( array[1], 3 );
	BOOST_CHECK_EQUAL( array[0], 5 );
}



BOOST_AUTO_TEST_CASE( canSetAllValuesAtOnce )
{
	using namespace finael::cuTool;

	Array<int, 3> array( 5 );
	array.set( 3 );

	BOOST_CHECK_EQUAL( array[1], 3 );
	BOOST_CHECK_EQUAL( array[0], 3 );
}






BOOST_AUTO_TEST_SUITE_END()
