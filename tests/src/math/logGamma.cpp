/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#define BOOST_TEST_MODULE "finael LogGamma"
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "../../../include/math/LogGamma.h"
#include "../../../include/math/Complex.h"


BOOST_AUTO_TEST_SUITE( logGammaTests )



BOOST_AUTO_TEST_CASE( smallRealArgument )
{
	using namespace finael::cuMath;

	double arg( 3.5 );
	double expectedResult( 1.20097360234707422481 );

	LogGamma logGamma;
	BOOST_CHECK_CLOSE( logGamma( arg ), expectedResult, 1.e-11 );
}



BOOST_AUTO_TEST_CASE( DoesNotRespectPrincipleValue )
{
	using namespace finael::cuMath;

	Complex<double> arg( 3, 5 );
	Complex<double> expectedResult( -2.8176279605487707752L, 0.0964039840628855947L );

	LogGamma logGamma;
	auto result = logGamma( arg );

	BOOST_CHECK_CLOSE( result.real(), expectedResult.real(), 1.e-12 );
	BOOST_CHECK_CLOSE( result.imag(), expectedResult.imag(), 1.e-11 );
}



BOOST_AUTO_TEST_CASE( CanReturnValueIgnoringLogBranch )
{
	using namespace finael::cuMath;

	Complex<double> arg( 3, 5 );
	Complex<double> expectedResult( -2.8176279605487707752L, 0.0964039840628855947L );

	LogGamma logGamma;
	auto result = logGamma.calcIgnoringLogBranch( arg );

	constexpr auto correction = -2*CONST<double>::PI;

	BOOST_CHECK_CLOSE( result.real(), expectedResult.real(), 1.e-12 );
	BOOST_CHECK_CLOSE( result.imag() + correction, expectedResult.imag(), 1.e-11 );
}



BOOST_AUTO_TEST_CASE( largeNegativeRealComponent )
{
	using namespace finael::cuMath;

	Complex<double> arg( -3000, 5 );
	Complex<double> expectedResult( -21037.890773276715963L, 0.7627652694572665411L );

	LogGamma logGamma;
	auto result = logGamma( arg );

	BOOST_CHECK_CLOSE( result.real(), expectedResult.real(), 1.e-20 );
	BOOST_CHECK_CLOSE( result.imag(), expectedResult.imag(), 1.e-10 );
}



BOOST_AUTO_TEST_CASE( largeImaginaryComponent )
{
	using namespace finael::cuMath;

	Complex<double> arg( -16, 230 );
	Complex<double> expectedResult( -450.10664338196887186L, 1.5056621473675376704L );

	LogGamma logGamma;
	auto result = logGamma( arg );

	BOOST_CHECK_CLOSE( result.real(), expectedResult.real(), 1.e-20 );
	BOOST_CHECK_CLOSE( result.imag(), expectedResult.imag(), 1.e-10 );
}



BOOST_AUTO_TEST_CASE( largeImaginaryComponentFloat )
{
	using namespace finael::cuMath;

	Complex<double> arg( -16, 30 );
	Complex<double> expectedResult( -103.08977318728391017L, -2.1960793239395402998L );

	LogGamma logGamma;
	auto result = logGamma( arg );

	BOOST_CHECK_CLOSE( result.real(), expectedResult.real(), 1.e-5 );
	BOOST_CHECK_CLOSE( result.imag(), expectedResult.imag(), 1.e-3 );
}



BOOST_AUTO_TEST_SUITE_END()
