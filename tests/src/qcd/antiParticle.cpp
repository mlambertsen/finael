/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#define BOOST_TEST_MODULE "finael Anti Particle"
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "../../../include/qcd/AntiParticle.h"
#include "../../../include/qcd/PartonFlavourConverter.h"


BOOST_AUTO_TEST_SUITE( AntiParticleTest )



BOOST_AUTO_TEST_CASE( quarksBecomeAntiQuarks )
{
	using namespace finael::cuQcd;

	BOOST_CHECK_EQUAL( PartonFlavourConverter<PartonFlavour::UBAR>::INDEX, PartonFlavourConverter<AntiParticle<PartonFlavour::U>::FLAVOUR>::INDEX);
	BOOST_CHECK_EQUAL( PartonFlavourConverter<PartonFlavour::DBAR>::INDEX, PartonFlavourConverter<AntiParticle<PartonFlavour::D>::FLAVOUR>::INDEX);
	BOOST_CHECK_EQUAL( PartonFlavourConverter<PartonFlavour::SBAR>::INDEX, PartonFlavourConverter<AntiParticle<PartonFlavour::S>::FLAVOUR>::INDEX);
	BOOST_CHECK_EQUAL( PartonFlavourConverter<PartonFlavour::CBAR>::INDEX, PartonFlavourConverter<AntiParticle<PartonFlavour::C>::FLAVOUR>::INDEX);
	BOOST_CHECK_EQUAL( PartonFlavourConverter<PartonFlavour::BBAR>::INDEX, PartonFlavourConverter<AntiParticle<PartonFlavour::B>::FLAVOUR>::INDEX);
	BOOST_CHECK_EQUAL( PartonFlavourConverter<PartonFlavour::TBAR>::INDEX, PartonFlavourConverter<AntiParticle<PartonFlavour::T>::FLAVOUR>::INDEX);
}



BOOST_AUTO_TEST_CASE( antiQuarksBecomeQuarks )
{
	using namespace finael::cuQcd;

	BOOST_CHECK_EQUAL( PartonFlavourConverter<PartonFlavour::U>::INDEX, PartonFlavourConverter<AntiParticle<PartonFlavour::UBAR>::FLAVOUR>::INDEX);
	BOOST_CHECK_EQUAL( PartonFlavourConverter<PartonFlavour::D>::INDEX, PartonFlavourConverter<AntiParticle<PartonFlavour::DBAR>::FLAVOUR>::INDEX);
	BOOST_CHECK_EQUAL( PartonFlavourConverter<PartonFlavour::S>::INDEX, PartonFlavourConverter<AntiParticle<PartonFlavour::SBAR>::FLAVOUR>::INDEX);
	BOOST_CHECK_EQUAL( PartonFlavourConverter<PartonFlavour::C>::INDEX, PartonFlavourConverter<AntiParticle<PartonFlavour::CBAR>::FLAVOUR>::INDEX);
	BOOST_CHECK_EQUAL( PartonFlavourConverter<PartonFlavour::B>::INDEX, PartonFlavourConverter<AntiParticle<PartonFlavour::BBAR>::FLAVOUR>::INDEX);
	BOOST_CHECK_EQUAL( PartonFlavourConverter<PartonFlavour::T>::INDEX, PartonFlavourConverter<AntiParticle<PartonFlavour::TBAR>::FLAVOUR>::INDEX);
}



BOOST_AUTO_TEST_CASE( gluonStaysGluon )
{
	using namespace finael::cuQcd;

	BOOST_CHECK_EQUAL( PartonFlavourConverter<PartonFlavour::G>::INDEX, PartonFlavourConverter<AntiParticle<PartonFlavour::G>::FLAVOUR>::INDEX);
}



BOOST_AUTO_TEST_SUITE_END()
