! Give spline data in mellin space
      subroutine fort_get_spline_mell(n, iparton, st, iset, mmhtih, 
     &                                dssih, dssic, dssio, q2, file, 
     &                                filesize, larray, xarray, xdat,
     &                                paramellin)
!-----------------------------------------------------------------------
      implicit none
      include 'pdfSplineInterface.f'

      ! input/output variables
      integer, intent(in) :: n, iparton, st, iset, mmhtih, dssih, dssic,
     &                       dssio, filesize
      double precision, intent(in) :: q2
      character(len=100), intent(in) :: file
      logical*1, intent(in) :: larray
      double precision, intent(in) :: xarray(1:n)
      double precision, intent(out) :: xdat(0:n-1), paramellin(0:4*n-1)

      ! intern variables
      double precision paramel(0:3, 1:n), xdattemp(1:n), xarraytemp(1:n)
      integer ngrid, i, j
      character(len=filesize) :: tempfile
!-----------------------------------------------------------------------

      ! if array is given, set array
      if(larray) then
         do i = 1,n-1
            xarraytemp(i) = xarray(i)
         enddo
         ! because it will not be 1.0, even when is in c++
         xarraytemp(n) = 1.d0
         call PdfSplineGrid(xarraytemp)
      endif


      ! write file into a new character - otherwise is will have random
      ! behaviour for the characters coming after the real filename
      tempfile(1:filesize) = file(1:filesize)

      ! set the required spline (has to be done always, because we have
      ! only one fortran instance, but can have a lot c++ instances)
      call PdfSplineSet(st = st, iset = iset, mmhtih = mmhtih,
     &                  dssih = dssih, dssic = dssic, dssio = dssio,
     &                  q2 = q2, file = tempfile)

      ! get the parameters
      call PdfSplineGet(iparton,ngrid,xdattemp,paramel)

      ! give the parameters in arrays that are compatible to c++
      do j = 1, n
         do i = 0, 3
            paramellin((j-1)*4 + i) = paramel(i,j)
         enddo
         xdat(j-1) = xdattemp(j)
      enddo

      return
      end subroutine fort_get_spline_mell


! Get spline-pdf in x-space at given x
      subroutine fort_get_spline_x(iparton, x, pdf)
!-----------------------------------------------------------------------
      implicit none
      include 'PdfSplineInterface.f'
      ! input/output variables
      integer, intent(in) :: iparton
      double precision, intent(in) :: x
      double precision, intent(out) :: pdf
!-----------------------------------------------------------------------

      pdf = PdfSplineX(iparton,x)

      return
      end subroutine fort_get_spline_x



! Get real cteq value
      subroutine fort_get_ct12pdf(iparton, x, q2, pdf)
!-----------------------------------------------------------------------
      implicit none
      ! input/output variables
      integer, intent(in) :: iparton
      double precision, intent(in) :: x
      double precision, intent(in) :: q2
      double precision, intent(out) :: pdf

      ! intern variables
      double precision ct12pdf
!-----------------------------------------------------------------------

      pdf = ct12pdf(iparton, x, dsqrt(q2))

      return
      end subroutine fort_get_ct12pdf


! Get real mmht value
      subroutine fort_get_mmht14pdf(iparton, iset, infile, filesize, x,
     &                              q2, pdf)
!-----------------------------------------------------------------------
      implicit none
      ! input/output variables
      integer, intent(in) :: iparton, iset, filesize
      character(len=100), intent(in) :: infile
      double precision, intent(in) :: x
      double precision, intent(in) :: q2
      double precision, intent(out) :: pdf

      ! intern variables
      double precision getonepdf
      character(len=filesize) :: file
!-----------------------------------------------------------------------

      ! write file into a new character - otherwise is will have random
      ! behaviour for the characters coming after the real filename
      file(1:filesize) = infile(1:filesize)

      if((iparton.eq.0).or.(iparton.ge.3).or.(iparton.le.-3)) then
         pdf = getonepdf(file ,iset, x, dsqrt(q2),iparton)
      else if(iparton.eq.1) then
         pdf = getonepdf(file ,iset, x, dsqrt(q2),8)
     &         + getonepdf(file ,iset, x, dsqrt(q2),-2)
      else if(iparton.eq.2) then
         pdf = getonepdf(file ,iset, x, dsqrt(q2),7)
     &         + getonepdf(file ,iset, x, dsqrt(q2),-1)
      else if(iparton.eq.-1) then
         pdf = getonepdf(file ,iset, x, dsqrt(q2),-2)
      else if(iparton.eq.-2) then
         pdf = getonepdf(file ,iset, x, dsqrt(q2),-1)
      endif

      pdf = getonepdf('mmht2014nlo120',0,x,dsqrt(q2),8)/x
     &            + getonepdf('mmht2014nlo120',0,x,dsqrt(q2),-2)/x

      pdf = pdf/x

      return
      end subroutine fort_get_mmht14pdf


! Get real dss value
      subroutine fort_get_dss07ff(iparton, ih, ic, io, x, q2, ff)
!-----------------------------------------------------------------------
      implicit none
      ! input/output variables
      integer, intent(in) :: iparton, ih, ic, io
      double precision, intent(in) :: x
      double precision, intent(in) :: q2
      double precision, intent(out) :: ff

      ! intern variables
      double precision dss(-5:5)
!-----------------------------------------------------------------------

      call fdss(ih,ic,io,x,q2,dss(1),dss(-1),dss(2),dss(-2),dss(3),
     &          dss(-3),dss(4),dss(5),dss(0))
      dss(-4) = dss(4)
      dss(-5) = dss(5)
      ff = dss(iparton)/x

      return
      end subroutine fort_get_dss07ff