/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_OPTIONFLAGS_H_
#define FINAEL_CUVEGAS_OPTIONFLAGS_H_



namespace finael {
namespace cuVegas {



enum class PrngFlag {DEFAULT, LECUYER, CURAND, PHILOX};
enum class StrategyFlag {IMPORTANCE, FREE};
enum class ReductionFlag {CLASSIC, REFINED};
enum class VerbosityFlag {QUIET, GENTLE, VERBOSE};



} // namespace cuVegas
} // namespace finael



#endif // FINAEL_CUVEGAS_OPTIONFLAGS_H_
