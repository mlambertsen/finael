/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUQCD_CONSTANTS_H_
#define FINAEL_CUQCD_CONSTANTS_H_

#include "../common/cudaHostDevice.h"
#include "definitions.h"



namespace finael {
namespace cuQcd {



template<typename T>
struct CONSTANTS
{
	static constexpr T NC = {3.0};
	static constexpr T CF = {4.0/3.0};
	static constexpr T CA = {3.0};
	static constexpr T TR = {1.0/2.0};

	static constexpr T CMASS = {1.3};
	static constexpr T BMASS = {4.5};

	template<unsigned short numberFlavours>
	CUDA_HOST_DEVICE static constexpr T BETA0()
	{
		return (static_cast<T>( 11 ) * CA - static_cast<T>( 4 ) * TR * numberFlavours) / static_cast<T>( 3 );
	}

	template<unsigned short numberFlavours>
	CUDA_HOST_DEVICE static constexpr T BETA1()
	{
		return static_cast<T>( 34 ) / static_cast<T>( 3 ) * CA * CA - static_cast<T>( 4 ) * TR * numberFlavours * (static_cast<T>( 5 ) / static_cast<T>( 3 ) * CA + CF);
	}

	template<unsigned short numberFlavours, PQcdOrder order>
	CUDA_HOST_DEVICE static constexpr T LAMBDA()
	{
		return (order == PQcdOrder::LO && numberFlavours <= 4) ? static_cast<T>( 0.215 )
				: (order == PQcdOrder::LO && numberFlavours == 5) ? static_cast<T>( 0.165 )
				: (order == PQcdOrder::NLO && numberFlavours <= 4) ? static_cast<T>( 0.326 )
				: (order == PQcdOrder::NLO && numberFlavours == 5) ? static_cast<T>( 0.226 )
				: static_cast<T>( 0.0 );
	}
};





template<typename T>
constexpr T CONSTANTS<T>::NC;

template<typename T>
constexpr T CONSTANTS<T>::CF;

template<typename T>
constexpr T CONSTANTS<T>::CA;

template<typename T>
constexpr T CONSTANTS<T>::TR;

template<typename T>
constexpr T CONSTANTS<T>::CMASS;

template<typename T>
constexpr T CONSTANTS<T>::BMASS;



} // namespace cuQcd
} // namespace finael



#endif // FINAEL_CUQCD_CONSTANTS_H_
