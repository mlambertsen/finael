/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_PRINTER_CACHE_H_
#define FINAEL_CUVEGAS_PRINTER_CACHE_H_

#include "../../common/cudaHostDevice.h"

#include <exception>
#include <iomanip>
#include <ostream>



namespace finael {
namespace cuVegas {



class PrinterCache
{
public:
	CUDA_HOST PrinterCache( std::ostream& stream, const std::streamsize& precision )
		: stream_( stream )
		, precision_( stream_.precision( precision ) )
		, flags_( stream_.flags( std::ios::scientific ) )
	{
		if (!stream_.good()) throw std::runtime_error( "Stream is not good. Cannot print!" );
	}



	CUDA_HOST ~PrinterCache()
	{
		stream_.precision( precision_ );
		stream_.flags( flags_ );
	}



	CUDA_HOST PrinterCache( const PrinterCache& ) = delete;



	CUDA_HOST PrinterCache& operator=( const PrinterCache& ) = delete;



private:
	std::ostream& stream_;
	std::streamsize precision_;
	std::ios_base::fmtflags flags_;

};



} // namespace cuVegas
} // namespace finael



#endif // FINAEL_CUVEGAS_PRINTER_CACHE_H_
