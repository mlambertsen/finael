/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#define BOOST_TEST_MODULE "finael Pdf Spline Base"
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include <array>
#include <vector>

#include "../../../../include/qcd/pdfinterpolation/PdfSplineBase.h"
#include "../../../../include/math/Complex.h"



namespace finael {
namespace qcd {
namespace pdfInterpolation {
namespace pdfSplineBaseTest {


struct PdfDummy
{
	using value_type = double;
	double factor;

	PdfDummy()
		: PdfDummy( 1.0 )
	{}

	PdfDummy( double factorIn )
		: factor( factorIn )
	{}

	double operator()( double point ) const
	{
		return factor*exp( point );
	}
};



std::vector<double> defaultPoints {-2.0, -1.0, 0.0, 1.0, 2.0};



class PdfSpline : public PdfSplineBase<PdfDummy, double>
{
public:
	PdfSpline()
		: PdfSplineBase<PdfDummy, double>( defaultPoints.begin(), defaultPoints.end(), SplineType::NATURAL, 1.0 )
	{}

	void multiplyUnderlyingFunctionAndUpdate( double factor )
	{
		PdfSplineBase<PdfDummy, double>::function_.factor *= factor;
		PdfSplineBase<PdfDummy, double>::update();
	}
};



struct BoostTestPdfSplineBaseFixture
{
	BoostTestPdfSplineBaseFixture()
		: pdfSpline()
		, spline( defaultPoints.begin(), defaultPoints.end(), PdfDummy() )
		, mellinSpline( spline )
	{}

	PdfSpline pdfSpline;
	interpolation::Spline<double> spline;
	interpolation::MellinSpline<double> mellinSpline;
};



BOOST_FIXTURE_TEST_SUITE( pdfSplineBase, BoostTestPdfSplineBaseFixture )



BOOST_AUTO_TEST_CASE( passesOriginalValueOfUnderlyingFunctor )
{
	PdfDummy underlyingFunctor;

	BOOST_CHECK_EQUAL( pdfSpline.getTruePdf( 5.0 ), underlyingFunctor( 5.0 ) );
}



BOOST_AUTO_TEST_CASE( offersNumberOfPoints )
{
	BOOST_CHECK_EQUAL( pdfSpline.numberPoints(), spline.numberPoints() );
}



BOOST_AUTO_TEST_CASE( offersPointAtGivenIndex )
{
	std::size_t index = 2;
	BOOST_CHECK_EQUAL( pdfSpline.getPointAt( index ), spline.getPointAt( index ) );
}



BOOST_AUTO_TEST_CASE( offersMinimalPoint )
{
	BOOST_CHECK_EQUAL( pdfSpline.getMinimalPoint(), spline.getMinimalPoint() );
}



BOOST_AUTO_TEST_CASE( offersMaximalPoint )
{
	BOOST_CHECK_EQUAL( pdfSpline.getMaximalPoint(), spline.getMaximalPoint() );
}



BOOST_AUTO_TEST_CASE( canCheckIfGivenPointIsInCoveredRange )
{
	double pointInside { 1.0 };
	double pointOutside { 10.0 };

	BOOST_CHECK_EQUAL( pdfSpline.isInRange( pointInside ), spline.isInRange( pointInside ) );
	BOOST_CHECK_EQUAL( pdfSpline.isInRange( pointOutside ), spline.isInRange( pointOutside ) );
}



BOOST_AUTO_TEST_CASE( offersSplineType )
{
	BOOST_CHECK( pdfSpline.getType() == spline.getType() );
}



BOOST_AUTO_TEST_CASE( offersSplinePieces )
{
	auto pdfPiece = pdfSpline.getPiece( 1 );
	auto splinePiece = spline.getPiece( 1 );

	BOOST_CHECK_EQUAL( pdfPiece.constant, splinePiece.constant );
	BOOST_CHECK_EQUAL( pdfPiece.linear, splinePiece.linear );
	BOOST_CHECK_EQUAL( pdfPiece.square, splinePiece.square );
	BOOST_CHECK_EQUAL( pdfPiece.cubic, splinePiece.cubic );
}



BOOST_AUTO_TEST_CASE( isSplineFunctor )
{
	double point {1.5};
	BOOST_CHECK_EQUAL( pdfSpline( point ), spline( point ) );
}



BOOST_AUTO_TEST_CASE( isMellinSplineFunctor )
{
	cuMath::Complex<double> point {3.0, 2.0};

	BOOST_CHECK( pdfSpline( point ) == mellinSpline( point ) );
}



BOOST_AUTO_TEST_CASE( offersMellinSplineCoefficients )
{
	auto pdfCoefficients = pdfSpline.getCoefficientsAt( 1 );
	auto splineCoefficients = mellinSpline.getCoefficientsAt( 1 );

	BOOST_CHECK_EQUAL( pdfCoefficients.at( 0 ), splineCoefficients.at( 0 ) );
	BOOST_CHECK_EQUAL( pdfCoefficients.at( 1 ), splineCoefficients.at( 1 ) );
	BOOST_CHECK_EQUAL( pdfCoefficients.at( 2 ), splineCoefficients.at( 2 ) );
	BOOST_CHECK_EQUAL( pdfCoefficients.at( 3 ), splineCoefficients.at( 3 ) );
}



BOOST_AUTO_TEST_CASE( reactsOnTypeChange )
{
	spline.set( PdfDummy(), interpolation::SplineType::FIXED );
	mellinSpline.set( PdfDummy(), interpolation::SplineType::FIXED );
	pdfSpline.set( interpolation::SplineType::FIXED );

	double point {1.3};
	BOOST_CHECK_EQUAL( spline( point ), pdfSpline( point ) );

	cuMath::Complex<double> complexPoint( 2.3, 3.2 );
	BOOST_CHECK( mellinSpline( complexPoint ) == pdfSpline( complexPoint ) );
}



BOOST_AUTO_TEST_CASE( reactsOnChangesPoints )
{
	std::vector<double> newPoints {1.0, 2.0, 3.0, 4.0};
	spline.set( newPoints.begin(), newPoints.end(), PdfDummy() );
	mellinSpline.set( newPoints.begin(), newPoints.end(), PdfDummy() );
	pdfSpline.set( newPoints.begin(), newPoints.end() );

	double point {1.3};
	BOOST_CHECK_EQUAL( spline( point ), pdfSpline( point ) );

	cuMath::Complex<double> complexPoint( 2.3, 3.2 );
	BOOST_CHECK( mellinSpline( complexPoint ) == pdfSpline( complexPoint ) );
}



BOOST_AUTO_TEST_CASE( hasToBeUpdatedAfterFunctionChange )
{
	PdfDummy standardFunctor;
	standardFunctor.factor *= 2.0;
	spline.set( standardFunctor );
	mellinSpline.set( standardFunctor );

	pdfSpline.multiplyUnderlyingFunctionAndUpdate( 2.0 );

	double point {1.3};
	BOOST_CHECK_EQUAL( spline( point ), pdfSpline( point ) );

	cuMath::Complex<double> complexPoint( 2.3, 3.2 );
	BOOST_CHECK( mellinSpline( complexPoint ) == pdfSpline( complexPoint ) );
}



BOOST_AUTO_TEST_SUITE_END()


} // namespace pdfSplineBaseTest
} // namespace pdfInterpolation
} // namespace qcd
} // namespace finael
