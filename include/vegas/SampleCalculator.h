/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_SAMPLE_CALCULATOR_H_
#define FINAEL_CUVEGAS_SAMPLE_CALCULATOR_H_

#include "../common/cudaHostDevice.h"
#include "../tools/Array.h"

#include "increments/Increments.h"
#include "integrandTypes.h"
#include "KernelRelevantSettings.h"
#include "optionFlags.h"
#include "Samples.h"


namespace finael {
namespace cuVegas {



template<unsigned int dim, typename T, CudaHostDeviceType hostDeviceType>
class SampleCalculator
{
public:
	CUDA_HOST_DEVICE SampleCalculator( T jacobian )
		: samplePoint()
		, weight( jacobian )
	{}



	template<class Functor, class KernelPrng, ReductionFlag reductionMode>
	CUDA_HOST_DEVICE void operator()( const unsigned int idx, const Functor& functor, Samples<T, hostDeviceType, reductionMode>& samples, KernelPrng& prng, const Increments<dim, T, hostDeviceType>& increments, const KernelRelevantSettings<T, reductionMode>& settings )
	{
		for (auto dimIdx = 0u; dimIdx < dim; ++dimIdx)
		{
			auto binIdx = settings.getBinIdx( idx, dimIdx );
			auto randomNumber = prng.getRandomNumber( dimIdx*samples.size() + idx );
			T incrementPosition = (binIdx + randomNumber) * settings.getIncrementsPerBinsPerDim();
			auto incrementIdx = static_cast<unsigned int>( incrementPosition );
			if (sizeof( T ) == 4)
			{
				if (incrementIdx > settings.getMaxIncrementIdx())
				{
					incrementPosition = nextafterf( static_cast<T>( incrementIdx ), static_cast<T>( -1 ) );
					--incrementIdx;
				}
			}
			samples.setIncrementIdx( idx, dimIdx, incrementIdx );

			samplePoint[dimIdx] = determineSamplePointCoordinate( dimIdx, incrementIdx, increments, incrementPosition, functor.volume );
		}
		samples.evaluate( idx, samplePoint, functor, weight );
	}


private:
	IntegrandPointType<T, dim> samplePoint;
	T weight;



	CUDA_HOST_DEVICE T determineSamplePointCoordinate( unsigned int dimIdx, unsigned int incrementIdx, const Increments<dim, T, hostDeviceType>& increments, T incrementPosition, const VolumeType<T, dim>& limits )
	{
		T lowerIncrementLimit = (incrementIdx == 0) ? 0 : increments.at( dimIdx, incrementIdx - 1);
		T incrementSize = increments.at( dimIdx, incrementIdx ) - lowerIncrementLimit;
		weight *= incrementSize;
		T positionInIncrement = incrementPosition - incrementIdx;
		return limits[dimIdx].first + (lowerIncrementLimit + positionInIncrement * incrementSize)*(limits[dimIdx].second - limits[dimIdx].first);
	}
};



} // namespace cuVegas
} // namespace finael



#endif // FINAEL_CUVEGAS_SAMPLE_CALCULATOR_H_
