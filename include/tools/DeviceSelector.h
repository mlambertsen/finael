/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUTOOL_DEVICE_SELECTOR_H_
#define FINAEL_CUTOOL_DEVICE_SELECTOR_H_

#include <cstdlib>

#include "../common/cudaHostDevice.h"


namespace finael {
namespace cuTool {



class DeviceSelector
{
public:



	CUDA_HOST DeviceSelector() = delete;



#ifdef __CUDACC__
	CUDA_HOST static int setFirstAvailable()
	{
		int numberDevices;
		cudaGetDeviceCount( &numberDevices );

		for (int deviceIndex = 0; deviceIndex < numberDevices; ++deviceIndex)
		{
			cudaError_t error = cudaSetDevice( deviceIndex );
			if (error != cudaSuccess) continue;
			cudaFree( 0 );
			cudaDeviceSynchronize();
			error = cudaGetLastError();
			if (error == cudaSuccess) return deviceIndex;
		}
		std::printf( "No device available - stop program\n" );
		std::exit( EXIT_FAILURE );
	}
#else // __CUDACC__
	CUDA_HOST static int setFirstAvailable() {return 0;}
#endif // __CUDACC__
};



} // namespace cuTool
} // namespace finael

#endif // FINAEL_CUTOOL_DEVICE_SELECTOR_H_
