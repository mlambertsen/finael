/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#define BOOST_TEST_MODULE "finael Beta"
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "../../../include/math/Beta.h"
#include "../../../include/math/Complex.h"


BOOST_AUTO_TEST_SUITE( betaTests )



BOOST_AUTO_TEST_CASE( smallRealArguments )
{
	using namespace finael::cuMath;

	double arg1( 3.5 );
	double arg2( 5.5 );
	double expectedResult( 0.00431432096592836595 );

	Beta beta;
	BOOST_CHECK_CLOSE( beta( arg1, arg2 ), expectedResult, 1.e-11 );
}



BOOST_AUTO_TEST_CASE( smallComplexArguments )
{
	using namespace finael::cuMath;

	Complex<double> arg1( 3.5, 2.0 );
	Complex<double> arg2( 5.5, 1.5 );
	Complex<double> expectedResult( -0.00354761273179425370, -0.00121144553207106904 );

	Beta beta;
	auto result = beta( arg1, arg2 );
	BOOST_CHECK_CLOSE( result.real(), expectedResult.real(), 1.e-11 );
	BOOST_CHECK_CLOSE( result.imag(), expectedResult.imag(), 1.e-11 );
}



BOOST_AUTO_TEST_CASE( argumentsWithLargeRealDifference )
{
	using namespace finael::cuMath;

	Complex<double> arg1( -3500, 2.0 );
	Complex<double> arg2( 5.5, 1.5 );
	Complex<double> expectedResult( -5.48203594386111788609e-17, -1.40357762047726681750e-16 );

	Beta beta;
	auto result = beta( arg1, arg2 );
	BOOST_CHECK_CLOSE( result.real(), expectedResult.real(), 1.e-9 );
	BOOST_CHECK_CLOSE( result.imag(), expectedResult.imag(), 1.e-9 );
}



BOOST_AUTO_TEST_CASE( argumentsWithLargeImaginaryDifference )
{
	using namespace finael::cuMath;

	Complex<double> arg1( 3.5, 2000 );
	Complex<double> arg2( 5.5, 1.5 );
	Complex<double> expectedResult( 9.94377514791477177695e-17,	2.92578332963771474149e-16 );

	Beta beta;
	auto result = beta( arg1, arg2 );
	BOOST_CHECK_CLOSE( result.real(), expectedResult.real(), 10 );
	BOOST_CHECK_CLOSE( result.imag(), expectedResult.imag(), 1 );
}



BOOST_AUTO_TEST_CASE( argumentsWithLargeDifferenceInBothComponents )
{
	using namespace finael::cuMath;

	Complex<double> arg1( -3500, 2000 );
	Complex<double> arg2( 5.5, 1.5 );
	Complex<double> expectedResult( 2.43417021360449789645e-17,	2.04171129674984765002e-17 );

	Beta beta;
	auto result = beta( arg1, arg2 );
	BOOST_CHECK_CLOSE( result.real(), expectedResult.real(), 1.e-8 );
	BOOST_CHECK_CLOSE( result.imag(), expectedResult.imag(), 1.e-8 );
}



BOOST_AUTO_TEST_SUITE_END()
