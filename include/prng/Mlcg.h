/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_PRNG_MLCG_H_
#define FINAEL_PRNG_MLCG_H_



namespace finael {
namespace prng {



template<std::size_t mod, std::size_t mult>
class Mlcg // Multiplicative linear congruential generator
{
public:
	static constexpr std::size_t modulus = mod;
	static constexpr std::size_t multiplier = mult;



	Mlcg( std::size_t seed, std::size_t warmUp )
		: state_( seed )
	{
		for (auto i = 0u; i < warmUp; ++i)
		{
			getRandomNumber();
		}
	}



	explicit Mlcg( std::size_t seed )
		: Mlcg( seed, 0 )
	{}



	std::size_t getRandomNumber()
	{
		std::size_t k = state_ / quotient;
		state_ = multiplier * (state_ - k * quotient) - k * rest;
		if (state_ < 0) state_ += modulus;
		return state_;
	}



private:
	static constexpr std::size_t quotient = modulus/multiplier;
	static constexpr std::size_t rest = modulus%multiplier;

	int state_;
};



template<std::size_t mod, std::size_t mult>
constexpr std::size_t Mlcg<mod,mult>::modulus;

template<std::size_t mod, std::size_t mult>
constexpr std::size_t Mlcg<mod,mult>::multiplier;



} // namespace prng
} // namespace finael


#endif // FINAEL_PRNG_MLCG_H_
