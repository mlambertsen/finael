/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUMATH_DEFINITIONS_H_
#define FINAEL_CUMATH_DEFINITIONS_H_


namespace finael {
namespace cuMath {



enum class Sign {PLUS, MINUS};



} // namespace cuMath
} // namespace finael

#endif // FINAEL_CUMATH_DEFINITIONS_H_
