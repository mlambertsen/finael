/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_QCD_PDFINTERPOLATION_CT_SETTINGS_H_
#define FINAEL_QCD_PDFINTERPOLATION_CT_SETTINGS_H_



namespace finael {
namespace qcd {
namespace pdfInterpolation {



enum class CtSet {CT10, CT10N, CT10NN, CT14}; // CT10N is valid also for the data files in the folders ct10nFFS and ct10wn



} // namespace pdfInterpolation
} // namespace qcd
} // namespace finael



#endif // FINAEL_QCD_PDFINTERPOLATION_CT_SETTINGS_H_
