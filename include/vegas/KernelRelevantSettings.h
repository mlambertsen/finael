/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_KERNEL_RELEVANT_SETTINGS_H_
#define FINAEL_CUVEGAS_KERNEL_RELEVANT_SETTINGS_H_

#include <cassert>

#include "../common/cudaHostDevice.h"
#include "../math/generalFunctions.h"

#include "integrandTypes.h"
#include "InternalSettings.h"
#include "optionFlags.h"


namespace finael{
namespace cuVegas {



template<typename T, ReductionFlag reductionMode>
class KernelRelevantSettings;



template<typename T>
class KernelRelevantSettings<T, ReductionFlag::CLASSIC>
{
public:
	template<unsigned int dim>
	CUDA_HOST KernelRelevantSettings( const InternalSettings<dim>& settings, const VolumeType<T, dim>& limits )
		: incrementsPerDim( settings.incrementsPerDim )
		, binsPerDim( settings.binsPerDim )
		, binCapacity( settings.binCapacity )
		, incrementsPerBinsPerDim( static_cast<T>( settings.incrementsPerDim ) / settings.binsPerDim )
		, jacobian( settings.calculateJacobian( limits ) )
	{}



	CUDA_HOST_DEVICE unsigned int getBinIdx( unsigned int idx, unsigned int dimIdx ) const
	{
		return (idx / (binCapacity*cuMath::pow( binsPerDim, dimIdx ))) % binsPerDim;
	}



	CUDA_HOST_DEVICE unsigned int getMaxIncrementIdx() const
	{
		return incrementsPerDim - 1u;
	}



	CUDA_HOST_DEVICE T getIncrementsPerBinsPerDim() const
	{
		return incrementsPerBinsPerDim;
	}



	CUDA_HOST_DEVICE T getJacobian() const
	{
		return jacobian;
	}



private:
	const unsigned int incrementsPerDim;
	const unsigned int binsPerDim;
	const unsigned int binCapacity;
	const T incrementsPerBinsPerDim;
	const T jacobian;
};



template<typename T>
class KernelRelevantSettings<T, ReductionFlag::REFINED>
{
public:
	template<unsigned int dim>
	CUDA_HOST KernelRelevantSettings( const InternalSettings<dim>& settings, const VolumeType<T, dim>& limits )
		: binCapacity( settings.binCapacity )
		, incrementsPerDim( settings.incrementsPerDim )
		, incrementCapacity( settings.numberSamples()/std::pow( incrementsPerDim, dim ) )
		, binsPerIncrementPerDim( settings.binsPerDim / incrementsPerDim )
		, incrementsPerBinsPerDim( static_cast<T>( settings.incrementsPerDim ) / settings.binsPerDim )
		, jacobian( settings.calculateJacobian( limits ) )
	{}



	CUDA_HOST_DEVICE unsigned int getBinIdx( unsigned int idx, unsigned int dimIdx ) const
	{
		auto incrementIdx = (idx / (incrementCapacity * cuMath::pow( incrementsPerDim, dimIdx ))) % incrementsPerDim;
		auto binIdxInIncrement = (idx / (binCapacity * cuMath::pow( binsPerIncrementPerDim, dimIdx ))) % binsPerIncrementPerDim;

		return incrementIdx*binsPerIncrementPerDim + binIdxInIncrement;
	}



	CUDA_HOST_DEVICE unsigned int getMaxIncrementIdx() const
	{
		return incrementsPerDim - 1u;
	}



	CUDA_HOST_DEVICE T getIncrementsPerBinsPerDim() const
	{
		return incrementsPerBinsPerDim;
	}



	CUDA_HOST_DEVICE T getJacobian() const
	{
		return jacobian;
	}



private:
	const unsigned int binCapacity;
	const unsigned int incrementsPerDim;
	const unsigned int incrementCapacity;
	const unsigned int binsPerIncrementPerDim;
	const T incrementsPerBinsPerDim;
	const T jacobian;
};



} // namespace cuVegas
} // namespace finael


#endif // FINAEL_CUVEGAS_KERNEL_RELEVANT_SETTINGS_H_
