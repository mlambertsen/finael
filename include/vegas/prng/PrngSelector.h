/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_PRNG_SELECTOR_H_
#define FINAEL_CUVEGAS_PRNG_SELECTOR_H_

#include "../../common/cudaHostDevice.h"

#include "../optionFlags.h"


namespace finael {
namespace cuVegas {



template<CudaHostDeviceType hostDeviceType, PrngFlag prngInputFlag>
class PrngSelector
{
public:
	#ifndef __CUDACC__
	static_assert( hostDeviceType != CudaHostDeviceType::HOST || prngInputFlag != PrngFlag::CURAND, "You cannot choose fineal::vegas::VegasPRNG::CURAND, take fineal::vegas::VegasPRNG::LECUYER (=fineal::vegas::VegasPRNG::DEFAULT) instead" );
	#endif // __CUDACC__
	static constexpr PrngFlag prngFlag = prngInputFlag;
};



template<CudaHostDeviceType hostDeviceType, PrngFlag prngInputFlag>
constexpr PrngFlag PrngSelector<hostDeviceType, prngInputFlag>::prngFlag;





template<>
class PrngSelector<CudaHostDeviceType::HOST, PrngFlag::DEFAULT>
{
public:
	static constexpr PrngFlag prngFlag = PrngFlag::LECUYER;
};



constexpr PrngFlag PrngSelector<CudaHostDeviceType::HOST, PrngFlag::DEFAULT>::prngFlag;





#ifdef __CUDACC__
template<>
class PrngSelector<CudaHostDeviceType::DEVICE, PrngFlag::DEFAULT>
{
public:
	static constexpr PrngFlag prngFlag = PrngFlag::PHILOX;
};


constexpr PrngFlag PrngSelector<CudaHostDeviceType::DEVICE, PrngFlag::DEFAULT>::prngFlag;
#endif // __CUDACC__



} // namespace cuVegas
} // namespace finael



#endif // FINAEL_CUVEGAS_PRNG_SELECTOR_H_
