/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_PRNG_PHILOX_WRAPPER_H_
#define FINAEL_PRNG_PHILOX_WRAPPER_H_

#include "../common/cudaHostDevice.h"
#include "Random123Philox.h"
#include "Random123Uniform.h"
#include "PhiloxWrapperInfo.h"



namespace finael {
namespace prng {



template<typename T>
class PhiloxWrapper
{
public:
	CUDA_HOST_DEVICE PhiloxWrapper( const unsigned int counter1 = 0xdecafbad, const unsigned int counter2 = 0xf00dcafe, const unsigned int counter3 = 0xdeadbeef, const unsigned int counter4 = 0xbeeff00d )
		: localCounter_(0)
	{
		counters_[0] = counter1;
		counters_[1] = counter2;
		counters_[2] = counter3;
		counters_[3] = counter4;
		keys_[0] = 0; // global counter (counting kernels - has to be used by user!)
		keys_[1] = 0; // counter inside one kernel
	}



	CUDA_HOST_DEVICE void setCounter( const unsigned int value, const unsigned int index )
	{
		counters_[index] = value;
	}



	CUDA_HOST_DEVICE void prepareForKernel()
	{
		++keys_[0];
		localCounter_ = 0;
	}



	CUDA_HOST_DEVICE T getRandomNumber( const unsigned int value, const unsigned int index = 0u )
	{
		setCounter( value, index );
		return getRandomNumber();
	}



	CUDA_HOST_DEVICE T getRandomNumber()
	{
		if (localCounter_ == 0)
		{
			localCounter_ = PhiloxWrapperInfo<T>::NumberInts - 1;
			++keys_[1];
			resultUnion_.philoxResult = philox4x32( counters_, keys_ );
		}
		else
		{
			--localCounter_;
		}
		return r123::u01fixedpt<T>( resultUnion_.integerResult[localCounter_] );
	}



private:
	philox4x32_key_t keys_;
	philox4x32_ctr_t counters_;

	union
	{
		philox4x32_ctr_t philoxResult;
		typename PhiloxWrapperInfo<T>::IntType integerResult[PhiloxWrapperInfo<T>::NumberInts];
	} resultUnion_;

	short localCounter_;
};



} // namespace prng
} // namespace finael

#endif // FINAEL_PRNG_PHILOX_WRAPPER_H_
