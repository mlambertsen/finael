      interface
         subroutine PdfSplineSet(st,iset,mmhtih,dssih,dssic,dssio,q2,
     &                          file)
            integer,intent(in),optional :: st,iset,mmhtih,dssih,dssic,
     &                                     dssio
            double precision,intent(in),optional :: q2
            character(len=*),intent(in),optional :: file
         end subroutine PdfSplineSet
      end interface
      interface PdfSplineGrid
         subroutine PdfSplineGrid_twoarray(xarray,narray)
            integer,intent(in) :: narray(:)
            double precision, intent(in) :: xarray(:)
         end subroutine PdfSplineGrid_twoarray
         subroutine PdfSplineGrid_simple(xmin,n)
            integer,intent(in) :: n
            double precision, intent(in) :: xmin
         end subroutine PdfSplineGrid_simple
         subroutine PdfSplineGrid_onearray(xarray)
            double precision, intent(in) :: xarray(:)
         end subroutine PdfSplineGrid_onearray
         subroutine PdfSplineGrid_onearrayreal(xarray)
            real, intent(in) :: xarray(:)
         end subroutine PdfSplineGrid_onearrayreal
      end interface
      interface
         subroutine PdfSplineGet(iparton,nout,xout,paraout)
            integer,intent(in) :: iparton
            integer,intent(out) :: nout
            double precision,intent(out) :: xout(:), paraout(:,:)
         end subroutine PdfSplineGet
      end interface
      interface
         double complex function PdfSplineM(iparton,N)
            integer,intent(in) :: iparton
            double complex,intent(in) :: N
         end function PdfSplineM
      end interface
      interface
         double precision function PdfSplineX(iparton,x)
            integer,intent(in) :: iparton
            double precision, intent(in) :: x
         end function PdfSplineX
      end interface
      interface
         subroutine PdfSplinePara(io)
            integer, intent(in),optional :: io
         end subroutine PdfSplinePara
      end interface
      interface
         double precision function PdfSplineAlphas(q2,iord,fr2,mur,
     &                                             asmur,mc,mb,mt)
            double precision, intent(in) :: q2
            integer, intent(in), optional :: iord
            double precision, intent(in), optional :: fr2,mur,asmur,
     &                                                mc,mb,mt
         end function PdfSplineAlphas
      end interface