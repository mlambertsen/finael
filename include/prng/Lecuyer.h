/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_PRNG_LECUYER_H_
#define FINAEL_PRNG_LECUYER_H_

#include <cmath>

#include "ShuffleTable.h"
#include "Mlcg.h"



namespace finael {
namespace prng {



template<typename T, std::size_t shuffleTableSize>
class Lecuyer
{
public:
	Lecuyer()
		: mlcg1_( mlcgSeed, mlcgWarmup )
		, mlcg2_( mlcgSeed, mlcgWarmup )
		, shuffleTable_( mlcg1_ )
	{}



	T getRandomNumber()
	{
		int intOutput = shuffleTable_.use( mlcg1_.getRandomNumber() ) - mlcg2_.getRandomNumber();
		if (intOutput < 1) intOutput += maxMlcg1;
		shuffleTable_.setIndexDefiner( intOutput );

		T output = intOutput * inverseModulusMlcg1;
		return std::min( output, maxOutput );
	}



private:
	using Mlcg1 = Mlcg<2147483563, 40014>;
	using Mlcg2 = Mlcg<2147483399, 40692>;
	static constexpr std::size_t mlcgSeed = 1;
	static constexpr std::size_t mlcgWarmup = 23;
	static constexpr T inverseModulusMlcg1 = 1 / static_cast<T>( Mlcg1::modulus );
	static constexpr std::size_t maxMlcg1 = Mlcg1::modulus - 1u;
	static const T maxOutput;



	Mlcg1 mlcg1_;
	Mlcg2 mlcg2_;
	ShuffleTable<shuffleTableSize, maxMlcg1> shuffleTable_;
};



template<typename T, std::size_t shuffleTableSize>
const T Lecuyer<T, shuffleTableSize>::maxOutput = (sizeof(T) == 4) ? std::nextafterf( 1.0f, 0.0f ) : std::nextafter( 1.0, 0.0 );



} // namespace prng
} // namespace finael



#endif // FINAEL_PRNG_LECUYER_H_
