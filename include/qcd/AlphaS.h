/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUQCD_ALPHAS_H_
#define FINAEL_CUQCD_ALPHAS_H_

#include <cmath>

#include "../common/cudaHostDevice.h"
#include "../math/constants.h"
#include "../tools/typeTraits.h"

#include "constants.h"
#include "definitions.h"



namespace finael {
namespace cuQcd {



template<unsigned short numberFlavours, PQcdOrder order, typename T>
class AlphaS
{
public:
	using ScaleType = T;
	using ScalarType = cuTool::ValueTypeOrInputType_t<T>;
	static constexpr unsigned short NUMBERFLAVOURS = numberFlavours;
	static constexpr PQcdOrder ORDER = order;



	CUDA_HOST_DEVICE T operator()( T scale ) const
	{
		constexpr ScalarType LAMBDA = CONSTANTS<ScalarType>::template LAMBDA<NUMBERFLAVOURS, order>();
		constexpr ScalarType BETA0 = CONSTANTS<ScalarType>::template BETA0<NUMBERFLAVOURS>();
		constexpr ScalarType BETA1 = CONSTANTS<ScalarType>::template BETA1<NUMBERFLAVOURS>();

		T scalelog = log( scale * scale / (LAMBDA * LAMBDA) );
		return (order == PQcdOrder::LO) ? static_cast<ScalarType>( 4 ) * cuMath::CONST<ScalarType>::PI / (BETA0 * scalelog)
			: (order == PQcdOrder::NLO) ? static_cast<ScalarType>( 4 ) * cuMath::CONST<ScalarType>::PI / (BETA0 * scalelog) * (static_cast<ScalarType>( 1 ) - BETA1 * log( scalelog ) / (scalelog*BETA0*BETA0))
			: static_cast<T>( 0 );
	}
};



template<unsigned short numberFlavours, PQcdOrder order, typename T>
constexpr unsigned short AlphaS<numberFlavours, order, T>::NUMBERFLAVOURS;

template<unsigned short numberFlavours, PQcdOrder order, typename T>
constexpr PQcdOrder AlphaS<numberFlavours, order, T>::ORDER;



} // namespace cuQcd
} // namespace finael

#endif // FINAEL_CUQCD_ALPHAS_H_
