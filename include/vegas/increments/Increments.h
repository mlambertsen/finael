/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_INCREMENTS_H_
#define FINAEL_CUVEGAS_INCREMENTS_H_

#include "../../common/cudaHostDevice.h"
#include "../../tools/SmartArray.h"

#include "../InternalSettings.h"


namespace finael {
namespace cuVegas {



template<unsigned int dim, typename T, CudaHostDeviceType hostOrDevice>
class Increments
{
public:
	using type = T;
	static constexpr unsigned int dimension = dim;
	static constexpr CudaHostDeviceType hostDeviceType = hostOrDevice;



	friend class Increments<dim, T, CudaHostDeviceType::HOST>;
	#ifdef __CUDACC__
	friend class Increments<dim, T, CudaHostDeviceType::DEVICE>;
	#endif // __CUDACC__



	CUDA_HOST Increments()
		: array_()
	{}



	CUDA_HOST Increments( unsigned int incrementsPerDim )
		: array_( incrementsPerDim*dim )
	{}



	CUDA_HOST void reset()
	{
		array_.newArray( 0u );
	}



	CUDA_HOST bool isReset() const
	{
		return array_.size() == 0u;
	}



	CUDA_HOST bool isConsistent( const InternalSettings<dim>& settings ) const
	{
		return sizePerDim() == settings.incrementsPerDim;
	}



	template<CudaHostDeviceType otherDevice>
	CUDA_HOST void update( const Increments<dim, T, otherDevice>& increments )
	{
		array_ = increments.array_;
	}



	CUDA_HOST_DEVICE unsigned int sizePerDim() const
	{
		return array_.size()/dim;
	}



	CUDA_HOST_DEVICE const T& at( unsigned int dimIdx, unsigned int incrementIdx ) const
	{
		return array_[dimIdx*sizePerDim() + incrementIdx];
	}



	CUDA_HOST_DEVICE T& at( unsigned int dimIdx, unsigned int incrementIdx )
	{
		return array_[dimIdx*sizePerDim() + incrementIdx];
	}



private:
	cuTool::SmartArray<T, hostOrDevice> array_;
};



template<unsigned int dim, typename T, CudaHostDeviceType hostOrDevice>
constexpr CudaHostDeviceType Increments<dim, T, hostOrDevice>::hostDeviceType;

template<unsigned int dim, typename T, CudaHostDeviceType hostOrDevice>
constexpr unsigned int Increments<dim, T, hostOrDevice>::dimension;



} // namespace cuVegas
} // namespace finael


#endif // FINAEL_CUVEGAS_INCREMENTS_H_
