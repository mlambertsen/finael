/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_INTERPOLATION_SPLINE_ERROR_H_
#define FINAEL_INTERPOLATION_SPLINE_ERROR_H_

#include <stdexcept>
#include <string>


namespace finael {
namespace interpolation {


class SplineError : public std::out_of_range
{
public:
	SplineError( const std::string& whatArg )
		: std::out_of_range( "Spline error: " + whatArg )
	{}
};


} // interpolation
} // finael



#endif // FINAEL_INTERPOLATION_SPLINE_ERROR_H_
