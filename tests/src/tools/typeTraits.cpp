/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#define BOOST_TEST_MODULE "finael Array"
#define BOOST_TEST_DYN_LINK

#include <type_traits>

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "../../../include/tools/typeTraits.h"


template<typename T>
struct HasValueType
{
	using value_type = T;
};



struct HasNoValueType {};



BOOST_AUTO_TEST_SUITE( typeTraits )



BOOST_AUTO_TEST_CASE( valueTypeOrInputType )
{
	using namespace finael::cuTool;

	bool preserveFundamentalType = std::is_same<ValueTypeOrInputType_t<double>, double>::value;
	BOOST_CHECK( preserveFundamentalType );

	bool getValueTypeIfPresent = std::is_same<ValueTypeOrInputType_t<HasValueType<double>>, double>::value;
	BOOST_CHECK( getValueTypeIfPresent );

	bool defaultToGivenTypeIfValueTypeNotPresent = std::is_same<ValueTypeOrInputType_t<HasNoValueType>, HasNoValueType>::value;
	BOOST_CHECK( defaultToGivenTypeIfValueTypeNotPresent );
}




BOOST_AUTO_TEST_SUITE_END()
