/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#define BOOST_TEST_MODULE "finael Parton Converters"
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "../../../include/qcd/PartonIndexConverter.h"
#include "../../../include/qcd/PartonFlavourConverter.h"


BOOST_AUTO_TEST_SUITE( partonConverters )



BOOST_AUTO_TEST_CASE( areBijective )
{
	using namespace finael::cuQcd;

	BOOST_CHECK_EQUAL( 0, PartonFlavourConverter<PartonIndexConverter<0>::FLAVOUR>::INDEX);
	BOOST_CHECK_EQUAL( 1, PartonFlavourConverter<PartonIndexConverter<1>::FLAVOUR>::INDEX);
	BOOST_CHECK_EQUAL( -1, PartonFlavourConverter<PartonIndexConverter<-1>::FLAVOUR>::INDEX);
	BOOST_CHECK_EQUAL( 2, PartonFlavourConverter<PartonIndexConverter<2>::FLAVOUR>::INDEX);
	BOOST_CHECK_EQUAL( -2, PartonFlavourConverter<PartonIndexConverter<-2>::FLAVOUR>::INDEX);
	BOOST_CHECK_EQUAL( 3, PartonFlavourConverter<PartonIndexConverter<3>::FLAVOUR>::INDEX);
	BOOST_CHECK_EQUAL( -3, PartonFlavourConverter<PartonIndexConverter<-3>::FLAVOUR>::INDEX);
	BOOST_CHECK_EQUAL( 4, PartonFlavourConverter<PartonIndexConverter<4>::FLAVOUR>::INDEX);
	BOOST_CHECK_EQUAL( -4, PartonFlavourConverter<PartonIndexConverter<-4>::FLAVOUR>::INDEX);
	BOOST_CHECK_EQUAL( 5, PartonFlavourConverter<PartonIndexConverter<5>::FLAVOUR>::INDEX);
	BOOST_CHECK_EQUAL( -5, PartonFlavourConverter<PartonIndexConverter<-5>::FLAVOUR>::INDEX);
	BOOST_CHECK_EQUAL( 6, PartonFlavourConverter<PartonIndexConverter<6>::FLAVOUR>::INDEX);
	BOOST_CHECK_EQUAL( -6, PartonFlavourConverter<PartonIndexConverter<-6>::FLAVOUR>::INDEX);
}



BOOST_AUTO_TEST_SUITE_END()
