/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_PRNG_H_
#define FINAEL_CUVEGAS_PRNG_H_

#include "../../common/cudaHostDevice.h"

#include "PrngTypeSetter.h"


namespace finael {
namespace cuVegas {



template<unsigned int dim, typename T, CudaHostDeviceType hostDeviceType, PrngFlag prngFlag>
class Prng
{
public:
	typename PrngTypeSetter<prngFlag>::template KernelPrngType<dim, T, hostDeviceType> kernelPrng;



	CUDA_HOST void prepareForIntegration( const unsigned int numberSamples )
	{
		kernelPrng.prepareForIntegration( prng, numberSamples );
	}



private:
	typename PrngTypeSetter<prngFlag>::template PrngType<T> prng;
};



} // namespace cuVegas
} // namespace finael

#endif // FINAEL_CUVEGAS_PRNG_H_
