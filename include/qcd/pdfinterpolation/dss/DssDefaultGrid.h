/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_QCD_PDF_INTERPOLATION_DSS_DEFAULT_GRID_H_
#define FINAEL_QCD_PDF_INTERPOLATION_DSS_DEFAULT_GRID_H_

#include <array>



namespace finael {
namespace qcd {
namespace pdfInterpolation {



template<typename T>
class DssDefaultGrid
{
public:
	static constexpr std::size_t SIZE = 35;
	static constexpr std::array<T,SIZE> POINTS = {{0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.095, 0.1, 0.125, 0.15, 0.175, 0.2, 0.225, 0.25, 0.275, 0.3, 0.325, 0.35, 0.375, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.93, 1.0}};
};

template<typename T>
constexpr std::size_t DssDefaultGrid<T>::SIZE;

template<typename T>
constexpr std::array<T,DssDefaultGrid<T>::SIZE> DssDefaultGrid<T>::POINTS;



} // namespace pdfInterpolation
} // namespace qcd
} // namespace finael



#endif // FINAEL_QCD_PDF_INTERPOLATION_DSS_DEFAULT_GRID_H_
