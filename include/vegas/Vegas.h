/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_VEGAS_H_
#define FINAEL_CUVEGAS_VEGAS_H_

#include <ostream>
#include <utility>
#include <type_traits>

#include "../common/cudaHostDevice.h"
#include "../common/cudaError.h"

#include "increments/Increments.h"
#include "increments/IncrementsAdjuster.h"
#include "integrationKernel.h"
#include "integrandTypes.h"
#include "InternalSettings.h"
#include "KernelRelevantSettings.h"
#include "optionFlags.h"
#include "Parameters.h"
#include "printer/Printer.h"
#include "prng/Prng.h"
#include "prng/PrngSelector.h"
#include "reducer/Reducer.h"
#include "ResultsConstructor.h"
#include "Samples.h"
#include "Settings.h"


namespace finael {
namespace cuVegas {



template<unsigned int dim, typename T = double, CudaHostDeviceType hostOrDevice = CudaHostDeviceType::HOST, PrngFlag prngInputFlag = PrngFlag::DEFAULT>
class Vegas
{
public:
	using type = T;
	using Volume = VolumeType<T, dim>;
	using IntegrandPoint = IntegrandPointType<T, dim>;
	static constexpr PrngFlag prngFlag = PrngSelector<hostOrDevice, prngInputFlag>::prngFlag;
	static constexpr unsigned int dimension = dim;
	static constexpr CudaHostDeviceType hostDeviceType = hostOrDevice;



	CUDA_HOST Vegas()
		: settings_()
		, internalSettings_()
		, accumulatedSums_()
		, prng_()
		, increments_()
		, printer_()
	{}



	template<class Functor>
	CUDA_HOST Results<T> integrate( const Functor& functor )
	{
		increments_.reset();
		return integrateKeepingOldGrid( functor );
	}



	template<class Functor>
	CUDA_HOST Results<T> integrateKeepingOldGrid( const Functor& functor )
	{
		accumulatedSums_.reset();
		return integrateKeepingOldGridAndResults( functor );
	}



	template<class Functor>
	CUDA_HOST Results<T> integrateKeepingOldGridAndResults( const Functor& functor )
	{
		internalSettings_.update( settings_ );
		IncrementsAdjuster::match( increments_, internalSettings_ );

		return integrateKeepingAllSettings( functor );
	}



	template<class Functor>
	CUDA_HOST Results<T> integrateKeepingAllSettings( const Functor& functor )
	{
		if (!internalSettings_.isInitialized) integrate( functor );

		printer_.printSettings( settings_, internalSettings_, prngFlag, functor.volume );

		performIntegration( functor );

		return ResultsConstructor<T>::constructResults( accumulatedSums_ );
	}



	CUDA_HOST void setNumberSamples( unsigned int numberSamples )
	{
		settings_.numberSamples = numberSamples;
	}



	CUDA_HOST void setNumberIterations( unsigned int numberIterations )
	{
		settings_.numberIterations = numberIterations;
	}



	CUDA_HOST void setStrategy( StrategyFlag strategy )
	{
		settings_.strategy = strategy;
	}



	CUDA_HOST void setAdaptionRate( T adaptionRate )
	{
		settings_.adaptionRate = adaptionRate;
	}



	CUDA_HOST void setReduction( ReductionFlag reduction )
	{
		settings_.reduction = reduction;
	}



	CUDA_HOST void setBlockDim( unsigned int blockDim )
	{
		settings_.blockDim = blockDim;
	}



	CUDA_HOST void setVerbosity( VerbosityFlag verbosity )
	{
		printer_.setVerbosity( verbosity );
	}



	CUDA_HOST void setOutputStream( std::ostream& stream )
	{
		printer_.setOutputStream( stream );
	}



	CUDA_HOST void setPrintPrecision( T precision )
	{
		printer_.setPrecision( precision );
	}



private:
	static_assert( std::is_floating_point<T>::value, "fineal::cuVegas::Vegas second template parameter has to be a floating point type!" );



	Settings<T> settings_;
	InternalSettings<dim> internalSettings_;
	AccumulatedSums<T> accumulatedSums_;
	Prng<dim, T, hostDeviceType, prngFlag> prng_;
	Increments<dim, T, CudaHostDeviceType::HOST> increments_;
	Printer<dim, T> printer_;



	template<class Functor>
	CUDA_HOST void performIntegration( const Functor& functor )
	{
		if (internalSettings_.reduction == ReductionFlag::CLASSIC)
		{
			return performIntegrationFixedReduction<ReductionFlag::CLASSIC>( functor );
		}
		else // if (internalSettings_.reduction == ReductionFlag::REFINED)
		{
			return performIntegrationFixedReduction<ReductionFlag::REFINED>( functor );
		}
	}



	template<ReductionFlag reductionMode, class Functor>
	CUDA_HOST void performIntegrationFixedReduction( const Functor& functor )
	{
		KernelRelevantSettings<T, reductionMode> kernelRelevantSettings( internalSettings_, functor.volume );
		Samples<T, hostDeviceType, reductionMode> samples( internalSettings_.numberSamples(), dim );

		for (auto iterationIdx = 0u; iterationIdx < settings_.numberIterations; ++iterationIdx)
		{
			performSingleIntegration( functor, kernelRelevantSettings, samples );
		}
	}



	template<ReductionFlag reductionMode, class Functor>
	CUDA_HOST void performSingleIntegration( const Functor& functor, const KernelRelevantSettings<T, reductionMode>& kernelRelevantSettings, Samples<T, hostDeviceType, reductionMode>& samples )
	{
		calculateSamples( functor, kernelRelevantSettings, samples );

		Reducer<dim, T, hostDeviceType, reductionMode> reducer( internalSettings_ );
		auto estimateSumAndWeightsManager = reducer( samples, printer_ );

		accumulatedSums_.add( estimateSumAndWeightsManager.first );

		printer_.printIterationInfos( accumulatedSums_, estimateSumAndWeightsManager.first, increments_ );

		estimateSumAndWeightsManager.second.smoothen();
		IncrementsAdjuster::improve( increments_, estimateSumAndWeightsManager.second, settings_.adaptionRate );
	}



	template<ReductionFlag reductionMode, class Functor>
	CUDA_HOST Samples<T, hostDeviceType, reductionMode> calculateSamples( const Functor& functor, const KernelRelevantSettings<T, reductionMode>& kernelRelevantSettings, Samples<T, hostDeviceType, reductionMode>& samples )
	{
		Increments<dim, T, hostDeviceType> increments;
		increments.update( increments_ );
		prng_.prepareForIntegration( samples.size() );
		unsigned int numberBlocks = (samples.size() - 1) / internalSettings_.blockDim + 1;
		if (hostDeviceType == CudaHostDeviceType::HOST)
		{
			integrationKernel::calculateSamplesHost( numberBlocks, internalSettings_.blockDim, functor, samples, prng_.kernelPrng, increments, kernelRelevantSettings );
		}
		#ifdef __CUDACC__
		else if (hostDeviceType == CudaHostDeviceType::DEVICE)
		{
			integrationKernel::calculateSamplesDevice<<<numberBlocks, internalSettings_.blockDim>>>( functor, samples, prng_.kernelPrng, increments, kernelRelevantSettings );
			CHECK_CUDA_ERROR( "Error in integrationKernel::calculateSamplesDevice" );
		}
		#endif

		return samples;
	}
};



template<unsigned int dim, typename T, CudaHostDeviceType hostOrDevice, PrngFlag prngInputFlag>
constexpr PrngFlag Vegas<dim, T, hostOrDevice, prngInputFlag>::prngFlag;

template<unsigned int dim, typename T, CudaHostDeviceType hostOrDevice, PrngFlag prngInputFlag>
constexpr unsigned int Vegas<dim, T, hostOrDevice, prngInputFlag>::dimension;

template<unsigned int dim, typename T, CudaHostDeviceType hostOrDevice, PrngFlag prngInputFlag>
constexpr CudaHostDeviceType Vegas<dim, T, hostOrDevice, prngInputFlag>::hostDeviceType;


} // namespace cuVegas
} // namespace finael



#endif // FINAEL_CUVEGAS_VEGAS_H_
