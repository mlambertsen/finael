! Copyright (c) 2014-2017 Martin Lambertsen
! License: MIT (https://opensource.org/licenses/MIT)

      double precision function mmht14pdf_getter(iparton, iset, infile,
     &                                            filesize, x, q)
!-----------------------------------------------------------------------
      implicit none
      ! input/output variables
      integer, intent(in) :: iparton, iset, filesize
      character(len=100), intent(in) :: infile
      double precision, intent(in) :: x
      double precision, intent(in) :: q

      ! intern variables
      double precision getonepdf
      character(len=filesize) :: file
!-----------------------------------------------------------------------

      ! write file into a new character - otherwise is will have random
      ! behaviour for the characters coming after the real filename
      file(1:filesize) = infile(1:filesize)

      mmht14pdf_getter = getonepdf(file ,iset, x, q, iparton)

      return
      end function mmht14pdf_getter
