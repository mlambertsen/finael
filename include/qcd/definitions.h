/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUQCD_DEFINITIONS_H_
#define FINAEL_CUQCD_DEFINITIONS_H_



namespace finael {
namespace cuQcd {



enum class PartonFlavour {G, U, UBAR, D, DBAR, S, SBAR, C, CBAR, B, BBAR, T, TBAR}; // sequence is used explicit in PartonDistribution.h!
enum class FlavourMode {ONE, TWO, THREE, FOUR, FIVE, SIX};
enum class Hadron {PION, KAON, PROTON, ALL};
enum class PQcdOrder {LO, NLO};



} // namespace cuQcd



namespace qcd {


using cuQcd::PartonFlavour;
using cuQcd::FlavourMode;
using cuQcd::Hadron;
using cuQcd::PQcdOrder;



} // namespace qcd
} // namespace finael


#endif // FINAEL_CUQCD_DEFINITIONS_H_
