/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_PRNG_CURAND_WRAPPER_H_
#define FINAEL_PRNG_CURAND_WRAPPER_H_

// Problem: 1.0 is possible value -> has to be excluded later on, if necessary

// It is possible to compile this class with g++ compiler, but not to use it!

#include <iostream>
#include <cstdlib>
#include "../common/cudaHostDevice.h"
#include "../tools/SmartArray.h"
#ifdef __CUDACC__
#include <curand.h>
#endif // __CUDACC__



namespace finael {
namespace prng {



template<typename T>
class CurandWrapper
{
public:
#ifdef __CUDACC__
	CUDA_HOST CurandWrapper( void )
		: randomNumbers_( 0 )
	{
		curandCreateGenerator( &curandGenerator_, CURAND_RNG_PSEUDO_MTGP32 );
	}



	CUDA_HOST ~CurandWrapper( void )
	{
		curandDestroyGenerator( curandGenerator_ );
	}



	CUDA_HOST cuTool::SmartArray<T, CudaHostDeviceType::DEVICE> getRandomNumbers( const unsigned int numberRandomNumbers )
	{
			if (randomNumbers_.size() != numberRandomNumbers)
			{
				randomNumbers_.newArray( numberRandomNumbers );
			}

			generateRandomNumbers();

			return randomNumbers_;
	}



private:
	cuTool::SmartArray<T, CudaHostDeviceType::DEVICE> randomNumbers_;
	curandGenerator_t curandGenerator_;



	CUDA_HOST void generateRandomNumbers()
	{
		// all valid types use specialized template functions
		std::cout << "Curand cannot generate random numbers of requested type\nstop program \n";
		std::exit( EXIT_FAILURE );
	}



#else // __CUDACC__



	CUDA_HOST cuTool::SmartArray<T, CudaHostDeviceType::HOST> getRandomNumbers( const unsigned int /*numberRandomNumbers */ )
	{

		std::cout << "Curand does not work on host\nstop program \n";
		std::exit( EXIT_FAILURE );
	}



#endif // __CUDACC__
};



#ifdef __CUDACC__
template<>
void CurandWrapper<double>::generateRandomNumbers()
{
	curandGenerateUniformDouble( curandGenerator_, &randomNumbers_[0], randomNumbers_.size() );
}



template<>
void CurandWrapper<float>::generateRandomNumbers()
{
	curandGenerateUniform( curandGenerator_, &randomNumbers_[0], randomNumbers_.size() );
}


#endif // __CUDACC__



} // namespace prng
} // namespace finael


#endif // FINAEL_PRNG_CURAND_WRAPPER_H_
