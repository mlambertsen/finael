/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_PRNG_TYPE_SETTER_H_
#define FINAEL_CUVEGAS_PRNG_TYPE_SETTER_H_

#include "../../common/cudaHostDevice.h"

#include "KernelCurand.h"
#include "KernelLecuyer.h"
#include "KernelPhilox.h"
#include "../optionFlags.h"


namespace finael {
namespace cuVegas {



template<PrngFlag prng>
class PrngTypeSetter {};



template<>
class PrngTypeSetter<PrngFlag::LECUYER>
{
private:
	static constexpr std::size_t shuffleBoardSize = 32;
public:
	template<typename T>
	using PrngType = prng::Lecuyer<T,shuffleBoardSize>;

	template<unsigned int dim, typename T, CudaHostDeviceType hostDeviceType>
	using KernelPrngType = KernelLecuyer<dim, T, shuffleBoardSize, hostDeviceType>;
};



template<>
class PrngTypeSetter<PrngFlag::CURAND>
{
public:
	template<typename T>
	using PrngType = prng::CurandWrapper<T>;

	template<unsigned int dim, typename T, CudaHostDeviceType hostDeviceType>
	using KernelPrngType = KernelCurand<dim, T, hostDeviceType>;
};



template<>
class PrngTypeSetter<PrngFlag::PHILOX>
{
public:
	template<typename T>
	using PrngType = bool; // not relevant, philox has complete functionality already in VegasKernelPhilox

	template<unsigned int dim, typename T, CudaHostDeviceType hostDeviceType>
	using KernelPrngType = KernelPhilox<dim, T, hostDeviceType>;
};




} // namespace cuVegas
} // namespace finael



#endif // FINAEL_CUVEGAS_PRNG_TYPE_SETTER_H_
