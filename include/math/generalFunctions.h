/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUMATH_GENERAL_FUNCTIONS_H_
#define FINAEL_CUMATH_GENERAL_FUNCTIONS_H_

#include <cmath>

#include "../common/cudaHostDevice.h"

#include "Complex.h"
#include "constants.h"
#include "definitions.h"



namespace finael {
namespace cuMath {



template<Sign sign, typename T>
CUDA_HOST_DEVICE inline T addOrSubstract( const T& value1, const T& value2 )
{
	if (sign == Sign::PLUS) return value1 + value2;
	else return value1 - value2;
}



//------------------------------------------
// pow functions for integer exponents
//------------------------------------------
template<typename T, unsigned int N>
struct Pow
{
	CUDA_HOST_DEVICE static constexpr T pow( const T& base )
	{
		return Pow<T, N - 1>::pow( base ) * base;
	}
};



template<typename T>
struct Pow<T, 1>
{
	CUDA_HOST_DEVICE static constexpr T pow( const T& base )
	{
		return base;
	}
};



template<typename T>
struct Pow<T, 0>
{
	CUDA_HOST_DEVICE static constexpr T pow( const T& /* base */ )
	{
		return static_cast<T>( 1 );
	}
};



template<typename T>
CUDA_HOST_DEVICE T pow( const T& base, const unsigned int exponent)
{
	T result = static_cast<T>( 1 );
	for (unsigned int i = 0; i < exponent; ++i)
	{
		result *= base;
	}
	return result;
}



template<typename T>
CUDA_HOST_DEVICE T pow( const T& base, const int exponent )
{
	if (exponent >= 0) return pow( base, static_cast<unsigned int>( exponent ) );
	else return static_cast<T>(1.0)/pow( base, static_cast<unsigned int>( -exponent ) );
}



//------------------------------------------
// cospi and sinpi for arbitrary complex classes to use special cuda functions
//------------------------------------------
template<template<typename> class Complex, typename T>
CUDA_HOST_DEVICE inline Complex<T> sinpi( const Complex<T>& complex )
{
	#if __CUDACC__
		if (sizeof(T) == 4)	return Complex<T>( ::sinpif( complex.real() ) * ::coshf( CONST<T>::PI * complex.imag() ), ::cospif( complex.real() ) * ::sinhf( CONST<T>::PI * complex.imag() ) );
		else return Complex<T>( ::sinpi( complex.real() ) * ::cosh( CONST<T>::PI * complex.imag() ), ::cospi( complex.real() ) * ::sinh( CONST<T>::PI * complex.imag() ) );
	#else
		return Complex<T>( ::sin( CONST<T>::PI * complex.real() ) * ::cosh( CONST<T>::PI * complex.imag() ), ::cos( CONST<T>::PI * complex.real() )*::sinh( CONST<T>::PI * complex.imag() ) );
	#endif
}


template<template<typename> class Complex, typename T>
CUDA_HOST_DEVICE inline Complex<T> cospi( const Complex<T>& complex )
{
	#if __CUDACC__
		if (sizeof(T) == 4)	return Complex<T>( ::cospif( complex.real() ) * ::coshf( CONST<T>::PI * complex.imag() ), -::sinpif( complex.real() ) * ::sinhf( CONST<T>::PI * complex.imag() ) );
		else return Complex<T>( ::cospi( complex.real() ) * ::cosh( CONST<T>::PI * complex.imag() ), -::sinpi( complex.real() ) * ::sinh( CONST<T>::PI * complex.imag() ) );
	#else
		return Complex<T>( ::cos( CONST<T>::PI * complex.real() ) * ::cosh( CONST<T>::PI * complex.imag() ), -::sin( CONST<T>::PI * complex.real() ) * ::sinh( CONST<T>::PI * complex.imag() ) );
	#endif
}



} // namespace cuMath
} // namespace finael

#endif // FINAEL_CUMATH_GENERAL_FUNCTIONS_H_
