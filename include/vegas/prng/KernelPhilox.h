/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_KERNEL_PHILOX_H_
#define FINAEL_CUVEGAS_KERNEL_PHILOX_H_

#include "../../common/cudaHostDevice.h"
#include "../../prng/PhiloxWrapper.h"


namespace finael {
namespace cuVegas {



template<unsigned int dim, typename T, CudaHostDeviceType hostDeviceType>
class KernelPhilox
{
public:
	CUDA_HOST void prepareForIntegration( const bool, const unsigned int /* numberSamples */ )
	{
		philox_.prepareForKernel();
	}



	CUDA_HOST_DEVICE T getRandomNumber( const unsigned int index )
	{
		return philox_.getRandomNumber( index );
	}



private:
	prng::PhiloxWrapper<T> philox_;
};



} // namespace cuVegas
} // namespace finael



#endif // FINAEL_CUVEGAS_KERNEL_PHILOX_H_
