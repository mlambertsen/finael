/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_PARAMETERS_H_
#define FINAEL_CUVEGAS_PARAMETERS_H_



namespace finael {
namespace cuVegas {



struct Parameters
{
	static constexpr unsigned int MAXIMUMNUMBERINCREMENTS = 50u;
};



constexpr unsigned int Parameters::MAXIMUMNUMBERINCREMENTS;


} // namespace cuVegas
} // namespace finael



#endif // FINAEL_CUVEGAS_PARAMETERS_H_
