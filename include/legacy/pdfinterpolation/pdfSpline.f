!_______________________________________________________________________
!
! ####  ###   ####  ####  ####  #     #  #   #  ####
! #  #  #  #  #     #     #  #  #     #  ##  #  #
! ###   #   # ###   ####  ###   #     #  # # #  ###
! #     #  #  #        #  #     #     #  #  ##  #           Version 0.33
! #     ###   #     ####  #     ####  #  #   #  ####          18.06.2015
! _______________________________________________________________________
! 
! Calculating splines of CT12 PDFs, MMHT14 PDFs and DSS07 FFs
!             in x and in Mellin moment N space
!             and giving coefficients to implement Mellin moments in a
!             useful way
!_______________________________________________________________________
!
! References:
!
!    PDFSPLINE: arXiv:XXXX.XXXXX
!
!    Using CT12 PDFs: arXiv:1302.6246
!    Using MMHT14 PDFs: arXiv:1412.3989
!    Using MMHT14 PDFs and alphas-routine: arXiv:hep-ph/0408244
!    Using DSS07 FFs: arXiv:hep-ph/0703242
! _______________________________________________________________________
!
! WARNING: Please note, that the used PDF/FF sets are given in the
!          following intervals
!                CT12:   [1.d-8 ; 1d0]
!                MMHT14: [1.d-6 ; 1d0]
!                DSS07:  [1.d-2 ; 1d0]
!          For smaller values of x, extrapolations procedures are used
!          by the corresponding code of CT12, MMHT14 or DSS07.
!_______________________________________________________________________
!
! Known Bug: A transformation back into x space gives a wrong result
!            for the smallest point of the calculated grid
!_______________________________________________________________________
!
! To use PdfSpline, the calling routine/program has to include the
! interfaces, given in the file 'pdfSplineInterface.f', so for example
! by
!
!          program my_program
!          implicit none
!          include 'pdfSplineInterface.f'
!          ...
!
! If this is done, one has access to the following list of functions
! and subroutines
!
!
!        CALL PdfSplineGRID(...)
!             to specify a grid used to calculate the splines
!             The arguments can be
!             (1): CALL PdfSplineGRID(XARRAY)
!                     where
!                        XARRAY: Explicit array (double precision)
!                     The last entry has to be 1.d0!
!             (2): CALL PdfSplineGRID(N, XMIN)
!                     where
!                        N: Number of grid points (integer)
!                        XMIN: Minimum x of the grid (double precision)
!                     The grid points will be distributed
!                     logarithmically in the intervall [XMIN,1.d0]
!             (3): CALL PdfSplineGRID(XARRAY, NARRAY)
!                     where
!                        XARRAY: Array of x-values defining intervals
!                                (double precision)
!                        NARRAY: Number of grid points for the intervals
!                                defined by XARRAY (integer)
!                     Example: XARRAY = (/1.d-1,1.d-2/)
!                              NARRAY = (/10,30/)
!                          --> 10 points in [1.d-1,1.d-2]
!                              30 points in (1.d-2,1.d0]
!                     The points  are logarithmically distributed in
!                     each intervall
!             The maximum number of grid points is NMAX = 1000. To
!             increase NMAX, simple change it at the beginning of this
!             code.
!
!
!
!        CALL PdfSplineSET(ST, ISET, MMHTIH, DSSIH, DSSIC, DSSIO, Q2,
!                          FILE)
!             to choose options. These are
!                ST: Spline type (integer)
!                      = 0 natural spline
!                      = 1 first derivatives at the ends of the grid
!                          are fixed by numerical differntiation using
!                          the interpolation routine of underlying ISET
!                      = 2 as ST=1 but using the grid for the numerical
!                          differentiation
!                ISET: Interpolated function (integer)
!                        = 1 for CT12 pdfs
!                        = 2 for MMHT14 pdfs
!                        = 3 for DSS07 ffs
!                      The corresponding grids are required!
!                      The corresponding standalone codes are included
!                      in this file.
!                MMHTIH: Only used for ISET = 2 (integer)
!                        Corresponds to parameter "ih" of MMHT14 set
!                DSSIH: Only used for ISET = 3 (integer)
!                       Corresponds to parameter "ih" of DSS set
!                DSSIC: Only used for ISET = 3 (integer)
!                       Corresponds to parameter "ic" of DSS set
!                DSSIO: Only used for ISET = 3 (integer)
!                       Corresponds to parameter "io" of DSS set
!                Q2: Scale in GeV**2 (double precision)
!                FILE: Specification, if using ISET = 1 or 2
!                                     (character of maximum length 100)
!                        Used to call data from CT12 or MMHT14 grids,
!                        therefore the same character as for a direct
!                        call of this routines
!             All parameters are OPTIONAL, if they are not set by the
!             user, the program will use
!                ST = 0
!                ISET = 1
!                MMHTIH = 0
!                DSSIH = 1
!                DSSIC = 0
!                DSSIO = 1
!                Q2 = 1000.d0
!                FILE = 'ct10n00'
!             If the routine PdfSplineGRID was not called yet,
!             PdfSplineSET will generate the grid used by the codes of
!             CT12/MMHT14/DSS07.
!
!
!
!        CALL PdfSplinePARA(IO)
!             Prints the current parameters into i/o unit IO. If unit IO
!             is not open or not given (the argument is optional), it is
!             chosen to be terminal output.
!             If a grid is already defined, it prints also the full
!             grid.
!             The argument is
!                IO: i/o unit (integer)
!
!
!        CALL PdfSplineGET(IPARTON, NOUT, XOUT, PARAOUT)
!             To get parameter for Mellin moments of the interpolated
!             functions. A prior call of PdfSplineSET() is required.
!             The arguments are
!                IPARTON: Defining the parton flavour (integer)
!                              (-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5)
!                          for (bb, cb, sb, db, ub, g, u, d, s, c, b)
!         Output NOUT: Integer Output - number of points used in the
!                                       chosen grid
!         Output XOUT: Array of double precisions; size NOUT
!                      Used grid points in x space
!         Output PARAOUT: 2-dim Array of double precisions;
!                         size (4,NOUT); are parameter g_{j,i},
!                         see arXiv:XXXX.XXXXX eq. (X)
!
!
!
!        PdfSplineX(IPARTON, X)
!             Evaluates splines directly in x space. A prior call of 
!             PdfSplineSET() is required.
!             The arguments are
!                IPARTON: As before
!                X: Bjorken x (double precision)
!
!
!
!        PdfSplineM(IPARTON, N)
!             Evaluates splines directly in N space.  A prior call of
!             PdfSplineSET() is required.
!             The arguments are
!                IPARTON: As before
!                N: Mellin variable (double complex)
!
!
!
! Additional we provide an alpha_s routine, that will fit to the chosen
! pdf/ff set:
!
!        PdfSplineALPHAS(Q2,IORD,FR2,MUR,ASMUR,MC,MB,MT)
!             Gives strong coupling constant, using the alphas routine
!             from Andreas Vogt's QCD-PEGASUS package and consulted by
!             MMHT14 if the spline has ISET = 1 and using the CT12Alphas
!             routine otherwise.
!                Recommended:
!                   Q2: Scale in GeV**2
!                Optional (and only effecting the MMHT-case, the
!                          standard/initial value is given in brackets):
!                   IORD: Perturbative order, N^mLO, m=0,1,2,3 (=2)
!                   FR2: Ratio of mu_f**2 to mu_r**2 (1.d0)
!                   MUR: Input mu_r in GeV (1.d0)
!                   ASMUR: Input value of alphas_s at mu_r (0.5d0)
!                   MC: Charm quark mass (1.4d0)
!                   MB: Bottom quark mass (4.75d0)
!                   MT: Top quar mass (1.d10)
!_______________________________________________________________________


!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      module PdfSplineModule
! Module that contains the spline function not to use by the user 
! directly and global varialbles
!-----------------------------------------------------------------------
      implicit none
      save
      ! maximum number of grid points
      integer, parameter :: nmax = 1000

      ! standard values if not specified by user
      integer :: st_int = 0 ! natural spline
      integer :: iset_int = 1 ! CT12
      integer :: mmhtih_int = 0 ! MMHT central pdfs
      integer :: dssih_int = 1 ! DSS hadron type: pion
      integer :: dssic_int = 0 ! DSS hadron charge: average
      integer :: dssio_int = 1 ! DSS order: NLO
      double precision :: q2_int = 1000.d0
      character(len=100) :: file_int = 'ct10n.00' ! CT12 central NLO
      
      ! Further variables
      integer :: ngrid = -1 ! number of grid points
      double precision :: xdat(1:nmax) ! x-grid
      double precision :: fdat(-5:5,1:nmax) ! function values on grid
      double precision :: pxdat(-5:5,1:nmax)
      double precision :: pmdat(-5:5,0:3,1:nmax)
      logical :: grid_exist = .false.
      logical :: spline_exist = .false.
!-----------------------------------------------------------------------

      contains

      subroutine PdfSplineCalc
! Contructs spline
!-----------------------------------------------------------------------
      implicit none
      integer i, j, k, is
      double precision ct12pdf, u(1:ngrid), v(1:ngrid), w(1:ngrid),
     & a(0:3,1:ngrid), dss(-5:5), getonepdf, eps, df1, dfn
      character(len=100) pf
!-----------------------------------------------------------------------

      ! step size for numerical derivatives of functions
      eps = 1.d-10

      select case (iset_int)
         case(1) ! CTEQ
            do j = -5,5 ! for every flavour
               do i = 1,ngrid
                  fdat(j,i) = ct12pdf(j,xdat(i),dsqrt(q2_int))
                  u(i) = xdat(i) ! used format for subroutine calcspline
                  v(i) = fdat(j,i)
               enddo
               ! numerical derivatives at end points using CT12
               ! interpolation
               df1 = (ct12pdf(j,u(1)+eps,dsqrt(q2_int))-v(1))/eps
               dfn = (v(ngrid)-ct12pdf(j,u(ngrid)-eps,dsqrt(q2_int)))
     &                /eps
               ! calculate spline parameter in x space
               call calcspline (st_int,u,v,df1,dfn,w)
               ! calculate spline parameter in N space
               call mellinspline(u,v,w,a)
               do i = 1,ngrid
                  pxdat(j,i) = w(i) ! write spline parameter in x space
                                    ! for flavour j
                  do k = 0,3
                     pmdat(j,k,i) = a(k,i) ! write spline parameter in N
                                           ! space for flavour j
                  enddo
               enddo
            enddo
         case(2) ! MMHT
            pf = trim(file_int) ! hold expressions short and clear
            is = mmhtih_int

            do i = 1,ngrid
               do j = 3,5 ! MMHT have different numeration of flavours
                  fdat(j,i) = 1.d0/xdat(i)*
     &               getonepdf(pf,is,xdat(i),dsqrt(q2_int),j)
                  fdat(-j,i) = 1.d0/xdat(i)*
     &               getonepdf(pf,is,xdat(i),dsqrt(q2_int),-j)
               enddo
               fdat(-2,i) = 1.d0/xdat(i)*
     &               getonepdf(pf,is,xdat(i),dsqrt(q2_int),-1)
               fdat(-1,i) = 1.d0/xdat(i)*
     &               getonepdf(pf,is,xdat(i),dsqrt(q2_int),-2)
               fdat(0,i) = 1.d0/xdat(i)*
     &               getonepdf(pf,is,xdat(i),dsqrt(q2_int),0)
               fdat(1,i) = 1.d0/xdat(i)*
     &              (getonepdf(pf,is,xdat(i),dsqrt(q2_int),8)
     &            + getonepdf(pf,is,xdat(i),dsqrt(q2_int),-2))
               fdat(2,i) = 1.d0/xdat(i)*
     &              (getonepdf(pf,is,xdat(i),dsqrt(q2_int),7)
     &            + getonepdf(pf,is,xdat(i),dsqrt(q2_int),-1))
            enddo
            do j = -5,5
               do i = 1,ngrid
                  u(i) = xdat(i) ! used format for subroutine calcspline
                  v(i) = fdat(j,i)
               enddo
               ! numerical derivatives at end points using MMHT14
               ! interpolation
               select case (j)
                  case(-5:-3,0,3:5)
                     df1 = (getonepdf(pf,is,u(1)+eps,dsqrt(q2_int),j)
     &                     /(u(1)+eps)-fdat(j,1))/eps
                     dfn = (fdat(j,ngrid)-
     &                     getonepdf(pf,is,u(ngrid)-eps,dsqrt(q2_int),j)
     &                      /(u(ngrid)-eps))/eps
                  case(-2)
                     df1 = (getonepdf(pf,is,u(1)+eps,dsqrt(q2_int),-1)
     &                     /(u(1)+eps)-fdat(j,1))/eps
                     dfn = (fdat(j,ngrid)-
     &                    getonepdf(pf,is,u(ngrid)-eps,dsqrt(q2_int),-1)
     &                      /(u(ngrid)-eps))/eps
                  case(-1)
                     df1 = (getonepdf(pf,is,u(1)+eps,dsqrt(q2_int),-2)
     &                     /(u(1)+eps)-fdat(j,1))/eps
                     dfn = (fdat(j,ngrid)-
     &                    getonepdf(pf,is,u(ngrid)-eps,dsqrt(q2_int),-2)
     &                      /(u(ngrid)-eps))/eps
                  case(1)
                     df1 = ((getonepdf(pf,is,u(1)+eps,dsqrt(q2_int),8)
     &                      +getonepdf(pf,is,u(1)+eps,dsqrt(q2_int),-2))
     &                     /(u(1)+eps)-fdat(j,1))/eps
                     dfn = (fdat(j,ngrid)-
     &                    (getonepdf(pf,is,u(ngrid)-eps,dsqrt(q2_int),8)
     &                  +getonepdf(pf,is,u(ngrid)-eps,dsqrt(q2_int),-2))
     &                     /(u(ngrid)-eps))/eps
                  case(2)
                     df1 = ((getonepdf(pf,is,u(1)+eps,dsqrt(q2_int),7)
     &                      +getonepdf(pf,is,u(1)+eps,dsqrt(q2_int),-1))
     &                     /(u(1)+eps)-fdat(j,1))/eps
                     dfn = (fdat(j,ngrid)-
     &                    (getonepdf(pf,is,u(ngrid)-eps,dsqrt(q2_int),7)
     &                  +getonepdf(pf,is,u(ngrid)-eps,dsqrt(q2_int),-1))
     &                     /(u(ngrid)-eps))/eps
               end select
               ! calculate spline parameter in x space
               call calcspline (st_int,u,v,df1,dfn,w)
               ! calculate spline parameter in N space
               call mellinspline(u,v,w,a)
               do i = 1,ngrid
                  pxdat(j,i) = w(i) ! write spline parameter in x space
                                    ! for flavour j
                  do k = 0,3
                     pmdat(j,k,i) = a(k,i) ! write spline parameter in N
                                           ! space for flavour j
                  enddo
               enddo
            enddo
         case(3) ! DSS
            do i = 1,ngrid
               ! all flavours are called at once
               call fDSS(dssih_int,dssic_int,dssio_int,xdat(i),q2_int,
     &                   dss(1),dss(-1),dss(2),dss(-2),dss(3),dss(-3),
     &                   dss(4),dss(5),dss(0))
               dss(-4) = dss(4)
               dss(-5) = dss(5)
               ! adjust the output from x*f to f
               do j = -5,5
                  fdat(j,i) = dss(j)/xdat(i)
               enddo
            enddo
            do j = -5,5
               do i = 1,ngrid
                  u(i) = xdat(i) ! used format for subroutine calcspline
                  v(i) = fdat(j,i)
               enddo
               ! numerical derivatives at end points using DSS07
               ! interpolation
               call fDSS(dssih_int,dssic_int,dssio_int,u(1)+eps,q2_int,
     &                   dss(1),dss(-1),dss(2),dss(-2),dss(3),dss(-3),
     &                   dss(4),dss(5),dss(0))
               df1 = (dss(j)/(u(1)+eps)-v(1))/eps
               call fDSS(dssih_int,dssic_int,dssio_int,u(ngrid)-eps,
     &                   q2_int,dss(1),dss(-1),dss(2),dss(-2),dss(3),
     &                   dss(-3),dss(4),dss(5),dss(0))
               dfn = (v(ngrid)-dss(j)/(u(ngrid)-eps))/eps
               ! calculate spline parameter in x space
               call calcspline (st_int,u,v,df1,dfn,w)
               ! calculate spline parameter in N space
               call mellinspline(u,v,w,a)
               do i = 1,ngrid
                  pxdat(j,i) = w(i) ! write spline parameter in x space
                                    ! for flavour j
                  do k = 0,3
                     pmdat(j,k,i) = a(k,i) ! write spline parameter in N
                                           ! space for flavour j
                  enddo
               enddo
            enddo
         case default
            stop 'Error by PdfSpline - Error 0'
      end select

      return
      end subroutine PdfSplineCalc


      subroutine mellinspline(x,f,parain,paraout)
! Calculates parameters of spline in N-space
! Input: Number of grid points n, x-grid, data-grid (f), parameter from
!        subroutine spline (parain)
! Output: Parameters for Splines in N-space, such that
!     spline(N) = sum_j=0^3 sum_i=0^n-1 xdat(i)^(N+j)*paraout(j,i)/(N+j)
!         (see also function PdfSplineM)
!-----------------------------------------------------------------------
      implicit none
      ! input/output variables
      double precision, intent(in) :: x(1:), f(1:), parain(1:)
      double precision, intent(out) :: paraout(0:,1:)
      
      ! intern variables
      integer :: n
      integer i
      double precision a(2:size(x)), b(2:size(x)), c(2:size(x)),
     & d(2:size(x))
!-----------------------------------------------------------------------

      ! check array sizes
      if((size(x).eq.size(f)).and.(size(x).eq.size(parain)).and.
     &   (size(x).eq.size(paraout,2))) then
         n = size(x)
      else
         stop 'Index error by PdfSpline - Error 1a'
      endif
      if(size(paraout,1).ne.4) then
         stop 'Index error by PdfSpline - Error 1b'
      endif

      ! write parameters for polynom a*x^3+b*x^2+c*x+d
      do i = 2,n
         a(i) = (parain(i)-parain(i-1))/(x(i)-x(i-1))/6.d0
         b(i) = (parain(i-1)*x(i)-parain(i)*x(i-1))
     & /(x(i)-x(i-1))/2.d0
         c(i) = (f(i)-f(i-1))/(x(i)-x(i-1))+
     & (x(i)-x(i-1))*(parain(i-1)-parain(i))/6.d0+
     & (parain(i)*(x(i-1)**2.d0)-parain(i-1)*(x(i)**2.d0))
     &  /(x(i)-x(i-1))/2.d0
         d(i) = (f(i-1)*x(i)-f(i)*x(i-1))
     & /(x(i)-x(i-1))+((x(i)-x(i-1))*(parain(i)*x(i-1)-
     & parain(i-1)*x(i))+(parain(i-1)*(x(i)**3.d0)-parain(i)
     & *(x(i-1)**3.d0))/(x(i)-x(i-1)))/6.d0
      enddo

      ! write parameters for spline in mellin space
      do i = 2,n-1
         paraout(0,i) = d(i)-d(i+1)
         paraout(1,i) = c(i)-c(i+1)
         paraout(2,i) = b(i)-b(i+1)
         paraout(3,i) = a(i)-a(i+1)
      enddo
      paraout(0,1) = -d(2)
      paraout(1,1) = -c(2)
      paraout(2,1) = -b(2)
      paraout(3,1) = -a(2)
      paraout(0,n) = d(n)
      paraout(1,n) = c(n)
      paraout(2,n) = b(n)
      paraout(3,n) = a(n)

      end subroutine mellinspline



      subroutine calcspline(st, x, f, df1, dfn, para)
! Calculates parameters of a natural spline defined by x_i, i=1...n
! grid points
! Input: x(1:n) and function values f(1:n), dimension n, boundary con-
!        dition of spline: st = 0 -> natural spline
!                          st = 1 -> fixed first derivatives, Input by
!                                    df1 and dfn
!                          st = 2 -> as st=1, but first derivatives are
!                                    calculated by grid
! Output: Parameter of spline para(1:n)
!-----------------------------------------------------------------------
      implicit none
      ! input/output variables
      integer, intent(in) :: st
      double precision, intent(in) :: x(1:), f(1:), df1, dfn
      double precision, intent(out) :: para(1:)

      ! intern variables
      integer n, i
      double precision u(1:size(x)-1), l, d1, dn
!-----------------------------------------------------------------------

      ! check array sizes
      if((size(x).eq.size(f)).and.(size(x).eq.size(para))) then
         n = size(x)
      else
         stop 'Index error by PdfSpline - Error 2'
      endif


! Vector u stores diagonal elements of the U matrix of LU decomposition
! where the entries are shifted by 1, because the natural u(1)=1

! l stores the entry below the diagonal during the LU decomposition
! (in every step only one is needed and then never again)

! During the LU decomposition the equation system Lq=y is solved
! para stores the result q and gives then the input for the solution of
! Up=q

      ! start LU decomposition and solving the Lq=y equation system
      if(st.eq.0) then ! natural spline
         para(1) = 0.d0
         l = 0.d0
      else ! fixed first derivation at boundarys
         if (st.eq.1) then
            d1 = df1
            dn = dfn
         else if (st.eq.2) then ! calculate first derivatives from grid
            d1 = (f(2)-f(1))/(x(2)-x(1))
            dn = (f(n)-f(n-1))/(x(n)-x(n-1))
         endif
         para(1) = 3.d0/(x(2)-x(1))*((f(2)-f(1))/(x(2)-x(1))-d1)
         l = -(x(2)-x(1))/6.d0
      endif
      u(1) = (x(3)-x(1))/3.d0 + l/2.d0

      do i = 2,n-1
         para(i) = (f(i+1)-f(i))/(x(i+1)-x(i))
     &              -(f(i)-f(i-1))/(x(i)-x(i-1))+l*para(i-1)
         if (i.eq.(n-1)) then ! near end the matrix looks different
            l = -0.5d0/u(i-1)
            u(n-1) = 1.d0 + l*(x(n)-x(n-1))/6.d0
         else
            l = -(x(i+1)-x(i))/6.d0/u(i-1)
            u(i) = (x(i+2)-x(i))/3.d0 + l*(x(i+1)-x(i))/6.d0
         endif
      enddo

      ! first step of final solution
      if(st.eq.0) then
         para(n) = 0.d0
      else
         para(n) = 3.d0/(x(n)-x(n-1))*(dn-(f(n)-f(n-1))/(x(n)-x(n-1)))
     &             + l*para(n-1)
      endif

      ! Solve the rest of the Up=q equation system
      para(n) = para(n)/u(n-1)
      do i = n-1,2,-1
         para(i) = (para(i)-para(i+1)*(x(i+1)-x(i))/6.d0)/u(i-1)
      enddo
      if(st.eq.0) then ! end is again depending on boundary condition
         para(1) = 0.d0
      else
         para(1) = (para(1)-0.5d0*para(2))
      endif

      return
      end subroutine calcspline


      double precision function spline(x, xdat, fdat, para)
! Calculates spline at given value x
! Input: dimension n, xdat(1:n), function values f(1:n) and output of
!        subroutine spline para(1:n), x where the spline has to be 
!        calculated
! Output: Spline at x
!-----------------------------------------------------------------------
      implicit none
      ! input variables
      double precision, intent(in) :: x, xdat(1:), fdat(1:), para(1:)

      ! intern variables
      integer ilow, ihigh, inew, i
      double precision a, b, h
!-----------------------------------------------------------------------

      ! check array sizes
      if((size(xdat).ne.size(fdat)).or.(size(xdat).ne.size(para))) then
         stop 'Index error by PdfSpline - Error 3'
      endif

      ! check if x is inside the range given by xdat
      if((x.lt.xdat(1)).or.(x.gt.xdat(size(xdat)))) then
         print*, 'Given x = ', x
         print*, 'Given x range = ', xdat(1), ' to ', xdat(size(xdat))
         stop 'Given x not in range of spline'
      endif

      ! minimum and maximum value of array
      ilow = 1
      ihigh = size(xdat)

      ! find correct interval in the splines
      do while ((ihigh-ilow).gt.1)
         inew = (ihigh+ilow)/2 ! find middle (fortran will round to
                               !              lower number)
         ! adjust the interval
         if (xdat(inew).ge.x) then
            ihigh = inew
         else
            ilow = inew
         endif
      enddo

      ! calculate spline
      i = ihigh
      h = xdat(i) - xdat(i-1)
      a = (x-xdat(i-1))
      b = (xdat(i)-x)
      spline = (para(i)*a*a*a+para(i-1)*b*b*b)/(6.d0*h)
     &         + a*(fdat(i)/h-h*para(i)/6.d0)
     &         + b*(fdat(i-1)/h-h*para(i-1)/6.d0)

      return
      end function spline


      end module PdfSplineModule



!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      subroutine PdfSplinePara(io)
! Prints the current active parameters into the command line
!-----------------------------------------------------------------------
      use PdfSplineModule
      implicit none
      ! input variables
      integer, intent(in),optional :: io
      ! intern variables
      integer :: i, j
      logical :: l
!-----------------------------------------------------------------------

 100  format(A)
 101  format(A,I3)
 102  format(A,1PE11.5)
 103  format(A,I5)
 104  format(12(1PE13.5))

      if(present(io)) then
         i = io
      else
         i = -5 ! should always write in terminal
      endif
      ! check if io is open, otherwise write into terminal
      inquire(unit=i,opened=l)
      if (l) then
         write(i,100) '# Current parameters are:'
         write(i,101) '#    ST = ', st_int
         write(i,101) '#  ISET = ', iset_int
         write(i,101) '#  MMHT = ', mmhtih_int
         write(i,101) '# DSSIH = ', dssih_int
         write(i,101) '# DSSIC = ', dssic_int
         write(i,101) '# DSSIO = ', dssio_int
         write(i,102) '#    Q2 = ', q2_int
         write(i,100) '#  FILE = ' // trim(file_int)
         write(i,100) ''
         write(i,100) '# Current grid is:'
         if(spline_exist) then
            write(i,103) '# Number of grid points:', ngrid
            write(i,100) '# x            PDF/FF'
            write(i,100) '#              bottom-bar   charm-bar    '//
     &                    'strange-bar  down-bar     up-bar       '//
     &                    'gluon        up           down         '//
     &                    'strange      charm        bottom'
            do j = 1,ngrid
               write(i,104) xdat(j), fdat(-5,j), fdat(-4,j),
     &                      fdat(-3,j), fdat(-2,j), fdat(-1,j),
     &                      fdat(0,j), fdat(1,j), fdat(2,j), fdat(3,j),
     &                      fdat(4,j), fdat(5,j)
            enddo
         else if (grid_exist) then
            write(i,103) '# Number of grid points:', ngrid
            write(i,100) '# x'
            do j = 1,ngrid
               write(io,104) xdat(j)
            enddo
         else
            write(io,100) '# No grid generated yet!'
         endif
      else
         write(*,100) 'Current parameters are:'
         write(*,101) '   ST = ', st_int
         write(*,101) ' ISET = ', iset_int
         write(*,101) ' MMHT = ', mmhtih_int
         write(*,101) 'DSSIH = ', dssih_int
         write(*,101) 'DSSIC = ', dssic_int
         write(*,101) 'DSSIO = ', dssio_int
         write(*,102) '   Q2 = ', q2_int
         write(*,100) ' FILE = ' // trim(file_int)
         write(*,100) ''
         write(*,100) 'Current grid is:'
         if(spline_exist) then
            write(*,103) 'Number of grid points:', ngrid
            write(*,100) '  x            PDF/FF'
            write(*,100) '               bottom-bar   charm-bar    '//
     &                   'strange-bar  down-bar     up-bar       '//
     &                   'gluon        up           down         '//
     &                   'strange      charm        bottom'
            do j = 1,ngrid
               write(*,104) xdat(j), fdat(-5,j), fdat(-4,j), fdat(-3,j),
     &                      fdat(-2,j), fdat(-1,j), fdat(0,j),
     &                      fdat(1,j), fdat(2,j), fdat(3,j), fdat(4,j),
     &                      fdat(5,j)
            enddo
         else if (grid_exist) then
            write(*,103) 'Number of grid points:', ngrid
            write(*,100) 'x'
            do j = 1,ngrid
               write(*,104) xdat(j)
            enddo
         else
            write(*,100) 'No grid generated yet!'
         endif
      endif

      return
      end



!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      subroutine PdfSplineGrid_twoarray(xarray,narray)
! Generates x-grid (writes in PdfSplineModule the variables ngrid and
!                   xdat)
! Input: Array narray (integer) and array xarray (double precision), 
!        defining the grid
! Output: -
!-----------------------------------------------------------------------
      use PdfSplineModule
      implicit none
      ! input variables
      integer,intent(in) :: narray(1:)
      double precision, intent(in) :: xarray(1:)

      ! intern variables
      integer i, j, npoint, ntemp
      double precision xtemp(1:size(xarray)+1)
!-----------------------------------------------------------------------

      if(size(narray).eq.size(xarray)) then
         npoint = size(narray)
      else
         stop 'Size of arrays xarray and narray is not equal'
      endif

      ! calculate grid points
      ! first check, if given points are sorted and limits are ok
      if (xarray(1).lt.0.d0) then
         stop 'Given xarray ill-defined: Contains value smaller zero'
      endif
      do i = 1,npoint-1
         if (xarray(i).ge.xarray(i+1)) then
            stop 'Given xarray ill-defined: Entries not sorted'
         endif
      enddo
      if (xarray(npoint).ge.1.d0) then
         stop 'Given xarray ill-defined: Contains value greater one'
      endif
      ! write given point in an array including 1.d0
      do i = 1,npoint
         xtemp(i) = xarray(i)
      enddo
      xtemp(npoint+1) = 1.d0
      ! calculate number of grid points
      ngrid = 0
      do i = 1,npoint
         ngrid = ngrid + narray(i)
      enddo
      ! check if defined array in module is large enough
      if (ngrid.gt.nmax) then
         print*, 'Grid too large. Define a smaller grid or change ' //
     &           'the parameter NMAX in PdfSpline'
         stop
      endif
      ! write grid points in module
      ! first interval is [x1,x2]
      do j = 1,narray(1)
         xdat(j) = xtemp(1)*(xtemp(2)/xtemp(1))
     &                      **(dble(j-1)/dble(narray(1)-1))
      enddo
      ntemp = narray(1)
      ! all other intervals are (xi,xi+1]
      do i = 2, npoint
         do j = 1,narray(i)
            xdat(ntemp+j)=xtemp(i)*(xtemp(i+1)/xtemp(i))
     &                      **(dble(j)/dble(narray(i)))
         enddo
         ntemp = ntemp + narray(i)
      enddo
      ! set last entry exactly on 1.d0 to avoid error in function spline
      xdat(ngrid) = 1.d0
      
      grid_exist = .true.

      end subroutine PdfSplineGrid_twoarray



!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      subroutine PdfSplineGrid_simple(xmin,n)
! Generates x-grid (writes in PdfSplineModule the variables ngrid and
!                   xdat)
! Input: Number of grid points n (integer) and minimum value of grid
!        xmin (double precision)
! Output: -
!-----------------------------------------------------------------------
      use PdfSplineModule
      implicit none
      ! input variables
      integer,intent(in) :: n
      double precision, intent(in) :: xmin

      ! intern variables
      integer i
!-----------------------------------------------------------------------

      ! check if xmin is ok
      if ((xmin.lt.0.d0).or.(xmin.gt.1.d0)) then
         stop 'Given xmin in PdfSplineGrid is ill-defined'
      endif

      ngrid = n
      ! check if defined array in module is large enough
      if (ngrid.gt.nmax) then
         print*, 'Grid too large. Define a smaller grid or change ' //
     &           'the parameter NMAX in PdfSpline'
         stop
      endif
      ! calculate grid points
      do i = 0,ngrid-1
         xdat(i+1) = xmin*(1.d0/xmin)**(dble(i)/dble(ngrid-1))
      enddo

      grid_exist = .true.

      end subroutine PdfSplineGrid_simple


!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      subroutine PdfSplineGrid_onearray(xarray)
! Generates x-grid (writes in PdfSplineModule the variables ngrid and
!                   xdat)
! Input: Explicit grid (double precision)
! Output: -
!-----------------------------------------------------------------------
      use PdfSplineModule
      implicit none
      ! input variables
      double precision, intent(in) :: xarray(1:)

      ! intern variables
      integer i
!-----------------------------------------------------------------------

      ngrid = size(xarray)
      ! check if defined array in module is large enough
      if (ngrid.gt.nmax) then
         print*, 'Grid too large. Define a smaller grid or change ' //
     &           'the parameter NMAX in PdfSpline'
         stop
      endif
      ! check if xarray is sorted and has good limits
      if (xarray(1).lt.0.d0) then
         stop 'Given xarray ill-defined: Contains value smaller zero'
      endif
      do i = 1,ngrid-1
         if (xarray(i).ge.xarray(i+1)) then
            stop 'Given xarray ill-defined: Entries not sorted'
         endif
      enddo
      if (xarray(ngrid).ne.1.d0) then
         stop 'Given xarray ill-defined: Last entry not equal 1.d0'
      endif

      ! write array in module
      do i = 1, ngrid
         xdat(i) = xarray(i)
      enddo

      grid_exist = .true.

      end subroutine PdfSplineGrid_onearray


!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      subroutine PdfSplineGrid_onearrayreal(xarray)
! Generates x-grid (writes in PdfSplineModule the variables ngrid and
!                   xdat)
! Input: Explicit grid (real)
! Output: -
!-----------------------------------------------------------------------
      use PdfSplineModule
      implicit none
      ! input variables
      real, intent(in) :: xarray(1:)

      ! intern variables
      integer i
      double precision xtemp(1:size(xarray))
!-----------------------------------------------------------------------

      ngrid = size(xarray)
      ! check if defined array in module is large enough
      if (ngrid.gt.nmax) then
         print*, 'Grid too large. Define a smaller grid or change ' //
     &           'the parameter NMAX in PdfSpline'
         stop
      endif
      ! check if xarray is sorted and has good limits
      if (dble(xarray(1)).lt.0.d0) then
         stop 'Given xarray ill-defined: Contains value smaller zero'
      endif
      do i = 1,ngrid-1
         if (dble(xarray(i)).ge.dble(xarray(i+1))) then
            stop 'Given xarray ill-defined: Entries not sorted'
         endif
      enddo
      if (dble(xarray(ngrid)).ne.1.d0) then
         stop 'Given xarray ill-defined: Last entry not equal 1.d0'
      endif

      ! write array in module
      do i = 1, ngrid
         xdat(i) = dble(xarray(i))
      enddo

      grid_exist = .true.

      end subroutine PdfSplineGrid_onearrayreal

!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      subroutine PdfSplineSet(st,iset,mmhtih,dssih,dssic,dssio,q2,file)
! Initiates spline for chosen dataset
! Input: Integer specifying the data set, integer to specify the spline
!        and scale Q^2 for first construction of spline
! Output: -
!-----------------------------------------------------------------------
      use PdfSplineModule
      implicit none
      interface PdfSplineGrid
         subroutine PdfSplineGrid_twoarray(xarray,narray)
            integer,intent(in) :: narray(:)
            double precision, intent(in) :: xarray(:)
         end subroutine PdfSplineGrid_twoarray
         subroutine PdfSplineGrid_simple(xmin,n)
            integer,intent(in) :: n
            double precision, intent(in) :: xmin
         end subroutine PdfSplineGrid_simple
         subroutine PdfSplineGrid_onearray(xarray)
            double precision, intent(in) :: xarray(:)
         end subroutine PdfSplineGrid_onearray
         subroutine PdfSplineGrid_onearrayreal(xarray)
            real, intent(in) :: xarray(:)
         end subroutine PdfSplineGrid_onearrayreal
      end interface
      ! input variables
      integer,intent(in),optional :: st,iset,mmhtih,dssih,dssic,dssio
      double precision,intent(in),optional :: q2
      character(len=*),intent(in),optional :: file

      ! intern variables
      character(len=100) tempfile
      integer i
!-----------------------------------------------------------------------
      integer fini
      common / fragini / fini
!-----------------------------------------------------------------------

 100   format (A,I2,A,1PE11.5)

      ! set given variables
      if (present(st)) then
         if ((st.ge.0).and.(st.le.2)) then
            st_int = st
         else
            stop 'Given st not known'
         endif
      endif
      if (present(iset)) then
         if ((iset.ge.1).and.(iset.le.3)) then
            iset_int = iset
         else
            stop 'Given iset not known'
         endif
      endif
      if (present(mmhtih)) then
         mmhtih_int = mmhtih
      endif
      if (present(dssih)) then
         dssih_int = dssih
      endif
      if (present(dssic)) then
         dssic_int = dssic
      endif
      if (present(dssio)) then
         dssio_int = dssio
      endif
      if (present(q2)) then
         q2_int = q2
      endif
      if (present(file)) then
         file_int = trim(file)
      endif

      ! if no grid is set, take default grid of pdfs/ffs
      if(.not.grid_exist) then
         print*, 'No grid was specified. Take default grid:'
         if (iset_int.eq.1) then
            print*, 'Choose CT12 standard grid'
            call PdfSplineGrid(
     & (/ 1.00000E-08, 1.21429E-08, 1.47452E-08, 1.79052E-08,
     & 2.17424E-08, 2.64020E-08, 3.20601E-08, 3.89307E-08, 4.72737E-08,
     & 5.74047E-08, 6.97067E-08, 8.46450E-08, 1.02784E-07, 1.24811E-07,
     & 1.51558E-07, 1.84036E-07, 2.23475E-07, 2.71364E-07, 3.29515E-07,
     & 4.00127E-07, 4.85869E-07, 5.89983E-07, 7.16405E-07, 8.69916E-07,
     & 1.05632E-06, 1.28265E-06, 1.55748E-06, 1.89246E-06, 2.29791E-06,
     & 2.79021E-06, 3.38796E-06, 4.11373E-06, 4.99500E-06, 6.06542E-06,
     & 7.36521E-06, 8.94351E-06, 1.08600E-05, 1.31871E-05, 1.60128E-05,
     & 1.94439E-05, 2.36100E-05, 2.86685E-05, 3.48104E-05, 4.22676E-05,
     & 5.13215E-05, 6.23137E-05, 7.56585E-05, 9.18586E-05, 1.11524E-04,
     & 1.35393E-04, 1.64364E-04, 1.99506E-04, 2.42160E-04, 2.93908E-04,
     & 3.56677E-04, 4.32795E-04, 5.25075E-04, 6.36911E-04, 7.72675E-04,
     & 9.36769E-04, 1.13534E-03, 1.37545E-03, 1.66555E-03, 2.01568E-03,
     & 2.43777E-03, 2.94587E-03, 3.55639E-03, 4.28845E-03, 5.16411E-03,
     & 6.20855E-03, 7.45038E-03, 8.92125E-03, 1.06533E-02, 1.26882E-02,
     & 1.50629E-02, 1.78170E-02, 2.09857E-02, 2.46144E-02, 2.87390E-02,
     & 3.33827E-02, 3.85725E-02, 4.43307E-02, 5.06675E-02, 5.75878E-02,
     & 6.50902E-02, 7.31673E-02, 8.18063E-02, 9.09903E-02, 1.00699E-01,
     & 1.10910E-01, 1.21599E-01, 1.32742E-01, 1.44313E-01, 1.56289E-01,
     & 1.68647E-01, 1.81352E-01, 1.94402E-01, 2.07762E-01, 2.21416E-01,
     & 2.35344E-01, 2.49539E-01, 2.63971E-01, 2.78631E-01, 2.93504E-01,
     & 3.08578E-01, 3.23841E-01, 3.39282E-01, 3.54899E-01, 3.70662E-01,
     & 3.86575E-01, 4.02629E-01, 4.18816E-01, 4.35130E-01, 4.51563E-01,
     & 4.68110E-01, 4.84763E-01, 5.01519E-01, 5.18372E-01, 5.35316E-01,
     & 5.52349E-01, 5.69465E-01, 5.86660E-01, 6.03932E-01, 6.21282E-01,
     & 6.38684E-01, 6.56165E-01, 6.73708E-01, 6.91311E-01, 7.08970E-01,
     & 7.26683E-01, 7.44449E-01, 7.62263E-01, 7.80124E-01, 7.98027E-01,
     & 8.15970E-01, 8.33949E-01, 8.51958E-01, 8.69991E-01, 8.88043E-01,
     & 9.06086E-01, 9.24098E-01, 9.42012E-01, 9.59663E-01, 9.76499E-01,
     & 9.90190E-01, 9.96561E-01, 9.98540E-01, 9.99336E-01, 9.99749E-01,
     & 1.00000E+00 /))
         else if (iset_int.eq.2) then
            print*, 'Choose MMHT14 standard grid'
            call PdfSplineGrid(
     & (/ 1d-6, 2d-6, 4d-6, 6d-6, 8d-6, 1d-5, 2d-5, 4d-5, 6d-5,
     & 8d-5, 1d-4, 2d-4, 4d-4, 6d-4, 8d-4, 1d-3, 2d-3, 4d-3, 6d-3, 8d-3,
     & 1d-2, 1.4d-2, 2d-2, 3d-2, 4d-2, 6d-2, 8d-2, .1d0, .125d0, .15d0,
     & .175d0, .2d0, .225d0, .25d0, .275d0, .3d0, .325d0, .35d0, .375d0,
     & .4d0, .425d0, .45d0, .475d0, .5d0, .525d0, .55d0, .575d0, .6d0,
     & .625d0, .65d0, .675d0, .7d0, .725d0, .75d0, .775d0, .8d0, .825d0,
     & .85d0, .875d0, .9d0, .925d0, .95d0, .975d0, 1d0 /))
         else if (iset_int.eq.3) then
            print*, 'Choose DSS07 standard grid'
            call PdfSplineGrid(
     & (/ 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08,
     & 0.09, 0.095, 0.1, 0.125, 0.15, 0.175, 0.2, 0.225, 0.25, 0.275,
     & 0.3, 0.325, 0.35, 0.375, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7,
     & 0.75, 0.8, 0.85, 0.9, 0.93, 1.0 /))
         endif
      endif

      ! for taking cteq, initiate cteq code
      if(iset_int.eq.1) then
         tempfile = trim(file_int)//'.pds'
         call setct12(tempfile)
      endif

      ! Calculate the complete grid
      call PdfSplineCalc

      ! Spline calculated successfully
      spline_exist = .true.

      return
      end subroutine PdfSplineSet


!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      subroutine PdfSplineGet(iparton,nout,xout,paraout)
! Gives spline parameter for given parton flavour
! Input: Parton flavour (iparton)
! Output: Number of points n, x-data (1:n) and spline parameter 
!         in Mellin space (0:3,1:n)
!-----------------------------------------------------------------------
      use PdfSplineModule
      implicit none
      ! input/output variables
      integer, intent(in) :: iparton
      integer, intent(out) :: nout
      double precision,intent(out) :: xout(1:),paraout(0:,1:)

      ! intern variables
      integer i, j
!-----------------------------------------------------------------------

      ! check if already a spline is calculated
      if(.not.spline_exist) then
       stop 'No spline calculated - use subroutine PdfSplineSet() first'
      endif
      
      ! give user the number of points of the grid
      nout = ngrid
      ! check if the arrays of the user are large enough
      if(size(xout).lt.ngrid) then
         print*, 'Size of array xout too small - should be at least',
     &            ngrid
         stop
      endif
      if(size(paraout,2).lt.ngrid) then
         print*, 'Size of second dimension of array paraout too small'//
     &           ' - should be at least', ngrid
         stop
      endif
      if(size(paraout,1).lt.4) then
         print*, 'Size of first dimension of array paraout too small'//
     &           ' - should be 4'
         stop
      endif
      ! get data of iparton into the output parameter
      do i = 1,ngrid
         do j = 0,3
            paraout(j,i)=pmdat(iparton,j,i)
         enddo
         ! write x data in vector of correct dimension
         xout(i) = xdat(i)
      enddo

      return
      end subroutine PdfSplineGet


!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      double complex function PdfSplineM(iparton, N)
! Calculates distribution for parton (iparton) at mellin moment (N)
! Input: Parton flavour iparton, bjorken x and mellin Moment N
! Output: Spline at mellin Moment N
!-----------------------------------------------------------------------
      use PdfSplineModule
      implicit none
      ! input variables
      integer, intent(in) :: iparton
      double complex, intent(in) :: N
      
      ! intern variables
      integer i, j
!-----------------------------------------------------------------------

      ! check if already a spline is calculated
      if(.not.spline_exist) then
       stop 'No spline calculated - use subroutine PdfSplineSet() first'
      endif

      PdfSplineM = dcmplx(0.d0,0.d0)
      do j=0,3
         do i=1,ngrid
            PdfSplineM = PdfSplineM +
     &                   (xdat(i)**(N+j))*pmdat(iparton,j,i)/(N+j)
         enddo
      enddo

      return
      end function PdfSplineM


!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      double precision function PdfSplineX(iparton, x)
! Calculates distribution for parton (iparton) at mellin moment (N)
! Input: Parton flavour iparton, bjorken x and mellin Moment N
! Output: Spline at mellin Moment N
!-----------------------------------------------------------------------
      use PdfSplineModule
      implicit none
      ! input variables
      integer, intent(in) :: iparton
      double precision, intent(in) :: x
      
      ! intern variables
      integer i
      double precision u(1:ngrid), v(1:ngrid), xtemp(1:ngrid)
!-----------------------------------------------------------------------

      ! check if already a spline is calculated
      if(.not.spline_exist) then
       stop 'No spline calculated - use subroutine PdfSplineSet() first'
      endif

      ! write vectors for function spline for chosen parton flavour
      do i = 1,ngrid
         u(i) = pxdat(iparton,i)
         v(i) = fdat(iparton,i)
         xtemp(i) = xdat(i)
      enddo

      PdfSplineX = spline(x, xtemp, v, u)

      return
      end function PdfSplineX
!


!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      double precision function PdfSplineAlphas(q2,iord,fr2,mur,asmur,
     &                          mc,mb,mt)
! Calculates the strong coupling constant at scale q2 (GeV**2)
! corresponding to the parent set. For DSS the CTEQ alpha_s routine
! will be used
!-----------------------------------------------------------------------
      use PdfSplineModule
      implicit none
      ! input variables
      double precision, intent(in) :: q2
      integer, intent(in), optional :: iord
      double precision, intent(in), optional :: fr2,mur,asmur,mc,mb,mt

      ! intern variables
      integer :: iord_int
      double precision :: fr2_int, mur_int, asmur_int, mc_int, mb_int,
     &                    mt_int
      logical :: first

      ! used functions
      double precision mstwalphas, ct12alphas
!-----------------------------------------------------------------------

      save
      data first / .true. /

      ! check if optional arguments are present
      if (present(iord)) then
         iord_int = iord
      else
         if (first) then
            iord_int = 2
         endif
      endif
      if (present(fr2)) then
         fr2_int = fr2
      else
         if (first) then
            fr2_int = 1.d0
         endif
      endif
      if (present(mur)) then
         mur_int = mur
      else
         if (first) then
            mur_int = 1.d0
         endif
      endif
      if (present(asmur)) then
         asmur_int = asmur
      else
         if (first) then
            asmur_int = 0.5d0
         endif
      endif
      if (present(mc)) then
         mc_int = mc
      else
         if (first) then
            mc_int = 1.4d0
         endif
      endif
      if (present(mb)) then
         mb_int = mb
      else
         if (first) then
            mb_int = 4.75d0
         endif
      endif
      if (present(mt)) then
         mt_int = mt
      else
         if (first) then
            mt_int = 1.d10
         endif
      endif
      ! for mmht init the alphas-routine if parameter has changed
      if((iset_int.eq.2).and.(first.or.((present(iord))
     &                             .or.(present(fr2)).or.(present(mur))
     &                             .or.(present(asmur)).or.(present(mc))
     &                             .or.(present(mb)).or.(present(mt)))))
     &then
         call initalphas(iord_int,fr2_int,mur_int,asmur_int,
     &                   mc_int,mb_int,mt_int)
      endif

      ! calculate alpha_s
      select case(iset_int)
         case(2) ! mmht = mstw
            PdfSplineAlphas = mstwalphas(dsqrt(q2))
         case default ! ct12 and dss
            PdfSplineAlphas = ct12alphas(dsqrt(q2))
      end select

      first = .false.

      return
      end function PdfSplineAlphas







!ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
!ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
!ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
!ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
!ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
! PDF/FF standalone codes
!ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
!ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
!ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
!ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
!ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ


!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
! CT12
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

C============================================================================
C                CTEQ-TEA Parton Distribution Functions: version 2012
C                       April 22, 2012
C
C   Reference: J. Gao, M. Guzzi, J. Huston, H.-L. Lai, Z. Li, P. M.
C   Nadolsky, J. Pumplin, D. Stump, C.-P. Yuan,  arXiv:1302.6246
C
C   This package provides a standard interface for CT10 and CT12 parton
C   distribution functions

C   The following sets of CTEQ PDF table files can be computed 
C    with this program: 
C   (1) 1+50 of CT10W NNLO PDF's;
C   (2) 1+52 sets of CT10W NLO PDF's;
C   (3) 4 sets of CT10W NNLO and NLO PDF's with alternative alpha_s values;

C ===========================================================================
C   The table grids are generated for 
C    *  10^-8 < x < 1 and 1.3 < Q < 10^5 (GeV).
C
C   PDF values outside of the above range are returned using extrapolation.
C
C   The Table_Files are assumed to be in the working directory.
C
C   Before using the PDF, it is necessary to do the initialization by
C       Call SetCT12(Iset)
C   where Tablefile is a 40-character text string with the name of the
c   the desired PDF specified in the above table.table (.pds) file
C
C   Other provided functions include:
C   The function CT12Pdf (Iparton, X, Q)
C     returns the parton distribution inside the proton for parton [Iparton]
C     at [X] Bjorken_X and scale [Q] (GeV) in PDF set [Iset].
C     Iparton  is the parton label (5, 4, 3, 2, 1, 0, -1, ......, -5)
C                              for (b, c, s, d, u, g, u_bar, ..., b_bar),
C
C   The function CT12Alphas (Q) 
C     returns the value of the QCD coupling strength alpha_s(Q) at
C     an energy scale Q. The alpha_s value is obtained from the interpolation
C     of a numerical table of alpha_s included in each .pds file. 
C     In the PDF fit, the alpha_s values are obtained at each momentum
c     scale by evolution in the HOPPET program at the respective QCD order 
C     (NLO or NNLO). The table of alpha_s values at discrete Q values
C     is included in the input .pds file. The function CT12Alphas
c     estimates alpha_s at an arbitrary Q value, which agrees
c     with the direct evolution by HOPPET within a fraction of percent
c     point at typical Q.
C 
C   The function CT12Mass(i)
c     returns the value of the quark mass for the i-th flavor.
c     The flavors are:
c     1  2  3  4  5  6
c     u  d  s  c  b  t
c
C   Values of various PDF parameters assumed in the computation of the
c    PDFs can be obtained by 
C     Call CT12GetPars( xmin,Qini,Qmax,Nloops,Nfl), 
C   which returns
c     xmin, the minimal value of x;
c     Qmin,  the initial Q scale for the PDF evolution;  
c     Qmax,  the maximal Q scale included in the PDF table;
c     Nloop, the number of QCD loops (order of the PDF in the QCD coupling);
c     Nfl,   the maximal number of quark flavors assumed in the PDF and 
c            alpha_s evolution.

C   These programs, as provided, are in double precision.  By removing the
C   "Implicit Double Precision" lines, they can also be run in single
C   precision.
C   If you have detailed questions concerning these CT12 distributions,
C   or if you find problems/bugs using this package, direct inquires to
C   nadolsky@physics.smu.edu.
C
C===========================================================================

      Function CT12Pdf (Iparton, X, Q)
      Implicit Double Precision (A-H,O-Z)
      Logical Warn
      integer isetch, ipdsformat
      Common
     > / CtqPar2 / Nx, Nt, NfMx, MxVal
     > / QCDtbl /  AlfaQ, Qalfa, Ipk, Iorder, Nfl !for external use
     >  /Setchange/ Isetch, ipdsset, ipdsformat

      Data Warn /.true./
      Data Qsml /.3d0/
      save Warn

      if (ipdsset.ne.1) 
     >  STOP 'CT12Pdf: the PDF table was not initialized'

      If (X .lt. 0d0 .or. X .gt. 1D0) Then
        Print *, 'X out of range in CT12Pdf: ', X
        CT12Pdf = 0D0
        Return
      Endif

      If (Q .lt. Qsml) Then
        Print *, 'Q out of range in CT12Pdf: ', Q
        Stop
      Endif

      If (abs(Iparton).gt. NfMx) Then
        If (Warn) Then
C        print a warning for calling extra flavor
          Warn = .false.
          Print *, 'Warning: Iparton out of range in CT12Pdf! '
          Print *, 'Iparton, MxFlvN0: ', Iparton, NfMx
        Endif
        CT12Pdf = 0D0
      else

        CT12Pdf = PartonX12 (Iparton, X, Q)
        if (CT12Pdf.lt.0D0) CT12Pdf = 0D0
      endif                     !if (abs(Iparton...

      Return

C                             ********************
      End

      Subroutine SetCT12(Tablefile)    
      Implicit Double Precision (A-H,O-Z)
      Character Tablefile*40
      Common /Setchange/ Isetch, ipdsset, ipdsformat
      data ipdsset, ipdsformat/0,0/
      save

      IU= NextUn()
      Open(IU, File=Tablefile, Status='OLD', Err=100)
      Call Readpds0 (IU)
      Close (IU)
      Isetch=1; ipdsset=1
      Return

 100  Print *, ' Data file ', Tablefile, ' cannot be opened '
     >  //'in SetCT12!!'
      Stop
C                             ********************
      End

      subroutine CT12GetPars(xmin,Qini,Qmax,Nloops,Nfl)
c Get various parameters associated with the PDF grid
c Output: xmin  is the minimal value of x 
c         Qmin  is the initial Q scale  
c         Qmax  is the maximal Q scale
c         Nloop is the number of QCD loops
c         Nfl   is the maximal number of quark flavors
      implicit none
      double precision Qini0, Qmax0, Xmin0, xmin, Qini, Qmax
      integer Nloops, Ipk, Iorder, Nfl,Nfl0
      double precision AlfaQ, Qalfa

      common / XQrange / Qini0, Qmax0, Xmin0
      common / QCDtbl /  AlfaQ, Qalfa, Ipk, Iorder, Nfl0
     
      Qini=Qini0; Qmax=Qmax0; Xmin=Xmin0
      Nloops=Iorder-1; Nfl=Nfl0
      
      return 
      end


      Function CT12Alphas (QQ)

      Implicit Double Precision (A-H,O-Z)

      PARAMETER (MXX = 201, MXQ = 40, MXF = 6, MaxVal=4)
      PARAMETER (MXPQX = (MXF+1+MaxVal) * MXQ * MXX)
      double precision Alsout
      
      Common
     > / CtqPar1 / qBase,XV(0:MXX), TV(0:MXQ), UPD(MXPQX),AlsCTEQ(0:mxq)
     > / CtqPar2 / Nx, Nt, NfMx, MxVal
     > / XQrange / Qini, Qmax, Xmin
     > /Setchange/ Isetch, ipdsset, ipdsformat

      Data Q, JQ /-1D0, 0/
      save

      if (ipdsset.ne.1) 
     >  STOP 'CT12Alphas: the PDF table was not initialized'

      
      if (ipdsformat.lt.11) then
        print *
        print *, 
     >    'STOP in CT12alphas: the PDF table file has an older format'
        print *,
     >    'and does not include the table of QCD coupling values.'
        print *, 
     >    'You can still compute the PDFs, but do not call'
        print *,
     >    'the CT12alphas function for the interpolation of alpha_s.'
        stop
      endif

      Q = QQ
      tt = log(log(Q/qBase))

c         --------------   Find lower end of interval containing Q, i.e.,
c                          get jq such that qv(jq) .le. q .le. qv(jq+1)...
      JLq = -1
      JU = NT+1
 13   If (JU-JLq .GT. 1) Then
        JM = (JU+JLq) / 2
        If (tt .GE. TV(JM)) Then
            JLq = JM
          Else
            JU = JM
          Endif
          Goto 13
       Endif

      If     (JLq .LE. 0) Then
         Jq = 0
      Elseif (JLq .LE. Nt-2) Then
C                                  keep q in the middle, as shown above
         Jq = JLq - 1
      Else
C                         JLq .GE. Nt-1 case:  Keep at least 4 points >= Jq.
        Jq = Nt - 3

      Endif
C                                 This is the interpolation variable in Q
      Call Polint4F (TV(jq), AlsCTEQ(jq), tt, Alsout)
      
      CT12Alphas = Alsout
      
      Return
C                                       ********************
      End


      function CT12Mass(i)
c     Returns the value of the quark mass for the i-th flavor 
c     The flavors are:
c     1  2  3  4  5  6
c     u  d  s  c  b  t
      implicit none
      double precision CT12Mass, Amass
      integer  Isetch, ipdsset, i, ipdsformat
      common/Setchange/ Isetch, ipdsset, ipdsformat
     >  / Masstbl / Amass(6)


      if (ipdsset.ne.1) 
     >  STOP 'CT12Mass: the PDF table was not initialized'

      CT12Mass = Amass(i)

      return 
      end


      Subroutine Readpds0 (Nu)
      Implicit Double Precision (A-H,O-Z)
      Character Line*80
      integer ipdsformat
      PARAMETER (MXX = 201, MXQ = 40, MXF = 6, MaxVal=4)
      PARAMETER (MXPQX = (MXF+1+MaxVal) * MXQ * MXX)
      double precision qv(0:mxq)

      Common
     > / CtqPar1 / qBase,XV(0:MXX),TV(0:MXQ),UPD(MXPQX), AlsCTEQ(0:mxq)
     > / CtqPar2 / Nx, Nt, NfMx, MxVal
     > / XQrange / Qini, Qmax, Xmin
     > / Masstbl / Amass(6)
     > / QCDtbl /  AlfaQ, Qalfa, Ipk, Iorder, Nfl !for external use
     > /Setchange/ Isetch, ipdsset, ipdsformat

      Read  (Nu, '(A)') Line
      Read  (Nu, '(A)') Line

      if (Line(1:11) .eq. '  ipk, Ordr') then !post-CT10 .pds format;
c Set alphas(MZ) at scale Zm, quark masses, and evolution type
        ipdsformat = 10 !Post-CT10 .pds format
        Read (Nu, *) ipk, Dr, Qalfa, AlfaQ, (amass(i),i=1,6) 
        Iorder = Nint(Dr)        
        read (Nu, '(A)') Line
        if (Line(1:7) .eq. '  IMASS' ) then
          ipdsformat = 11 !CT12 .pds format
          read (Nu, *) aimass, fswitch, N0, N0, N0, Nfmx, MxVal
          Nfl=Nfmx
        else                    !Pre-CT12 format
          Read  (Nu, *) N0, N0, N0, NfMx, MxVal
        endif                   !Line(1:7)
        
      else                      !old .pds format;      
        ipdsformat = 6           !CTEQ6.6 .pds format; alpha_s  is not specified        
        Read (Nu, *) Dr, fl, Alambda, (amass(i),i=1,6)  !set Lambda_QCD
        Iorder = Nint(Dr); Nfl = Nint(fl)

        Read  (Nu, '(A)') Line
        Read  (Nu, *) dummy,dummy,dummy, NfMx, MxVal, N0
      endif                     !Line(1:11...
      
      Read  (Nu, '(A)') Line
      Read  (Nu, *) NX,  NT, N0, NG, N0
      
      if (ng.gt.0) Read  (Nu, '(A)') (Line, i=1,ng+1)

      Read  (Nu, '(A)') Line
      if (ipdsformat.ge.11) then !CT12 format with alpha_s values
        Read  (Nu, *) QINI, QMAX, (qv(i),TV(I), AlsCTEQ(I), I =0, NT)
      else                      !pre-CT12 format
        Read  (Nu, *) QINI, QMAX, (qv(i),TV(I), I =0, NT)
      endif                     !ipdsformat.ge.11

c check that qBase is consistent with the definition of Tv(0:nQ) for 2 values of Qv
      qbase1 = Qv(1)/Exp(Exp(Tv(1)))
      qbase2 = Qv(nT)/Exp(Exp(Tv(NT)))
      if (abs(qbase1-qbase2).gt.1e-5) then
        print *, 'Readpds0: something wrong with qbase'
        print *,'qbase1, qbase2=',qbase1,qbase2
        stop
      else
        qbase=(qbase1+qbase2)/2.0d0
      endif                     !abs(qbase1...

      Read  (Nu, '(A)') Line
      Read  (Nu, *) XMIN, aa, (XV(I), I =1, NX)
      XV(0)=0D0
      
      Nblk = (NX+1) * (NT+1)
      Npts =  Nblk  * (NfMx+1+MxVal)
      Read  (Nu, '(A)') Line
      Read  (Nu, *, IOSTAT=IRET) (UPD(I), I=1,Npts)

      Return
C                        ****************************
      End

      Function PartonX12 (IPRTN, XX, QQ)

c  Given the parton distribution function in the array U in
c  COMMON / PEVLDT / , this routine interpolates to find
c  the parton distribution at an arbitray point in x and q.
c
      Implicit Double Precision (A-H,O-Z)

      PARAMETER (MXX = 201, MXQ = 40, MXF = 6, MaxVal=4)
      PARAMETER (MXPQX = (MXF+1+MaxVal) * MXQ * MXX)

      Common
     > / CtqPar1 / qBase, XV(0:MXX), TV(0:MXQ),UPD(MXPQX),AlsCTEQ(0:mxq)
     > / CtqPar2 / Nx, Nt, NfMx, MxVal
     > / XQrange / Qini, Qmax, Xmin
     > /Setchange/ Isetch, ipdsset, ipdsformat

      Dimension fvec(4), fij(4)
      Dimension xvpow(0:mxx)
      Data OneP / 1.00001 /
      Data xpow / 0.3d0 /       !**** choice of interpolation variable
      Data nqvec / 4 /
      Data ientry / 0 /
      Data X, Q, JX, JQ /-1D0, -1D0, 0, 0/
      Save xvpow
      Save X, Q, JX, JQ, JLX, JLQ
      Save ss, const1, const2, const3, const4, const5, const6
      Save sy2, sy3, s23, tt, t12, t13, t23, t24, t34, ty2, ty3
      Save tmp1, tmp2, tdet

c store the powers used for interpolation on first call...
      if(Isetch .eq. 1) then
         Isetch = 0

         xvpow(0) = 0D0
         do i = 1, nx
            xvpow(i) = xv(i)**xpow
         enddo
      elseIf((XX.eq.X).and.(QQ.eq.Q)) then
      	goto 99
      endif

      X = XX
      Q = QQ
      tt = log(log(Q/qBase))

c      -------------    find lower end of interval containing x, i.e.,
c                       get jx such that xv(jx) .le. x .le. xv(jx+1)...
      JLx = -1
      JU = Nx+1
 11   If (JU-JLx .GT. 1) Then
         JM = (JU+JLx) / 2
         If (X .Ge. XV(JM)) Then
            JLx = JM
         Else
            JU = JM
         Endif
         Goto 11
      Endif
C                     Ix    0   1   2      Jx  JLx         Nx-2     Nx
C                           |---|---|---|...|---|-x-|---|...|---|---|
C                     x     0  Xmin               x                 1
C
      If     (JLx .LE. -1) Then
        Print '(A,1pE12.4)','Severe error: x <= 0 in PartonX12! x = ',x
        Stop
      ElseIf (JLx .Eq. 0) Then
         Jx = 0
      Elseif (JLx .LE. Nx-2) Then

C                For interrior points, keep x in the middle, as shown above
         Jx = JLx - 1
      Elseif (JLx.Eq.Nx-1 .or. x.LT.OneP) Then

C                  We tolerate a slight over-shoot of one (OneP=1.00001),
C              perhaps due to roundoff or whatever, but not more than that.
C                                      Keep at least 4 points >= Jx
         Jx = JLx - 2
      Else
        Print '(A,1pE12.4)','Severe error: x > 1 in PartonX12! x = ',x
        Stop
      Endif
C          ---------- Note: JLx uniquely identifies the x-bin; Jx does not.

C                       This is the variable to be interpolated in
      ss = x**xpow

      If (JLx.Ge.2 .and. JLx.Le.Nx-2) Then

c     initiation work for "interior bins": store the lattice points in s...
      svec1 = xvpow(jx)
      svec2 = xvpow(jx+1)
      svec3 = xvpow(jx+2)
      svec4 = xvpow(jx+3)

      s12 = svec1 - svec2
      s13 = svec1 - svec3
      s23 = svec2 - svec3
      s24 = svec2 - svec4
      s34 = svec3 - svec4

      sy2 = ss - svec2
      sy3 = ss - svec3

c constants needed for interpolating in s at fixed t lattice points...
      const1 = s13/s23
      const2 = s12/s23
      const3 = s34/s23
      const4 = s24/s23
      s1213 = s12 + s13
      s2434 = s24 + s34
      sdet = s12*s34 - s1213*s2434
      tmp = sy2*sy3/sdet
      const5 = (s34*sy2-s2434*sy3)*tmp/s12
      const6 = (s1213*sy2-s12*sy3)*tmp/s34

      EndIf

c         --------------Now find lower end of interval containing Q, i.e.,
c                          get jq such that qv(jq) .le. q .le. qv(jq+1)...
      JLq = -1
      JU = NT+1
 12   If (JU-JLq .GT. 1) Then
         JM = (JU+JLq) / 2
         If (tt .GE. TV(JM)) Then
            JLq = JM
         Else
            JU = JM
         Endif
         Goto 12
       Endif

      If     (JLq .LE. 0) Then
         Jq = 0
      Elseif (JLq .LE. Nt-2) Then
C                                  keep q in the middle, as shown above
         Jq = JLq - 1
      Else
C                         JLq .GE. Nt-1 case:  Keep at least 4 points >= Jq.
        Jq = Nt - 3

      Endif
C                                   This is the interpolation variable in Q

      If (JLq.GE.1 .and. JLq.LE.Nt-2) Then
c                                        store the lattice points in t...
      tvec1 = Tv(jq)
      tvec2 = Tv(jq+1)
      tvec3 = Tv(jq+2)
      tvec4 = Tv(jq+3)

      t12 = tvec1 - tvec2
      t13 = tvec1 - tvec3
      t23 = tvec2 - tvec3
      t24 = tvec2 - tvec4
      t34 = tvec3 - tvec4

      ty2 = tt - tvec2
      ty3 = tt - tvec3

      tmp1 = t12 + t13
      tmp2 = t24 + t34

      tdet = t12*t34 - tmp1*tmp2

      EndIf


c get the pdf function values at the lattice points...

 99   If (Iprtn .Gt. MxVal) Then
         Ip = - Iprtn
      Else
         Ip = Iprtn
      EndIf
      jtmp = ((Ip + NfMx)*(NT+1)+(jq-1))*(NX+1)+jx+1

      Do it = 1, nqvec
        J1  = jtmp + it*(NX+1)

       If (Jx .Eq. 0) Then
C                      For the first 4 x points, interpolate x^2*f(x,Q)
C                      This applies to the two lowest bins JLx = 0, 1
C            We can not put the JLx.eq.1 bin into the "interrior" section
C                           (as we do for q), since Upd(J1) is undefined.
         fij(1) = 0
         fij(2) = Upd(J1+1) * XV(1)**2
         fij(3) = Upd(J1+2) * XV(2)**2
         fij(4) = Upd(J1+3) * XV(3)**2
C
C                 Use Polint which allows x to be anywhere w.r.t. the grid

         Call Polint4F (XVpow(0), Fij(1), ss, Fx)

         If (x .GT. 0D0)  Fvec(it) =  Fx / x**2
C                                              Pdf is undefined for x.eq.0
       ElseIf  (JLx .Eq. Nx-1) Then
C                                                This is the highest x bin:

        Call Polint4F (XVpow(Nx-3), Upd(J1), ss, Fx)

        Fvec(it) = Fx

       Else
C                       for all interior points, use Jon's in-line function
C                              This applied to (JLx.Ge.2 .and. JLx.Le.Nx-2)
         sf2 = Upd(J1+1)
         sf3 = Upd(J1+2)

         g1 =  sf2*const1 - sf3*const2
         g4 = -sf2*const3 + sf3*const4

         Fvec(it) = (const5*(Upd(J1)-g1)
     &               + const6*(Upd(J1+3)-g4)
     &               + sf2*sy3 - sf3*sy2) / s23

       Endif

      enddo
C                                   We now have the four values Fvec(1:4)
c     interpolate in t...

      If (JLq .LE. 0) Then
C                         1st Q-bin, as well as extrapolation to lower Q
        Call Polint4F (TV(0), Fvec(1), tt, ff)

      ElseIf (JLq .GE. Nt-1) Then
C                         Last Q-bin, as well as extrapolation to higher Q
        Call Polint4F (TV(Nt-3), Fvec(1), tt, ff)
      Else
C                         Interrior bins : (JLq.GE.1 .and. JLq.LE.Nt-2)
C       which include JLq.Eq.1 and JLq.Eq.Nt-2, since Upd is defined for
C                         the full range QV(0:Nt)  (in contrast to XV)
        tf2 = fvec(2)
        tf3 = fvec(3)

        g1 = ( tf2*t13 - tf3*t12) / t23
        g4 = (-tf2*t34 + tf3*t24) / t23

        h00 = ((t34*ty2-tmp2*ty3)*(fvec(1)-g1)/t12
     &    +  (tmp1*ty2-t12*ty3)*(fvec(4)-g4)/t34)

        ff = (h00*ty2*ty3/tdet + tf2*ty3 - tf3*ty2) / t23
      EndIf

      PartonX12 = ff

      Return
C                                       ********************
      End


      SUBROUTINE POLINT4F (XA,YA,X,Y)

      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
C  The POLINT4 routine is based on the POLINT routine from "Numerical Recipes",
C  but assuming N=4, and ignoring the error estimation.
C  suggested by Z. Sullivan.
      DIMENSION XA(*),YA(*)

      H1=XA(1)-X
      H2=XA(2)-X
      H3=XA(3)-X
      H4=XA(4)-X

      W=YA(2)-YA(1)
      DEN=W/(H1-H2)
      D1=H2*DEN
      C1=H1*DEN

      W=YA(3)-YA(2)
      DEN=W/(H2-H3)
      D2=H3*DEN
      C2=H2*DEN

      W=YA(4)-YA(3)
      DEN=W/(H3-H4)
      D3=H4*DEN
      C3=H3*DEN

      W=C2-D1
      DEN=W/(H1-H3)
      CD1=H3*DEN
      CC1=H1*DEN

      W=C3-D2
      DEN=W/(H2-H4)
      CD2=H4*DEN
      CC2=H2*DEN

      W=CC2-CD1
      DEN=W/(H1-H4)
      DD1=H4*DEN
      DC1=H1*DEN

      If((H3+H4).lt.0D0) Then
         Y=YA(4)+D3+CD2+DD1
      Elseif((H2+H3).lt.0D0) Then
         Y=YA(3)+D2+CD1+DC1
      Elseif((H1+H2).lt.0D0) Then
         Y=YA(2)+C2+CD1+DC1
      ELSE
         Y=YA(1)+C1+CC1+DC1
      ENDIF

      RETURN
C               *************************
      END

      Function NextUn()
C                                 Returns an unallocated FORTRAN i/o unit.
      Logical EX
C
      Do 10 N = 10, 300
         INQUIRE (UNIT=N, OPENED=EX)
         If (.NOT. EX) then
            NextUn = N
            Return
         Endif
 10   Continue
      Stop ' There is no available I/O unit. '
C               *************************
      End
C


!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
! MMHT (MSTW)
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

C----------------------------------------------------------------------
C--   Fortran interpolation code for MSTW/MMHT PDFs, building on existing
C--   MRST Fortran code and Jeppe Andersen's C++ code.
C--   Three user interfaces:
C--    call GetAllPDFs(prefix,ih,x,q,upv,dnv,usea,dsea,
C--                    str,sbar,chm,cbar,bot,bbar,glu,phot)
C--    call GetAllPDFsAlt(prefix,ih,x,q,xpdf,xphoton)
C--    xf = GetOnePDF(prefix,ih,x,q,f)
C--   See enclosed example.f for usage.
C--   Original code by Graeme Watt <Graeme.Watt(at)durham.ac.uk>.
C--   Updated 25/06/2010: Enlarge allowed range for m_c and m_b.
C--   Updated 25/01/2011: Fix "NaN" bug for q <= m_c when m_c^2 < 1.25 GeV^2.
C----------------------------------------------------------------------

C----------------------------------------------------------------------

C--   Traditional MRST-like interface: return all flavours.
C--   (Note the additional "sbar", "cbar", "bbar" and "phot"
C--   compared to previous MRST releases.)
      subroutine GetAllPDFs(prefix,ih,x,q,
     &     upv,dnv,usea,dsea,str,sbar,chm,cbar,bot,bbar,glu,phot)
      implicit none
      integer ih
      double precision x,q,upv,dnv,usea,dsea,str,sbar,chm,cbar,
     &     bot,bbar,glu,phot,GetOnePDF,up,dn,sv,cv,bv
      character*(*) prefix

C--   Quarks.
      dn  = GetOnePDF(prefix,ih,x,q,1)
      up  = GetOnePDF(prefix,ih,x,q,2)
      str = GetOnePDF(prefix,ih,x,q,3)
      chm = GetOnePDF(prefix,ih,x,q,4)
      bot = GetOnePDF(prefix,ih,x,q,5)

C--   Valence quarks.
      dnv = GetOnePDF(prefix,ih,x,q,7)
      upv = GetOnePDF(prefix,ih,x,q,8)
      sv  = GetOnePDF(prefix,ih,x,q,9)
      cv  = GetOnePDF(prefix,ih,x,q,10)
      bv  = GetOnePDF(prefix,ih,x,q,11)
      
C--   Antiquarks = quarks - valence quarks.
      dsea = dn - dnv
      usea = up - upv
      sbar = str - sv
      cbar = chm - cv
      bbar = bot - bv

C--   Gluon.
      glu = GetOnePDF(prefix,ih,x,q,0)

C--   Photon (= zero unless considering QED contributions).
      phot = GetOnePDF(prefix,ih,x,q,13)

      return
      end

C----------------------------------------------------------------------

C--   Alternative LHAPDF-like interface: return PDFs in an array.
      subroutine GetAllPDFsAlt(prefix,ih,x,q,xpdf,xphoton)
      implicit none
      integer ih,f
      double precision x,q,xpdf(-6:6),xphoton,xvalence,GetOnePDF
      character*(*) prefix

      do f = 1, 6
         xpdf(f) = GetOnePDF(prefix,ih,x,q,f) ! quarks
         xvalence = GetOnePDF(prefix,ih,x,q,f+6) ! valence quarks
         xpdf(-f) = xpdf(f) - xvalence ! antiquarks
      end do
      xpdf(0) = GetOnePDF(prefix,ih,x,q,0) ! gluon
      xphoton = GetOnePDF(prefix,ih,x,q,13) ! photon
      
      return
      end

C----------------------------------------------------------------------

C--   Get only one parton flavour 'f', using PDG notation
C--   (apart from gluon has f=0, not 21):
C--    f =   -6,  -5,  -4,  -3,  -2,  -1,0,1,2,3,4,5,6
C--      = tbar,bbar,cbar,sbar,ubar,dbar,g,d,u,s,c,b,t.
C--   Can also get valence quarks directly:
C--    f =  7, 8, 9,10,11,12.
C--      = dv,uv,sv,cv,bv,tv.
C--   Photon: f = 13.
      double precision function GetOnePDF(prefix,ih,x,q,f)
      implicit none
      logical warn,fatal
      parameter(warn=.false.,fatal=.true.)
C--   Set warn=.true. to turn on warnings when extrapolating.
C--   Set fatal=.false. to return zero instead of terminating when
C--    invalid input values of x and q are used.
      integer ih,f,nhess,nx,nq,np,nqc0,nqb0,n,m,ip,io,
     &     alphaSorder,alphaSnfmax,nExtraFlavours,lentrim
      double precision x,q,xmin,xmax,qsqmin,qsqmax,mc2,mb2,eps,
     &     dummy,qsq,xlog,qsqlog,res,res1,anom,ExtrapolatePDF,
     &     InterpolatePDF,distance,tolerance,
     &     mCharm,mBottom,alphaSQ0,alphaSMZ
      parameter(nx=64,nq=48,np=12)
      parameter(xmin=1d-6,xmax=1d0,qsqmin=1d0,qsqmax=1d9,eps=1d-6)
      parameter(nhess=2*25)
      character set*2,prefix*(*),filename*60,oldprefix(0:nhess)*50
      character dummyChar,dummyWord*50
      double precision ff(np,nx,nq)
      double precision qqorig(nq),qq(nq),xx(nx),cc(np,0:nhess,nx,nq,4,4)
      double precision xxl(nx),qql(nq)
C--   Store distance along each eigenvector, tolerance,
C--   heavy quark masses and alphaS parameters in COMMON block.
      common/mstwCommon/distance,tolerance,
     &     mCharm,mBottom,alphaSQ0,alphaSMZ,alphaSorder,alphaSnfmax
      save
      data xx/1d-6,2d-6,4d-6,6d-6,8d-6,
     &     1d-5,2d-5,4d-5,6d-5,8d-5,
     &     1d-4,2d-4,4d-4,6d-4,8d-4,
     &     1d-3,2d-3,4d-3,6d-3,8d-3,
     &     1d-2,1.4d-2,2d-2,3d-2,4d-2,6d-2,8d-2,
     &     .1d0,.125d0,.15d0,.175d0,.2d0,.225d0,.25d0,.275d0,
     &     .3d0,.325d0,.35d0,.375d0,.4d0,.425d0,.45d0,.475d0,
     &     .5d0,.525d0,.55d0,.575d0,.6d0,.625d0,.65d0,.675d0,
     &     .7d0,.725d0,.75d0,.775d0,.8d0,.825d0,.85d0,.875d0,
     &     .9d0,.925d0,.95d0,.975d0,1d0/
      data qqorig/1.d0,
     &     1.25d0,1.5d0,0.d0,0.d0,2.5d0,3.2d0,4d0,5d0,6.4d0,8d0,
     &     1d1,1.2d1,0.d0,0.d0,2.6d1,4d1,6.4d1,1d2,
     &     1.6d2,2.4d2,4d2,6.4d2,1d3,1.8d3,3.2d3,5.6d3,1d4,
     &     1.8d4,3.2d4,5.6d4,1d5,1.8d5,3.2d5,5.6d5,1d6,
     &     1.8d6,3.2d6,5.6d6,1d7,1.8d7,3.2d7,5.6d7,1d8,
     &     1.8d8,3.2d8,5.6d8,1d9/

      if (f.lt.-6.or.f.gt.13) then
         print *,"Error: invalid parton flavour = ",f
         stop
      end if

      if (ih.lt.0.or.ih.gt.nhess) then
         print *,"Error: invalid eigenvector number = ",ih
         stop
      end if

C--   Check if the requested parton set is already in memory.
      if (oldprefix(ih).ne.prefix) then

C--   Start of initialisation for eigenvector set "i" ...
C--   Do this only the first time the set "i" is called,
C--   OR if the prefix has changed from the last time.

C--   Check that the character arrays "oldprefix" and "filename"
C--   are large enough.
         if (lentrim(prefix).gt.len(oldprefix(ih))) then
            print *,"Error in GetOnePDF: increase size of oldprefix"
            stop
         else if (lentrim(prefix)+7.gt.len(filename)) then
            print *,"Error in GetOnePDF: increase size of filename"
            stop
         end if

         write(set,'(I2.2)') ih  ! convert integer to string
C--   Remove trailing blanks from prefix before assigning filename.
         filename = prefix(1:lentrim(prefix))//'.'//set//'.dat'
C--   Line below can be commented out if you don't want this message.
         print *,"Reading PDF grid from ",filename(1:lentrim(filename))
         open(unit=33,file=filename,iostat=io,status='old')
         if (io.ne.0) then
            print *,"Error in GetOnePDF: can't open ",
     &           filename(1:lentrim(filename))
            stop
         end if

C--   Read header containing heavy quark masses and alphaS values.
         read(33,*) 
         read(33,*)
         read(33,*) dummyChar,dummyWord,dummyWord,dummyChar,
     &        distance,tolerance
         read(33,*) dummyChar,dummyWord,dummyChar,mCharm
         read(33,*) dummyChar,dummyWord,dummyChar,mBottom
         read(33,*) dummyChar,dummyWord,dummyChar,alphaSQ0
         read(33,*) dummyChar,dummyWord,dummyChar,alphaSMZ
         read(33,*) dummyChar,dummyWord,dummyWord,dummyChar,
     &        alphaSorder,alphaSnfmax
         read(33,*) dummyChar,dummyWord,dummyChar,nExtraFlavours
         read(33,*)
         read(33,*)
         mc2=mCharm**2
         mb2=mBottom**2

C--   Check that the heavy quark masses are sensible.
C--   Redistribute grid points if not in usual range.
         do m=1,nq
            qq(m) = qqorig(m)
         end do
         if (mc2.le.qq(1).or.mc2+eps.ge.qq(8)) then
            print *,"Error in GetOnePDF: invalid mCharm = ",mCharm
            stop
         else if (mc2.lt.qq(2)) then
            nqc0=2
            qq(4)=qq(2)
            qq(5)=qq(3)
         else if (mc2.lt.qq(3)) then
            nqc0=3
            qq(5)=qq(3)
         else if (mc2.lt.qq(6)) then
            nqc0=4
         else if (mc2.lt.qq(7)) then
            nqc0=5
            qq(4)=qq(6)
         else
            nqc0=6
            qq(4)=qq(6)
            qq(5)=qq(7)
         end if
         if (mb2.le.qq(12).or.mb2+eps.ge.qq(17)) then
            print *,"Error in GetOnePDF: invalid mBottom = ",mBottom
            stop
         else if (mb2.lt.qq(13)) then
            nqb0=13
            qq(15)=qq(13)
         else if (mb2.lt.qq(16)) then
            nqb0=14
         else
            nqb0=15
            qq(14)=qq(16)
         end if
         qq(nqc0)=mc2
         qq(nqc0+1)=mc2+eps
         qq(nqb0)=mb2
         qq(nqb0+1)=mb2+eps

C--   The nExtraFlavours variable is provided to aid compatibility
C--   with future grids where, for example, a photon distribution
C--   might be provided (cf. the MRST2004QED PDFs).
         if (nExtraFlavours.lt.0.or.nExtraFlavours.gt.1) then
            print *,"Error in GetOnePDF: invalid nExtraFlavours = ",
     &           nExtraFlavours
            stop
         end if

C--   Now read in the grids from the grid file.
         do n=1,nx-1
            do m=1,nq
               if (nExtraFlavours.gt.0) then
                  if (alphaSorder.eq.2) then ! NNLO
                     read(33,'(12(1pe12.4))',iostat=io)
     &                    (ff(ip,n,m),ip=1,12)
                  else          ! LO or NLO
                     ff(10,n,m) = 0.d0 ! = chm-cbar
                     ff(11,n,m) = 0.d0 ! = bot-bbar
                     read(33,'(10(1pe12.4))',iostat=io)
     &                    (ff(ip,n,m),ip=1,9),ff(12,n,m)
                  end if
               else             ! nExtraFlavours = 0
                  if (alphaSorder.eq.2) then ! NNLO
                     ff(12,n,m) = 0.d0 ! = photon
                     read(33,'(11(1pe12.4))',iostat=io)
     &                 (ff(ip,n,m),ip=1,11)
                  else          ! LO or NLO
                     ff(10,n,m) = 0.d0 ! = chm-cbar
                     ff(11,n,m) = 0.d0 ! = bot-bbar
                     ff(12,n,m) = 0.d0 ! = photon
                     read(33,'(9(1pe12.4))',iostat=io)
     &                    (ff(ip,n,m),ip=1,9)
                  end if
               end if
               if (io.ne.0) then
                  print *,"Error in GetOnePDF reading ",filename
                  stop
               end if
            enddo
         enddo

C--   Check that ALL the file contents have been read in.
         read(33,*,iostat=io) dummy
         if (io.eq.0) then
            print *,"Error in GetOnePDF: not at end of ",filename
            stop
         end if
         close(unit=33)

C--   PDFs are identically zero at x = 1.
         do m=1,nq
            do ip=1,np
               ff(ip,nx,m)=0d0
            enddo
         enddo

         do n=1,nx
            xxl(n)=log10(xx(n))
         enddo
         do m=1,nq
            qql(m)=log10(qq(m))
         enddo

C--   Initialise all parton flavours.
         do ip=1,np
            call InitialisePDF(ip,np,ih,nhess,nx,nq,nqc0,nqb0,
     &           xxl,qql,ff,cc)
         enddo

         oldprefix(ih) = prefix

C--   ... End of initialisation for eigenvector set "ih".

      end if                    ! oldprefix(ih).ne.prefix

C----------------------------------------------------------------------

      qsq=q*q
C--   If mc2 < qsq < mc2+eps, then qsq = mc2+eps.
      if (qsq.gt.qq(nqc0).and.qsq.lt.qq(nqc0+1)) qsq = qq(nqc0+1)
C--   If mb2 < qsq < mb2+eps, then qsq = mb2+eps.
      if (qsq.gt.qq(nqb0).and.qsq.lt.qq(nqb0+1)) qsq = qq(nqb0+1)
      
      xlog=log10(x)
      qsqlog=log10(qsq)

      res = 0.d0

      if (f.eq.0) then          ! gluon
         ip = 1
      else if (f.ge.1.and.f.le.5) then ! quarks
         ip = f+1
      else if (f.le.-1.and.f.ge.-5) then ! antiquarks
         ip = -f+1
      else if (f.ge.7.and.f.le.11) then ! valence quarks
         ip = f
      else if (f.eq.13) then    ! photon
         ip = 12
      else if (abs(f).ne.6.and.f.ne.12) then
         if (warn.or.fatal) print *,"Error in GetOnePDF: f = ",f
         if (fatal) stop
      end if
      
      if (x.le.0.d0.or.x.gt.xmax.or.q.le.0.d0) then

         if (warn.or.fatal) print *,"Error in GetOnePDF: x,qsq = ",
     &        x,qsq
         if (fatal) stop

      else if (abs(f).eq.6.or.f.eq.12) then ! set top quarks to zero
         
         res = 0.d0

      else if (qsq.lt.qsqmin) then ! extrapolate to low Q^2

         if (warn) then
            print *, "Warning in GetOnePDF, extrapolating: f = ",f,
     &           ", x = ",x,", q = ",q
         end if

         if (x.lt.xmin) then    ! extrapolate to low x

            res = ExtrapolatePDF(ip,np,ih,nhess,xlog,
     &           log10(qsqmin),nx,nq,xxl,qql,cc)
            res1 = ExtrapolatePDF(ip,np,ih,nhess,xlog,
     &           log10(1.01D0*qsqmin),nx,nq,xxl,qql,cc)
            if (f.le.-1.and.f.ge.-5) then ! antiquark = quark - valence
               res = res - ExtrapolatePDF(ip+5,np,ih,nhess,xlog,
     &              log10(qsqmin),nx,nq,xxl,qql,cc)
               res1 = res1 - ExtrapolatePDF(ip+5,np,ih,nhess,xlog,
     &              log10(1.01D0*qsqmin),nx,nq,xxl,qql,cc)
            end if
            
         else                   ! do usual interpolation
            
            res = InterpolatePDF(ip,np,ih,nhess,xlog,
     &           log10(qsqmin),nx,nq,xxl,qql,cc)
            res1 = InterpolatePDF(ip,np,ih,nhess,xlog,
     &           log10(1.01D0*qsqmin),nx,nq,xxl,qql,cc)
            if (f.le.-1.and.f.ge.-5) then ! antiquark = quark - valence
               res = res - InterpolatePDF(ip+5,np,ih,nhess,xlog,
     &              log10(qsqmin),nx,nq,xxl,qql,cc)
               res1 = res1 - InterpolatePDF(ip+5,np,ih,nhess,xlog,
     &              log10(1.01D0*qsqmin),nx,nq,xxl,qql,cc)
            end if
            
         end if

C--   Calculate the anomalous dimension, dlog(xf)/dlog(qsq),
C--   evaluated at qsqmin.  Then extrapolate the PDFs to low
C--   qsq < qsqmin by interpolating the anomalous dimenion between
C--   the value at qsqmin and a value of 1 for qsq << qsqmin.
C--   If value of PDF at qsqmin is very small, just set
C--   anomalous dimension to 1 to prevent rounding errors.
C--   Impose minimum anomalous dimension of -2.5.
         if (abs(res).ge.1.D-5) then
            anom = max( -2.5D0, (res1-res)/res/0.01D0 )
         else
            anom = 1.D0
         end if
         res = res*(qsq/qsqmin)**(anom*qsq/qsqmin+1.D0-qsq/qsqmin)

      else if (x.lt.xmin.or.qsq.gt.qsqmax) then ! extrapolate

         if (warn) then
            print *, "Warning in GetOnePDF, extrapolating: f = ",f,
     &           ", x = ",x,", q = ",q
         end if

         res = ExtrapolatePDF(ip,np,ih,nhess,xlog,
     &        qsqlog,nx,nq,xxl,qql,cc)
         
         if (f.le.-1.and.f.ge.-5) then ! antiquark = quark - valence
            res = res - ExtrapolatePDF(ip+5,np,ih,nhess,xlog,
     &           qsqlog,nx,nq,xxl,qql,cc)
         end if

      else                      ! do usual interpolation
         
         res = InterpolatePDF(ip,np,ih,nhess,xlog,
     &        qsqlog,nx,nq,xxl,qql,cc)

         if (f.le.-1.and.f.ge.-5) then ! antiquark = quark - valence
            res = res - InterpolatePDF(ip+5,np,ih,nhess,xlog,
     &           qsqlog,nx,nq,xxl,qql,cc)
         end if
            
      end if
      
      GetOnePDF = res

      return
      end

C----------------------------------------------------------------------

      subroutine InitialisePDF(ip,np,ih,nhess,nx,my,myc0,myb0,
     &     xx,yy,ff,cc)
      implicit none
      integer nhess,ih,nx,my,myc0,myb0,j,k,l,m,n,ip,np
      double precision xx(nx),yy(my),ff(np,nx,my),
     &     ff1(nx,my),ff2(nx,my),ff12(nx,my),ff21(nx,my),
     &     yy0(4),yy1(4),yy2(4),yy12(4),z(16),
     &     cl(16),cc(np,0:nhess,nx,my,4,4),iwt(16,16),
     &     polderiv1,polderiv2,polderiv3,d1,d2,d1d2,xxd

      data iwt/1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
     &     0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,
     &     -3,0,0,3,0,0,0,0,-2,0,0,-1,0,0,0,0,
     &     2,0,0,-2,0,0,0,0,1,0,0,1,0,0,0,0,
     &     0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,
     &     0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,
     &     0,0,0,0,-3,0,0,3,0,0,0,0,-2,0,0,-1,
     &     0,0,0,0,2,0,0,-2,0,0,0,0,1,0,0,1,
     &     -3,3,0,0,-2,-1,0,0,0,0,0,0,0,0,0,0,
     &     0,0,0,0,0,0,0,0,-3,3,0,0,-2,-1,0,0,
     &     9,-9,9,-9,6,3,-3,-6,6,-6,-3,3,4,2,1,2,
     &     -6,6,-6,6,-4,-2,2,4,-3,3,3,-3,-2,-1,-1,-2,
     &     2,-2,0,0,1,1,0,0,0,0,0,0,0,0,0,0,
     &     0,0,0,0,0,0,0,0,2,-2,0,0,1,1,0,0,
     &     -6,6,-6,6,-3,-3,3,3,-4,4,2,-2,-2,-2,-1,-1,
     &     4,-4,4,-4,2,2,-2,-2,2,-2,-2,2,1,1,1,1/

      do m=1,my
         ff1(1,m)=polderiv1(xx(1),xx(2),xx(3),
     &        ff(ip,1,m),ff(ip,2,m),ff(ip,3,m))
         ff1(nx,m)=polderiv3(xx(nx-2),xx(nx-1),xx(nx),
     &        ff(ip,nx-2,m),ff(ip,nx-1,m),ff(ip,nx,m))
         do n=2,nx-1
            ff1(n,m)=polderiv2(xx(n-1),xx(n),xx(n+1),
     &           ff(ip,n-1,m),ff(ip,n,m),ff(ip,n+1,m))
         enddo
      enddo

C--   Calculate the derivatives at qsq=mc2,mc2+eps,mb2,mb2+eps
C--   in a similar way as at the endpoints qsqmin and qsqmax.
      do n=1,nx
         do m=1,my
            if (myc0.eq.2.and.m.eq.1) then
               ff2(n,m)=(ff(ip,n,m+1)-ff(ip,n,m))/(yy(m+1)-yy(m))
            else if (myc0.eq.2.and.m.eq.2) then
               ff2(n,m)=(ff(ip,n,m)-ff(ip,n,m-1))/(yy(m)-yy(m-1))
            else if (m.eq.1.or.m.eq.myc0+1.or.m.eq.myb0+1) then
               ff2(n,m)=polderiv1(yy(m),yy(m+1),yy(m+2),
     &              ff(ip,n,m),ff(ip,n,m+1),ff(ip,n,m+2))
            else if (m.eq.my.or.m.eq.myc0.or.m.eq.myb0) then
               ff2(n,m)=polderiv3(yy(m-2),yy(m-1),yy(m),
     &              ff(ip,n,m-2),ff(ip,n,m-1),ff(ip,n,m))
            else
               ff2(n,m)=polderiv2(yy(m-1),yy(m),yy(m+1),
     &              ff(ip,n,m-1),ff(ip,n,m),ff(ip,n,m+1))
            end if
         end do
      end do

C--   Calculate the cross derivatives (d/dx)(d/dy).
      do m=1,my
         ff12(1,m)=polderiv1(xx(1),xx(2),xx(3),
     &        ff2(1,m),ff2(2,m),ff2(3,m))
         ff12(nx,m)=polderiv3(xx(nx-2),xx(nx-1),xx(nx),
     &        ff2(nx-2,m),ff2(nx-1,m),ff2(nx,m))
         do n=2,nx-1
            ff12(n,m)=polderiv2(xx(n-1),xx(n),xx(n+1),
     &           ff2(n-1,m),ff2(n,m),ff2(n+1,m))
         enddo
      enddo

C--   Calculate the cross derivatives (d/dy)(d/dx).
      do n=1,nx
         do m = 1, my
            if (myc0.eq.2.and.m.eq.1) then
               ff21(n,m)=(ff1(n,m+1)-ff1(n,m))/(yy(m+1)-yy(m))
            else if (myc0.eq.2.and.m.eq.2) then
               ff21(n,m)=(ff1(n,m)-ff1(n,m-1))/(yy(m)-yy(m-1))
            else if (m.eq.1.or.m.eq.myc0+1.or.m.eq.myb0+1) then
               ff21(n,m)=polderiv1(yy(m),yy(m+1),yy(m+2),
     &              ff1(n,m),ff1(n,m+1),ff1(n,m+2))
            else if (m.eq.my.or.m.eq.myc0.or.m.eq.myb0) then
               ff21(n,m)=polderiv3(yy(m-2),yy(m-1),yy(m),
     &              ff1(n,m-2),ff1(n,m-1),ff1(n,m))
            else
               ff21(n,m)=polderiv2(yy(m-1),yy(m),yy(m+1),
     &              ff1(n,m-1),ff1(n,m),ff1(n,m+1))
            end if
         end do
      end do

C--   Take the average of (d/dx)(d/dy) and (d/dy)(d/dx).
      do n=1,nx
         do m = 1, my
            ff12(n,m)=0.5*(ff12(n,m)+ff21(n,m))
         end do
      end do

      do n=1,nx-1
         do m=1,my-1
            d1=xx(n+1)-xx(n)
            d2=yy(m+1)-yy(m)
            d1d2=d1*d2
            
            yy0(1)=ff(ip,n,m)
            yy0(2)=ff(ip,n+1,m)
            yy0(3)=ff(ip,n+1,m+1)
            yy0(4)=ff(ip,n,m+1)
            
            yy1(1)=ff1(n,m)
            yy1(2)=ff1(n+1,m)
            yy1(3)=ff1(n+1,m+1)
            yy1(4)=ff1(n,m+1)
            
            yy2(1)=ff2(n,m)
            yy2(2)=ff2(n+1,m)
            yy2(3)=ff2(n+1,m+1)
            yy2(4)=ff2(n,m+1)
            
            yy12(1)=ff12(n,m)
            yy12(2)=ff12(n+1,m)
            yy12(3)=ff12(n+1,m+1)
            yy12(4)=ff12(n,m+1)
            
            do k=1,4
               z(k)=yy0(k)
               z(k+4)=yy1(k)*d1
               z(k+8)=yy2(k)*d2
               z(k+12)=yy12(k)*d1d2
            enddo
            
            do l=1,16
               xxd=0.d0
               do k=1,16
                  xxd=xxd+iwt(k,l)*z(k)
               enddo
               cl(l)=xxd
            enddo
            l=0
            do k=1,4
               do j=1,4
                  l=l+1
                  cc(ip,ih,n,m,k,j)=cl(l)
               enddo
            enddo
         enddo
      enddo
      return
      end

C----------------------------------------------------------------------

      double precision function InterpolatePDF(ip,np,ih,nhess,x,y,
     &     nx,my,xx,yy,cc)
      implicit none
      integer ih,nx,my,nhess,locx,l,m,n,ip,np
      double precision xx(nx),yy(my),cc(np,0:nhess,nx,my,4,4),
     &     x,y,z,t,u

      n=locx(xx,nx,x)
      m=locx(yy,my,y)
      
      t=(x-xx(n))/(xx(n+1)-xx(n))
      u=(y-yy(m))/(yy(m+1)-yy(m))
      
      z=0.d0
      do l=4,1,-1
         z=t*z+((cc(ip,ih,n,m,l,4)*u+cc(ip,ih,n,m,l,3))*u
     &        +cc(ip,ih,n,m,l,2))*u+cc(ip,ih,n,m,l,1)
      enddo

      InterpolatePDF = z

      return
      end

C----------------------------------------------------------------------

      double precision function ExtrapolatePDF(ip,np,ih,nhess,x,y,
     &     nx,my,xx,yy,cc)
      implicit none
      integer ih,nx,my,nhess,locx,n,m,ip,np
      double precision xx(nx),yy(my),cc(np,0:nhess,nx,my,4,4),
     &     x,y,z,f0,f1,z0,z1,InterpolatePDF
      
      n=locx(xx,nx,x)           ! 0: below xmin, nx: above xmax
      m=locx(yy,my,y)           ! 0: below qsqmin, my: above qsqmax
      
C--   If extrapolation in small x only:
      if (n.eq.0.and.m.gt.0.and.m.lt.my) then
         f0 = InterpolatePDF(ip,np,ih,nhess,xx(1),y,nx,my,xx,yy,cc)
         f1 = InterpolatePDF(ip,np,ih,nhess,xx(2),y,nx,my,xx,yy,cc)
         if (f0.gt.1.d-3.and.f1.gt.1.d-3) then
            z = exp(log(f0)+(log(f1)-log(f0))/(xx(2)-xx(1))*(x-xx(1)))
         else
            z = f0+(f1-f0)/(xx(2)-xx(1))*(x-xx(1))
         end if
C--   If extrapolation into large q only:
      else if (n.gt.0.and.m.eq.my) then
         f0 = InterpolatePDF(ip,np,ih,nhess,x,yy(my),nx,my,xx,yy,cc)
         f1 = InterpolatePDF(ip,np,ih,nhess,x,yy(my-1),nx,my,xx,yy,cc)
         if (f0.gt.1.d-3.and.f1.gt.1.d-3) then
            z = exp(log(f0)+(log(f0)-log(f1))/(yy(my)-yy(my-1))*
     &           (y-yy(my)))
         else
            z = f0+(f0-f1)/(yy(my)-yy(my-1))*(y-yy(my))
         end if
C--   If extrapolation into large q AND small x:
      else if (n.eq.0.and.m.eq.my) then
         f0 = InterpolatePDF(ip,np,ih,nhess,xx(1),yy(my),nx,my,xx,yy,cc)
         f1 = InterpolatePDF(ip,np,ih,nhess,xx(1),yy(my-1),nx,my,xx,yy,
     &        cc)
         if (f0.gt.1.d-3.and.f1.gt.1.d-3) then
            z0 = exp(log(f0)+(log(f0)-log(f1))/(yy(my)-yy(my-1))*
     &           (y-yy(my)))
         else
            z0 = f0+(f0-f1)/(yy(my)-yy(my-1))*(y-yy(my))
         end if
         f0 = InterpolatePDF(ip,np,ih,nhess,xx(2),yy(my),nx,my,xx,yy,cc)
         f1 = InterpolatePDF(ip,np,ih,nhess,xx(2),yy(my-1),nx,my,xx,yy,
     &        cc)
         if (f0.gt.1.d-3.and.f1.gt.1.d-3) then
            z1 = exp(log(f0)+(log(f0)-log(f1))/(yy(my)-yy(my-1))*
     &           (y-yy(my)))
         else
            z1 = f0+(f0-f1)/(yy(my)-yy(my-1))*(y-yy(my))
         end if
         if (z0.gt.1.d-3.and.z1.gt.1.d-3) then
            z = exp(log(z0)+(log(z1)-log(z0))/(xx(2)-xx(1))*(x-xx(1)))
         else
            z = z0+(z1-z0)/(xx(2)-xx(1))*(x-xx(1))
         end if
      else
         print *,"Error in ExtrapolatePDF"
         stop
      end if

      ExtrapolatePDF = z      

      return
      end

C----------------------------------------------------------------------

      integer function locx(xx,nx,x)
C--   returns an integer j such that x lies inbetween xx(j) and xx(j+1).
C--   nx is the length of the array with xx(nx) the highest element.
      implicit none
      integer nx,jl,ju,jm
      double precision x,xx(nx)
      if(x.eq.xx(1)) then
         locx=1
         return
      endif
      if(x.eq.xx(nx)) then
         locx=nx-1  
         return
      endif
      ju=nx+1
      jl=0
    1 if((ju-jl).le.1) go to 2
      jm=(ju+jl)/2
      if(x.ge.xx(jm)) then
         jl=jm
      else
         ju=jm
      endif
      go to 1
    2 locx=jl
      return
      end

C----------------------------------------------------------------------

      double precision function polderiv1(x1,x2,x3,y1,y2,y3)
C--   returns the estimate of the derivative at x1 obtained by a
C--   polynomial interpolation using the three points (x_i,y_i).
      implicit none
      double precision x1,x2,x3,y1,y2,y3
      polderiv1=(x3*x3*(y1-y2)+2.d0*x1*(x3*(-y1+y2)+x2*(y1-y3))
     &     +x2*x2*(-y1+y3)+x1*x1*(-y2+y3))/((x1-x2)*(x1-x3)*(x2-x3))
      return
      end

      double precision function polderiv2(x1,x2,x3,y1,y2,y3)
C--   returns the estimate of the derivative at x2 obtained by a
C--   polynomial interpolation using the three points (x_i,y_i).
      implicit none
      double precision x1,x2,x3,y1,y2,y3
      polderiv2=(x3*x3*(y1-y2)-2.d0*x2*(x3*(y1-y2)+x1*(y2-y3))
     &     +x2*x2*(y1-y3)+x1*x1*(y2-y3))/((x1-x2)*(x1-x3)*(x2-x3))
      return
      end

      double precision function polderiv3(x1,x2,x3,y1,y2,y3)
C--   returns the estimate of the derivative at x3 obtained by a
C--   polynomial interpolation using the three points (x_i,y_i).
      implicit none
      double precision x1,x2,x3,y1,y2,y3
      polderiv3=(x3*x3*(-y1+y2)+2.d0*x2*x3*(y1-y3)+x1*x1*(y2-y3)
     &     +x2*x2*(-y1+y3)+2.d0*x1*x3*(-y2+y3))/
     &     ((x1-x2)*(x1-x3)*(x2-x3))
      return
      end

C----------------------------------------------------------------------

C--   G.W. 05/07/2010 Avoid using Fortran 90 intrinsic function
C--   "len_trim" since not supported by all Fortran 77 compilers.
      integer function lentrim(s)
      implicit none
      character*(*) s
      do lentrim = len(s), 1, -1
         if (s(lentrim:lentrim) .ne. ' ') return
      end do
      return
      end

C----------------------------------------------------------------------


!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
! DSS07
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

********************************************************************
*                                                                  *
*        fDSS  UNPOLARIZED FRAGMENTATION FUNCTIONS                 *
*  D.de Florian, R.Sassot, M.Stratmann   hep-ph/0703242)           *
*                                                                  *
*     CALL fDSS (IH,IC,IO, X, Q2, U, UB, D, DB, S, SB, C, B, GL)   *
*                                                                  *	
*  INPUT:                                                          *
*  IH = hadron type    1: PION                                     *
*                      2: KAON                                     *
*                      3: PROTON                                   *
*                      4: CHARGED HADRONS                          *
*                                                                  *
*  IC = Hadron Charge  0: 0 (as average of + and -)                *
*                      1: +                                        *
*                     -1: -                                        *
*                                                                  *
*  IO= Order           0: LO                                       *
*                      1: NLO                                      *
*                                                                  *
*            X                    (between  0.05   and  1.0)       *
*            Q2 = scale in GeV**2 (between  1.0    and  1.D5)      *
*             (for values outside the allowed range the program    *
*              writes a warning and extrapolates to the x and      *
*              Q2 values requested)                                *
*                                                                  *
*   OUTPUT: U, UB, D, DB, S, SB,   C,           B,       GL        *
*           U Ubar D Dbar S Sbar Charm=Cbar Bottom=Bbar Gluon      *
*           Always X times the distribution is returned            *
*                                                                  *
*                                                                  *
*   COMMON:  The main program or the calling routine has to have   *
*            a common block  COMMON / FRAGINI / FINI , and  FINI   *
*            has always to be zero when DSS is called for the      *
*            first time or when the SET has been changed.          *
*                                                                  *
********************************************************************

      SUBROUTINE fDSS (IH,IC,IO, X, Q2, U, UB, D, DB, S, SB, C, B, GL)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INTEGER FINI
      PARAMETER (NPART=9, NX=35, NQ=24, NARG=2)
      DIMENSION XUTOTF(NX,NQ), XDTOTF(NX,NQ), XSTOTF(NX,NQ)
      DIMENSION XUVALF(NX,NQ), XDVALF(NX,NQ), XSVALF(NX,NQ)
      DIMENSION XCTOTF(NX,NQ), XBTOTF(NX,NQ)
      DIMENSION XGF(NX,NQ), PARTON (NPART,NQ,NX-1)
      DIMENSION QS(NQ), XB(NX), XT(NARG), NA(NARG), ARRF(NX+NQ) 
      COMMON / FRAGINI / FINI
      SAVE XUTOTF, XDTOTF, XSTOTF, XCTOTF, XBTOTF, XGF, NA, ARRF
      SAVE XUVALF, XDVALF, XSVALF
*...BJORKEN-X AND Q**2 VALUES OF THE GRID :
       DATA QS / 1.d0, 1.25D0, 1.5D0, 2.5D0, 
     1           4.0D0, 6.4D0, 1.0D1, 1.5D1, 2.5D1, 4.0D1, 6.4D1,
     2           1.0D2, 1.8D2, 3.2D2, 5.8D2, 1.0D3, 1.8D3,
     3           3.2D3, 5.8D3, 1.0D4, 1.8D4, 3.2D4, 5.8D4, 1.0D5/
       DATA XB /0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09,
     4        0.095, 0.1, 0.125, 0.15, 0.175, 0.2, 0.225, 0.25, 0.275,
     5        0.3, 0.325, 0.35, 0.375, 0.4, 0.45,  0.5, 0.55,
     6        0.6, 0.65,  0.7,  0.75,  0.8, 0.85,  0.9 , 0.93, 1.0/
*...CHECK OF X AND Q2 VALUES : 
       IF ( (X.LT.0.05D0) .OR. (X.GT.1.0D0) ) THEN
C           WRITE(6,91)
C  91       FORMAT (2X,'PARTON INTERPOLATION: X OUT OF RANGE')
C          STOP
       ENDIF
       IF ( (Q2.LT.1.D0) .OR. (Q2.GT.1.D5) ) THEN
           WRITE(6,92) 
  92       FORMAT (2X,'PARTON INTERPOLATION: Q2 OUT OF RANGE')
C          STOP
       ENDIF
*...INITIALIZATION :
*    SELECTION AND READING OF THE GRID :
       IF (FINI.NE.0) GOTO 16
      IF ((IH.EQ.1).and.(IO.EQ.1)) THEN
       IIREAD=11       
       OPEN(IIREAD,FILE='PINLO.GRID')
      ELSEIF ((IH.EQ.1).and.(IO.EQ.0)) THEN
       IIREAD=12       
       OPEN(IIREAD,FILE='PILO.GRID')
      ELSEIF ((IH.EQ.2).and.(IO.EQ.1)) THEN
       IIREAD=11       
       OPEN(IIREAD,FILE='KANLO.GRID')
       ELSEIF ((IH.EQ.2).and.(IO.EQ.0)) THEN
       IIREAD=12       
       OPEN(IIREAD,FILE='KALO.GRID')
      ELSEIF ((IH.EQ.3).and.(IO.EQ.1)) THEN
       IIREAD=11       
       OPEN(IIREAD,FILE='PRONLO.GRID')       
      ELSEIF ((IH.EQ.3).and.(IO.EQ.0)) THEN
       IIREAD=12       
       OPEN(IIREAD,FILE='PROLO.GRID')       
      ELSEIF ((IH.EQ.4).and.(IO.EQ.1)) THEN
       IIREAD=11       
       OPEN(IIREAD,FILE='HNLO.GRID')       
      ELSEIF ((IH.EQ.4).and.(IO.EQ.0)) THEN
       IIREAD=12       
       OPEN(IIREAD,FILE='HLO.GRID')       
	ELSE
         WRITE(6,93)
 93      FORMAT (2X,' WRONG SET')
         STOP
      END IF
C
       DO 15 M = 1, NX-1 
       DO 15 N = 1, NQ
       READ(IIREAD,90) PARTON(1,N,M), PARTON(2,N,M), PARTON(3,N,M), 
     1                 PARTON(4,N,M), PARTON(5,N,M), PARTON(6,N,M),
     2                 PARTON(7,N,M), PARTON(8,N,M), PARTON(9,N,M)
  90   FORMAT (9(1PE10.3))
  15   CONTINUE
       CLOSE(IIREAD)
C
      FINI = 1
*....ARRAYS FOR THE INTERPOLATION SUBROUTINE :
      DO 10 IQ = 1, NQ
      DO 20 IX = 1, NX-1
        XB0 = XB(IX) 
        XB1 = 1.D0-XB(IX)
        XUTOTF(IX,IQ) = PARTON(1,IQ,IX) / (XB1**4 * XB0**0.5)
        XDTOTF(IX,IQ) = PARTON(2,IQ,IX) / (XB1**4 * XB0**0.5)
        XSTOTF(IX,IQ) = PARTON(3,IQ,IX) / (XB1**4 * XB0**0.5) 
        XCTOTF(IX,IQ) = PARTON(4,IQ,IX) / (XB1**7 * XB0**0.3) 
        XBTOTF(IX,IQ) = PARTON(5,IQ,IX) / (XB1**7 * XB0**0.3)
        XGF(IX,IQ)    = PARTON(6,IQ,IX) / (XB1**4 * XB0**0.3)
        XUVALF(IX,IQ) = PARTON(7,IQ,IX) / (XB1**4 * XB0**0.5)
        XDVALF(IX,IQ) = PARTON(8,IQ,IX) / (XB1**4 * XB0**0.5)
        XSVALF(IX,IQ) = PARTON(9,IQ,IX) / (XB1**4 * XB0**0.5)
  20  CONTINUE
        XUTOTF(NX,IQ) = 0.D0
        XDTOTF(NX,IQ) = 0.D0
        XSTOTF(NX,IQ) = 0.D0
        XCTOTF(NX,IQ) = 0.D0
        XBTOTF(NX,IQ) = 0.D0
        XGF(NX,IQ)    = 0.D0
        XUVALF(NX,IQ) = 0.D0
        XDVALF(NX,IQ) = 0.D0
        XSVALF(NX,IQ) = 0.D0
  10  CONTINUE  
      NA(1) = NX
      NA(2) = NQ
      DO 30 IX = 1, NX
        ARRF(IX) = DLOG(XB(IX))
  30  CONTINUE
      DO 40 IQ = 1, NQ
        ARRF(NX+IQ) = DLOG(QS(IQ))
  40  CONTINUE
  16  CONTINUE
*...INTERPOLATION :
      XT(1) = DLOG(X)
      XT(2) = DLOG(Q2)
      UTOT = SFINT(NARG,XT,NA,ARRF,XUTOTF) * (1.D0-X)**4 * X**0.5
      DTOT = SFINT(NARG,XT,NA,ARRF,XDTOTF) * (1.D0-X)**4 * X**0.5 
      STOT = SFINT(NARG,XT,NA,ARRF,XSTOTF) * (1.D0-X)**4 * X**0.5
      CTOT = SFINT(NARG,XT,NA,ARRF,XCTOTF) * (1.D0-X)**7 * X**0.3
      BTOT = SFINT(NARG,XT,NA,ARRF,XBTOTF) * (1.D0-X)**7 * X**0.3
      GL   = SFINT(NARG,XT,NA,ARRF,XGF)    * (1.D0-X)**4 * X**0.3
      UVAL = SFINT(NARG,XT,NA,ARRF,XUVALF) * (1.D0-X)**4 * X**0.5
      DVAL = SFINT(NARG,XT,NA,ARRF,XDVALF) * (1.D0-X)**4 * X**0.5 
      SVAL = SFINT(NARG,XT,NA,ARRF,XSVALF) * (1.D0-X)**4 * X**0.5
       
       Up  = (UTOT+UVAL)/2.
       UBp = (UTOT-UVAL)/2.
       Dp  = (DTOT+DVAL)/2.
       DBp = (DTOT-DVAL)/2.
       Sp  = (STOT+SVAL)/2.
       SBp = (STOT-SVAL)/2.
       Cp  =  CTOT/2.
       Bp  =  BTOT/2.
              
       IF (IC.EQ.1) THEN
       U  = Up
       UB = UBp
       D  = Dp 
       DB = DBp
       S  = Sp
       SB = SBp
       C  = Cp
       B  = Bp
       ELSEIF (IC.EQ.-1) THEN
       U  = UBp
       UB = Up
       D  = DBp 
       DB = Dp
       S  = SBp
       SB = Sp
       C  = Cp
       B  = Bp
       ELSEIF (IC.EQ.0) THEN
       U  = (UBp+Up)/2.
       UB =  U
       D  = (DBp+Dp)/2. 
       DB =  D
       S  = (SBp+Sp)/2.
       SB =  S
       C  =  Cp
       B  =  Bp 
       ELSE
         WRITE(6,94)
 94      FORMAT (2X,' WRONG CHARGE')
         STOP
       END IF 
 60   RETURN
       END
*
*...CERN LIBRARY ROUTINE E104 (INTERPOLATION) :
*
      FUNCTION SFINT(NARG,ARG,NENT,ENT,TABLE)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION ARG(2),NENT(2),ENT(59),TABLE(840)
      DIMENSION D(2),NCOMB(2),IENT(2)
      KD=1
      M=1
      JA=1
         DO 5 I=1,NARG
      NCOMB(I)=1
      JB=JA-1+NENT(I)
         DO 2 J=JA,JB
      IF (ARG(I).LE.ENT(J)) GO TO 3
    2 CONTINUE
      J=JB
    3 IF (J.NE.JA) GO TO 4
      J=J+1
    4 JR=J-1
      D(I)=(ENT(J)-ARG(I))/(ENT(J)-ENT(JR))
      IENT(I)=J-JA
      KD=KD+IENT(I)*M
      M=M*NENT(I)
    5 JA=JB+1
      SFINT=0.D0
   10 FAC=1.D0
      IADR=KD
      IFADR=1
         DO 15 I=1,NARG
      IF (NCOMB(I).EQ.0) GO TO 12
      FAC=FAC*(1.D0-D(I))
      GO TO 15
   12 FAC=FAC*D(I)
      IADR=IADR-IFADR
   15 IFADR=IFADR*NENT(I)
      SFINT=SFINT+FAC*TABLE(IADR)
      IL=NARG
   40 IF (NCOMB(IL).EQ.0) GO TO 80
      NCOMB(IL)=0
      IF (IL.EQ.NARG) GO TO 10
      IL=IL+1
         DO 50  K=IL,NARG
   50 NCOMB(K)=1
      GO TO 10
   80 IL=IL-1
      IF(IL.NE.0) GO TO 40
      RETURN
      END





!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
! alphas to use with mmht
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

C----------------------------------------------------------------------
C--   Stand-alone code for alpha_s cannibalised (with permission)
C--   from Andreas Vogt's QCD-PEGASUS package (hep-ph/0408244).
C--   The running coupling alpha_s is obtained at N^mLO (m = 0,1,2,3)
C--   by solving the renormalisation group equation in the MSbar scheme
C--   by a fourth-order Runge-Kutta integration.  Transitions from
C--   n_f to n_f+1 flavours are made when the factorisation scale
C--   mu_f equals the pole masses m_h (h = c,b,t).  At exactly
C--   the thresholds m_{c,b,t}, the number of flavours n_f = {3,4,5}.
C--   The top quark mass should be set to be very large to evolve with
C--   a maximum of five flavours.  The factorisation scale mu_f may be
C--   a constant multiple of the renormalisation scale mu_r.  The input
C--   factorisation scale mu_(f,0) should be less than or equal to
C--   the charm quark mass.  However, if it is greater than the
C--   charm quark mass, the value of alpha_s at mu_(f,0) = 1 GeV will
C--   be found using a root-finding algorithm.
C--
C--   Example of usage.
C--   First call the initialisation routine (only needed once):
C--
C--    IORD = 2                  ! perturbative order (N^mLO,m=0,1,2,3)
C--    FR2 = 1.D0                ! ratio of mu_f^2 to mu_r^2
C--    MUR = 1.D0                ! input mu_r in GeV
C--    ASMUR = 0.5D0             ! input value of alpha_s at mu_r
C--    MC = 1.4D0                ! charm quark mass
C--    MB = 4.75D0               ! bottom quark mass
C--    MT = 1.D10                ! top quark mass
C--    CALL INITALPHAS(IORD, FR2, MUR, ASMUR, MC, MB, MT)
C--
C--   Then get alpha_s at a renormalisation scale mu_r with:
C--
C--    MUR = 100.D0              ! renormalisation scale in GeV
C--    ALFAS = MSTWALPHAS(MUR)
C--
C----------------------------------------------------------------------
C--   Comments to Graeme Watt <Graeme.Watt(at)cern.ch>
C----------------------------------------------------------------------

      SUBROUTINE INITALPHAS(IORD, FR2, MUR, ASMUR, MC, MB, MT)
C--   IORD = 0 (LO), 1 (NLO), 2 (NNLO), 3 (NNNLO).
C--   FR2 = ratio of mu_f^2 to mu_r^2 (must be a fixed value).
C--   MUR = input renormalisation scale (in GeV) for alpha_s.
C--   ASMUR = input value of alpha_s at the renormalisation scale MUR.
C--   MC,MB,MT = heavy quark masses in GeV.
      IMPLICIT NONE
      INTEGER IORD,IORDc,MAXF,MODE
      DOUBLE PRECISION FR2,MUR,ASMUR,MC,MB,MT,EPS,A,B,DZEROX,
     &     R0c,FR2c,MURc,ASMURc,MCc,MBc,MTc,FINDALPHASR0,R0,ASI
      COMMON / DZEROXcommon / FR2c,MURc,ASMURc,MCc,MBc,MTc,R0c,IORDc
      PARAMETER(EPS=1.D-10,MAXF=10000,MODE=1)
      EXTERNAL FINDALPHASR0

      IF (MUR*sqrt(FR2).LE.MC) THEN ! Check that MUF <= MC.
         R0 = MUR
         ASI = ASMUR
      ELSE                      ! Solve for alpha_s at R0 = 1 GeV.
C--   Copy variables to common block.
         R0c = 1.D0/sqrt(FR2)
         IORDc = IORD
         FR2c = FR2
         MURc = MUR
         ASMURc = ASMUR
         MCc = MC
         MBc = MB
         MTc = MT
C--   Now get alpha_s(R0) corresponding to alpha_s(MUR).
         A = 0.02D0              ! lower bound for alpha_s(R0)
         B = 2.00D0              ! upper bound for alpha_s(R0)
         R0 = R0c
         ASI = DZEROX(A,B,EPS,MAXF,FINDALPHASR0,MODE)
      END IF

      CALL INITALPHASR0(IORD, FR2, R0, ASI, MC, MB, MT)

      RETURN
      END

C----------------------------------------------------------------------

C--   Find the zero of this function using DZEROX.
      DOUBLE PRECISION FUNCTION FINDALPHASR0(ASI)
      IMPLICIT NONE
      INTEGER IORD
      DOUBLE PRECISION FR2, R0, ASI, MC, MB, MT, MUR, ASMUR, MSTWALPHAS
      COMMON / DZEROXcommon / FR2, MUR, ASMUR, MC, MB, MT, R0, IORD

      CALL INITALPHASR0(IORD, FR2, R0, ASI, MC, MB, MT)
      FINDALPHASR0 = MSTWALPHAS(MUR) - ASMUR ! solve equal to zero

      RETURN
      END

C----------------------------------------------------------------------

      SUBROUTINE INITALPHASR0(IORD, FR2, R0, ASI, MC, MB, MT)
C--   IORD = 0 (LO), 1 (NLO), 2 (NNLO), 3 (NNNLO).
C--   FR2 = ratio of mu_f^2 to mu_r^2 (must be a fixed value).
C--   R0 = input renormalisation scale (in GeV) for alphas_s.
C--   ASI = input value of alpha_s at the renormalisation scale R0.
C--   MC,MB,MT = heavy quark masses in GeV.
C--   Must have R0*sqrt(FR2) <= MC to call this subroutine.
      IMPLICIT NONE
      INTEGER IORD,NAORD,NASTPS,IVFNS,NFF
      DOUBLE PRECISION FR2,R0,ASI,MC,MB,MT,LOGFR,R20,
     &     PI,ZETA,CF,CA,TR,AS0,M20,MC2,MB2,MT2
      PARAMETER(PI = 3.1415 92653 58979 D0)

      COMMON / RZETA  / ZETA(6)
      COMMON / COLOUR / CF, CA, TR
      COMMON / ASINP  / AS0, M20
      COMMON / ASPAR  / NAORD, NASTPS
      COMMON / VARFLV / IVFNS
      COMMON / NFFIX  / NFF
      COMMON / FRRAT  / LOGFR

*
* ..QCD colour factors
*
      CA = 3.D0
      CF = 4./3.D0
      TR = 0.5 D0
*
* ..The lowest integer values of the Zeta function
*
      ZETA(1) = 0.5772 15664 90153 D0
      ZETA(2) = 1.64493 40668 48226 D0
      ZETA(3) = 1.20205 69031 59594 D0
      ZETA(4) = 1.08232 32337 11138 D0
      ZETA(5) = 1.03692 77551 43370 D0
      ZETA(6) = 1.01734 30619 84449 D0

      IVFNS = 1                 ! variable flavour-number scheme (VFNS)
C      IVFNS = 0                 ! fixed flavour-number scheme (FFNS)
      NFF = 4                   ! number of flavours for FFNS
      NAORD = IORD              ! perturbative order of alpha_s
      NASTPS = 20               ! num. steps in Runge-Kutta integration
      R20 = R0**2               ! input renormalisation scale
      MC2 = MC**2               ! mu_f^2 for charm threshold
      MB2 = MB**2               ! mu_f^2 for bottom threshold
      MT2 = MT**2               ! mu_f^2 for top threshold
      LOGFR = LOG(FR2)          ! log of ratio of mu_f^2 to mu_r^2
      M20 = R20 * FR2           ! input factorisation scale

*
* ..Stop some nonsense
*
      IF ( (IVFNS .EQ. 0) .AND. (NFF .LT. 3) ) THEN
         WRITE (6,*) 'Wrong flavour number for FFNS evolution. STOP'
         STOP
      END IF
      IF ( (IVFNS .EQ. 0) .AND. (NFF .GT. 5) ) THEN
         WRITE (6,*) 'Wrong flavour number for FFNS evolution. STOP'
         STOP
      END IF
*     
      IF ( NAORD .GT. 3 ) THEN
         WRITE (6,*) 'Specified order in a_s too high. STOP' 
         STOP
      END IF
*
      IF ( (IVFNS .NE. 0) .AND. (FR2 .GT. 4.001D0) ) THEN
         WRITE (6,*) 'Too low mu_r for VFNS evolution. STOP'
         STOP
      END IF
*
      IF ( (IVFNS .EQ. 1) .AND. (M20 .GT. MC2) ) THEN
         WRITE (6,*) 'Too high mu_0 for VFNS evolution. STOP'
         STOP
      END IF
*     
      IF ( (ASI .GT. 2.D0) .OR. (ASI .LT. 2.D-2) ) THEN
         WRITE (6,*) 'alpha_s out of range. STOP'
         STOP
      END IF
*     
      IF ( (IVFNS .EQ. 1) .AND. (MC2 .GT. MB2) ) THEN
         WRITE (6,*) 'Wrong charm-bottom mass hierarchy. STOP'
         STOP
      END IF
      IF ( (IVFNS .EQ. 1) .AND. (MB2 .GT. MT2) ) THEN
         WRITE (6,*) 'Wrong bottom-top mass hierarchy. STOP'
         STOP
      END IF
*

C--   Store the beta function coefficients in a COMMON block.
      CALL BETAFCT

C--   Store a_s = alpha_s(mu_r^2)/(4 pi) at the input scale R0.
      AS0 = ASI / (4.D0* PI)

C--   Store alpha_s at the heavy flavour thresholds in a COMMON block.
       IF (IVFNS .NE. 0) THEN
          CALL EVNFTHR (MC2, MB2, MT2)
       END IF

      RETURN
      END

C----------------------------------------------------------------------

      DOUBLE PRECISION FUNCTION MSTWALPHAS(MUR)
      IMPLICIT NONE
      INTEGER NFF,IVFNS,NF
      DOUBLE PRECISION PI,LOGFR,AS0,M20,ASC,M2C,ASB,M2B,AST,M2T,M2,MUR,
     &     R2,ASI,ASF,R20,R2T,R2B,R2C,AS
      PARAMETER ( PI = 3.1415 92653 58979 D0 )
*
* ..Input common blocks 
* 
       COMMON / NFFIX  / NFF
       COMMON / VARFLV / IVFNS 
       COMMON / FRRAT  / LOGFR
       COMMON / ASINP  / AS0, M20
       COMMON / ASFTHR / ASC, M2C, ASB, M2B, AST, M2T

       R2 = MUR**2
       M2 = R2 * EXP(+LOGFR)
       IF (IVFNS .EQ. 0) THEN
*
*   Fixed number of flavours
*
          NF  = NFF
          R20 = M20 * R2/M2
          ASI = AS0
          ASF = AS (R2, R20, AS0, NF)
*
       ELSE
*
* ..Variable number of flavours
*
          IF (M2 .GT. M2T) THEN
             NF = 6
             R2T = M2T * R2/M2
             ASI = AST
             ASF = AS (R2, R2T, AST, NF)
*
          ELSE IF (M2 .GT. M2B) THEN
             NF = 5
             R2B = M2B * R2/M2
             ASI = ASB
             ASF = AS (R2, R2B, ASB, NF)
*     
          ELSE IF (M2 .GT. M2C) THEN
             NF = 4
             R2C = M2C * R2/M2
             ASI = ASC
             ASF = AS (R2, R2C, ASC, NF)
*     
          ELSE
             NF = 3
             R20 = M20 * R2/M2
             ASI = AS0
             ASF = AS (R2, R20, AS0, NF)
*       
          END IF
*
       END IF
*
* ..Final value of alpha_s
*
       MSTWALPHAS = 4.D0*PI*ASF

       RETURN
       END
*
* =================================================================av==


* =====================================================================
*
* ..The threshold matching of the QCD coupling in the MS(bar) scheme,  
*    a_s = alpha_s(mu_r^2)/(4 pi),  for  NF -> NF + 1  active flavours 
*    up to order a_s^4 (NNNLO).
*
* ..The value  ASNF  of a_s for NF flavours at the matching scale, the 
*    logarithm  LOGRH = ln (mu_r^2/m_H^2) -- where m_H is the pole mass
*    of the heavy quark -- and  NF  are passed as arguments to the 
*    function  ASNF1.  The order of the expansion  NAORD  (defined as 
*    the 'n' in N^nLO) is provided by the common-block  ASPAR.
*
* ..The matching coefficients are inverted from Chetyrkin, Kniehl and
*    Steinhauser, Phys. Rev. Lett. 79 (1997) 2184. The QCD colour
*    factors have been hard-wired in these results. The lowest integer 
*    values of the Zeta function are given by the common-block  RZETA.
*
* =====================================================================
*
*
      DOUBLE PRECISION FUNCTION ASNF1 (ASNF, LOGRH, NF)
*
      IMPLICIT NONE
      INTEGER NF, NAORD, NASTPS, PRVCLL, K1, K2
      DOUBLE PRECISION ASNF,LOGRH,ZETA,CMC,CMCI30,CMCF30,CMCF31,
     &     CMCI31,ASP,LRHP

      DIMENSION CMC(3,0:3)
*
* ---------------------------------------------------------------------
*
* ..Input common-blocks 
*
      COMMON / ASPAR  / NAORD, NASTPS
      COMMON / RZETA  / ZETA(6)
*
* ..Variables to be saved for the next call
*
      SAVE CMC, CMCI30, CMCF30, CMCF31, CMCI31, PRVCLL
*
* ---------------------------------------------------------------------
*
* ..The coupling-constant matching coefficients (CMC's) up to NNNLO 
*   (calculated and saved in the first call of this routine)
*
       IF (PRVCLL .NE. 1) THEN
*
         CMC(1,0) =  0.D0
         CMC(1,1) =  2./3.D0
*
         CMC(2,0) = 14./3.D0
         CMC(2,1) = 38./3.D0
         CMC(2,2) =  4./9.D0  
*
         CMCI30 = + 80507./432.D0 * ZETA(3) + 58933./1944.D0 
     1            + 128./3.D0 * ZETA(2) * (1.+ DLOG(2.D0)/3.D0)
         CMCF30 = - 64./9.D0 * (ZETA(2) + 2479./3456.D0)
         CMCI31 =   8941./27.D0
         CMCF31 = - 409./27.D0
         CMC(3,2) = 511./9.D0
         CMC(3,3) = 8./27.D0
*
         PRVCLL = 1
*
       END IF
*
* ---------------------------------------------------------------------
*
* ..The N_f dependent CMC's, and the alpha_s matching at order NAORD 
*
       CMC(3,0) = CMCI30 + NF * CMCF30
       CMC(3,1) = CMCI31 + NF * CMCF31
*
       ASNF1 = ASNF
       IF (NAORD .EQ. 0) GO TO 1
       ASP   = ASNF
*
       DO 11 K1 = 1, NAORD 
         ASP = ASP * ASNF
         LRHP = 1.D0
*
       DO 12 K2 = 0, K1
         ASNF1 = ASNF1 + ASP * CMC(K1,K2) * LRHP
         LRHP = LRHP * LOGRH
*
  12   CONTINUE
  11   CONTINUE
*
* ---------------------------------------------------------------------
*
   1   RETURN
       END

*
* =================================================================av==
*
* ..The subroutine  EVNFTHR  for the evolution of  a_s = alpha_s/(4 pi)
*    from a three-flavour initial scale to the four- to six-flavour
*    thresholds (identified with the squares of the corresponding quark
*    masses).  The results are written to the common-block  ASFTHR.
*
* ..The input scale  M20 = mu_(f,0)^2  and the corresponding value 
*    AS0  of a_s  are provided by  ASINP.  The fixed scale logarithm
*    LOGFR = ln (mu_f^2/mu_r^2) is specified in  FRRAT.  The alpha_s
*    matching is done by the function ASNF1.
*
* =====================================================================
*
*
       SUBROUTINE EVNFTHR (MC2, MB2, MT2)
*
       IMPLICIT NONE
       DOUBLE PRECISION MC2, MB2, MT2, M20, M2C, M2B, M2T, R20, R2C, 
     1                  R2B, R2T, AS, ASNF1, AS0, ASC, ASB, AST,
     2                  ASC3, ASB4, AST5, LOGFR, SC, SB, ST
*
* ---------------------------------------------------------------------
* 
* ..Input common blocks
*  
       COMMON / ASINP  / AS0, M20
       COMMON / FRRAT  / LOGFR
*
* ..Output common blocks
*
       COMMON / ASFTHR / ASC, M2C, ASB, M2B, AST, M2T

* ---------------------------------------------------------------------
*
* ..Coupling constants at and evolution distances to/between thresholds
* 
       R20 = M20 * EXP(-LOGFR)
*
* ..Charm
*
       M2C  = MC2
       R2C  = M2C * R20/M20
       ASC3 = AS (R2C, R20, AS0, 3)
       SC   = LOG (AS0 / ASC3)
       ASC  = ASNF1 (ASC3, -LOGFR, 3)
*
* ..Bottom 
*
       M2B  = MB2
       R2B  = M2B * R20/M20
       ASB4 = AS (R2B, R2C, ASC, 4)
       SB   = LOG (ASC / ASB4)
       ASB  = ASNF1 (ASB4, -LOGFR, 4)
*
* ..Top
*
       M2T  = MT2
       R2T  = M2T * R20/M20
       AST5 = AS (R2T, R2B, ASB, 5)
       ST   = LOG (ASB / AST5)
       AST  = ASNF1 (AST5, -LOGFR, 5)

       RETURN
       END

*
* =================================================================av==
*
* ..The running coupling of QCD,  
*
*         AS  =  a_s  =  alpha_s(mu_r^2)/(4 pi),
*
*    obtained by integrating the evolution equation for a fixed number
*    of massless flavours  NF.  Except at leading order (LO),  AS  is 
*    obtained using a fourth-order Runge-Kutta integration.
*
* ..The initial and final scales  R20  and  R2,  the value  AS0  at
*    R20, and  NF  are passed as function arguments.  The coefficients 
*    of the beta function up to  a_s^5 (N^3LO)  are provided by the 
*    common-block  BETACOM.  The order of the expansion  NAORD (defined
*    as the 'n' in N^nLO) and the number of steps  NASTPS  for the 
*    integration beyond LO are given by the common-block  ASPAR.
*
* =====================================================================
*
*
      DOUBLE PRECISION FUNCTION AS (R2, R20, AS0, NF)
*
      IMPLICIT NONE
      INTEGER NFMIN, NFMAX, NF, NAORD, NASTPS, K1
      DOUBLE PRECISION R2, R20, AS0, SXTH, BETA0, BETA1, BETA2, BETA3,
     &     FBETA1,FBETA2,FBETA3,A,LRRAT,DLR,XK0,XK1,XK2,XK3
      PARAMETER (NFMIN = 3, NFMAX = 6)
      PARAMETER ( SXTH = 0.16666 66666 66666 D0 )
*
* ---------------------------------------------------------------------
*
* ..Input common-blocks 
*
       COMMON / ASPAR  / NAORD, NASTPS
       COMMON / BETACOM   / BETA0 (NFMIN:NFMAX), BETA1 (NFMIN:NFMAX),
     ,                   BETA2 (NFMIN:NFMAX), BETA3 (NFMIN:NFMAX)
*
* ..The beta functions FBETAn at N^nLO for n = 1, 2, and 3
*
       FBETA1(A) = - A**2 * ( BETA0(NF) + A *   BETA1(NF) )
       FBETA2(A) = - A**2 * ( BETA0(NF) + A * ( BETA1(NF)
     ,                        + A * BETA2(NF) ) )
       FBETA3(A) = - A**2 * ( BETA0(NF) + A * ( BETA1(NF)
     ,                        + A * (BETA2(NF) + A * BETA3(NF)) ) )
*
* ---------------------------------------------------------------------
*
* ..Initial value, evolution distance and step size
*
       AS = AS0
       LRRAT = LOG (R2/R20)
       DLR = LRRAT / NASTPS
*
* ..Solution of the evolution equation depending on  NAORD
*   (fourth-order Runge-Kutta beyond the leading order)
*
       IF (NAORD .EQ. 0) THEN
*
         AS = AS0 / (1.+ BETA0(NF) * AS0 * LRRAT)
*
       ELSE IF (NAORD .EQ. 1) THEN
*
       DO 2 K1 = 1, NASTPS
         XK0 = DLR * FBETA1 (AS)
         XK1 = DLR * FBETA1 (AS + 0.5 * XK0)
         XK2 = DLR * FBETA1 (AS + 0.5 * XK1)
         XK3 = DLR * FBETA1 (AS + XK2)
         AS = AS + SXTH * (XK0 + 2.* XK1 + 2.* XK2 + XK3)
  2    CONTINUE
*
       ELSE IF (NAORD .EQ. 2) THEN
*
       DO 3 K1 = 1, NASTPS
         XK0 = DLR * FBETA2 (AS)
         XK1 = DLR * FBETA2 (AS + 0.5 * XK0)
         XK2 = DLR * FBETA2 (AS + 0.5 * XK1)
         XK3 = DLR * FBETA2 (AS + XK2)
         AS = AS + SXTH * (XK0 + 2.* XK1 + 2.* XK2 + XK3)
  3    CONTINUE
*  
       ELSE IF (NAORD .EQ. 3) THEN
*
       DO 4 K1 = 1, NASTPS
         XK0 = DLR * FBETA3 (AS)
         XK1 = DLR * FBETA3 (AS + 0.5 * XK0)
         XK2 = DLR * FBETA3 (AS + 0.5 * XK1)
         XK3 = DLR * FBETA3 (AS + XK2)
         AS = AS + SXTH * (XK0 + 2.* XK1 + 2.* XK2 + XK3)
  4    CONTINUE
       END IF
*
* ---------------------------------------------------------------------
*
       RETURN
       END

*
* =================================================================av==
*
* ..The subroutine BETAFCT for the coefficients  BETA0...BETA3  of the 
*    beta function of QCD up to order alpha_s^5 (N^3LO), normalized by 
*
*        d a_s / d ln mu_r^2  =  - BETA0 a_s^2 - BETA1 a_s^3 - ... 
*
*    with  a_s = alpha_s/(4*pi). 
*
* ..The MSbar coefficients are written to the common-block BETACOM for 
*   NF = 3...6  (parameters NFMIN, NFMAX) quark flavours.
*
* ..The factors CF, CA and TF  are taken from the common-block  COLOUR.
*    Beyond NLO the QCD colour factors are hard-wired in this routine,
*    and the numerical coefficients are truncated to six digits.
*
* =====================================================================
*
*
       SUBROUTINE BETAFCT
*
       IMPLICIT DOUBLE PRECISION (A - Z)
       INTEGER NFMIN, NFMAX, NF
       PARAMETER (NFMIN = 3, NFMAX = 6)
*
* ---------------------------------------------------------------------
*
* ..Input common-block
*
       COMMON / COLOUR / CF, CA, TR
*
* ..Output common-block
*
       COMMON / BETACOM   / BETA0 (NFMIN:NFMAX), BETA1 (NFMIN:NFMAX),
     1                   BETA2 (NFMIN:NFMAX), BETA3 (NFMIN:NFMAX)

*
* ---------------------------------------------------------------------
*
* ..The full LO and NLO coefficients 
*
       B00 =  11./3.D0 * CA
       B01 =  -4./3.D0 * TR
       B10 =  34./3.D0 * CA**2
       B11 = -20./3.D0 * CA*TR - 4.* CF*TR
*
* ..Flavour-number loop and output to the array
*
       DO 1 NF = NFMIN, NFMAX
*
       BETA0(NF) = B00 + B01 * NF
       BETA1(NF) = B10 + B11 * NF
*
       BETA2(NF) = 1428.50 - 279.611 * NF + 6.01852 * NF**2
       BETA3(NF) = 29243.0 - 6946.30 * NF + 405.089 * NF**2 
     1             + 1.49931 * NF**3
*
* ---------------------------------------------------------------------
*
  1    CONTINUE
*
       RETURN
       END
*
* =================================================================av==


C--   G.W. DZEROX taken from CERNLIB to find the zero of a function.
      DOUBLE PRECISION FUNCTION DZEROX(A0,B0,EPS,MAXF,F,MODE)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C     Based on
C
C        J.C.P. Bus and T.J. Dekker, Two Efficient Algorithms with
C        Guaranteed Convergence for Finding a Zero of a Function,
C        ACM Trans. Math. Software 1 (1975) 330-345.
C
C        (MODE = 1: Algorithm M;    MODE = 2: Algorithm R)
      CHARACTER*80 ERRTXT
      LOGICAL LMT
      DIMENSION IM1(2),IM2(2),LMT(2)
      PARAMETER (Z1 = 1, HALF = Z1/2)
      DATA IM1 /2,3/, IM2 /-1,3/
      DZEROX = 0.D0             ! G.W. to prevent compiler warning
      IF(MODE .NE. 1 .AND. MODE .NE. 2) THEN
       C=0
       WRITE(ERRTXT,101) MODE
       WRITE(6,*) ERRTXT
       GO TO 99
      ENDIF
      FA=F(B0)
      FB=F(A0)
      IF(FA*FB .GT. 0) THEN
       C=0
       WRITE(ERRTXT,102) A0,B0
       WRITE(6,*) ERRTXT
       GO TO 99
      ENDIF
      ATL=ABS(EPS)
      B=A0
      A=B0
      LMT(2)=.TRUE.
      MF=2
    1 C=A
      FC=FA
    2 IE=0
    3 IF(ABS(FC) .LT. ABS(FB)) THEN
       IF(C .NE. A) THEN
        D=A
        FD=FA
       END IF
       A=B
       B=C
       C=A
       FA=FB
       FB=FC
       FC=FA
      END IF
      TOL=ATL*(1+ABS(C))
      H=HALF*(C+B)
      HB=H-B
      IF(ABS(HB) .GT. TOL) THEN
       IF(IE .GT. IM1(MODE)) THEN
        W=HB
       ELSE
        TOL=TOL*SIGN(Z1,HB)
        P=(B-A)*FB
        LMT(1)=IE .LE. 1
        IF(LMT(MODE)) THEN
         Q=FA-FB
         LMT(2)=.FALSE.
        ELSE
         FDB=(FD-FB)/(D-B)
         FDA=(FD-FA)/(D-A)
         P=FDA*P
         Q=FDB*FA-FDA*FB
        END IF
        IF(P .LT. 0) THEN
         P=-P
         Q=-Q
        END IF
        IF(IE .EQ. IM2(MODE)) P=P+P
        IF(P .EQ. 0 .OR. P .LE. Q*TOL) THEN
         W=TOL
        ELSEIF(P .LT. HB*Q) THEN
         W=P/Q
        ELSE
         W=HB
        END IF
       END IF
       D=A
       A=B
       FD=FA
       FA=FB
       B=B+W
       MF=MF+1
       IF(MF .GT. MAXF) THEN
        WRITE(6,*) "Error in DZEROX: TOO MANY FUNCTION CALLS"
        GO TO 99
       ENDIF
       FB=F(B)
       IF(FB .EQ. 0 .OR. SIGN(Z1,FC) .EQ. SIGN(Z1,FB)) GO TO 1
       IF(W .EQ. HB) GO TO 2
       IE=IE+1
       GO TO 3
      END IF
      DZEROX=C
   99 CONTINUE
      RETURN
  101 FORMAT('Error in DZEROX: MODE = ',I3,' ILLEGAL')
  102 FORMAT('Error in DZEROX: F(A) AND F(B) HAVE THE SAME SIGN, A = ',
     1     1P,D15.8,', B = ',D15.8)
      END

C ---------------------------------------------------------------------