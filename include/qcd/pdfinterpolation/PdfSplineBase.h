/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_QCD_PDFINTERPOLATION_PDF_SPLINE_BASE_H_
#define FINAEL_QCD_PDFINTERPOLATION_PDF_SPLINE_BASE_H_

#include "../../interpolation/MellinSpline.h"



namespace finael {
namespace qcd {
namespace pdfInterpolation {



template<class Function, class ... Types>
class PdfSplineBase
{
public:
	using SplineType = interpolation::SplineType;
	using value_type = typename Function::value_type;



	static constexpr unsigned int coefficientsPerPoint = interpolation::MellinSpline<value_type>::coefficientsPerPoint;



	virtual ~PdfSplineBase() = default;



	void set( const SplineType type )
	{
		spline_.set( function_, type );
		updateMellinSpline();
	}



	template<class PointIt>
	void set( PointIt firstPoint, PointIt lastPoint )
	{
		spline_.set( firstPoint, lastPoint, function_ );
		updateMellinSpline();
	}



	value_type getTruePdf( const value_type& xValue ) const
	{
		return function_( xValue );
	}



	std::size_t numberPoints() const
	{
		return spline_.numberPoints();
	}



	value_type getPointAt( const std::size_t index ) const
	{
		return spline_.getPointAt( index );
	}



	value_type getMinimalPoint() const
	{
		return spline_.getMinimalPoint();
	}



	value_type getMaximalPoint() const
	{
		return spline_.getMaximalPoint();
	}



	bool isInRange( const value_type& point ) const
	{
		return spline_.isInRange( point );
	}



	SplineType getType() const
	{
		return spline_.getType();
	}



	interpolation::SplinePiece<value_type> getPiece( const std::size_t index ) const
	{
		return spline_.getPiece( index );
	}



	std::vector<value_type> getCoefficientsAt( const std::size_t index ) const
	{
		return mellinSpline_.getCoefficientsAt( index );
	}



	value_type operator()( const value_type& point ) const
	{
		return spline_( point );
	}



	template<template<typename> class Complex>
	Complex<value_type> operator()( const Complex<value_type>& mellinVariable ) const
	{
		return mellinSpline_( mellinVariable );
	}



protected:
	Function function_;



	template<class PointIt>
	PdfSplineBase( PointIt firstPoint, PointIt lastPoint, const SplineType type, Types ... args )
		: function_( args... )
		, spline_( firstPoint, lastPoint, function_, type )
		, mellinSpline_( spline_ )
	{}



	void update()
	{
		spline_.set( function_ );
		updateMellinSpline();
	}



private:
	interpolation::Spline<value_type> spline_;
	interpolation::MellinSpline<value_type> mellinSpline_;



	void updateMellinSpline()
	{
		mellinSpline_.set( spline_ );
	}
};



template<class Function, class ... Types>
constexpr unsigned int PdfSplineBase<Function, Types...>::coefficientsPerPoint;



} // namespace pdfInterpolation
} // namespace qcd
} // namespace finael


#endif // FINAEL_QCD_PDFINTERPOLATION_PDF_SPLINE_BASE_H_
