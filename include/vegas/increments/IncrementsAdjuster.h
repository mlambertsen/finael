/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_INCREMENTS_ADJUSTER_H_
#define FINAEL_CUVEGAS_INCREMENTS_ADJUSTER_H_

#include <vector>

#include "../../common/cudaHostDevice.h"

#include "../InternalSettings.h"

#include "Increments.h"
#include "Subincrements.h"
#include "WeightsManager.h"


namespace finael {
namespace cuVegas {



class IncrementsAdjuster
{
public:
	template<unsigned int dim, typename T>
	CUDA_HOST static void improve( Increments<dim, T, CudaHostDeviceType::HOST>& increments, WeightsManager<dim, T>& weightsManager, T adaptionRate )
	{
		Increments<dim, T, CudaHostDeviceType::HOST> newIncrements( increments.sizePerDim() );

		for (auto dimIdx = 0u; dimIdx < dim; ++dimIdx)
		{
			Subincrements<T, SubdividingMode::IMPROVE> numbersSubincrements( weightsManager.getWeights( dimIdx ), adaptionRate );
			adjust( dimIdx, newIncrements, increments, numbersSubincrements );
		}

		increments.update( newIncrements );
	}



	template<unsigned int dim, typename T>
	CUDA_HOST static void match( Increments<dim, T, CudaHostDeviceType::HOST>& increments, const InternalSettings<dim>& settings )
	{
		if (increments.isConsistent( settings )) return;

		if (increments.isReset()) makeEquidistant( increments, settings.incrementsPerDim );
		else stretchIncrements( increments, settings.incrementsPerDim );
	}



private:
	template<unsigned int dim, typename T>
	CUDA_HOST static void makeEquidistant( Increments<dim, T, CudaHostDeviceType::HOST>& increments, unsigned int incrementsPerDim )
	{
		Increments<dim, T, CudaHostDeviceType::HOST> newIncrements( incrementsPerDim );
		for (auto incrementIdx = 0u; incrementIdx < newIncrements.sizePerDim(); ++incrementIdx)
		{
			T incrementPosition = static_cast<T>( incrementIdx + 1 )/newIncrements.sizePerDim();
			for (auto dimIdx = 0u; dimIdx < dim; ++dimIdx)
			{
				newIncrements.at( dimIdx, incrementIdx ) = incrementPosition;
			}
		}
		increments.update( newIncrements );
	}



	template<unsigned int dim, typename T>
	CUDA_HOST static void stretchIncrements( Increments<dim, T, CudaHostDeviceType::HOST>& increments, unsigned int incrementsPerDim )
	{
		Increments<dim, T, CudaHostDeviceType::HOST> newIncrements( incrementsPerDim );

		for (auto dimIdx = 0u; dimIdx < dim; ++dimIdx)
		{
			Subincrements<T, SubdividingMode::STRETCH> numbersSubincrements( increments.sizePerDim(), incrementsPerDim );
			adjust( dimIdx, newIncrements, increments, numbersSubincrements );
		}

		increments.update( newIncrements );
	}



	template<unsigned int dim, typename T, SubdividingMode mode>
	CUDA_HOST static void adjust( unsigned int dimIdx, Increments<dim, T, CudaHostDeviceType::HOST>& newIncrements, const Increments<dim, T, CudaHostDeviceType::HOST>& oldIncrements, const Subincrements<T, mode>& numbersSubincrements )
	{
		const T averageNumberSubincrements = numbersSubincrements.average();

		T accumulatedSubincrements = 0;
		int processedIncrements = -1;
		for (auto incrementIdx = 0u; incrementIdx < newIncrements.sizePerDim() - 1; ++incrementIdx)
		{
			accumulateIncrementsToAverage( numbersSubincrements, averageNumberSubincrements, accumulatedSubincrements, processedIncrements );
			T oldIncrement = oldIncrements.at( dimIdx, processedIncrements );
			T oldIncrementSize =  oldIncrement - (( processedIncrements == 0 ) ? 0 : oldIncrements.at( dimIdx, processedIncrements - 1 ));

			accumulatedSubincrements -= averageNumberSubincrements;
			newIncrements.at( dimIdx, incrementIdx ) = oldIncrement - oldIncrementSize * accumulatedSubincrements/numbersSubincrements[processedIncrements];
		}
		newIncrements.at( dimIdx, numbersSubincrements.size() - 1 ) = 1;
	}



	template<typename T, SubdividingMode mode>
	CUDA_HOST static void accumulateIncrementsToAverage( const Subincrements<T, mode>& numbersSubincrements, const T averageNumberSubincrements, T& accumulatedSubincrements, int& processedIncrements )
	{
		while( accumulatedSubincrements < averageNumberSubincrements )
		{
			++processedIncrements;
			accumulatedSubincrements += numbersSubincrements[processedIncrements];
		}
	}
};



} // namespace cuVegas
} // namespace finael




#endif // FINAEL_CUVEGAS_INCREMENTS_ADJUSTER_H_
