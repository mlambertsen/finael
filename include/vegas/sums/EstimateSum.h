/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_ESTIMATE_SUM_H_
#define FINAEL_CUVEGAS_ESTIMATE_SUM_H_

#include "../../common/cudaHostDevice.h"

#include "Estimate.h"
#include "FunctionSums.h"


namespace finael {
namespace cuVegas {



template<typename T>
class EstimateSum
{
public:
	CUDA_HOST EstimateSum()
		: estimate( 0 )
		, variance( 0 )
		, cardinality( 0u )
	{}



	CUDA_HOST EstimateSum( const Estimate<T>& sumOfEstimates, unsigned int sumsCardinality )
		: estimate( sumOfEstimates.estimate )
		, variance( sumOfEstimates.variance )
		, cardinality( sumsCardinality )
	{}



	CUDA_HOST T getEstimate() const
	{
		return estimate/cardinality;
	}



	CUDA_HOST T getVariance() const
	{
		return variance/cardinality/cardinality;
	}




	CUDA_HOST EstimateSum& operator+=( const Estimate<T>& newEstimate )
	{
		estimate += newEstimate.estimate;
		variance += newEstimate.variance;
		++cardinality;

		return *this;
	}



	CUDA_HOST EstimateSum& operator+=( const EstimateSum& other )
	{
		estimate += other.estimate;
		variance += other.variance;
		cardinality += other.cardinality;

		return *this;
	}



private:
	T estimate;
	T variance;
	unsigned int cardinality;
};



} // namespace cuVegas
} // namespace finael


#endif // FINAEL_CUVEGAS_ESTIMATE_SUM_H_
