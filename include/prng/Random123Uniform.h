/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_PRNG_RANDOM123_UNIFORM_H_
#define FINAEL_PRNG_RANDOM123_UNIFORM_H_

#include "../external/Random123/uniform.hpp"

#endif // FINAEL_PRNG_RANDOM123_UNIFORM_H_
