/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_KERNEL_LECUYER_H_
#define FINAEL_CUVEGAS_KERNEL_LECUYER_H_

#include <limits>
#include <stdexcept>

#include "../../common/cudaHostDevice.h"
#include "../../prng/Lecuyer.h"
#include "../../tools/SmartArray.h"


namespace finael {
namespace cuVegas {



template<unsigned int dim, typename T, std::size_t shuffleBoardSize, CudaHostDeviceType hostDeviceType>
class KernelLecuyer
{
public:
	CUDA_HOST void prepareForIntegration( prng::Lecuyer<T, shuffleBoardSize>& lecuyer, const unsigned int numberSamples )
	{
		auto numberRandomNumbers = detectOverflow( numberSamples );
		cuTool::SmartArray<T, CudaHostDeviceType::HOST> randomNumbersHost( numberRandomNumbers );

		for (unsigned int i = 0; i < numberRandomNumbers; ++i)
		{
			randomNumbersHost[i] = lecuyer.getRandomNumber();
		}

		randomNumbers_ = randomNumbersHost;
	}



	CUDA_HOST_DEVICE T getRandomNumber( const unsigned int index ) const
	{
		return randomNumbers_[index];
	}



private:
	cuTool::SmartArray<T, hostDeviceType> randomNumbers_;



	CUDA_HOST unsigned int detectOverflow( unsigned int numberSamples ) const
	{
		if (std::numeric_limits<unsigned int>::max() < static_cast<T>( numberSamples ) * dim) throw std::overflow_error( "Number of samples too large." );
		return dim * numberSamples;
	}
};



} // namespace cuVegas
} // namespace finael



#endif // FINAEL_CUVEGAS_KERNEL_LECUYER_H_
