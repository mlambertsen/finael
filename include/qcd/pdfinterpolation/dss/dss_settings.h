/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_QCD_PDFINTERPOLATION_DSS_SETTINGS_H_
#define FINAEL_QCD_PDFINTERPOLATION_DSS_SETTINGS_H_



namespace finael {
namespace qcd {
namespace pdfInterpolation {



enum class DssSet {V07, V14};



} // namespace pdfInterpolation
} // namespace qcd
} // namespace finael



#endif // FINAEL_QCD_PDFINTERPOLATION_DSS_SETTINGS_H_
