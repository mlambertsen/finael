/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_QCD_PDFINTERPOLATION_FORTRAN_MMHT2014_FORTRAN_H_
#define FINAEL_QCD_PDFINTERPOLATION_FORTRAN_MMHT2014_FORTRAN_H_



namespace finael {
namespace qcd {
namespace pdfInterpolation {
namespace fortran {



extern "C"
{
	double mmht14pdf_getter_( int* iparton, int* iset, char* infile, int* filesize, const double* x, double* q );
}



} // namespace fortran
} // namespace pdfInterpolation
} // namespace qcd
} // namespace finael



#endif // FINAEL_QCD_PDFINTERPOLATION_FORTRAN_MMHT2014_FORTRAN_H_
