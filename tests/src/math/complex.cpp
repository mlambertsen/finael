/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#define BOOST_TEST_MODULE "finael Complex"
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "../../../include/math/Complex.h"



BOOST_AUTO_TEST_SUITE( complex )



BOOST_AUTO_TEST_CASE( defaultConstructor )
{
	using namespace finael::cuMath;
	int standardInitializer {0};
	Complex<int> complex;

	BOOST_CHECK_EQUAL( complex.real(), standardInitializer );
	BOOST_CHECK_EQUAL( complex.imag(), standardInitializer );
}



BOOST_AUTO_TEST_CASE( oneArgumentConstructor )
{
	using namespace finael::cuMath;
	int standardInitializer {0};
	int real {2};
	Complex<int> complex( real );

	BOOST_CHECK_EQUAL( complex.real(), real );
	BOOST_CHECK_EQUAL( complex.imag(), standardInitializer );
}



BOOST_AUTO_TEST_CASE( twoArgumentConstructor )
{
	using namespace finael::cuMath;
	int real {2};
	int imag {1};
	Complex<int> complex( real, imag );

	BOOST_CHECK_EQUAL( complex.real(), real );
	BOOST_CHECK_EQUAL( complex.imag(), imag );
}



BOOST_AUTO_TEST_CASE( copyConstructor )
{
	using namespace finael::cuMath;
	int real {2};
	int imag {1};
	Complex<int> complexOrig( real, imag );
	Complex<int> complex( complexOrig );

	BOOST_CHECK_EQUAL( complex.real(), real );
	BOOST_CHECK_EQUAL( complex.imag(), imag );
}



BOOST_AUTO_TEST_CASE( assignmentOperator )
{
	using namespace finael::cuMath;
	int real {2};
	int imag {1};
	Complex<int> complexOrig( real, imag );
	Complex<int> complex;

	complex = complexOrig;

	BOOST_CHECK_EQUAL( complex.real(), real );
	BOOST_CHECK_EQUAL( complex.imag(), imag );
}



BOOST_AUTO_TEST_CASE( norm )
{
	using namespace finael::cuMath;
	int real {2};
	int imag {1};
	Complex<int> complex( real, imag );

	BOOST_CHECK_EQUAL( complex.norm(), 5 );
}



BOOST_AUTO_TEST_CASE( abs )
{
	using namespace finael::cuMath;
	int real {3};
	int imag {4};
	Complex<int> complex( real, imag );

	BOOST_CHECK_EQUAL( complex.abs(), 5 );
}



BOOST_AUTO_TEST_CASE( argFirstSector )
{
	using namespace finael::cuMath;
	int real {3};
	int imag {4};
	Complex<float> complexFloat( real, imag );
	Complex<double> complexDouble( real, imag );

	BOOST_CHECK_EQUAL( complexFloat.arg(), 0.927295218001612232428512462922428804057074108572240527621f );
	BOOST_CHECK_EQUAL( complexDouble.arg(), 0.927295218001612232428512462922428804057074108572240527621 );
}



BOOST_AUTO_TEST_CASE( argSecondSector )
{
	using namespace finael::cuMath;
	int real {-3};
	int imag {4};
	Complex<float> complexFloat( real, imag );
	Complex<double> complexDouble( real, imag );

	BOOST_CHECK_EQUAL( complexFloat.arg(), 2.214297435588181006034130920357074080140095290802865293353f );
	BOOST_CHECK_EQUAL( complexDouble.arg(), 2.214297435588181006034130920357074080140095290802865293353 );
}



BOOST_AUTO_TEST_CASE( argThirdSector )
{
	using namespace finael::cuMath;
	int real {-6};
	int imag {-12345};
	Complex<float> complexFloat( real, imag );
	Complex<double> complexDouble( real, imag );

	BOOST_CHECK_EQUAL( complexFloat.arg(), -1.57128235348809678930452603388689980693685564091722080891f );
	BOOST_CHECK_EQUAL( complexDouble.arg(), -1.57128235348809678930452603388689980693685564091722080891 );
}



BOOST_AUTO_TEST_CASE( argForthSector )
{
	using namespace finael::cuMath;
	int real {12345};
	int imag {-6};
	Complex<float> complexFloat( real, imag );
	Complex<double> complexDouble( real, imag );

	BOOST_CHECK_EQUAL( complexFloat.arg(), -0.00048602669320017007320434224714836483827094122966789842f );
	BOOST_CHECK_EQUAL( complexDouble.arg(), -0.00048602669320017007320434224714836483827094122966789842 );
}



BOOST_AUTO_TEST_CASE( conj )
{
	using namespace finael::cuMath;
	int real {1};
	int imag {2};
	Complex<int> complex( real, imag );

	complex = complex.conj();

	BOOST_CHECK_EQUAL( complex.real(), real );
	BOOST_CHECK_EQUAL( complex.imag(), -imag );

	complex = complex.conj();

	BOOST_CHECK_EQUAL( complex.real(), real );
	BOOST_CHECK_EQUAL( complex.imag(), imag );
}



BOOST_AUTO_TEST_CASE( addAssignmentOperator )
{
	using namespace finael::cuMath;
	int real {2};
	int imag {1};
	Complex<int> summand( real, imag );
	Complex<int> complex;

	complex += summand;

	BOOST_CHECK_EQUAL( complex.real(), real );
	BOOST_CHECK_EQUAL( complex.imag(), imag );

	complex += 4;

	BOOST_CHECK_EQUAL( complex.real(), real + 4 );
	BOOST_CHECK_EQUAL( complex.imag(), imag );
}



BOOST_AUTO_TEST_CASE( addAssignmentOperatorMixedMode )
{
	using namespace finael::cuMath;
	int real {2};
	int imag {1};
	Complex<int> complex( real, imag );

	complex += 4.0;

	BOOST_CHECK_EQUAL( complex.real(), real + 4 );
	BOOST_CHECK_EQUAL( complex.imag(), imag );

	complex += 3.0f;

	BOOST_CHECK_EQUAL( complex.real(), real + 7 );
	BOOST_CHECK_EQUAL( complex.imag(), imag );
}



BOOST_AUTO_TEST_CASE( subtractAssignmentOperator )
{
	using namespace finael::cuMath;
	int real {2};
	int imag {1};
	Complex<int> subtract( real, imag );
	Complex<int> complex;

	complex -= subtract;

	BOOST_CHECK_EQUAL( complex.real(), -real );
	BOOST_CHECK_EQUAL( complex.imag(), -imag );
}



BOOST_AUTO_TEST_CASE( subtractAssignmentOperatorMixedMode )
{
	using namespace finael::cuMath;
	int real {2};
	int imag {1};
	Complex<int> complex( real, imag );

	complex -= 3.0;

	BOOST_CHECK_EQUAL( complex.real(), real - 3 );
	BOOST_CHECK_EQUAL( complex.imag(), imag );

	complex -= 5.0f;

	BOOST_CHECK_EQUAL( complex.real(), real - 8 );
	BOOST_CHECK_EQUAL( complex.imag(), imag );
}



BOOST_AUTO_TEST_CASE( multiplyAssignmentOperator )
{
	using namespace finael::cuMath;
	Complex<int> factor( 2, 1 );
	Complex<int> complex( 3, 5 );

	complex *= factor;

	BOOST_CHECK_EQUAL( complex.real(), 1 );
	BOOST_CHECK_EQUAL( complex.imag(), 13 );
}



BOOST_AUTO_TEST_CASE( multiplyAssignmentOperatorMixedMode )
{
	using namespace finael::cuMath;
	int real {2};
	int imag {1};
	Complex<int> complex( real, imag );

	complex *= 3.0;

	BOOST_CHECK_EQUAL( complex.real(), real*3 );
	BOOST_CHECK_EQUAL( complex.imag(), imag*3 );

	complex *= 2.0f;

	BOOST_CHECK_EQUAL( complex.real(), real*6 );
	BOOST_CHECK_EQUAL( complex.imag(), imag*6 );
}



BOOST_AUTO_TEST_CASE( divideAssignmentOperator )
{
	using namespace finael::cuMath;
	Complex<float> divisorFloat( 2, 1 );
	Complex<float> complexFloat( 3, 5 );
	Complex<double> divisorDouble( 2, 1 );
	Complex<double> complexDouble( 3, 5 );

	complexFloat /= divisorFloat;
	complexDouble /= divisorDouble;

	BOOST_CHECK_EQUAL( complexFloat.real(), 2.2f );
	BOOST_CHECK_EQUAL( complexFloat.imag(), 1.4f );

	BOOST_CHECK_EQUAL( complexDouble.real(), 2.2 );
	BOOST_CHECK_EQUAL( complexDouble.imag(), 1.4 );
}



BOOST_AUTO_TEST_CASE( divideAssignmentOperatorMixedMode )
{
	using namespace finael::cuMath;
	int real {10};
	int imag {30};
	Complex<int> complex( real, imag );

	complex /= 2.0;

	BOOST_CHECK_EQUAL( complex.real(), real/2 );
	BOOST_CHECK_EQUAL( complex.imag(), imag/2 );

	complex /= 5.0f;

	BOOST_CHECK_EQUAL( complex.real(), real/10 );
	BOOST_CHECK_EQUAL( complex.imag(), imag/10 );
}



BOOST_AUTO_TEST_CASE( nonMemberRealAndImag )
{
	using namespace finael::cuMath;
	int realInit {1};
	int imagInit {2};
	Complex<int> complex( realInit, imagInit );

	BOOST_CHECK_EQUAL( real( complex ), realInit );
	BOOST_CHECK_EQUAL( imag( complex ), imagInit );
}



BOOST_AUTO_TEST_CASE( nonMemberNorm )
{
	using namespace finael::cuMath;
	int real {2};
	int imag {1};
	Complex<int> complex( real, imag );

	BOOST_CHECK_EQUAL( finael::cuMath::norm( complex ), 5 );
}



BOOST_AUTO_TEST_CASE( nonMemberAbs )
{
	using namespace finael::cuMath;
	int real {3};
	int imag {4};
	Complex<int> complex( real, imag );

	BOOST_CHECK_EQUAL( finael::cuMath::abs( complex ), 5 );
}



BOOST_AUTO_TEST_CASE( nonMemberArgFirstSector )
{
	using namespace finael::cuMath;
	int real {3};
	int imag {4};
	Complex<float> complexFloat( real, imag );
	Complex<double> complexDouble( real, imag );

	BOOST_CHECK_EQUAL( finael::cuMath::arg( complexFloat ), 0.927295218001612232428512462922428804057074108572240527621f );
	BOOST_CHECK_EQUAL( finael::cuMath::arg( complexDouble ), 0.927295218001612232428512462922428804057074108572240527621 );
}



BOOST_AUTO_TEST_CASE( nonMemberArgSecondSector )
{
	using namespace finael::cuMath;
	int real {-3};
	int imag {4};
	Complex<float> complexFloat( real, imag );
	Complex<double> complexDouble( real, imag );

	BOOST_CHECK_EQUAL( finael::cuMath::arg( complexFloat ), 2.214297435588181006034130920357074080140095290802865293353f );
	BOOST_CHECK_EQUAL( finael::cuMath::arg( complexDouble ), 2.214297435588181006034130920357074080140095290802865293353 );
}



BOOST_AUTO_TEST_CASE( nonMemberArgThirdSector )
{
	using namespace finael::cuMath;
	int real {-6};
	int imag {-12345};
	Complex<float> complexFloat( real, imag );
	Complex<double> complexDouble( real, imag );

	BOOST_CHECK_EQUAL( finael::cuMath::arg( complexFloat ), -1.57128235348809678930452603388689980693685564091722080891f );
	BOOST_CHECK_EQUAL( finael::cuMath::arg( complexDouble ), -1.57128235348809678930452603388689980693685564091722080891 );
}



BOOST_AUTO_TEST_CASE( nonMemberArgForthSector )
{
	using namespace finael::cuMath;
	int real {12345};
	int imag {-6};
	Complex<float> complexFloat( real, imag );
	Complex<double> complexDouble( real, imag );

	BOOST_CHECK_EQUAL( finael::cuMath::arg( complexFloat ), -0.00048602669320017007320434224714836483827094122966789842f );
	BOOST_CHECK_EQUAL( finael::cuMath::arg( complexDouble ), -0.00048602669320017007320434224714836483827094122966789842 );
}



BOOST_AUTO_TEST_CASE( nonMemberConj )
{
	using namespace finael::cuMath;
	int real {1};
	int imag {2};
	Complex<int> complex( real, imag );

	complex = finael::cuMath::conj( complex );

	BOOST_CHECK_EQUAL( complex.real(), real );
	BOOST_CHECK_EQUAL( complex.imag(), -imag );

	complex = finael::cuMath::conj( complex );

	BOOST_CHECK_EQUAL( complex.real(), real );
	BOOST_CHECK_EQUAL( complex.imag(), imag );
}



BOOST_AUTO_TEST_CASE( operatorPlus )
{
	using namespace finael::cuMath;
	int real {2};
	int imag {1};
	Complex<int> summand( real, imag );
	Complex<int> complex;

	complex = complex + summand;

	BOOST_CHECK_EQUAL( complex.real(), real );
	BOOST_CHECK_EQUAL( complex.imag(), imag );

	complex = complex + 4;

	BOOST_CHECK_EQUAL( complex.real(), real + 4 );
	BOOST_CHECK_EQUAL( complex.imag(), imag );

	complex = 4 + complex;

	BOOST_CHECK_EQUAL( complex.real(), real + 8 );
	BOOST_CHECK_EQUAL( complex.imag(), imag );
}



BOOST_AUTO_TEST_CASE( operatorPlusMixedMode )
{
	using namespace finael::cuMath;
	int real {2};
	int imag {1};
	Complex<int> complex( real, imag );

	complex = complex + 4.0;

	BOOST_CHECK_EQUAL( complex.real(), real + 4 );
	BOOST_CHECK_EQUAL( complex.imag(), imag );

	complex = complex + 3.0f;

	BOOST_CHECK_EQUAL( complex.real(), real + 7 );
	BOOST_CHECK_EQUAL( complex.imag(), imag );

	complex = 4.0 + complex;

	BOOST_CHECK_EQUAL( complex.real(), real + 11 );
	BOOST_CHECK_EQUAL( complex.imag(), imag );

	complex = 3.0f + complex;

	BOOST_CHECK_EQUAL( complex.real(), real + 14 );
	BOOST_CHECK_EQUAL( complex.imag(), imag );
}



BOOST_AUTO_TEST_CASE( operatorMinus )
{
	using namespace finael::cuMath;
	int real {2};
	int imag {1};
	Complex<int> subtract( real, imag );
	Complex<int> complex;

	complex = complex - subtract;

	BOOST_CHECK_EQUAL( complex.real(), -real );
	BOOST_CHECK_EQUAL( complex.imag(), -imag );

	complex = complex - 4;

	BOOST_CHECK_EQUAL( complex.real(), -real - 4 );
	BOOST_CHECK_EQUAL( complex.imag(), -imag );

	complex = 4 - complex;

	BOOST_CHECK_EQUAL( complex.real(), real + 8 );
	BOOST_CHECK_EQUAL( complex.imag(), imag );
}



BOOST_AUTO_TEST_CASE( operatorMinusMixedMode )
{
	using namespace finael::cuMath;
	int real {2};
	int imag {1};
	Complex<int> complex( real, imag );

	complex = complex - 4.0;

	BOOST_CHECK_EQUAL( complex.real(), real - 4 );
	BOOST_CHECK_EQUAL( complex.imag(), imag );

	complex = complex - 3.0f;

	BOOST_CHECK_EQUAL( complex.real(), real - 7 );
	BOOST_CHECK_EQUAL( complex.imag(), imag );

	complex = 4.0 - complex;

	BOOST_CHECK_EQUAL( complex.real(), -real + 11 );
	BOOST_CHECK_EQUAL( complex.imag(), -imag );

	complex = 3.0f - complex;

	BOOST_CHECK_EQUAL( complex.real(), real - 8 );
	BOOST_CHECK_EQUAL( complex.imag(), imag );
}



BOOST_AUTO_TEST_CASE( operatorMultiply )
{
	using namespace finael::cuMath;
	Complex<int> factor( 1, 2 );
	Complex<int> complex( 3, 5 );

	complex = complex * factor;

	BOOST_CHECK_EQUAL( complex.real(), -7 );
	BOOST_CHECK_EQUAL( complex.imag(), 11 );

	complex = complex * 2;

	BOOST_CHECK_EQUAL( complex.real(), -14 );
	BOOST_CHECK_EQUAL( complex.imag(), 22 );

	complex = 4 * complex;

	BOOST_CHECK_EQUAL( complex.real(), -56 );
	BOOST_CHECK_EQUAL( complex.imag(), 88 );
}



BOOST_AUTO_TEST_CASE( operatorMultiplyMixedMode )
{
	using namespace finael::cuMath;
	Complex<int> complex( 1, 2 );

	complex = complex * 4.0;

	BOOST_CHECK_EQUAL( complex.real(), 4 );
	BOOST_CHECK_EQUAL( complex.imag(), 8 );

	complex = complex * 3.0f;

	BOOST_CHECK_EQUAL( complex.real(), 12 );
	BOOST_CHECK_EQUAL( complex.imag(), 24 );

	complex = 4.0 * complex;

	BOOST_CHECK_EQUAL( complex.real(), 48 );
	BOOST_CHECK_EQUAL( complex.imag(), 96 );

	complex = 3.0f * complex;

	BOOST_CHECK_EQUAL( complex.real(), 144 );
	BOOST_CHECK_EQUAL( complex.imag(), 288 );
}



BOOST_AUTO_TEST_CASE( operatorDivide )
{
	using namespace finael::cuMath;
	Complex<float> divisorFloat( 2, 1 );
	Complex<float> complexFloat( 3, 5 );
	Complex<double> divisorDouble( 2, 1 );
	Complex<double> complexDouble( 3, 5 );

	complexFloat = complexFloat / divisorFloat;
	complexDouble = complexDouble / divisorDouble;

	BOOST_CHECK_EQUAL( complexFloat.real(), 2.2f );
	BOOST_CHECK_EQUAL( complexFloat.imag(), 1.4f );

	BOOST_CHECK_EQUAL( complexDouble.real(), 2.2 );
	BOOST_CHECK_EQUAL( complexDouble.imag(), 1.4 );

	complexFloat = complexFloat / 2.0f;
	complexDouble = complexDouble / 2.0;

	BOOST_CHECK_EQUAL( complexFloat.real(), 1.1f );
	BOOST_CHECK_EQUAL( complexFloat.imag(), 0.7f );

	BOOST_CHECK_EQUAL( complexDouble.real(), 1.1 );
	BOOST_CHECK_EQUAL( complexDouble.imag(), 0.7 );

	complexFloat = 1.0f / complexFloat;
	complexDouble = 1.0 / complexDouble;

	BOOST_CHECK_EQUAL( complexFloat.real(), 0.64705882352941176470588235294117647058823529411764705882f );
	BOOST_CHECK_CLOSE( complexFloat.imag(), -0.41176470588235294117647058823529411764705882352941176470f, 1.e-5 );

	BOOST_CHECK_EQUAL( complexDouble.real(), 0.64705882352941176470588235294117647058823529411764705882 );
	BOOST_CHECK_CLOSE( complexDouble.imag(), -0.41176470588235294117647058823529411764705882352941176470, 1.e-13 );
}



BOOST_AUTO_TEST_CASE( operatorIsEqual )
{
	using namespace finael::cuMath;

	Complex<int> first( 1, 1 );
	Complex<int> second( 1, 2 );
	Complex<int> third( 2, 1 );
	Complex<int> fourth( 2, 2 );

	BOOST_CHECK_EQUAL( first == first, true );
	BOOST_CHECK_EQUAL( first == second, false );
	BOOST_CHECK_EQUAL( first == third, false );
	BOOST_CHECK_EQUAL( first == fourth, false );
}



BOOST_AUTO_TEST_CASE( operatorIsNotEqual )
{
	using namespace finael::cuMath;

	Complex<int> first( 1, 1 );
	Complex<int> second( 1, 2 );
	Complex<int> third( 2, 1 );
	Complex<int> fourth( 2, 2 );

	BOOST_CHECK_EQUAL( first != first, false );
	BOOST_CHECK_EQUAL( first != second, true );
	BOOST_CHECK_EQUAL( first != third, true );
	BOOST_CHECK_EQUAL( first != fourth, true );
}



BOOST_AUTO_TEST_CASE( root )
{
	using namespace finael::cuMath;

	Complex<double> onRealAxis( 4, 0 );
	Complex<double> onPositiveImaginaryAxis( 0, 4 );
	Complex<double> onNegativeImaginaryAxis( 0, -4 );
	Complex<double> inFirstSector( 1, 1 );
	Complex<double> inSecondSector( -1, 1 );
	Complex<double> inThirdSector( -1, -1 );
	Complex<double> inForthSector( 1, -1 );

	BOOST_CHECK_EQUAL( onRealAxis.sqrt(), Complex<double>( 2, 0 ) );
	BOOST_CHECK_EQUAL( onPositiveImaginaryAxis.sqrt(), Complex<double>( 1.4142135623730950488016887242096980, 1.4142135623730950488016887242096980 ) );
	BOOST_CHECK_EQUAL( onNegativeImaginaryAxis.sqrt(), Complex<double>( 1.4142135623730950488016887242096980, -1.4142135623730950488016887242096980 ) );
	BOOST_CHECK_EQUAL( inFirstSector.sqrt(), Complex<double>( 1.0986841134678099660398011952, 0.4550898605622273413043577578 ) );
	BOOST_CHECK_EQUAL( inSecondSector.sqrt(), Complex<double>( 0.4550898605622273413043577578, 1.0986841134678099660398011952 ) );
	BOOST_CHECK_EQUAL( inThirdSector.sqrt(), Complex<double>( 0.4550898605622273413043577578, -1.0986841134678099660398011952 ) );
	BOOST_CHECK_EQUAL( inForthSector.sqrt(), Complex<double>( 1.0986841134678099660398011952, -0.4550898605622273413043577578 ) );

	BOOST_CHECK_EQUAL( inFirstSector.sqrt(), sqrt( inFirstSector ) );
}



BOOST_AUTO_TEST_SUITE_END()
