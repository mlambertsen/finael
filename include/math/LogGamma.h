/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUMATH_LOG_GAMMA_H_
#define FINAEL_CUMATH_LOG_GAMMA_H_

#include "../common/cudaHostDevice.h"

#include "Complex.h"
#include "generalFunctions.h"
#include "constants.h"


namespace finael {
namespace cuMath {



class LogGamma
{
public:
	static constexpr int approximationLimit = 15;



	template<template<typename> class Complex, typename T>
	CUDA_HOST_DEVICE Complex<T> operator()( const Complex<T>& arg ) const
	{
		return correctLogBranch( calcIgnoringLogBranch( arg ) );
	}



	template<typename T>
	CUDA_HOST_DEVICE T operator()( const T& arg ) const
	{
		return operator()( Complex<T>( arg, 0 ) ).real();
	}



	template<template<typename> class Complex, typename T>
	CUDA_HOST_DEVICE Complex<T> calcIgnoringLogBranch( const Complex<T>& arg ) const
	{
		if (arg.real() < (1 - approximationLimit) && isReflectable( arg )) return reflect( arg );
		if (arg.real() >= approximationLimit) return approximateForLargeRealPart( arg );

		return calculateViaRecurrence( arg );
	}



private:
	template<template<typename> class Complex, typename T>
	CUDA_HOST_DEVICE Complex<T> correctLogBranch( const Complex<T>& arg ) const
	{
		using std::abs;
		T imag = arg.imag();
		if (abs( imag ) > CONST<T>::PI)
		{
			int branchDiff = floor( (imag + CONST<T>::PI)/(2*CONST<T>::PI) );
			imag -= branchDiff*(2*CONST<T>::PI);
		}
		return Complex<T>( arg.real(), imag );
	}



	template<template<typename> class Complex, typename T>
	CUDA_HOST_DEVICE bool isReflectable( const Complex<T>& arg ) const
	{
		constexpr T imaginaryLimit = (sizeof(T) == 4) ? 14 : 110;
		return ::abs(arg.imag()) < imaginaryLimit;
	}



	template<template<typename> class Complex, typename T>
	CUDA_HOST_DEVICE Complex<T> calculateViaRecurrence( const Complex<T>& arg ) const
	{
		auto recurrenceSteps = approximationLimit - static_cast<unsigned int>( floor( arg.real() ) );
		return approximateForLargeRealPart( arg + recurrenceSteps ) + accumulateRecurrenceSum( arg, recurrenceSteps );
	}



	template<template<typename> class Complex, typename T>
	CUDA_HOST_DEVICE Complex<T> accumulateRecurrenceSum( const Complex<T>& arg, unsigned int recurrenceSteps ) const
	{
		Complex<T> recurrenceSum;
		for (auto step = 0u; step < recurrenceSteps; ++step)
		{
			recurrenceSum -= log( arg + step );
		}
		return recurrenceSum;
	}



	template<template<typename> class Complex, typename T>
	CUDA_HOST_DEVICE Complex<T> approximateForLargeRealPart( const Complex<T>& arg ) const
	{
		constexpr T logRoot2Pi = 0.9189385332046727417803297364056176L;
		const Complex<T> argSquare = arg*arg;

		return (arg - static_cast<T>( 0.5 ))*log( arg ) - arg + logRoot2Pi + (1 - (1 - (1 - 3/(4*argSquare))/(static_cast<T>( 3.5 )*argSquare))/(30*argSquare))/(12*arg);
	}



	template<template<typename> class Complex, typename T>
	CUDA_HOST_DEVICE Complex<T> reflect( const Complex<T>& arg ) const
	{
		return log( CONST<T>::PI / sinpi( arg ) ) - approximateForLargeRealPart( 1 - arg );
	}
};



constexpr int LogGamma::approximationLimit;



} // namespace cuMath
} // namespace finael



#endif // FINAEL_CUMATH_LOG_GAMMA_H_
