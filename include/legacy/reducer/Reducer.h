#ifndef REDUCER_H_
#define REDUCER_H_

#include <iostream>
#include <cstdlib>
#include <vector>
#include "../../common/cudaHostDevice.h"
#include "../../tools/SmartArray.h"
#include "reducer_kernel.h"



namespace finael {
namespace reducer {



template<typename T, CudaHostDeviceType hostDeviceType = CudaHostDeviceType::HOST> class Reducer
{
public:
	CUDA_HOST Reducer( const std::vector<T>& inputVector )
		: array_( inputVector )
		, maximumSize_( inputVector.size() )
		, valuesPerThreadInConsecutiveRuns_( 2 )
	{}



	CUDA_HOST Reducer( const cuTool::SmartArray<T, hostDeviceType>& inputArray )
		: array_( inputArray )
		, maximumSize_( inputArray.size() )
		, valuesPerThreadInConsecutiveRuns_( 2 )
	{}



	CUDA_HOST void setData( const std::vector<T>& inputVector )
	{
		array_ = inputVector;
		maximumSize_ = inputVector.size();
	}



	CUDA_HOST void setData(const cuTool::SmartArray<T, hostDeviceType>& inputArray)
	{
		array_ = inputArray;
		maximumSize_ = inputArray.size();
	}



	CUDA_HOST void setValuesPerThreadInConsecutiveRuns( const unsigned int valuesPerThread )
	{
		valuesPerThreadInConsecutiveRuns_ = valuesPerThread;
	}



	template<class ReduceOperation> CUDA_HOST T reduce( unsigned int size, const unsigned int offset = 0, const unsigned int valuesPerThread = 2 )
	{
		checkRange( offset, size );

		unsigned int numberThreads;
		unsigned int numberBlocks;
		unsigned int sharedMemorySize;

		getNumberThreadsAndBlocks( size, valuesPerThread, numberThreads, numberBlocks, sharedMemorySize );

		cuTool::SmartArray<T, hostDeviceType> outputArray( numberBlocks );

		chooseKernel<ReduceOperation>( array_, outputArray, offset, size, valuesPerThread, numberThreads, numberBlocks, sharedMemorySize );

		size = numberBlocks;

		T result = reduceLoop( outputArray, size );

		return result;
	}



private:
	cuTool::SmartArray<T, hostDeviceType> array_;
	unsigned int maximumSize_;
	unsigned int valuesPerThreadInConsecutiveRuns_;



	CUDA_HOST void checkRange( const unsigned int offset, const unsigned int size )
	{
		if (offset + size > maximumSize_)
		{
			std::cout << "Array too small - Reducer stops program\n";
			std::cout << "Values have been:\n";
			std::cout << " Number elements requested = " << size << "\n";
			std::cout << "                    Offset = " << offset << "\n";
			std::cout << "Number elements in Reducer = " << maximumSize_ << "\n";
			std::exit(EXIT_FAILURE);
		}
	}



	CUDA_HOST void getNumberThreadsAndBlocks( const unsigned int size, const unsigned int valuesPerThread, unsigned int& numberThreads, unsigned int& numberBlocks, unsigned int& sharedMemorySize )
	{
		unsigned int neededThreads = (size - 1)/valuesPerThread + 1;

		if(neededThreads > 256) numberThreads = 512;
		else if (neededThreads > 128) numberThreads = 256;
		else if (neededThreads > 64) numberThreads = 128;
		else if (neededThreads > 32)	numberThreads = 64;
		else if (neededThreads > 16)	numberThreads = 32;
		else if (neededThreads > 8) numberThreads = 16;
		else if (neededThreads > 4) numberThreads = 8;
		else if (neededThreads > 2) numberThreads = 4;
		else if (neededThreads > 1) numberThreads = 2;
		else numberThreads = 1;

		numberBlocks = (size - 1) / (valuesPerThread * numberThreads) + 1;
		sharedMemorySize = numberThreads * sizeof(T);
	}



	CUDA_HOST T reduceLoop( cuTool::SmartArray<T, hostDeviceType>& outputArray, unsigned int size )
	{
		cuTool::SmartArray<T, hostDeviceType> tempOutputArray;
		bool writeToTempOutputArray = true;

		unsigned int numberThreads;
		unsigned int numberBlocks;
		unsigned int sharedMemorySize;
		unsigned int offset(0); // offset completely taken into account by first kernel run
		unsigned int valuesPerThread( valuesPerThreadInConsecutiveRuns_ );

		if (size > 1) // second out-array (if blocks are not started "in order")
		{
			getNumberThreadsAndBlocks( size, valuesPerThread, numberThreads, numberBlocks, sharedMemorySize );
			tempOutputArray.newArray( numberBlocks );
		}

		while (size > 1)
		{
			if (writeToTempOutputArray)
			{
				chooseKernel<Simple>( outputArray, tempOutputArray, offset, size, valuesPerThread, numberThreads, numberBlocks, sharedMemorySize );
				writeToTempOutputArray = false;
			}
			else
			{
				chooseKernel<Simple>( tempOutputArray, outputArray, offset, size, valuesPerThread, numberThreads, numberBlocks, sharedMemorySize );
				writeToTempOutputArray = true;
			}
			size = (size - 1) / ( valuesPerThread * numberThreads ) + 1;
			getNumberThreadsAndBlocks( size, valuesPerThread, numberThreads, numberBlocks, sharedMemorySize );
		}

		T result;
		if (writeToTempOutputArray)
		{
			result = outputArray.getToHost(0);
		}
		else
		{
			result = tempOutputArray.getToHost(0);
		}

		return result;
	}



	template<class ReduceOperation> CUDA_HOST void chooseKernel( const cuTool::SmartArray<T, hostDeviceType>& inputArray, const cuTool::SmartArray<T, hostDeviceType>& outputArray, const unsigned int offset, const unsigned int size, const unsigned int valuesPerThread, const unsigned int numberThreads, const unsigned int numberBlocks, const unsigned int sharedMemorySize )
	{
		switch(numberThreads)
		{
			case 512:
				chooseValuesPerThread<512, ReduceOperation>( inputArray, outputArray, offset, size, valuesPerThread, numberBlocks, sharedMemorySize );
				break;
			case 256:
				chooseValuesPerThread<256, ReduceOperation>( inputArray, outputArray, offset, size, valuesPerThread, numberBlocks, sharedMemorySize );
				break;
			case 128:
				chooseValuesPerThread<128, ReduceOperation>( inputArray, outputArray, offset, size, valuesPerThread, numberBlocks, sharedMemorySize );
				break;
			case 64:
				chooseValuesPerThread<64, ReduceOperation>( inputArray, outputArray, offset, size, valuesPerThread, numberBlocks, sharedMemorySize );
				break;
			case 32:
				chooseValuesPerThread<32, ReduceOperation>( inputArray, outputArray, offset, size, valuesPerThread, numberBlocks, sharedMemorySize );
				break;
			case 16:
				chooseValuesPerThread<16, ReduceOperation>( inputArray, outputArray, offset, size, valuesPerThread, numberBlocks, sharedMemorySize );
				break;
			case 8:
				chooseValuesPerThread<8, ReduceOperation>( inputArray, outputArray, offset, size, valuesPerThread, numberBlocks, sharedMemorySize );
				break;
			case 4:
				chooseValuesPerThread<4, ReduceOperation>( inputArray, outputArray, offset, size, valuesPerThread, numberBlocks, sharedMemorySize );
				break;
			case 2:
				chooseValuesPerThread<2, ReduceOperation>( inputArray, outputArray, offset, size, valuesPerThread, numberBlocks, sharedMemorySize );
				break;
			case 1:
				chooseValuesPerThread<1, ReduceOperation>( inputArray, outputArray, offset, size, valuesPerThread, numberBlocks, sharedMemorySize );
				break;
		}
	}



	template<unsigned int blockSize, class ReduceOperation> CUDA_HOST void chooseValuesPerThread( const cuTool::SmartArray<T, hostDeviceType>& inputArray, const cuTool::SmartArray<T, hostDeviceType>& outputArray, const unsigned int offset, const unsigned int size, const unsigned int valuesPerThread, const unsigned int numberBlocks, const unsigned int sharedMemorySize )
	{
		if (valuesPerThread == 0)
		{
			std::cout << "Values per thread is zero (ill-defined!).\n";
			std::cout << "Reducer stops program.\n";
			std::exit(EXIT_FAILURE);
		}

		if (valuesPerThread > 5)
		{
			std::cout << "Maximum values per thread is 5. To use larger values, add code in \"chooseValuesPerThread\" in \"reducer.h\".\n";
			std::cout << "Reducer stops program.\n";
			std::exit(EXIT_FAILURE);
		}

		switch (valuesPerThread)
		{
			// case 10:
			// 	chooseKernelType<blockSize, 10, ReduceOperation>( inputArray, outputArray, offset, size, numberBlocks, sharedMemorySize );
			// 	break;
			// case 9:
			// 	chooseKernelType<blockSize, 9, ReduceOperation>( inputArray, outputArray, offset, size, numberBlocks, sharedMemorySize );
			// 	break;
			// case 8:
			// 	chooseKernelType<blockSize, 8, ReduceOperation>( inputArray, outputArray, offset, size, numberBlocks, sharedMemorySize );
			// 	break;
			// case 7:
			// 	chooseKernelType<blockSize, 7, ReduceOperation>( inputArray, outputArray, offset, size, numberBlocks, sharedMemorySize );
			// 	break;
			// case 6:
			// 	chooseKernelType<blockSize, 6, ReduceOperation>( inputArray, outputArray, offset, size, numberBlocks, sharedMemorySize );
			// 	break;
			case 5:
				chooseKernelType<blockSize, 5, ReduceOperation>( inputArray, outputArray, offset, size, numberBlocks, sharedMemorySize );
				break;
			case 4:
				chooseKernelType<blockSize, 4, ReduceOperation>( inputArray, outputArray, offset, size, numberBlocks, sharedMemorySize );
				break;
			case 3:
				chooseKernelType<blockSize, 3, ReduceOperation>( inputArray, outputArray, offset, size, numberBlocks, sharedMemorySize );
				break;
			case 2:
				chooseKernelType<blockSize, 2, ReduceOperation>( inputArray, outputArray, offset, size, numberBlocks, sharedMemorySize );
				break;
			case 1:
				chooseKernelType<blockSize, 1, ReduceOperation>( inputArray, outputArray, offset, size, numberBlocks, sharedMemorySize );
				break;
		}
	}



	template<unsigned int blockSize, unsigned int valuesPerThread, class ReduceOperation> CUDA_HOST void chooseKernelType( const cuTool::SmartArray<T, hostDeviceType>& inputArray, const cuTool::SmartArray<T, hostDeviceType>& outputArray, const unsigned int offset, const unsigned int size, const unsigned int numberBlocks, const unsigned int sharedMemorySize )
	{
		switch(hostDeviceType)
		{
			case CudaHostDeviceType::HOST:
				kernel::reduceHost<blockSize, valuesPerThread, ReduceOperation>( inputArray, outputArray, offset, size, numberBlocks );
				break;
			#ifdef __CUDACC__
			case CudaHostDeviceType::DEVICE:
				kernel::reduce<blockSize, valuesPerThread, ReduceOperation><<<numberBlocks, blockSize, sharedMemorySize>>>( inputArray, outputArray, offset, size );
				CHECK_CUDA_ERROR("Error in reduction kernel");
				break;
			#endif // __CUDACC__
		}
	}
};



} // namespace reducer
} // namespace finael


#endif // REDUCER_H_