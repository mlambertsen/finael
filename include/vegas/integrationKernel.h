/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_INTEGRATION_KERNEL_H_
#define FINAEL_CUVEGAS_INTEGRATION_KERNEL_H_

#include "../common/cudaHostDevice.h"

#include "KernelRelevantSettings.h"
#include "SampleCalculator.h"


namespace finael {
namespace cuVegas {
namespace integrationKernel {


#ifdef __CUDACC__
template<class Samples, class Functor, class KernelPrng, class Increments, class KernelRelevantSettings>
__global__ void calculateSamplesDevice( const Functor functor, Samples samples, KernelPrng prng, const Increments increments, const KernelRelevantSettings settings )
{
	unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;
	if(idx < samples.size())
	{
		SampleCalculator<Increments::dimension, typename Increments::type, Increments::hostDeviceType> sampleCalculator( settings.getJacobian() );
		sampleCalculator( idx, functor, samples, prng, increments, settings );
	}
}
#endif // __CUDACC__





template<class Samples, class Functor, class KernelPrng, class Increments, class KernelRelevantSettings>
CUDA_HOST void calculateSamplesHost( const unsigned int numberBlocks, const unsigned int blockDim, const Functor& functor, Samples& samples, KernelPrng& prng, const Increments& increments, const KernelRelevantSettings& settings )
{
	for (unsigned int blockIdx = 0; blockIdx < numberBlocks; ++blockIdx)
	{
		for (unsigned int threadIdx = 0; threadIdx < blockDim; ++threadIdx)
		{
			unsigned int idx = blockIdx * blockDim + threadIdx;
			if(idx < samples.size())
			{
				SampleCalculator<Increments::dimension, typename Increments::type, Increments::hostDeviceType> sampleCalculator( settings.getJacobian() );
				sampleCalculator( idx, functor, samples, prng, increments, settings );
			}
		}
	}
}



} // namespace integrationKernel
} // namespace cuVegas
} // namespace finael



#endif // FINAEL_CUVEGAS_INTEGRATION_KERNEL_H_
