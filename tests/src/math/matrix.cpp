/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#define BOOST_TEST_MODULE "finael Matrix"
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "../../../include/math/Matrix.h"
#include "../../../include/math/Vector.h"
#include "../../../include/math/Complex.h"



BOOST_AUTO_TEST_SUITE( matrix )



BOOST_AUTO_TEST_CASE( defaultConstructor )
{
	using namespace finael::cuMath;
	constexpr unsigned int rows = 3;
	constexpr unsigned int columns = 2;
	int standardInitializer { 0 };
	Matrix<rows, columns, int> matrix;

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), standardInitializer );
		}
	}
}



BOOST_AUTO_TEST_CASE( fixedValueConstructor )
{
	using namespace finael::cuMath;
	constexpr unsigned int rows = 3;
	constexpr unsigned int columns = 2;
	int value { 23 };
	Matrix<rows, columns, int> matrix( value );

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), value );
		}
	}
}



BOOST_AUTO_TEST_CASE( arrayConstructor )
{
	using namespace finael::cuMath;
	using namespace finael::cuTool;
	constexpr unsigned int rows = 3;
	constexpr unsigned int columns = 2;
	Array<Array<int, columns>, rows> array {{ {{1, 2}}, {{3, 4}}, {{5, 6}} }};
	Matrix<rows, columns, int> matrix( array );

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), array[row][column] );
		}
	}
}



BOOST_AUTO_TEST_CASE( setAllValues )
{
	using namespace finael::cuMath;
	constexpr unsigned int rows = 3;
	constexpr unsigned int columns = 2;
	Matrix<rows, columns, int> matrix;
	int value { 23 };
	matrix.set( value );

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), value );
		}
	}
}



BOOST_AUTO_TEST_CASE( setSingleValue )
{
	using namespace finael::cuMath;
	constexpr unsigned int rows = 3;
	constexpr unsigned int columns = 2;
	Matrix<rows, columns, int> matrix;
	matrix.set( 0, 1, 5 );

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			int expectedValue = 0;
			if (row == 0 && column == 1)
			{
				expectedValue = 5;
			}
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), expectedValue );
		}
	}
}



BOOST_AUTO_TEST_CASE( setArray )
{
	using namespace finael::cuMath;
	using namespace finael::cuTool;
	constexpr unsigned int rows = 3;
	constexpr unsigned int columns = 2;
	Matrix<rows, columns, int> matrix;
	Array<Array<int, columns>, rows> array {{ {{1, 2}}, {{3, 4}}, {{5, 6}} }};
	matrix.set( array );
	// matrix.set( {{ {{1, 2}}, {{3, 4}}, {{5, 6}} }} );

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), array[row][column] );
		}
	}
}



BOOST_AUTO_TEST_CASE( copyConstructor )
{
	using namespace finael::cuMath;
	constexpr unsigned int rows = 3;
	constexpr unsigned int columns = 2;
	Matrix<rows, columns, int> matrix( 23 );
	Matrix<rows, columns, int> copy( matrix );

	matrix.set( 42 );

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( copy.at( row ).at( column ), 23 );
		}
	}
}



BOOST_AUTO_TEST_CASE( assignmentOperator )
{
	using namespace finael::cuMath;
	constexpr unsigned int rows = 3;
	constexpr unsigned int columns = 2;
	Matrix<rows, columns, int> matrix( 23 );
	Matrix<rows, columns, int> copy;

	copy = matrix;

	matrix.set( 42 );

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( copy.at( row ).at( column ), 23 );
		}
	}
}



BOOST_AUTO_TEST_CASE( operatorPlusEqual )
{
	using namespace finael::cuMath;
	using namespace finael::cuTool;
	constexpr unsigned int rows = 3;
	constexpr unsigned int columns = 2;
	Array<Array<int, columns>, rows> array {{ {{1, 2}}, {{3, 4}}, {{5, 6}} }};
	Matrix<rows, columns, int> matrix( array );

	matrix += matrix;

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), 2*array[row][column] );
		}
	}
}



BOOST_AUTO_TEST_CASE( operatorMinusEqual )
{
	using namespace finael::cuMath;
	using namespace finael::cuTool;
	constexpr unsigned int rows = 3;
	constexpr unsigned int columns = 2;
	Array<Array<int, columns>, rows> array {{ {{1, 2}}, {{3, 4}}, {{5, 6}} }};
	Matrix<rows, columns, int> matrix( array );

	matrix -= matrix;

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), 0 );
		}
	}
}



BOOST_AUTO_TEST_CASE( operatorTimesEqual )
{
	using namespace finael::cuMath;
	using namespace finael::cuTool;
	constexpr unsigned int rows = 3;
	constexpr unsigned int columns = 2;
	Array<Array<int, columns>, rows> array {{ {{1, 2}}, {{3, 4}}, {{5, 6}} }};
	Matrix<rows, columns, int> matrix( array );
	int factor {2};

	matrix *= factor;

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), factor*array[row][column] );
		}
	}
}



BOOST_AUTO_TEST_CASE( operatorTimesEqualComplex )
{
	using namespace finael::cuMath;
	constexpr unsigned int rows = 3;
	constexpr unsigned int columns = 2;
	Matrix<rows, columns, Complex<int>> matrix( Complex<int>( 16 ) );

	Complex<int> complexFactor( 2 );
	matrix *= complexFactor;

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), Complex<int>( 32 ) );
		}
	}

	int factor {2};
	matrix *= factor;

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), Complex<int>( 64 ) );
		}
	}
}



BOOST_AUTO_TEST_CASE( operatorDivideEqual )
{
	using namespace finael::cuMath;
	using namespace finael::cuTool;
	constexpr unsigned int rows = 3;
	constexpr unsigned int columns = 2;
	Array<Array<int, columns>, rows> array {{ {{2, 4}}, {{6, 8}}, {{10, 12}} }};
	Matrix<rows, columns, int> matrix( array );
	int divisor{ 2 };

	matrix /= divisor;

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), array[row][column]/divisor );
		}
	}
}



BOOST_AUTO_TEST_CASE( operatorDivideEqualComplex )
{
	using namespace finael::cuMath;
	constexpr unsigned int rows = 3;
	constexpr unsigned int columns = 2;
	Matrix<rows, columns, Complex<int>> matrix( Complex<int>( 16 ) );

	Complex<int> complexDivisor( 2 );
	matrix /= complexDivisor;

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), Complex<int>( 8 ) );
		}
	}

	int divisor {2};
	matrix /= divisor;

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), Complex<int>( 4 ) );
		}
	}
}



BOOST_AUTO_TEST_CASE( operatorPlus )
{
	using namespace finael::cuMath;
	using namespace finael::cuTool;
	constexpr unsigned int rows = 3;
	constexpr unsigned int columns = 2;
	Array<Array<int, columns>, rows> array {{ {{1, 2}}, {{3, 4}}, {{5, 6}} }};
	Matrix<rows, columns, int> matrix( array );

	matrix = matrix + matrix;

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), 2*array[row][column] );
		}
	}
}



BOOST_AUTO_TEST_CASE( operatorMinus )
{
	using namespace finael::cuMath;
	using namespace finael::cuTool;
	constexpr unsigned int rows = 3;
	constexpr unsigned int columns = 2;
	Array<Array<int, columns>, rows> array {{ {{1, 2}}, {{3, 4}}, {{5, 6}} }};
	Matrix<rows, columns, int> matrix( array );

	matrix = matrix - matrix;

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), 0 );
		}
	}
}



BOOST_AUTO_TEST_CASE( operatorTimes )
{
	using namespace finael::cuMath;
	using namespace finael::cuTool;
	constexpr unsigned int rows = 3;
	constexpr unsigned int columns = 2;
	Array<Array<int, columns>, rows> array {{ {{1, 2}}, {{3, 4}}, {{5, 6}} }};
	Matrix<rows, columns, int> matrix( array );

	int factor {2};
	matrix = matrix * factor;

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), factor*array[row][column] );
		}
	}


	matrix = factor * matrix;

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), factor*factor*array[row][column] );
		}
	}

	double doubleFactor{ 2.0 };
	matrix = matrix * doubleFactor;

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), factor*factor*doubleFactor*array[row][column] );
		}
	}

	matrix = doubleFactor * matrix;

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), factor*factor*doubleFactor*doubleFactor*array[row][column] );
		}
	}
}



BOOST_AUTO_TEST_CASE( operatorTimesComplex )
{
	using namespace finael::cuMath;
	using namespace finael::cuTool;
	constexpr unsigned int rows = 3;
	constexpr unsigned int columns = 2;
	Complex<int> startValue( 5 );
	Matrix<rows, columns, Complex<int>> matrix( startValue );

	Complex<int> complexFactor( 2 );
	matrix = complexFactor * matrix;

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), complexFactor*startValue );
		}
	}

	matrix = matrix * complexFactor;

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), complexFactor*complexFactor*startValue );
		}
	}

	int factor {2};
	matrix = matrix * factor;

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), complexFactor*complexFactor*factor*startValue );
		}
	}


	matrix = factor * matrix;

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), complexFactor*complexFactor*factor*factor*startValue );
		}
	}

	double doubleFactor{ 2.0 };
	matrix = matrix * doubleFactor;

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), complexFactor*complexFactor*factor*factor*doubleFactor*startValue );
		}
	}

	matrix = doubleFactor * matrix;

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), complexFactor*complexFactor*factor*factor*doubleFactor*doubleFactor*startValue );
		}
	}
}



BOOST_AUTO_TEST_CASE( operatorDivide )
{
	using namespace finael::cuMath;
	constexpr unsigned int rows = 3;
	constexpr unsigned int columns = 2;
	int value{ 1024 };
	Matrix<rows, columns, int> matrix( value );

	int divisor {2};
	matrix = matrix / divisor;

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), value/divisor );
		}
	}


	double doubleDivisor{ 2.0 };
	matrix = matrix / doubleDivisor;

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), value/divisor/doubleDivisor );
		}
	}
}



BOOST_AUTO_TEST_CASE( operatorDivideComplex )
{
	using namespace finael::cuMath;
	constexpr unsigned int rows = 3;
	constexpr unsigned int columns = 2;
	Complex<int> value( 1024 );
	Matrix<rows, columns, Complex<int>> matrix( value );

	Complex<int> complexDivisor( 2 );
	matrix = matrix / complexDivisor;

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), value/complexDivisor );
		}
	}

	int divisor {2};
	matrix = matrix / divisor;

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), (value/complexDivisor)/divisor );
		}
	}

	double doubleDivisor{ 2.0 };
	matrix = matrix / doubleDivisor;

	for (unsigned int row = 0; row < rows; ++row)
	{
		for (unsigned int column = 0; column < columns; ++column)
		{
			BOOST_CHECK_EQUAL( matrix.at( row ).at( column ), ((value/complexDivisor)/divisor)/doubleDivisor );
		}
	}
}



BOOST_AUTO_TEST_CASE( operatorIsEqual )
{
	using namespace finael::cuMath;
	constexpr unsigned int rows = 3;
	constexpr unsigned int columns = 2;
	Matrix<rows, columns, int> first( {{ {{1, 2}}, {{3, 4}}, {{5, 6}} }} );
	Matrix<rows, columns, int> second( {{ {{1, 2}}, {{4, 4}}, {{5, 6}} }} );
	Matrix<rows, columns, int> third( {{ {{1, 2}}, {{3, 4}}, {{5, 5}} }} );
	Matrix<rows, columns, int> fourth( {{ {{4, 5}}, {{6, 1}}, {{2, 3}} }} );

	BOOST_CHECK_EQUAL( first == first, true );
	BOOST_CHECK_EQUAL( first == second, false );
	BOOST_CHECK_EQUAL( first == third, false );
	BOOST_CHECK_EQUAL( first == fourth, false );
}



BOOST_AUTO_TEST_CASE( operatorIsNotEqual )
{
	using namespace finael::cuMath;
	constexpr unsigned int rows = 3;
	constexpr unsigned int columns = 2;
	Matrix<rows, columns, int> first( {{ {{1, 2}}, {{3, 4}}, {{5, 6}} }} );
	Matrix<rows, columns, int> second( {{ {{1, 2}}, {{4, 4}}, {{5, 6}} }} );
	Matrix<rows, columns, int> third( {{ {{1, 2}}, {{3, 4}}, {{5, 5}} }} );
	Matrix<rows, columns, int> fourth( {{ {{4, 5}}, {{6, 1}}, {{2, 3}} }} );

	BOOST_CHECK_EQUAL( first != first, false );
	BOOST_CHECK_EQUAL( first != second, true );
	BOOST_CHECK_EQUAL( first != third, true );
	BOOST_CHECK_EQUAL( first != fourth, true );
}



BOOST_AUTO_TEST_CASE( matrixVectorMultiplication )
{
	using namespace finael::cuMath;
	constexpr unsigned int rows = 3;
	constexpr unsigned int columns = 2;
	Matrix<rows, columns, int> matrix( {{ {{1, 2}}, {{3, 4}}, {{5, 6}} }} );

	Vector<columns, int> vector( {{7, 8}} );

	Vector<rows, int> result ( matrix*vector );

	Vector<rows, int> expectedResult( {{23, 53, 83}} );

	BOOST_CHECK_EQUAL( result, expectedResult );
}



BOOST_AUTO_TEST_CASE( matrixMatrixMultiplication )
{
	using namespace finael::cuMath;
	constexpr unsigned int rows = 3;
	constexpr unsigned int columns = 4;
	constexpr unsigned int common = 2;
	Matrix<rows, common, int> first( {{ {{1, 2}}, {{3, 4}}, {{5, 6}} }} );
	Matrix<common, columns, int> second( {{ {{7, 8, 9, 10}}, {{11, 12, 13, 14}} }} );

	Matrix<rows, columns, int> result( first * second );

	Matrix<rows, columns, int> expectedResult( {{ {{29, 32, 35, 38}}, {{65, 72, 79, 86}}, {{101, 112, 123, 134}} }} );

	BOOST_CHECK_EQUAL( result, expectedResult );
}



BOOST_AUTO_TEST_SUITE_END()
