/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#define BOOST_TEST_MODULE "finael Mellin Spline"
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include <vector>
#include "../../../include/math/Complex.h"

#include "../../../include/interpolation/MellinSpline.h"



namespace finael {
namespace interpolation {
namespace mellinSplineTest {



auto function = []( double point )
{
	return 10.0/(1.0 + point*point);
};



struct BoostTestMellinSplineFixture
{
	BoostTestMellinSplineFixture()
		: points {-2.0, -1.0, 0.0, 1.0, 2.0}
		, largestIndex ( points.size() - 1 )
		, mellinSpline( points.begin(), points.end(), function )
	{}



	std::vector<double> points;
	std::size_t largestIndex;
	MellinSpline<double> mellinSpline;
};



BOOST_FIXTURE_TEST_SUITE( mellinSpline, BoostTestMellinSplineFixture )



BOOST_AUTO_TEST_CASE( needsMinimumThreePoints )
{
	std::array<double, 2> tooSmall;
	BOOST_CHECK_THROW( MellinSpline<double>( tooSmall.begin(), tooSmall.end(), function ), SplineError );
}



BOOST_AUTO_TEST_CASE( sortsUnsortedInput )
{
	std::vector<double> unsortedPoints {1.0, 2.0, 0.0};

	Spline<double> aMellinSpline( unsortedPoints.begin(), unsortedPoints.end(), function );

	BOOST_CHECK_EQUAL( aMellinSpline.getPointAt( 0 ), 0.0 );
	BOOST_CHECK_EQUAL( aMellinSpline.getPointAt( 1 ), 1.0 );
	BOOST_CHECK_EQUAL( aMellinSpline.getPointAt( 2 ), 2.0 );
}



BOOST_AUTO_TEST_CASE( defaultsToNaturalMellinSpline )
{
	MellinSpline<double> aMellinSpline( points.begin(), points.end(), function );

	BOOST_CHECK( aMellinSpline.getType() == SplineType::NATURAL );
}



BOOST_AUTO_TEST_CASE( respondsPassedType )
{
	MellinSpline<double> aMellinSpline( points.begin(), points.end(), function, SplineType::FIXED );

	BOOST_CHECK( aMellinSpline.getType() == SplineType::FIXED );
}



BOOST_AUTO_TEST_CASE( numberPointsIsEqualToNumberKnotsOfOrdinarySpline )
{
	Spline<double> aSpline( points.begin(), points.end(), function );
	MellinSpline<double> aMellinSpline( aSpline );

	BOOST_CHECK( aMellinSpline.numberPoints() == aSpline.numberPoints() );
}



BOOST_AUTO_TEST_CASE( hasFourCoefficientsPerPoint )
{
	auto coefficients = mellinSpline.getCoefficientsAt( 0 );

	BOOST_CHECK( coefficients.size() == MellinSpline<double>::coefficientsPerPoint );
}



BOOST_AUTO_TEST_CASE( throwsIfCoefficientRequestWithInvalidIndex )
{
	BOOST_CHECK_THROW( mellinSpline.getCoefficientsAt( ++largestIndex ), SplineError );
}



BOOST_AUTO_TEST_CASE( calculatesCorrectCoefficients )
{
	constexpr auto coefficientsPerPoint = MellinSpline<double>::coefficientsPerPoint;
	std::vector<double> expected =
		{-110.0/7.0, 240.0/7.0, -216.0/7.0, 72.0/7.0,
		40.0/7.0, -120.0/7.0, 120.0/7.0, -40.0/7.0,
		0.0, 0.0, 0.0, 0.0,
		-40.0/7.0, 120.0/7.0, -120.0/7.0, 40.0/7.0,
		110.0/7.0, -240.0/7.0, 216.0/7.0, -72.0/7.0
		};


	for (auto point = 0u; point < mellinSpline.numberPoints(); ++point)
	{
		auto coefficients = mellinSpline.getCoefficientsAt( point );

		for (auto i = 0u; i < coefficientsPerPoint; ++i)
		{
			BOOST_CHECK_CLOSE( coefficients.at( i ), expected.at( point*coefficientsPerPoint + i ), 1.e-12 );
		}
	}
}



BOOST_AUTO_TEST_CASE( calculatesCorrectValueForSumInMellinSpaceWithRealArgument )
{
	cuMath::Complex<double> variable( 3.0, 0.0 );
	double expectedResult {1868.0/105.0};

	auto result = mellinSpline( variable );

	BOOST_CHECK_CLOSE( result.real(), expectedResult, 1.e-12 );
	BOOST_CHECK_SMALL( result.imag(), 1.e-12 );
}



BOOST_AUTO_TEST_CASE( calculatesCorrectValueForSumInMellinSpaceWithComplexArgument )
{
	cuMath::Complex<double> variable( -2.0, 3.0 );
	cuMath::Complex<double> expectedResult( -0.14353039125365332455, 0.23779036172193501994 );

	auto result = mellinSpline( variable );

	BOOST_CHECK_CLOSE( result.real(), expectedResult.real(), 1.e-12 );
	BOOST_CHECK_CLOSE( result.imag(), expectedResult.imag(), 1.e-11 );
}



BOOST_AUTO_TEST_SUITE_END()


} // namespace mellinSplineTest
} // namespace interpolation
} // namespace finael
