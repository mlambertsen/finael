/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_ESTIMATE_H_
#define FINAEL_CUVEGAS_ESTIMATE_H_

#include "../../common/cudaHostDevice.h"

#include "FunctionSums.h"


namespace finael {
namespace cuVegas {



template<typename T>
struct Estimate
{
public:
	using type  = T;



	T estimate;
	T variance;



	CUDA_HOST_DEVICE Estimate()
		: estimate( 0 )
		, variance( 0 )
	{}



	CUDA_HOST_DEVICE Estimate( const FunctionSums<T>& sums )
		: estimate( (sums.cardinality == 0) ? 0 : sums.sum / sums.cardinality )
		, variance( (sums.cardinality == 0) ? 0 : (sums.squaredSum/sums.cardinality - estimate*estimate)/(sums.cardinality - 1) )
	{
		const T EPSILON = (sizeof( T ) == 4) ? 1.e-30f : 1.e-100;
		if (variance <= 0) variance = EPSILON/(sums.cardinality - 1);
	}



	CUDA_HOST_DEVICE Estimate operator+=( const Estimate& other )
	{
		estimate += other.estimate;
		variance += other.variance;

		return *this;
	}
};



} // namespace cuVegas
} // namespace finael


#endif // FINAEL_CUVEGAS_ESTIMATE_H_
