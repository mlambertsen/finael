/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_REFINED_ESTIMATE_CALCULATOR_H_
#define FINAEL_CUVEGAS_REFINED_ESTIMATE_CALCULATOR_H_

#include "../../common/cudaHostDevice.h"

#include "../Samples.h"
#include "../sums/Estimate.h"
#include "../sums/FunctionSums.h"


namespace finael {
namespace cuVegas {



class RefinedEstimateCalculator
{
public:
	template<typename T, CudaHostDeviceType hostDeviceType>
	CUDA_HOST_DEVICE static inline Estimate<T> calculate( const Samples<T, hostDeviceType, ReductionFlag::REFINED>& samples, const unsigned int incrementIdx, const unsigned int elementsPerIncrement, const unsigned int binInIncrementIdx, unsigned int binCapacity )
	{
		FunctionSums<T> binSums;
		for (auto sampleIdx = 0u; sampleIdx < binCapacity; ++sampleIdx)
		{
			auto inputIdx = (incrementIdx * elementsPerIncrement + binInIncrementIdx) * binCapacity + sampleIdx;

			if (binInIncrementIdx < elementsPerIncrement) binSums += samples.getFunctionValue( inputIdx );
		}

		return Estimate<T>( binSums );
	}
};



} // namespace cuVegas
} // namespace finael


#endif // FINAEL_CUVEGAS_REFINED_ESTIMATE_CALCULATOR_H_
