/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#define BOOST_TEST_MODULE "finael PolyGamma"
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "../../../include/math/PolyGamma.h"
#include "../../../include/math/Complex.h"



BOOST_AUTO_TEST_SUITE( aPolyGamma )



BOOST_AUTO_TEST_CASE( digammaSmallRealArgument )
{
	using namespace finael::cuMath;
	PolyGamma<0> diGamma;

	double arg( 3 );
	double expectedResult( 0.92278433509846713939 );

	BOOST_CHECK_CLOSE( diGamma( arg ), expectedResult, 1.e-8 );
}



BOOST_AUTO_TEST_CASE( diGammaSmallComplexArgument )
{
	using namespace finael::cuMath;
	PolyGamma<0> diGamma;

	Complex<double> arg( -3, 2 );
	Complex<double> expectedResult( 1.3953607461432082959, 2.6246534364883840745 );

	BOOST_CHECK_CLOSE( diGamma( arg ).real(), expectedResult.real(), 1.e-8 );
	BOOST_CHECK_CLOSE( diGamma( arg ).imag(), expectedResult.imag(), 1.e-8 );
}



BOOST_AUTO_TEST_CASE( diGammaLargeRealComponent )
{
	using namespace finael::cuMath;
	PolyGamma<0> diGamma;

	Complex<double> arg( -3000, 2 );
	Complex<double> expectedResult( 8.0065344472057653978, 3.1409480098152752565 );

	BOOST_CHECK_CLOSE( diGamma( arg ).real(), expectedResult.real(), 1.e-8 );
	BOOST_CHECK_CLOSE( diGamma( arg ).imag(), expectedResult.imag(), 1.e-8 );
}



BOOST_AUTO_TEST_CASE( diGammaLargeImaginaryComponent )
{
	using namespace finael::cuMath;
	PolyGamma<0> diGamma;

	Complex<double> arg( -11, 120 );
	Complex<double> expectedResult( 4.7920599832702617444, 1.6663384321276013798 );

	BOOST_CHECK_CLOSE( diGamma( arg ).real(), expectedResult.real(), 1.e-8 );
	BOOST_CHECK_CLOSE( diGamma( arg ).imag(), expectedResult.imag(), 1.e-8 );
}



BOOST_AUTO_TEST_CASE( floatDiGammaLargeImaginaryComponent )
{
	using namespace finael::cuMath;
	PolyGamma<0> diGamma;

	Complex<float> arg( -11, 10 );
	Complex<float> expectedResult( 2.7239322305993974399, 2.4260266927999668835 );

	BOOST_CHECK_CLOSE( diGamma( arg ).real(), expectedResult.real(), 1.e-4 );
	BOOST_CHECK_CLOSE( diGamma( arg ).imag(), expectedResult.imag(), 1.e-4 );
}



BOOST_AUTO_TEST_CASE( diGammaLargeRealAndImaginary )
{
	using namespace finael::cuMath;
	PolyGamma<0> diGamma;

	Complex<double> arg( -3000, 2000 );
	Complex<double> expectedResult( 8.1903453398628067660, 2.5536669672019890358 );

	BOOST_CHECK_CLOSE( diGamma( arg ).real(), expectedResult.real(), 1.e-8 );
	BOOST_CHECK_CLOSE( diGamma( arg ).imag(), expectedResult.imag(), 1.e-8 );
}



BOOST_AUTO_TEST_CASE( trigammaSmallRealArgument )
{
	using namespace finael::cuMath;
	PolyGamma<1> triGamma;

	double arg( 3 );
	double expectedResult( 0.39493406684822643647 );

	BOOST_CHECK_CLOSE( triGamma( arg ), expectedResult, 1.e-8 );
}



BOOST_AUTO_TEST_CASE( trigammaSmallComplexArgument )
{
	using namespace finael::cuMath;
	PolyGamma<1> triGamma;

	Complex<double> arg( -3, 2 );
	Complex<double> expectedResult( -0.21548303904248736995, -0.12181963298746643449 );

	BOOST_CHECK_CLOSE( triGamma( arg ).real(), expectedResult.real(), 1.e-8 );
	BOOST_CHECK_CLOSE( triGamma( arg ).imag(), expectedResult.imag(), 1.e-8 );
}



BOOST_AUTO_TEST_CASE( trigammaComplexWithLargeRealPartArgument )
{
	using namespace finael::cuMath;
	PolyGamma<1> triGamma;

	Complex<double> arg( -3000, 2 );
	Complex<double> expectedResult( -0.00047095335398742697100, -2.2214806179426379e-7 );

	BOOST_CHECK_CLOSE( triGamma( arg ).real(), expectedResult.real(), 1.e-7 );
	BOOST_CHECK_CLOSE( triGamma( arg ).imag(), expectedResult.imag(), 1.e-7 );
}



BOOST_AUTO_TEST_CASE( trigammaComplexWithLargeImaginaryPartArgument )
{
	using namespace finael::cuMath;
	PolyGamma<1> triGamma;

	Complex<double> arg( -11, 230 );
	Complex<double> expectedResult( -0.0002168502002579322791, -0.0043369903749600723363 );

	BOOST_CHECK_CLOSE( triGamma( arg ).real(), expectedResult.real(), 1.e-7 );
	BOOST_CHECK_CLOSE( triGamma( arg ).imag(), expectedResult.imag(), 1.e-7 );
}



BOOST_AUTO_TEST_CASE( floatTrigammaComplexWithLargeImaginaryPartArgument )
{
	using namespace finael::cuMath;
	PolyGamma<1> triGamma;

	Complex<float> arg( -11, 30 );
	Complex<float> expectedResult( -0.011142950111343440158, -0.029063870683514101012 );

	BOOST_CHECK_CLOSE( triGamma( arg ).real(), expectedResult.real(), 1.e-4 );
	BOOST_CHECK_CLOSE( triGamma( arg ).imag(), expectedResult.imag(), 1.e-4 );
}



BOOST_AUTO_TEST_CASE( trigammaComplexWithLargeRealAndImaginaryArgument )
{
	using namespace finael::cuMath;
	PolyGamma<1> triGamma;

	Complex<double> arg( -3000, 2000 );
	Complex<double> expectedResult( -0.00023075443718707333534, -0.00015381065437718099345 );

	BOOST_CHECK_CLOSE( triGamma( arg ).real(), expectedResult.real(), 1.e-7 );
	BOOST_CHECK_CLOSE( triGamma( arg ).imag(), expectedResult.imag(), 1.e-7 );
}



BOOST_AUTO_TEST_CASE( tetragammaSmallRealArgument )
{
	using namespace finael::cuMath;
	PolyGamma<2> tetraGamma;

	double arg( 3 );
	double expectedResult( -0.15411380631918857080 );

	BOOST_CHECK_CLOSE( tetraGamma( arg ), expectedResult, 1.e-7 );
}



BOOST_AUTO_TEST_CASE( tetragammaSmallComplexArgument )
{
	using namespace finael::cuMath;
	PolyGamma<2> tetraGamma;

	Complex<double> arg( -3, 2 );
	Complex<double> expectedResult( -0.031668636387861625022, -0.053057239562468220776 );

	BOOST_CHECK_CLOSE( tetraGamma( arg ).real(), expectedResult.real(), 1.e-7 );
	BOOST_CHECK_CLOSE( tetraGamma( arg ).imag(), expectedResult.imag(), 1.e-7 );
}



BOOST_AUTO_TEST_CASE( tetragammaWithLargeRealPartArgument )
{
	using namespace finael::cuMath;
	PolyGamma<2> tetraGamma;

	Complex<double> arg( -3000, 2 );
	Complex<double> expectedResult( -1.1107393219761283e-7, -0.00086504823067994989460 );

	BOOST_CHECK_CLOSE( tetraGamma( arg ).real(), expectedResult.real(), 1.e-6 );
	BOOST_CHECK_CLOSE( tetraGamma( arg ).imag(), expectedResult.imag(), 1.e-7 );
}



BOOST_AUTO_TEST_CASE( tetragammaWithLargeImaginaryPartArgument )
{
	using namespace finael::cuMath;
	PolyGamma<2> tetraGamma;

	Complex<double> arg( -11, 230 );
	Complex<double> expectedResult( 0.000018762490544480159750, -1.880960344647408478e-6 );

	BOOST_CHECK_CLOSE( tetraGamma( arg ).real(), expectedResult.real(), 1.e-7 );
	BOOST_CHECK_CLOSE( tetraGamma( arg ).imag(), expectedResult.imag(), 1.e-7 );
}



BOOST_AUTO_TEST_CASE( floatTetragammaWithLargeImaginaryPartArgument )
{
	using namespace finael::cuMath;
	PolyGamma<2> tetraGamma;

	Complex<float> arg( -11, 30 );
	Complex<float> expectedResult( 0.00072055151778226441173, -0.00064779234544084529727 );

	BOOST_CHECK_CLOSE( tetraGamma( arg ).real(), expectedResult.real(), 1.e-5 );
	BOOST_CHECK_CLOSE( tetraGamma( arg ).imag(), expectedResult.imag(), 1.e-5 );
}



BOOST_AUTO_TEST_CASE( tetragammaWithLargeRealAndImaginaryPartArgument )
{
	using namespace finael::cuMath;
	PolyGamma<2> tetraGamma;

	Complex<double> arg( -3000, 2000 );
	Complex<double> expectedResult( -2.9589893228528483138e-8, -7.0984981618290704687e-8 );

	BOOST_CHECK_CLOSE( tetraGamma( arg ).real(), expectedResult.real(), 1.e-7 );
	BOOST_CHECK_CLOSE( tetraGamma( arg ).imag(), expectedResult.imag(), 1.e-7 );
}



BOOST_AUTO_TEST_SUITE_END()
