/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUMATH_CONSTANTS_H_
#define FINAEL_CUMATH_CONSTANTS_H_



namespace finael {
namespace cuMath {



template<typename T>
struct CONST
{
	constexpr static T PI = {3.1415926535897932384626433832795029L};
	constexpr static T EULER = {2.7182818284590452353602874713526625L};

	constexpr static T EULERGAMMA = {0.5772156649015328606065120900824024L};

	constexpr static T ZETA2 = PI*PI/6.0;
	constexpr static T ZETA3 = {1.2020569031595942853997381615114500L};
};



} // namespace cuMath
} // namespace finael

#endif // FINAEL_CUMATH_CONSTANTS_H_
