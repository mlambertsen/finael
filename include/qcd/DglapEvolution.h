/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUQCD_DGLAPEVOLUTION_H_
#define FINAEL_CUQCD_DGLAPEVOLUTION_H_

#include "../common/cudaHostDevice.h"
#include "definitions.h"
#include "PartonDistribution.h"
#include "EvolutionKernelCreator.h"



namespace finael {
namespace cuQcd {



template<class AlphaS, typename ScalarType>
class DglapEvolution
{
public:
	using ScaleType = typename AlphaS::ScaleType;
	static constexpr unsigned short NUMBERFLAVOURS = AlphaS::NUMBERFLAVOURS;
	static constexpr PQcdOrder ORDER = AlphaS::ORDER;



	template<class Complex>
	CUDA_HOST_DEVICE void evolve( PartonDistribution<NUMBERFLAVOURS, Complex, ScaleType>& dist, const ScaleType& newScale, const Complex& mellinMoment ) const
	{
		auto evolutionKernels = evolutionKernelCreator.getEvolutionKernels( alphaS( dist.getScale() ), alphaS( newScale ), mellinMoment );

		dist.evolve( evolutionKernels, newScale );
	}



private:
	AlphaS alphaS;
	EvolutionKernelCreator<NUMBERFLAVOURS, ORDER, ScalarType> evolutionKernelCreator;
};



template<class AlphaS, typename ScalarType>
constexpr unsigned short DglapEvolution<AlphaS, ScalarType>::NUMBERFLAVOURS;

template<class AlphaS, typename ScalarType>
constexpr PQcdOrder DglapEvolution<AlphaS, ScalarType>::ORDER;



} // namespace cuQcd
} // namespace finael


#endif // FINAEL_CUQCD_DGLAPEVOLUTION_H_
