/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_PRNG_SHUFFLE_TABLE_H_
#define FINAEL_PRNG_SHUFFLE_TABLE_H_

#include <array>



namespace finael {
namespace prng {



template<std::size_t numElements, std::size_t maxInput>
class ShuffleTable
{
public:
	template<class Prng>
	ShuffleTable( Prng& prng )
	{
		for (auto index = 0u; index < numElements; ++index)
		{
			table_[index] = prng.getRandomNumber();
		}
		indexDefiner_ = table_[0];
	}



	void setIndexDefiner( std::size_t indexDefiner )
	{
		indexDefiner_ = indexDefiner;
	}



	std::size_t use( std::size_t input )
	{
		std::size_t index = getIndex();
		std::size_t output = table_[index];
		table_[index] = input;
		return output;
	}



private:
	std::array<std::size_t, numElements> table_;
	std::size_t indexDefiner_;



	std::size_t getIndex() const
	{
		constexpr std::size_t inputRangePerStoredElement = maxInput / numElements + 1;
		return indexDefiner_/inputRangePerStoredElement;
	}
};



} // namespace prng
} // namespace finael


#endif // FINAEL_PRNG_SHUFFLE_TABLE_H_
