/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUTOOL_PAIR_H_
#define FINAEL_CUTOOL_PAIR_H_

#include "../common/cudaHostDevice.h"



namespace finael {
namespace cuTool {



template<typename T1, typename T2>
struct Pair
{
	using first_type = T1;
	using second_type = T2;

	T1 first;
	T2 second;



	CUDA_HOST_DEVICE Pair()
		: first()
		, second()
	{}



	CUDA_HOST_DEVICE Pair( T1 firstInput, T2 secondInput )
		: first( firstInput )
		, second( secondInput )
	{}
};



} // namespace cuTool
} // namespace finael

#endif // FINAEL_CUTOOL_PAIR_H_
