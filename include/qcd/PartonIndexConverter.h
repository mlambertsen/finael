/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUQCD_PARTON_INDEX_CONVERTER_H_
#define FINAEL_CUQCD_PARTON_INDEX_CONVERTER_H_

#include "definitions.h"



namespace finael {
namespace cuQcd {



template<short idx>
class PartonIndexConverter
{
public:
	static_assert(idx < 7 && idx > -7, "ERROR: Given PartonFlavour not implemented in PartonIndexConverter!");

	static constexpr PartonFlavour FLAVOUR = (idx == 0) ? PartonFlavour::G
								: (idx == 1) ? PartonFlavour::U
								: (idx == -1) ? PartonFlavour::UBAR
								: (idx == 2) ? PartonFlavour::D
								: (idx == -2) ? PartonFlavour::DBAR
								: (idx == 3) ? PartonFlavour::S
								: (idx == -3) ? PartonFlavour::SBAR
								: (idx == 4) ? PartonFlavour::C
								: (idx == -4) ? PartonFlavour::CBAR
								: (idx == 5) ? PartonFlavour::B
								: (idx == -5) ? PartonFlavour::BBAR
								: (idx == 6) ? PartonFlavour::T
								: (idx == -6) ? PartonFlavour::TBAR : PartonFlavour::G;
};





template<short idx>
constexpr PartonFlavour PartonIndexConverter<idx>::FLAVOUR;



} // namespace cuQcd
} // namespace finael



#endif // FINAEL_CUQCD_PARTON_INDEX_CONVERTER_H_
