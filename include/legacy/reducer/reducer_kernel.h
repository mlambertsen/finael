#ifndef REDUCER_KERNEL_H_
#define REDUCER_KERNEL_H_

#include "../../common/cudaHostDevice.h"
#include "reduce_function.h"
#include "basic_definitions.h"



namespace finael {
namespace reducer {
namespace kernel {



#ifdef __CUDACC__
template<unsigned int blockSize, typename T> CUDA_DEVICE void inline reduceSharedDataDevice( T* sharedData, unsigned int tid )
{
	if(blockSize >= 512)
	{
		if(tid < 256) sharedData[tid] += sharedData[tid + 256];
		__syncthreads();
	}
	if(blockSize >= 256)
	{
		if(tid < 128) sharedData[tid] += sharedData[tid + 128];
		__syncthreads();
	}
	if(blockSize >= 128)
	{
		if(tid < 64) sharedData[tid] += sharedData[tid + 64];
		__syncthreads();
	}

	#if (__CUDA_ARCH__ >= 300 )
		if (tid < 32)
		{
			// fetch sum from second warp
			if (blockSize >= 64) sharedData[tid] += sharedData[tid + 32];
			// reduce via shuffle
			T mySum = sharedData[tid];
			for (unsigned int offset = WARPSIZE/2; offset > 0; offset /= 2)
			{
				mySum += __shfl_down( mySum, offset );
			}
			if (tid == 0) sharedData[0] = mySum;
		}
	#else
		if(blockSize >= 64)
		{
			if(tid < 32) sharedData[tid] += sharedData[tid + 32];
			__syncthreads();
		}
		if(blockSize >= 32)
		{
			if(tid < 16) sharedData[tid] += sharedData[tid + 16];
			__syncthreads();
		}
		if(blockSize >= 16)
		{
			if(tid < 8) sharedData[tid] += sharedData[tid + 8];
			__syncthreads();
		}
		if(blockSize >= 8)
		{
			if(tid < 4) sharedData[tid] += sharedData[tid + 4];
			__syncthreads();
		}
		if(blockSize >= 4)
		{
			if(tid < 2) sharedData[tid] += sharedData[tid + 2];
			__syncthreads();
		}
		if(blockSize >= 2)
		{
			if(tid < 1) sharedData[tid] += sharedData[tid + 1];
			__syncthreads();
		}
	#endif
}
#endif /* __CUDACC__ */



template<unsigned int blockSize, typename T> CUDA_HOST void inline reduceSharedDataHost( T* sharedData, unsigned int numberBlocks )
{
	for (unsigned int blockSizeLimit = 512; blockSizeLimit > 1; blockSizeLimit /= 2)
	{
		for (unsigned int block = 0; block < numberBlocks; ++block)
		{
			for (unsigned int thread = 0; thread < blockSize; ++thread)
			{
				if (blockSize >= blockSizeLimit)
				{
					if (thread < blockSizeLimit / 2) sharedData[block * blockSize + thread] += sharedData[block * blockSize + thread + blockSizeLimit / 2];
				}
			}
		}
	}
}



template<unsigned int valuesPerThread, typename ReduceOperation, CudaHostDeviceType hostDeviceType, typename T> CUDA_HOST_DEVICE void inline reduceToSharedData( const cuTool::SmartArray<T, hostDeviceType>& inputArray, unsigned int offset, unsigned int size, unsigned int idx, unsigned int tid, T* sharedData )
{
	T mySum = ReduceOperation::template calcMyContribution<valuesPerThread>( inputArray, size, offset, idx );

	if (hostDeviceType == CudaHostDeviceType::HOST) sharedData[idx] = mySum;
	else sharedData[tid] = mySum;
}



#ifdef __CUDACC__
template<unsigned int blockSize, unsigned int valuesPerThread, typename ReduceOperation, CudaHostDeviceType hostDeviceType, typename T> __global__ void reduce( const cuTool::SmartArray<T, hostDeviceType> inputArray, cuTool::SmartArray<T, hostDeviceType> outputArray, const unsigned int offset, const unsigned int size )
{
	extern __shared__ T sharedData[];
	unsigned int tid = threadIdx.x;
	unsigned int idx = blockIdx.x*blockSize + tid;

	reduceToSharedData<valuesPerThread, ReduceOperation>( inputArray, offset, size, idx, tid, sharedData );
	__syncthreads();

	reduceSharedDataDevice<blockSize>( sharedData, tid );

	if(tid == 0) outputArray[blockIdx.x] = sharedData[0];
}
#endif


template<unsigned int blockSize, unsigned int valuesPerThread, typename ReduceOperation, CudaHostDeviceType hostDeviceType, typename T> CUDA_HOST void reduceHost( const cuTool::SmartArray<T, hostDeviceType> inputArray, cuTool::SmartArray<T, hostDeviceType> outputArray, const unsigned int offset, const unsigned int size, const unsigned int numberBlocks )
{
	T sharedData[numberBlocks * blockSize];

	for (unsigned int block = 0; block < numberBlocks; ++block)
	{
		for (unsigned int thread = 0; thread < blockSize; ++thread)
		{
			unsigned int idx = block * blockSize + thread;
			reduceToSharedData<valuesPerThread, ReduceOperation>( inputArray, offset, size, idx, thread, sharedData );
		}
	}

	reduceSharedDataHost<blockSize>( sharedData, numberBlocks );

	for (unsigned int block = 0; block < numberBlocks; ++block)
	{
		outputArray[block] = sharedData[block*blockSize];
	}
}



} // namespace kernel
} // namespace reducer
} // namespace finael


#endif // REDUCER_KERNEL_H_