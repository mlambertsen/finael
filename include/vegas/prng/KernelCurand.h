/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_KERNEL_CURAND_H_
#define FINAEL_CUVEGAS_KERNEL_CURAND_H_

#include <limits>
#include <stdexcept>

#include "../../common/cudaHostDevice.h"
#include "../../prng/CurandWrapper.h"
#include "../../tools/SmartArray.h"


namespace finael {
namespace cuVegas {



template<unsigned int dim, typename T, CudaHostDeviceType hostDeviceType>
class KernelCurand
{
public:
	CUDA_HOST void prepareForIntegration( prng::CurandWrapper<T>& curandWrapper, const unsigned int numberSamples )
	{
		auto numberRandomNumbers = detectOverflow( numberSamples );
		randomNumbers_ = curandWrapper.getRandomNumbers( numberRandomNumbers );
	}



	CUDA_HOST_DEVICE T getRandomNumber( const unsigned int index ) const
	{
		return randomNumbers_[index];
	}



private:
	cuTool::SmartArray<T, hostDeviceType> randomNumbers_;



	CUDA_HOST unsigned int detectOverflow( unsigned int numberSamples ) const
	{
		if (std::numeric_limits<unsigned int>::max() < static_cast<T>( numberSamples ) * dim) throw std::overflow_error( "Number of samples too large." );
		return dim * numberSamples;
	}
};



} // namespace cuVegas
} // namespace finael



#endif // FINAEL_CUVEGAS_KERNEL_CURAND_H_
