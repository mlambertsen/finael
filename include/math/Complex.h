/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUMATH_COMPLEX_H_
#define FINAEL_CUMATH_COMPLEX_H_

#include <ostream>
#include <cmath>

#include "../common/cudaHostDevice.h"

#include "constants.h"


namespace finael {
namespace cuMath {



template<typename T>
class Complex
{
public:
	using value_type = T;



	CUDA_HOST_DEVICE Complex( T realIn, T imagIn )
	: real_( realIn )
	, imag_( imagIn )
	{}



	explicit CUDA_HOST_DEVICE Complex( T realIn )
	: Complex( realIn, static_cast<T>(0) )
	{}



	CUDA_HOST_DEVICE Complex()
	: Complex( static_cast<T>(0) )
	{}



	CUDA_HOST_DEVICE constexpr T real() const
	{
		return this->real_;
	}



	CUDA_HOST_DEVICE constexpr T imag() const
	{
		return this->imag_;
	}



	CUDA_HOST_DEVICE Complex& operator+=( const Complex& rhs )
	{
		this->real_ += rhs.real_;
		this->imag_ += rhs.imag_;
		return *this;
	}



	CUDA_HOST_DEVICE Complex& operator+=( const T rhs )
	{
		this->real_ += rhs;
		return *this;
	}



	CUDA_HOST_DEVICE Complex& operator-=( const Complex& rhs )
	{
		this->real_ -= rhs.real_;
		this->imag_ -= rhs.imag_;
		return *this;
	}



	CUDA_HOST_DEVICE Complex& operator-=( const T rhs )
	{
		this->real_ -= rhs;
		return *this;
	}



	CUDA_HOST_DEVICE Complex& operator*=( const Complex& rhs )
	{
		T x_old = this->real_;
		T y_old = this->imag_;
		this->real_ = x_old*rhs.real_ - y_old*rhs.imag_;
		this->imag_ = x_old*rhs.imag_ + y_old*rhs.real_;
		return *this;
	}



	CUDA_HOST_DEVICE Complex& operator*=( const T rhs )
	{
		this->real_ *= rhs;
		this->imag_ *= rhs;
		return *this;
	}



	CUDA_HOST_DEVICE Complex& operator/=( const Complex &rhs )
	{
		*this = *this*rhs.conj()/rhs.norm();
		return *this;
	}



	CUDA_HOST_DEVICE Complex& operator/=( const T rhs )
	{
		this->real_ /= rhs;
		this->imag_ /= rhs;
		return *this;
	}



	CUDA_HOST_DEVICE Complex operator-() const
	{
		return Complex<T>(-this->real_,-this->imag_);
	}



	CUDA_HOST_DEVICE friend Complex operator+( Complex lhs, const T rhs)
	{
		return lhs += rhs;
	}



	CUDA_HOST_DEVICE friend Complex operator+( const T lhs, Complex rhs)
	{
		return rhs += lhs;
	}



	CUDA_HOST_DEVICE friend Complex operator-( Complex lhs, const T rhs)
	{
		return lhs -= rhs;
	}



	CUDA_HOST_DEVICE friend Complex operator-( const T lhs, const Complex& rhs)
	{
		Complex temp( lhs );
		return temp -= rhs;
	}



	CUDA_HOST_DEVICE friend Complex operator*( Complex lhs, const T rhs)
	{
		return lhs *= rhs;
	}



	CUDA_HOST_DEVICE friend Complex operator*( const T lhs, Complex rhs)
	{
		return rhs *= lhs;
	}



	CUDA_HOST_DEVICE friend Complex operator/( Complex lhs, const T rhs)
	{
		return lhs /= rhs;
	}



	CUDA_HOST_DEVICE friend Complex operator/( const T lhs, const Complex& rhs)
	{
		Complex temp( lhs );
		return temp /= rhs;
	}



	CUDA_HOST_DEVICE bool operator==( const Complex& rhs ) const
	{
		return ( real_ == rhs.real_ && imag_ == rhs.imag_ );
	}



	CUDA_HOST_DEVICE bool operator!=( const Complex& rhs ) const
	{
		return !operator==( rhs );
	}



	CUDA_HOST_DEVICE T norm() const
	{
		return ( this->real_ * this->real_ + this->imag_ * this->imag_ );
	}



	CUDA_HOST_DEVICE T abs() const
	{
		#if __CUDACC__
			if (sizeof(T) == 4)	return ::sqrtf( norm() );
			else
		#endif
		return ::sqrt( norm() );
	}



	CUDA_HOST_DEVICE bool isZero() const
	{
		return this->real_ == 0 && this->imag_ == 0;
	}



	CUDA_HOST_DEVICE T arg() const
	{
		#if __CUDACC__
			if (sizeof(T) == 4)	return ::atan2f( this->imag_, this->real_ );
			else
		#endif
		return ::atan2( this->imag_, this->real_ );
	}



	CUDA_HOST_DEVICE Complex conj() const
	{
		return Complex( this->real_, -this->imag_ );
	}



	CUDA_HOST_DEVICE Complex exp() const
	{
		#if __CUDACC__
			if (sizeof(T) == 4)	return Complex<T>( ::expf( this->real_ ) * ::cosf( this->imag_ ), ::expf( this->real_ ) * ::sinf( this->imag_ ) );
			else
		#endif
		return Complex( ::exp( this->real_ ) * ::cos( this->imag_ ), ::exp( this->real_ ) * ::sin( this->imag_ ) );
	}



	CUDA_HOST_DEVICE Complex log() const
	{
		#if __CUDACC__
			if (sizeof(T) == 4)	return Complex<T>( ::logf( this->abs() ), this->arg() );
			else
		#endif
		return Complex( ::log( this->abs() ), this->arg() );
	}



	CUDA_HOST_DEVICE Complex<T> pow( T exponent ) const
	{
		if (exponent == 0) return Complex( 1 );
		if (isZero() && exponent > 0) return Complex( 0 );
		return Complex( exponent * (this->log()) ).exp();
	}



	CUDA_HOST_DEVICE Complex<T> pow( const Complex<T>& exponent ) const
	{
		if (exponent.isZero()) return Complex( 1 );
		if (isZero()) return Complex( 0 );
		return Complex( exponent * (this->log()) ).exp();
	}



	CUDA_HOST_DEVICE Complex<T> sin() const
	{
		#if __CUDACC__
			if (sizeof(T) == 4)	return Complex<T>( ::sinf( this->real_ ) * ::coshf( this->imag_ ), ::cosf( this->real_ ) * ::sinhf( this->imag_ ) );
			else
		#endif
		return Complex( ::sin( this->real_ ) * ::cosh( this->imag_ ), ::cos( this->real_ ) * ::sinh( this->imag_ ) );
	}



	CUDA_HOST_DEVICE Complex<T> cos() const
	{
		#if __CUDACC__
			if (sizeof(T) == 4)	return Complex<T>( ::cosf( this->real_ ) * ::coshf( this->imag_ ), -::sinf( this->real_ ) * ::sinhf( this->imag_ ) );
			else
		#endif
		return Complex( ::cos( this->real_ ) * ::cosh( this->imag_ ), -::sin( this->real_ ) * ::sinh( this->imag_ ) );
	}



	CUDA_HOST_DEVICE Complex<T> sqrt() const
	{
		if (this->real_ == 0)
		{
			const auto root = ::sqrt( globalAbs( this->imag_ ) / 2 );
			return Complex<T>( root, (this->imag_ > 0) ? root : - root );
		}
		else
		{
			const auto root = ::sqrt( 2 * (this->abs() + globalAbs( this->real_ )) );
			const auto rootHalf = root / 2;
			return (this->real_ > 0) ? Complex<T>( rootHalf, this->imag_ / root ) : Complex<T>( globalAbs( this->imag_ ) / root, (this->imag_ > 0) ? rootHalf : - rootHalf);
		}
	}



private:
	T real_; // real
	T imag_; // imaginary



	CUDA_HOST_DEVICE T globalAbs( T val ) const
	{
		#if __CUDACC__
			if (sizeof(T) == 4) return ::fabsf( val );
			else
		#endif // __CUDACC__
		return ::fabs( val );
	}
};




template<typename T>
CUDA_HOST_DEVICE constexpr T real( const Complex<T>& complex )
{
	return complex.real();
}



template<typename T>
CUDA_HOST_DEVICE constexpr T imag( const Complex<T>& complex )
{
	return complex.imag();
}



template<typename T>
CUDA_HOST_DEVICE Complex<T> operator+( Complex<T> lhs, const Complex<T>& rhs )
{
	return lhs += rhs;
}



template<typename T>
CUDA_HOST_DEVICE Complex<T> operator+( Complex<T> lhs, const T rhs );



template<typename T>
CUDA_HOST_DEVICE Complex<T> operator+( const T lhs, Complex<T> rhs );



template<typename T>
CUDA_HOST_DEVICE Complex<T> operator-( Complex<T> lhs, const Complex<T>& rhs )
{
	return lhs -= rhs;
}



template<typename T>
CUDA_HOST_DEVICE Complex<T> operator-( Complex<T> lhs, const T rhs );



template<typename T>
CUDA_HOST_DEVICE Complex<T> operator-( const T lhs, const Complex<T>& rhs );



template<typename T>
CUDA_HOST_DEVICE Complex<T> operator*( Complex<T> lhs, const Complex<T>& rhs )
{
	return lhs *= rhs;
}



template<typename T>
CUDA_HOST_DEVICE Complex<T> operator*( Complex<T> lhs, const T rhs );



template<typename T>
CUDA_HOST_DEVICE Complex<T> operator*( const T lhs, Complex<T> rhs );



template<typename T>
CUDA_HOST_DEVICE Complex<T> operator/( Complex<T> lhs, const Complex<T>& rhs )
{
	return lhs /= rhs;
}



template<typename T>
CUDA_HOST_DEVICE Complex<T> operator/( Complex<T> lhs, const T rhs );



template<typename T>
CUDA_HOST_DEVICE Complex<T> operator/( const T lhs, const Complex<T>& rhs );



template<typename T>
CUDA_HOST_DEVICE T norm( const Complex<T>& complex )
{
	return complex.norm();
}



template<typename T>
CUDA_HOST_DEVICE T abs( const Complex<T>& complex )
{
	return complex.abs();
}



template<typename T>
CUDA_HOST_DEVICE T arg( const Complex<T>& complex )
{
	return complex.arg();
}



template<typename T>
CUDA_HOST_DEVICE Complex<T> conj( const Complex<T>& complex )
{
	return complex.conj();
}



template<typename T>
CUDA_HOST_DEVICE Complex<T> exp( const Complex<T>& complex )
{
	return complex.exp();
}



template<typename T>
CUDA_HOST_DEVICE Complex<T> log( const Complex<T>& complex )
{
	return complex.log();
}



template<typename T>
CUDA_HOST_DEVICE Complex<T> pow( const Complex<T>& complex, T exponent )
{
	return complex.pow( exponent );
}



template<typename T>
CUDA_HOST_DEVICE Complex<T> pow( const Complex<T>& complex, const Complex<T>& exponent )
{
	return complex.pow( exponent );
}



template<typename T>
CUDA_HOST_DEVICE Complex<T> pow( const T& base, const Complex<T>& exponent )
{
	return pow( Complex<T>( base ), exponent );
}



template<typename T>
CUDA_HOST_DEVICE Complex<T> sin( const Complex<T>& complex )
{
	return complex.sin();
}



template<typename T>
CUDA_HOST_DEVICE Complex<T> cos( const Complex<T>& complex )
{
	return complex.cos();
}



template<typename T>
CUDA_HOST_DEVICE Complex<T> sqrt( const Complex<T>& complex )
{
	return complex.sqrt();
}



template<typename T>
CUDA_HOST std::ostream& operator<<( std::ostream& stream, const Complex<T>& complex )
{
	stream << "(" << complex.real() << "," << complex.imag() << ")";
	return stream;
}



} // namespace cuMath
} // namespace finael

#endif // FINAEL_CUMATH_COMPLEX_H_
