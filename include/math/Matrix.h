/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUMATH_MATRIX_H_
#define FINAEL_CUMATH_MATRIX_H_

#include "../common/cudaHostDevice.h"
#include "../tools/Array.h"
#include "Vector.h"



namespace finael {
namespace cuMath {



template<unsigned int numberRows, unsigned int numberColumns, typename T>
class Matrix
{
public:
	static constexpr unsigned int ROWS = numberRows;
	static constexpr unsigned int COLUMNS = numberColumns;
	using value_type = T;



	CUDA_HOST_DEVICE Matrix()
		: data_()
	{}



	explicit CUDA_HOST_DEVICE Matrix( const T& value )
	{
		set( value );
	}



	explicit CUDA_HOST_DEVICE Matrix( const cuTool::Array<cuTool::Array<T, COLUMNS>, ROWS>& data_in )
	{
		set( data_in );
	}



	CUDA_HOST_DEVICE void set( const T& value )
	{
		data_.set( Vector<COLUMNS, T>( value ) );
	}



	CUDA_HOST_DEVICE void set( const unsigned int indexRow, const unsigned int indexColumn, const T& value )
	{
		data_[indexRow].set( indexColumn, value );
	}



	CUDA_HOST_DEVICE void setAt( const unsigned int indexRow, const unsigned int indexColumn, const T& value )
	{
		data_.at( indexRow ).setAt( indexColumn, value );
	}



	CUDA_HOST_DEVICE void set( const cuTool::Array<cuTool::Array<T, COLUMNS>, ROWS>& data_in )
	{
		for (unsigned int row = 0; row < ROWS; ++row)
		{
			data_[row].set( data_in[row] );
		}
	}



	CUDA_HOST_DEVICE Vector<COLUMNS,T>& operator[]( const unsigned int indexRow )
	{
		return data_[indexRow];
	}



	CUDA_HOST_DEVICE Vector<COLUMNS, T>& at( const unsigned int indexRow )
	{
		return data_.at( indexRow );
	}



	CUDA_HOST_DEVICE const Vector<COLUMNS,T>& operator[]( const unsigned int indexRow ) const
	{
		return data_[indexRow];
	}



	CUDA_HOST_DEVICE const Vector<COLUMNS,T>& at( const unsigned int indexRow ) const
	{
		return data_.at( indexRow );
	}



	CUDA_HOST_DEVICE Matrix& operator+=( const Matrix& rhs )
	{
		for (unsigned int row = 0; row < ROWS; ++row) data_[row] += rhs.data_[row];
		return *this;
	}



	CUDA_HOST_DEVICE Matrix& operator-=( const Matrix& rhs )
	{
		for (unsigned int row = 0; row < ROWS; ++row) data_[row] -= rhs.data_[row];
		return *this;
	}



	CUDA_HOST_DEVICE Matrix& operator*=( const T& rhs )
	{
		for (unsigned int row = 0; row < ROWS; ++row) data_[row] *= rhs;
		return *this;
	}



	CUDA_HOST_DEVICE friend Matrix operator*( Matrix lhs, const T& rhs )
	{
		return lhs *= rhs;
	}



	CUDA_HOST_DEVICE friend Matrix operator*( const T& lhs, Matrix rhs )
	{
		return rhs *= lhs;
	}



	template<unsigned int sc, unsigned int sr, typename U>
	CUDA_HOST_DEVICE friend Matrix<sc, sr, U>& operator*=( Matrix<sc, sr, U>& matrix, const typename U::value_type& rhs );



	CUDA_HOST_DEVICE Matrix& operator/=( const T& rhs )
	{
		for (unsigned int row = 0; row < ROWS; ++row) data_[row] /= rhs;
		return *this;
	}



	CUDA_HOST_DEVICE friend Matrix operator/( Matrix lhs, const T& rhs )
	{
		return lhs /= rhs;
	}



	template<unsigned int sc, unsigned int sr, typename U>
	CUDA_HOST_DEVICE friend Matrix<sc, sr, U>& operator/=( Matrix<sc, sr, U>& matrix, const typename U::value_type& rhs );



	CUDA_HOST_DEVICE Matrix& operator*=( const Matrix& rhs )
	{
		*this = *this * rhs;
		return *this;
	}



	CUDA_HOST_DEVICE bool operator==( const Matrix& rhs ) const
	{
		for (unsigned int row = 0; row < ROWS; ++row) if (data_[row] != rhs.data_[row]) return false;
		return true;
	}



	CUDA_HOST_DEVICE bool operator!=( const Matrix& rhs ) const
	{
		return !operator==( rhs );
	}



private:
	cuTool::Array<Vector<COLUMNS, T>, ROWS> data_;
};





template<unsigned int numberRows, unsigned int numberColumns, typename T>
constexpr unsigned int Matrix<numberRows, numberColumns, T>::ROWS;

template<unsigned int numberRows, unsigned int numberColumns, typename T>
constexpr unsigned int Matrix<numberRows, numberColumns, T>::COLUMNS;





template<unsigned int numberRows, unsigned int numberColumns, typename T>
CUDA_HOST_DEVICE Matrix<numberRows, numberColumns, T>& operator*=( Matrix<numberRows, numberColumns, T>& matrix, const typename T::value_type& rhs )
{
	for (unsigned int row = 0; row < numberRows; ++row) matrix.data_[row] *= rhs;
	return matrix;
}



template<unsigned int numberRows, unsigned int numberColumns, typename T>
CUDA_HOST_DEVICE Matrix<numberRows, numberColumns, T>& operator/=( Matrix<numberRows, numberColumns, T>& matrix, const typename T::value_type& rhs )
{
	for (unsigned int row = 0; row < numberRows; ++row) matrix.data_[row] /= rhs;
	return matrix;
}



template<unsigned int numberRows, unsigned int numberColumns, typename T>
CUDA_HOST_DEVICE Matrix<numberRows, numberColumns, T> operator+( Matrix<numberRows, numberColumns, T> lhs, const Matrix<numberRows, numberColumns, T>& rhs )
{
	return lhs += rhs;
}



template<unsigned int numberRows, unsigned int numberColumns, typename T>
CUDA_HOST_DEVICE Matrix<numberRows, numberColumns, T> operator-( Matrix<numberRows, numberColumns, T> lhs, const Matrix<numberRows, numberColumns, T>& rhs )
{
	return lhs -= rhs;
}



template<unsigned int numberRows, unsigned int numberColumns, typename T>
CUDA_HOST_DEVICE Matrix<numberRows, numberColumns, T> operator*( Matrix<numberRows, numberColumns, T> lhs, const T& rhs );



template<unsigned int numberRows, unsigned int numberColumns, typename T>
CUDA_HOST_DEVICE Matrix<numberRows, numberColumns, T> operator*( const T& lhs, Matrix<numberRows, numberColumns, T> rhs );



template<unsigned int numberRows, unsigned int numberColumns, typename T>
CUDA_HOST_DEVICE Matrix<numberRows, numberColumns, T> operator*( Matrix<numberRows, numberColumns, T> lhs, const typename T::value_type& rhs )
{
	return lhs *= rhs;
}



template<unsigned int numberRows, unsigned int numberColumns, typename T>
CUDA_HOST_DEVICE Matrix<numberRows, numberColumns, T> operator*( const typename T::value_type& lhs, Matrix<numberRows, numberColumns, T> rhs )
{
	return rhs *= lhs;
}



template<unsigned int numberRows, unsigned int numberColumns, typename T>
CUDA_HOST_DEVICE Matrix<numberRows, numberColumns, T> operator/( Matrix<numberRows, numberColumns, T> lhs, const T& rhs );



template<unsigned int numberRows, unsigned int numberColumns, typename T>
CUDA_HOST_DEVICE Matrix<numberRows, numberColumns, T> operator/( Matrix<numberRows, numberColumns, T> lhs, const typename T::value_type& rhs )
{
	return lhs /= rhs;
}



template<unsigned int numberRows, unsigned int sizeCommon, unsigned int numberColumns, typename T>
CUDA_HOST_DEVICE Matrix<numberRows, numberColumns, T> operator*( const Matrix<numberRows, sizeCommon, T>& lhs, const Matrix<sizeCommon, numberColumns, T>& rhs )
{
	// Efficiency improvement: For large Matrices one should tranpose one to improve the memory design. (Suggestion: Use the temporary Matrix we need anyway)
	Matrix<numberRows, numberColumns, T> result;
	for (unsigned int row = 0; row < numberRows; ++row)
	{
		for (unsigned int common = 0; common < sizeCommon; ++common)
		{
			for (unsigned int column = 0; column < numberColumns; ++column)
			{
				result[row][column] += lhs[row][common]*rhs[common][column];
			}
		}
	}
	return result;
}



template<unsigned int numberRows, unsigned int numberColumns, typename T>
CUDA_HOST_DEVICE Vector<numberRows, T> operator*( const Matrix<numberRows, numberColumns, T>& matrix, const Vector<numberColumns,T>& vector )
{
	Vector<numberRows, T> result;
	for (unsigned int row = 0; row < numberRows; ++row)
	{
		for (unsigned int column = 0; column < numberColumns; ++column)
		{
			result[row] += matrix[row][column]*vector[column];
		}
	}
	return result;
}



template<unsigned int numberRows, unsigned int numberColumns, typename T>
CUDA_HOST std::ostream& operator<<( std::ostream& stream, const Matrix<numberRows, numberColumns, T>& matrix )
{
	for (unsigned int row = 0; row < numberRows ; ++row)
	{
		for (unsigned int column = 0; column < numberColumns; ++column)
		{
			stream << matrix[row][column];
			if (column != numberColumns - 1) stream << ",\t";
		}
		if (row != numberRows - 1) stream << "\n";
	}
	return stream;
}



} // namespace cuMath
} // namespace finael

#endif // FINAEL_CUMATH_MATRIX_H_
