/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_FUNCTION_SUMS_H_
#define FINAEL_CUVEGAS_FUNCTION_SUMS_H_

#include "../../common/cudaHostDevice.h"


namespace finael {
namespace cuVegas {



template<typename T>
struct FunctionSums
{
	unsigned int cardinality;
	T sum;
	T squaredSum;



	CUDA_HOST_DEVICE FunctionSums( T value )
		: cardinality( 1u )
		, sum( value )
		, squaredSum( value*value )
	{}



	CUDA_HOST_DEVICE FunctionSums()
		: cardinality( 0u )
		, sum( 0 )
		, squaredSum( 0 )
	{}



	CUDA_HOST_DEVICE FunctionSums& operator+=( const T summand )
	{
		sum += summand;
		squaredSum += summand * summand;
		++cardinality;
		return *this;
	}



	CUDA_HOST_DEVICE FunctionSums& operator+=( const FunctionSums& other )
	{
		sum += other.sum;
		squaredSum += other.squaredSum;
		cardinality += other.cardinality;
		return *this;
	}
};



} // namespace cuVegas
} // namespace finael



#endif // FINAEL_CUVEGAS_FUNCTION_SUMS_H_
