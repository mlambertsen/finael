/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_QCD_PDFINTERPOLATION_MMHT2014_DEFAULT_GRID_H_
#define FINAEL_QCD_PDFINTERPOLATION_MMHT2014_DEFAULT_GRID_H_

#include <array>



namespace finael {
namespace qcd {
namespace pdfInterpolation {



template<typename T>
class Mmht2014DefaultGrid
{
public:
	static constexpr std::size_t SIZE = 64;
	static constexpr std::array<T,SIZE> POINTS = {{1E-6, 2E-6, 4E-6, 6E-6, 8E-6, 1E-5, 2E-5, 4E-5, 6E-5, 8E-5, 1E-4, 2E-4, 4E-4, 6E-4, 8E-4, 1E-3, 2E-3, 4E-3, 6E-3, 8E-3, 1E-2, 1.4E-2, 2E-2, 3E-2, 4E-2, 6E-2, 8E-2, .1E0, .125E0, .15E0, .175E0, .2E0, .225E0, .25E0, .275E0, .3E0, .325E0, .35E0, .375E0, .4E0, .425E0, .45E0, .475E0, .5E0, .525E0, .55E0, .575E0, .6E0, .625E0, .65E0, .675E0, .7E0, .725E0, .75E0, .775E0, .8E0, .825E0, .85E0, .875E0, .9E0, .925E0, .95E0, .975E0, 1E0}};
};

template<typename T>
constexpr std::size_t Mmht2014DefaultGrid<T>::SIZE;

template<typename T>
constexpr std::array<T, Mmht2014DefaultGrid<T>::SIZE> Mmht2014DefaultGrid<T>::POINTS;



} // namespace pdfInterpolation
} // namespace qcd
} // namespace finael



#endif // FINAEL_QCD_PDFINTERPOLATION_MMHT2014_DEFAULT_GRID_H_
