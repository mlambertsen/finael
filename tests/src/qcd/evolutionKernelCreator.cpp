/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#define BOOST_TEST_MODULE "finael Evolution Kernel Creator"
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "../../../include/qcd/EvolutionKernelCreator.h"
#include "../../../include/math/Complex.h"


BOOST_AUTO_TEST_SUITE( aEvolutionKernelCreator )



BOOST_AUTO_TEST_CASE( calculatesLoKernel )
{
	using namespace finael::cuQcd;
	using namespace finael::cuMath;

	EvolutionKernelCreator<3, PQcdOrder::LO, double> kernelCreator;
	double oldAlphaS = 3.0;
	double newAlphaS = 5.0;
	Complex<double> mellin( -2.0, 3.0 );

	auto result = kernelCreator.getEvolutionKernels( oldAlphaS, newAlphaS, mellin );

	BOOST_CHECK_CLOSE( result.nonSingletValenceKernel.real(), 1.1333997275022833, 1.e-13 );
	BOOST_CHECK_CLOSE( result.nonSingletValenceKernel.imag(), 0.78590320673442426, 1.e-13 );

	BOOST_CHECK_EQUAL( result.nonSingletValenceKernel, result.nonSingletNonValencePlusKernel );
	BOOST_CHECK_EQUAL( result.nonSingletValenceKernel, result.nonSingletNonValenceMinusKernel );

	BOOST_CHECK_CLOSE( result.singletKernel[0][0].real(), 1.1309626890346343, 1.e-13 );
	BOOST_CHECK_CLOSE( result.singletKernel[0][0].imag(), 0.78491370961673645, 1.e-13 );

	BOOST_CHECK_CLOSE( result.singletKernel[0][1].real(), -0.16005500310160081, 1.e-13 );
	BOOST_CHECK_CLOSE( result.singletKernel[0][1].imag(), 7.2477049843005181e-2, 1.e-12 );

	BOOST_CHECK_CLOSE( result.singletKernel[1][0].real(), -1.9461767390799033e-2, 1.e-13 );
	BOOST_CHECK_CLOSE( result.singletKernel[1][0].imag(), 5.1673789543245752e-2, 1.e-13 );

	BOOST_CHECK_CLOSE( result.singletKernel[1][1].real(), 0.53706552269647345, 1.e-12 );
	BOOST_CHECK_CLOSE( result.singletKernel[1][1].imag(), 2.1162148645874193, 1.e-13 );
}



BOOST_AUTO_TEST_CASE( calculatesNLoKernel )
{
	using namespace finael::cuQcd;
	using namespace finael::cuMath;

	EvolutionKernelCreator<3, PQcdOrder::NLO, double> kernelCreator;
	double oldAlphaS = 3.0;
	double newAlphaS = 5.0;
	Complex<double> mellin( -2.0, 3.0 );

	auto result = kernelCreator.getEvolutionKernels( oldAlphaS, newAlphaS, mellin );

	BOOST_CHECK_CLOSE( result.nonSingletValenceKernel.real(), 1.4208293784163353, 1.e-13 );
	BOOST_CHECK_CLOSE( result.nonSingletValenceKernel.imag(), 0.69759354339716784, 1.e-13 );

	BOOST_CHECK_CLOSE( result.nonSingletNonValencePlusKernel.real(), 1.4219933494115045, 1.e-13 );
	BOOST_CHECK_CLOSE( result.nonSingletNonValencePlusKernel.imag(), 0.69916373478860461, 1.e-13 );

	BOOST_CHECK_EQUAL( result.nonSingletValenceKernel, result.nonSingletNonValenceMinusKernel );

	BOOST_CHECK_CLOSE( result.singletKernel[0][0].real(), 1.4163060938208663, 1.e-13 );
	BOOST_CHECK_CLOSE( result.singletKernel[0][0].imag(), 0.74524395231721319, 1.e-13 );

	BOOST_CHECK_CLOSE( result.singletKernel[0][1].real(), 0.95593978677569003, 1.e-13 );
	BOOST_CHECK_CLOSE( result.singletKernel[0][1].imag(), 1.2210743964279576, 1.e-13 );

	BOOST_CHECK_CLOSE( result.singletKernel[1][0].real(), 0.11549144764440947, 1.e-13 );
	BOOST_CHECK_CLOSE( result.singletKernel[1][0].imag(), 0.16077307694564411, 1.e-13 );

	BOOST_CHECK_CLOSE( result.singletKernel[1][1].real(), 1.9479064307367084, 1.e-13 );
	BOOST_CHECK_CLOSE( result.singletKernel[1][1].imag(), 0.60864980770475596, 1.e-12 );

}



BOOST_AUTO_TEST_SUITE_END()
