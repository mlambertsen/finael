/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_BASIC_DEFINITIONS_H_
#define FINAEL_BASIC_DEFINITIONS_H_


namespace finael {



constexpr static unsigned int WARPSIZE = 32; // same variable is built-in variable warpSize in CUDA framework, defined here to use globally the same variable, in __device__ and in __host__ code (which simulates the __device__ code)



} // namespace finael

#endif // FINAEL_BASIC_DEFINITIONS_H_
