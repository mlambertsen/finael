/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_QCD_PDFINTERPOLATION_FORTRAN_DSS_FORTRAN_H_
#define FINAEL_QCD_PDFINTERPOLATION_FORTRAN_DSS_FORTRAN_H_



namespace finael {
namespace qcd {
namespace pdfInterpolation {
namespace fortran {



extern "C"
{
	void fdss_( int *is, int* ih, int* ic, int* io, double* x, double* q2, double* u, double* ub, double* d, double* db, double* s, double* sb, double* c, double* b, double* gl );



	extern struct
	{
		int fini;
	} fragini_;
}



} // namespace fortran
} // namespace pdfInterpolation
} // namespace qcd
} // namespace finael



#endif // FINAEL_QCD_PDFINTERPOLATION_FORTRAN_DSS_FORTRAN_H_
