/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUQCD_ANOMALOUS_DIMENSION_H_
#define FINAEL_CUQCD_ANOMALOUS_DIMENSION_H_

#include <utility>
#include "../common/cudaHostDevice.h"
#include "../math/floratosFunctions.h"
#include "../math/generalFunctions.h"
#include "../math/Vector.h"
#include "../math/Matrix.h"

#include "constants.h"
#include "definitions.h"


namespace finael {
namespace cuQcd {



template<unsigned short numberFlavours, PQcdOrder order, typename T>
class AnomalousDimension
{
public:
	static_assert(sizeof(T) == 0, "ERROR in AnomalousDimension: Requested order is not implemented!");
};





template<unsigned short numberFlavours, typename T>
class AnomalousDimension<numberFlavours, PQcdOrder::LO, T>
{
public:
	static constexpr unsigned short NUMBERFLAVOURS = numberFlavours;
	static constexpr unsigned short QCDORDER = 0;
	using CONST = CONSTANTS<T>;



	template<template<typename> class Complex>
	CUDA_HOST_DEVICE cuMath::Matrix<2, 2, Complex<T>> getDglapRelevant( const Complex<T>& arg ) const
	{
		Complex<T> sFloratos1( cuMath::sFloratos<1>( arg ) );
		cuMath::Matrix<2, 2, Complex<T>> gamma0Matrix;

		gamma0Matrix[0][0] = calculateNS( arg, sFloratos1 );
		gamma0Matrix[0][1] = getQG( arg );
		gamma0Matrix[1][0] = getGQ( arg );
		gamma0Matrix[1][1] = calculateGG( arg, sFloratos1 );
		return gamma0Matrix;
	}



	template<template<typename> class Complex>
	CUDA_HOST_DEVICE Complex<T> getQQ( const Complex<T>& arg ) const
	{
		return getNS<cuMath::Sign::PLUS>( arg );
	}



	template<template<typename> class Complex>
	CUDA_HOST_DEVICE Complex<T> getQG( const Complex<T>& arg ) const
	{
		return 4 * CONST::TR * NUMBERFLAVOURS * (arg * (arg + 1 ) + 2) / (arg * (arg + 1) * (arg + 2));
	}



	template<template<typename> class Complex>
	CUDA_HOST_DEVICE Complex<T> getGQ( const Complex<T>& arg ) const
	{
		return 2 * CONST::CF * (arg * (arg + 1) + 2) / (arg * (arg - 1) * (arg + 1));
	}



	template<template<typename> class Complex>
	CUDA_HOST_DEVICE Complex<T> getGG( const Complex<T>& arg ) const
	{
		return calculateGG( arg, cuMath::sFloratos<1>( arg ) );
	}



	template<template<typename> class Complex>
	CUDA_HOST_DEVICE Complex<T> getPS( const Complex<T>& arg ) const
	{
		return Complex<T>( 0 );
	}



	template<cuMath::Sign sign, template<typename> class Complex>
	CUDA_HOST_DEVICE Complex<T> getNS( const Complex<T>& arg ) const
	{
		return calculateNS( arg, cuMath::sFloratos<1>( arg ) );
	}



	template<template<typename> class Complex>
	CUDA_HOST_DEVICE Complex<T> getNSSea( const Complex<T>& arg ) const
	{
		return Complex<T>( 0 );
	}



private:



	template<template<typename> class Complex>
	CUDA_HOST_DEVICE Complex<T> calculateGG( const Complex<T>& arg, const Complex<T>& sFloratos1 ) const
	{
		return ( 11 * CONST::CA - 4 * CONST::TR * NUMBERFLAVOURS) / 3
			+ 4 * CONST::CA * ((2 * arg * (arg + 1) + 2) / (arg * (arg - 1) * (arg + 1) * (arg + 2)) - sFloratos1);
	}



	template<template<typename> class Complex>
	CUDA_HOST_DEVICE Complex<T> calculateNS( const Complex<T>& arg, const Complex<T>& sFloratos1 ) const
	{
		return CONST::CF * (3 + 2 / (arg * (arg + 1)) - 4 * sFloratos1);
	}
};





template<unsigned short numberFlavours, typename T>
constexpr unsigned short AnomalousDimension<numberFlavours, PQcdOrder::LO, T>::NUMBERFLAVOURS;

template<unsigned short numberFlavours, typename T>
constexpr unsigned short AnomalousDimension<numberFlavours, PQcdOrder::LO, T>::QCDORDER;





template<unsigned short numberFlavours, typename T>
class AnomalousDimension<numberFlavours, PQcdOrder::NLO, T>
{
public:
	static constexpr unsigned short NUMBERFLAVOURS = numberFlavours;
	static constexpr unsigned short QCDORDER = 1;
	using CONST = CONSTANTS<T>;



	template<template<typename> class Complex>
	class NloDglapRelevant
	{
	public:
		cuMath::Matrix<2, 2, Complex<T>> gamma1Matrix;
		Complex<T> nonSingletPlus;
		Complex<T> nonSingletMinus;
	};



	template<template<typename> class Complex>
	CUDA_HOST_DEVICE NloDglapRelevant<Complex> getDglapRelevant( const Complex<T>& arg ) const
	{
		Complex<T> sFloratos1( cuMath::sFloratos<1>( arg ) );
		Complex<T> sFloratos2( cuMath::sFloratos<2>( arg ) );
		Complex<T> sPrimeFloratos2Plus( cuMath::sPrimeFloratos<2,cuMath::Sign::PLUS>( 0.5 * arg ) );
		Complex<T> sPrimeFloratos3Plus( cuMath::sPrimeFloratos<3,cuMath::Sign::PLUS>( 0.5 * arg ) );
		Complex<T> sTildeFloratosPlus( cuMath::sTildeFloratos<cuMath::Sign::PLUS>( arg ) );

		Complex<T> nSPlus( calculateNS<cuMath::Sign::PLUS>( arg, sFloratos1, sFloratos2, sPrimeFloratos2Plus, sPrimeFloratos3Plus, sTildeFloratosPlus ) );

		NloDglapRelevant<Complex> result;
		result.gamma1Matrix[0][0] = nSPlus + getPS( arg );
		result.gamma1Matrix[0][1] = calculateQG( arg, sFloratos1, sFloratos2, sPrimeFloratos2Plus );
		result.gamma1Matrix[1][0] = calculateGQ( arg, sFloratos1, sFloratos2, sPrimeFloratos2Plus );
		result.gamma1Matrix[1][1] = calculateGG( arg, sFloratos1, sPrimeFloratos2Plus, sPrimeFloratos3Plus, sTildeFloratosPlus );

		result.nonSingletPlus = nSPlus;
		result.nonSingletMinus = calculateNS<cuMath::Sign::MINUS>( arg, sFloratos1, sFloratos2, cuMath::sPrimeFloratos<2,cuMath::Sign::MINUS>( 0.5 * arg ), cuMath::sPrimeFloratos<3,cuMath::Sign::MINUS>( 0.5 * arg ), cuMath::sTildeFloratos<cuMath::Sign::MINUS>( arg ) );

		return result;
	}



	template<template<typename> class Complex>
	CUDA_HOST_DEVICE Complex<T> getQQ( const Complex<T>& arg ) const
	{
		return getNS<cuMath::Sign::PLUS>( arg ) + getPS( arg );
	}



	template<template<typename> class Complex>
	CUDA_HOST_DEVICE Complex<T> getQG( const Complex<T>& arg ) const
	{
		return calculateQG( arg, cuMath::sFloratos<1>( arg ), cuMath::sFloratos<2>( arg ), cuMath::sPrimeFloratos<2,cuMath::Sign::PLUS>( 0.5 * arg ) );
	}



	template<template<typename> class Complex>
	CUDA_HOST_DEVICE Complex<T> getGQ( const Complex<T>& arg ) const
	{
		return calculateGQ( arg, cuMath::sFloratos<1>( arg ), cuMath::sFloratos<2>( arg ), cuMath::sPrimeFloratos<2, cuMath::Sign::PLUS>( 0.5 * arg ) );
	}



	template<template<typename> class Complex>
	CUDA_HOST_DEVICE Complex<T> getGG( const Complex<T>& arg ) const
	{
		return calculateGG( arg, cuMath::sFloratos<1>( arg ), cuMath::sPrimeFloratos<2, cuMath::Sign::PLUS>( 0.5 * arg ), cuMath::sPrimeFloratos<3, cuMath::Sign::PLUS>( 0.5 * arg ), cuMath::sTildeFloratos<cuMath::Sign::PLUS>( arg ) );
	}



	template<template<typename> class Complex>
	CUDA_HOST_DEVICE Complex<T> getPS( const Complex<T>& arg ) const
	{
		return 8 * CONST::CF * CONST::TR * NUMBERFLAVOURS * ( 5 * CPow<5, Complex>::pow( arg ) + 32 * CPow<4, Complex>::pow( arg ) + 49 * CPow<3, Complex>::pow( arg ) + 38 * CPow<2, Complex>::pow( arg ) + 28 * arg + 8) / ((arg - 1) * CPow<3, Complex>::pow( arg * (arg + 1) ) * CPow<2, Complex>::pow( arg + 2 ));
	}



	template<cuMath::Sign sign, template<typename> class Complex>
	CUDA_HOST_DEVICE Complex<T> getNS( const Complex<T>& arg ) const
	{
		return calculateNS<sign>( arg, cuMath::sFloratos<1>( arg ), cuMath::sFloratos<2>( arg ), cuMath::sPrimeFloratos<2, sign>( 0.5 * arg ), cuMath::sPrimeFloratos<3, sign>( 0.5 * arg ), cuMath::sTildeFloratos<sign>( arg ) );
	}



	template<template<typename> class Complex>
	CUDA_HOST_DEVICE Complex<T> getNSSea( const Complex<T>& arg ) const
	{
		return Complex<T>( 0 );
	}



private:
	template<unsigned int n, template<typename> class Complex>
	using CPow = cuMath::Pow<Complex<T>, n>;



	template<template<typename> class Complex>
	CUDA_HOST_DEVICE Complex<T> calculateQG( const Complex<T>& arg, const Complex<T>& sFloratos1, const Complex<T>& sFloratos2, const Complex<T>& sPrimeFloratos2Plus ) const
	{
		return 4 * CONST::CF * CONST::TR * NUMBERFLAVOURS * ((2 * (sFloratos1 * sFloratos1 - sFloratos2) + 5) * (arg * (arg + 1) + 2) / (arg * (arg + 1) * (arg + 2)) - 4 * sFloratos1 / CPow<2, Complex>::pow( arg ) + (11 * CPow<4, Complex>::pow( arg ) + 26 * CPow<3, Complex>::pow( arg ) + 15 * CPow<2, Complex>::pow( arg ) + 8 * arg + 4) / (CPow<3, Complex>::pow( arg * (arg + 1) ) * (arg + 2)))
		+ 4 * CONST::CA * CONST::TR * NUMBERFLAVOURS * (2 * (sFloratos2 - sFloratos1 * sFloratos1 - sPrimeFloratos2Plus) * (arg * (arg + 1) + 2) / (arg * (arg + 1) * (arg + 2)) + 8 * sFloratos1 * (2 * arg + 3) / (CPow<2, Complex>::pow( (arg + 1) * (arg + 2) )) + 2 * (CPow<9, Complex>::pow( arg ) + 6 * CPow<8, Complex>::pow( arg ) + 15 * CPow<7, Complex>::pow( arg ) + 25 * CPow<6, Complex>::pow( arg ) + 36 * CPow<5, Complex>::pow( arg ) + 85 * CPow<4, Complex>::pow( arg ) + 128 * CPow<3, Complex>::pow( arg ) + 104 * CPow<2, Complex>::pow( arg ) + 64 * arg + 16) / ((arg - 1) * CPow<3, Complex>::pow( arg * (arg + 1) * (arg + 2) )));
	}



	template<template<typename> class Complex>
	CUDA_HOST_DEVICE Complex<T> calculateGQ( const Complex<T>& arg, const Complex<T>& sFloratos1, const Complex<T>& sFloratos2, const Complex<T>& sPrimeFloratos2Plus ) const
	{
		return 2 * CONST::CF * CONST::CF * (2 * (5 * sFloratos1 - sFloratos2 - sFloratos1 * sFloratos1 ) * (arg * (arg + 1) + 2) / ((arg - 1) * arg * (arg + 1)) - 4 * sFloratos1 / CPow<2, Complex>::pow( arg + 1 ) - (12 * CPow<6, Complex>::pow( arg ) + 30 * CPow<5, Complex>::pow( arg ) + 43 * CPow<4, Complex>::pow( arg ) + 28 * CPow<3, Complex>::pow( arg ) - CPow<2, Complex>::pow( arg ) - 12 * arg - 4) / ((arg - 1) * CPow<3, Complex>::pow( arg * (arg + 1) )))
		+ 4 * CONST::CF * CONST::CA * ((sFloratos1 * sFloratos1 + sFloratos2 - sPrimeFloratos2Plus) * (arg * (arg + 1) + 2) / ((arg - 1) * arg * (arg + 1)) - sFloratos1 * (17 * CPow<4, Complex>::pow( arg ) + 41 * CPow<2, Complex>::pow( arg ) - 22 * arg - 12) / (3 * CPow<2, Complex>::pow( arg * (arg - 1) ) * (arg + 1)) + (109 * CPow<9, Complex>::pow( arg ) + 621 * CPow<8, Complex>::pow( arg ) + 1400 * CPow<7, Complex>::pow( arg ) + 1678 * CPow<6, Complex>::pow( arg ) + 695 * CPow<5, Complex>::pow( arg ) - 1031 * CPow<4, Complex>::pow( arg ) - 1304 * CPow<3, Complex>::pow( arg ) - 152 * CPow<2, Complex>::pow( arg ) + 432 * arg + 144) / (9 * CPow<3, Complex>::pow( arg * (arg + 1) ) * CPow<2, Complex>::pow( (arg - 1) * (arg + 2) )))
		+ 16.0 / 3.0 * CONST::CF * CONST::TR * NUMBERFLAVOURS * (1 / CPow<2, Complex>::pow( arg + 1 ) + (sFloratos1 - 8.0 / 3.0) * (arg * (arg + 1) + 2)/((arg - 1) * arg * (arg + 1)));
	}



	template<template<typename> class Complex>
	CUDA_HOST_DEVICE Complex<T> calculateGG( const Complex<T>& arg, const Complex<T>& sFloratos1, const Complex<T>& sPrimeFloratos2Plus, const Complex<T>& sPrimeFloratos3Plus, const Complex<T>& sTildeFloratosPlus ) const
	{
		return 16 * CONST::CA * CONST::TR * NUMBERFLAVOURS / 3 * (5.0 / 3.0 * sFloratos1 - 1 - (19 * CPow<4, Complex>::pow( arg ) + 38 * CPow<3, Complex>::pow( arg ) + 47 * CPow<2, Complex>::pow( arg ) + 28 * arg + 6) / (3 * (arg - 1) * CPow<2, Complex>::pow( arg * (arg + 1) ) * (arg + 2)))
		- 4 * CONST::CF * CONST::TR * NUMBERFLAVOURS * (1 + 2 * (2 * CPow<6, Complex>::pow( arg ) + 4 * CPow<5, Complex>::pow( arg ) + CPow<4, Complex>::pow( arg ) - 10 * CPow<3, Complex>::pow( arg ) - 5 * CPow<2, Complex>::pow( arg ) - 4 * arg - 4) / ((arg - 1) * CPow<3, Complex>::pow( arg * (arg + 1) ) * (arg + 2)))
		- 2 * CONST::CA * CONST::CA * (134.0 / 9.0 * sFloratos1 + 16 * sFloratos1 * (2 * CPow<5, Complex>::pow( arg ) + 5 * CPow<4, Complex>::pow( arg ) + 8 * CPow<3, Complex>::pow( arg ) + 7 * CPow<2, Complex>::pow( arg ) - 2 * arg - 2) / (CPow<2, Complex>::pow( (arg - 1) * arg * (arg + 1) * (arg + 2) )) - 16.0 / 3.0 + 4 * sPrimeFloratos2Plus * (2 * (arg * (arg + 1) + 1) / ((arg - 1) * arg * (arg + 1) * (arg + 2)) - sFloratos1) + 8 * sTildeFloratosPlus - sPrimeFloratos3Plus - (457 * CPow<9, Complex>::pow( arg ) + 2742 * CPow<8, Complex>::pow( arg ) + 6040 * CPow<7, Complex>::pow( arg ) + 6098 * CPow<6, Complex>::pow( arg ) + 1567 * CPow<5, Complex>::pow( arg ) - 2344 * CPow<4, Complex>::pow( arg ) - 1632 * CPow<3, Complex>::pow( arg ) + 560 * CPow<2, Complex>::pow( arg ) + 1488 * arg + 576) / (9 * CPow<2, Complex>::pow( arg - 1 ) * CPow<3, Complex>::pow( arg * (arg + 1) * (arg + 2) )));
	}



	template<cuMath::Sign sign, template<typename> class Complex>
	CUDA_HOST_DEVICE Complex<T> calculateNS( const Complex<T>& arg, const Complex<T>& sFloratos1, const Complex<T>& sFloratos2, const Complex<T>& sPrimeFloratos2, const Complex<T>& sPrimeFloratos3, const Complex<T>& sTildeFloratos ) const
	{
		return 4 * CONST::CF * CONST::CF * (3.0 / 8.0 - 2 * sFloratos1 * (2 * arg + 1) / CPow<2, Complex>::pow( arg * (arg + 1) ) - 2 * (2 * sFloratos1 - 1 / (arg * (arg + 1))) * (sFloratos2 - sPrimeFloratos2) - 3 * sFloratos2 - 8 * sTildeFloratos + sPrimeFloratos3 + (3 * CPow<3, Complex>::pow( arg ) + ((sign == cuMath::Sign::PLUS) ? (5 * CPow<2, Complex>::pow( arg ) + 4 * arg + 1) : (-3 * CPow<2, Complex>::pow( arg ) - 4 * arg - 3))) / CPow<3, Complex>::pow( arg * (arg + 1) ))
		+ 2 * CONST::CF * CONST::CA * (17.0 / 12.0 - 134.0 / 9.0 * sFloratos1 + 2 * (2 * sFloratos1 - 1 / (arg * (arg + 1))) * (2 * sFloratos2 - sPrimeFloratos2) + 22.0 / 3.0 * sFloratos2 + 8 * sTildeFloratos - sPrimeFloratos3 + (151 * CPow<4, Complex>::pow( arg ) + 236 * CPow<3, Complex>::pow( arg ) + ((sign == cuMath::Sign::PLUS) ? (52 * CPow<2, Complex>::pow( arg ) - 33 * arg) : (124 * CPow<2, Complex>::pow( arg ) + 39 * arg + 36))) / (9 * CPow<3, Complex>::pow( arg * (arg + 1) )))
		+ 2.0 / 3.0 * CONST::CF * CONST::TR * NUMBERFLAVOURS * (40.0 / 3.0 * sFloratos1 - 8 * sFloratos2 - 1 - 4.0 / 3.0 * (11 * CPow<2, Complex>::pow( arg ) + 5 * arg - 3) / CPow<2, Complex>::pow( arg * (arg + 1)));
	}
};





template<unsigned short numberFlavours, typename T>
constexpr unsigned short AnomalousDimension<numberFlavours, PQcdOrder::NLO, T>::NUMBERFLAVOURS;

template<unsigned short numberFlavours, typename T>
constexpr unsigned short AnomalousDimension<numberFlavours, PQcdOrder::NLO, T>::QCDORDER;



} // namespace cuQcd
} // namespace finael


#endif // FINAEL_CUQCD_ANOMALOUS_DIMENSION_H_
