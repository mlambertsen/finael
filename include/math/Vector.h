/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUMATH_VECTOR_H_
#define FINAEL_CUMATH_VECTOR_H_

#include "../common/cudaHostDevice.h"
#include "../tools/Array.h"



namespace finael {
namespace cuMath {



template<unsigned int size, typename T>
class Vector
{
public:
	static constexpr unsigned int SIZE = size;
	using value_type = T;



	CUDA_HOST_DEVICE Vector()
		: data_()
	{}



	explicit CUDA_HOST_DEVICE Vector( const T& value )
	{
		set( value );
	}



	CUDA_HOST_DEVICE Vector( const cuTool::Array<T, size>& data_in )
	{
		set( data_in );
	}



	CUDA_HOST_DEVICE void set( const unsigned int index, const T& value )
	{
		data_.set( index, value );
	}



	CUDA_HOST_DEVICE void setAt( const unsigned int index, const T& value )
	{
		data_.setAt( index, value );
	}



	CUDA_HOST_DEVICE void set( const T& value )
	{
		data_.set( value );
	}



	CUDA_HOST_DEVICE void set( const cuTool::Array<T, size>& data_in )
	{
		data_ = data_in;
	}



	CUDA_HOST_DEVICE T& operator[]( const unsigned int index )
	{
		return data_[index];
	}



	CUDA_HOST_DEVICE T& at( const unsigned int index )
	{
		return data_.at( index );
	}



	CUDA_HOST_DEVICE const T& operator[]( const unsigned int index ) const
	{
		return data_[index];
	}



	CUDA_HOST_DEVICE const T& at( const unsigned int index ) const
	{
		return data_.at( index );
	}



	CUDA_HOST_DEVICE Vector& operator+=( const Vector& rhs )
	{
		for (unsigned int index = 0; index < size; ++index) data_[index] += rhs.data_[index];
		return *this;
	}



	CUDA_HOST_DEVICE Vector& operator-=( const Vector& rhs )
	{
		for (unsigned int index = 0; index < size; ++index) data_[index] -= rhs.data_[index];
		return *this;
	}



	CUDA_HOST_DEVICE Vector& operator*=( const T& rhs )
	{
		for (unsigned int index = 0; index < size; ++index) data_[index] *= rhs;
		return *this;
	}



	CUDA_HOST_DEVICE friend Vector operator*( Vector lhs, const T& rhs )
	{
		return lhs *= rhs;
	}



	CUDA_HOST_DEVICE friend Vector operator*( const T& lhs, Vector rhs )
	{
		return rhs *= lhs;
	}



	template<unsigned int s, typename U>
	CUDA_HOST_DEVICE friend Vector<s, U>& operator*=( Vector<s, U>& vector, const typename U::value_type& rhs );



	CUDA_HOST_DEVICE Vector& operator/=( const T& rhs )
	{
		for (unsigned int index = 0; index < size; ++index) data_[index] /= rhs;
		return *this;
	}



	CUDA_HOST_DEVICE friend Vector operator/( Vector lhs, const T& rhs )
	{
		return lhs /= rhs;
	}



	template<unsigned int s, typename U>
	CUDA_HOST_DEVICE friend Vector<s, U>& operator/=( Vector<s, U>& vector, const typename U::value_type& rhs );



	CUDA_HOST_DEVICE bool operator==( const Vector& rhs ) const
	{
		for (unsigned int index = 0; index < size; ++index) if (data_[index] != rhs.data_[index]) return false;
		return true;
	}



	CUDA_HOST_DEVICE bool operator!=( const Vector& rhs ) const
	{
		return !operator==( rhs );
	}



private:
	cuTool::Array<T, size> data_;
};





template<unsigned int size, typename T>
constexpr unsigned int Vector<size, T>::SIZE;





template<unsigned int size, typename T>
CUDA_HOST_DEVICE Vector<size, T>& operator*=( Vector<size, T>& vector, const typename T::value_type& rhs )
{
	for (unsigned int index = 0; index < size; ++index) vector.data_[index] *= rhs;
	return vector;
}



template<unsigned int size, typename T>
CUDA_HOST_DEVICE Vector<size, T>& operator/=( Vector<size, T>& vector, const typename T::value_type& rhs )
{
	for (unsigned int index = 0; index < size; ++index) vector.data_[index] /= rhs;
	return vector;
}



template<unsigned int size, typename T>
CUDA_HOST_DEVICE Vector<size, T> operator+( Vector<size, T> lhs, const Vector<size, T>& rhs )
{
	return lhs += rhs;
}



template<unsigned int size, typename T>
CUDA_HOST_DEVICE Vector<size, T> operator-( Vector<size, T> lhs, const Vector<size, T>& rhs )
{
	return lhs -= rhs;
}



template<unsigned int size, typename T>
CUDA_HOST_DEVICE Vector<size, T> operator*( Vector<size, T> lhs, const T& rhs );



template<unsigned int size, typename T>
CUDA_HOST_DEVICE Vector<size, T> operator*( const T& lhs, Vector<size, T> rhs );



template<unsigned int size, typename T>
CUDA_HOST_DEVICE Vector<size,T> operator*( Vector<size, T> lhs, const typename T::value_type& rhs )
{
	return lhs *= rhs;
}



template<unsigned int size, typename T>
CUDA_HOST_DEVICE Vector<size,T> operator*( const typename T::value_type& lhs, Vector<size, T> rhs )
{
	return rhs *= lhs;
}



template<unsigned int size, typename T>
CUDA_HOST_DEVICE Vector<size, T> operator/( Vector<size, T> lhs, const T& rhs );



template<unsigned int size, typename T>
CUDA_HOST_DEVICE Vector<size,T> operator/( Vector<size, T> lhs, const typename T::value_type& rhs )
{
	return lhs /= rhs;
}



template<unsigned int size, typename T>
CUDA_HOST_DEVICE T operator*( const Vector<size, T>& lhs, const Vector<size,T>& rhs )
{
	T result(0);
	for (unsigned int index = 0; index < size; ++index) result += lhs[index]*rhs[index];
	return result;
}



template<unsigned int size, typename T>
CUDA_HOST std::ostream& operator<<( std::ostream& stream, const Vector<size, T>& vector )
{
	stream << "( ";
	for (unsigned int i = 0; i < size - 1; ++i)
	{
		stream << vector[i] << ", ";
	}
	stream << vector[size - 1] << " )";
	return stream;
}



} // namespace cuMath
} // namespace finael

#endif // FINAEL_CUMATH_VECTOR_H_
