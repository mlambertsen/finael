/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_QCD_PDF_INTERPOLATION_CT_SPLINE_H_
#define FINAEL_QCD_PDF_INTERPOLATION_CT_SPLINE_H_

#include <stdexcept>

#include "../../definitions.h"

#include "CtFortranCodeSelector.h"
#include "CtDefaultGrid.h"
#include "CtFunctor.h"
#include "../PdfSplineBase.h"



namespace finael {
namespace qcd {
namespace pdfInterpolation {



// template just for interface reasons, to fit with other classes. In principle only T = double makes sense, because CT provides only double
template<CtSet ctSet, typename T = double>
class CtSpline : public PdfSplineBase<CtFunctor<ctSet>, int, double, std::string>
{
private:
	using PdfSpline = PdfSplineBase<CtFunctor<ctSet>, int, double, std::string>;



public:
	using SplineType = typename PdfSpline::SplineType;



	template<class PointIt>
	CtSpline( PointIt firstPoint, PointIt lastPoint, const SplineType type, const std::string& setName, const PartonFlavour flavour, const T scale )
		: PdfSpline( firstPoint, lastPoint, type, convertPartonFlavour(flavour), static_cast<double>( scale ), setName )
		, setName_( setName )
	{}



	template<class PointIt>
	CtSpline( PointIt firstPoint, PointIt lastPoint, const std::string& setName, const PartonFlavour flavour, const T scale )
		: CtSpline( firstPoint, lastPoint, SplineType::NATURAL, setName, flavour, scale )
	{}



	CtSpline( const SplineType type, const std::string& setName, const PartonFlavour flavour, const T scale )
		: CtSpline( CtDefaultGrid<T, ctSet>::POINTS.begin(), CtDefaultGrid<T, ctSet>::POINTS.end(), type, setName, flavour, scale )
	{}



	CtSpline( const std::string& setName, const PartonFlavour flavour, const T scale )
		: CtSpline( SplineType::NATURAL, setName, flavour, scale )
	{}



	void setDatafile( const std::string &inputSetName )
	{
		if (setName_ != inputSetName)
		{
			setName_ = inputSetName;
			update();
		}
	}



	void setDefaultPoints()
	{
		PdfSpline::set( CtDefaultGrid<T, ctSet>::POINTS.begin(), CtDefaultGrid<T, ctSet>::POINTS.end() );
	}



	void setScale( const T scale )
	{
		double newScale = static_cast<double>( scale );
		if (newScale != PdfSpline::function_.scale_)
		{
			PdfSpline::function_.scale_ = newScale;
			update();
		}
	}



	void setFlavour( const PartonFlavour flavour )
	{
		int newFlavour = convertPartonFlavour( flavour );
		if (newFlavour != PdfSpline::function_.flavour_)
		{
			PdfSpline::function_.flavour_ = newFlavour;
			update();
		}
	}



private:
	std::string setName_;



	int convertPartonFlavour( const PartonFlavour flavour ) const
	{
		switch (flavour)
		{
			case PartonFlavour::U:
				return 1;
			case PartonFlavour::UBAR:
				return -1;
			case PartonFlavour::D:
				return 2;
			case PartonFlavour::DBAR:
				return -2;
			case PartonFlavour::S:
				return 3;
			case PartonFlavour::SBAR:
				return -3;
			case PartonFlavour::C:
				return 4;
			case PartonFlavour::CBAR:
				return -4;
			case PartonFlavour::B:
				return 5;
			case PartonFlavour::BBAR:
				return -5;
			case PartonFlavour::G:
				return 0;
			case PartonFlavour::T:
			case PartonFlavour::TBAR:
			default:
				throw std::invalid_argument( "Required PartonFlavour is not availible for CT." );
		}
	}



	void update()
	{
		updateFortranRoutine();
		PdfSpline::update();
	}



	void updateFortranRoutine() const
	{
		CtFortranCodeSelector<ctSet>::set( setName_ );
	}
};



} // namespace pdfInterpolation
} // namespace qcd
} // namespace finael



#endif // FINAEL_QCD_PDF_INTERPOLATION_CT_SPLINE_H_
