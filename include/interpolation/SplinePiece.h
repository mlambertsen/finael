/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_INTERPOLATION_SPLINE_PIECE_H_
#define FINAEL_INTERPOLATION_SPLINE_PIECE_H_



namespace finael {
namespace interpolation {



template<typename T>
struct SplinePiece
{
	T cubic {0};
	T square {0};
	T linear {0};
	T constant {0};



	T operator()( const T& point ) const
	{
		return cubic*point*point*point + square*point*point + linear*point + constant;
	}



	SplinePiece& operator+=( const SplinePiece& rhs )
	{
		cubic += rhs.cubic;
		square += rhs.square;
		linear += rhs.linear;
		constant += rhs.constant;

		return *this;
	}



	SplinePiece& operator-=( const SplinePiece& rhs )
	{
		cubic -= rhs.cubic;
		square -= rhs.square;
		linear -= rhs.linear;
		constant -= rhs.constant;

		return *this;
	}
};



template<typename T>
SplinePiece<T> operator+( SplinePiece<T> lhs, const SplinePiece<T>& rhs )
{
	return lhs += rhs;
}



template<typename T>
SplinePiece<T> operator-( SplinePiece<T> lhs, const SplinePiece<T>& rhs )
{
	return lhs -= rhs;
}



} // namespace interpolation
} // namespace finael



#endif // FINAEL_INTERPOLATION_SPLINE_PIECE_H_
