/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUQCD_PARTON_DISTRIBUTION_H_
#define FINAEL_CUQCD_PARTON_DISTRIBUTION_H_

#include "../common/cudaHostDevice.h"
#include "../math/definitions.h"
#include "../math/Vector.h"
#include "../tools/Array.h"

#include "EvolutionKernelContainer.h"
#include "ParticleValidator.h"
#include "PartonDistributionCreator.h"
#include "PartonFlavourConverter.h"
#include "definitions.h"


namespace finael {
namespace cuQcd {



template<unsigned short numFlavours, typename DistType, typename ScaleType>
class PartonDistribution
{
public:
	static constexpr unsigned short NUMBERFLAVOURS = numFlavours;
	using value_type = DistType;
	using scale_type = ScaleType;
	using Singlet = cuMath::Vector<2, value_type>;
	using NonSingletNonValence = cuMath::Vector<numFlavours - 1, value_type>;



	CUDA_HOST_DEVICE PartonDistribution( const cuTool::Array<value_type, 2*numFlavours + 1>& inputData, const scale_type& inputScale )
		: scale_( inputScale )
		, nonSingletValence_( Creator::createNonSingletValence( inputData ) )
		, nonSingletNonValencePlus_( Creator::template createNonSingletNonValence<cuMath::Sign::PLUS>( inputData ) )
		, nonSingletNonValenceMinus_( Creator::template createNonSingletNonValence<cuMath::Sign::MINUS>( inputData ) )
		, singlet_( Creator::createSinglet( inputData ) )
	{}



	template<PartonFlavour flavour>
	CUDA_HOST_DEVICE value_type get() const
	{
		if (ParticleValidator<flavour>::isGluon) return singlet_[1];
		else return extractNonGluon<flavour>();
	}



	CUDA_HOST_DEVICE scale_type getScale() const
	{
		return scale_;
	}



	CUDA_HOST_DEVICE void evolve( const EvolutionKernelContainer<value_type>& evolutionKernel, const scale_type& newScale )
	{
		singlet_ = evolutionKernel.singletKernel * singlet_;
		nonSingletValence_ *= evolutionKernel.nonSingletValenceKernel;
		nonSingletNonValencePlus_ *= evolutionKernel.nonSingletNonValencePlusKernel;
		nonSingletNonValenceMinus_ *= evolutionKernel.nonSingletNonValenceMinusKernel;
		scale_ = newScale;
	}



private:
	using Creator = PartonDistributionCreator<numFlavours, value_type>;



	scale_type scale_;
	value_type nonSingletValence_;
	NonSingletNonValence nonSingletNonValencePlus_;
	NonSingletNonValence nonSingletNonValenceMinus_;
	Singlet singlet_; // first entry is the Singlet, second the Gluon



	template<PartonFlavour flavour>
	CUDA_HOST_DEVICE value_type extractNonGluon() const
	{
		constexpr bool isQuark = ParticleValidator<flavour>::isQuark;
		constexpr cuMath::Sign sign = (isQuark) ? cuMath::Sign::PLUS : cuMath::Sign::MINUS;

		unsigned short nflavour = (isQuark) ? PartonFlavourConverter<flavour>::INDEX : -PartonFlavourConverter<flavour>::INDEX;

		value_type result = cuMath::addOrSubstract<sign>( singlet_[0], nonSingletValence_ ) / static_cast<value_type>( numFlavours );
		if (flavour != PartonFlavour::U && flavour != PartonFlavour::UBAR) result -= cuMath::addOrSubstract<sign>(nonSingletNonValencePlus_[nflavour - 2], nonSingletNonValenceMinus_[nflavour - 2]) / static_cast<value_type>( nflavour );

		for (unsigned short sumindex = nflavour + 1; sumindex <= numFlavours; ++sumindex)
		{
			result += cuMath::addOrSubstract<sign>( nonSingletNonValencePlus_[sumindex - 2], nonSingletNonValenceMinus_[sumindex - 2] ) / static_cast<value_type>(sumindex * (sumindex - 1));
		}

		return static_cast<value_type>( 0.5 ) * result;
	}
};





template<unsigned short numFlavours, typename DistType, typename ScaleType>
constexpr unsigned short PartonDistribution<numFlavours, DistType, ScaleType>::NUMBERFLAVOURS;



} // namespace cuQcd
} // namespace finael


#endif // FINAEL_CUQCD_PARTON_DISTRIBUTION_H_
