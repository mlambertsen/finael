/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#define BOOST_TEST_MODULE "finael Vector"
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "../../../include/math/Vector.h"
#include "../../../include/math/Complex.h"



BOOST_AUTO_TEST_SUITE( vector )



BOOST_AUTO_TEST_CASE( defaultConstructor )
{
	using namespace finael::cuMath;
	constexpr unsigned int dimension = 3;
	int standardInitializer { 0 };
	Vector<dimension, int> vector;

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ), standardInitializer );
	}
}



BOOST_AUTO_TEST_CASE( fixedValueConstructor )
{
	using namespace finael::cuMath;
	constexpr unsigned int dimension = 3;
	int value { 5 };
	Vector<dimension, int> vector( value );

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ), value );
	}
}



BOOST_AUTO_TEST_CASE( rawArrayConstructor )
{
	using namespace finael::cuMath;
	constexpr unsigned int dimension = 3;
	int value { 7 };
	int array[dimension] {value, value, value};
	Vector<dimension, int> vector( array );

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ), value );
	}

	Vector<dimension, int> anotherVector( {{1, 1, 1}} );

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( anotherVector.at( i ), 1 );
	}
}



BOOST_AUTO_TEST_CASE( setIndexValue )
{
	using namespace finael::cuMath;
	constexpr unsigned int dimension = 3;
	int standardInitializer {0};
	Vector<dimension, int> vector;

	int value { 5 };
	int index = 1;
	vector.set( index, value );

	BOOST_CHECK_EQUAL( vector.at( 0 ), standardInitializer );
	BOOST_CHECK_EQUAL( vector.at( 1 ), value );
	BOOST_CHECK_EQUAL( vector.at( 2 ), standardInitializer );
}



BOOST_AUTO_TEST_CASE( setValue )
{
	using namespace finael::cuMath;
	constexpr unsigned int dimension = 3;
	Vector<dimension, int> vector;

	int value { 5 };
	vector.set( value );

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ), value );
	}
}



BOOST_AUTO_TEST_CASE( setArray )
{
	using namespace finael::cuMath;
	constexpr unsigned int dimension = 3;
	Vector<dimension, int> vector;

	int value { 7 };
	int array[dimension] {value, value, value};
	vector.set( array );

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ), value );
	}
}



BOOST_AUTO_TEST_CASE( copyConstructor )
{
	using namespace finael::cuMath;
	constexpr unsigned int dimension = 3;

	Vector<dimension, int> vector( 23 );
	Vector<dimension, int> copy( vector );

	vector.set( 42 );

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( copy.at( i ), 23 );
	}
}



BOOST_AUTO_TEST_CASE( assignmentOperator )
{
	using namespace finael::cuMath;
	constexpr unsigned int dimension = 3;

	Vector<dimension, int> vector( 23 );
	Vector<dimension, int> copy;

	copy = vector;

	vector.set( 42 );

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( copy.at( i ), 23 );
	}
}



BOOST_AUTO_TEST_CASE( operatorPlusEqual )
{
	using namespace finael::cuMath;
	constexpr unsigned int dimension = 3;
	Vector<dimension, int> vector( 5 );
	Vector<dimension, int> summand( 7 );

	vector += summand;

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ), 12 );
	}
}



BOOST_AUTO_TEST_CASE( operatorMinusEqual )
{
	using namespace finael::cuMath;
	constexpr unsigned int dimension = 3;
	Vector<dimension, int> vector( 5 );
	Vector<dimension, int> subtract( 7 );

	vector -= subtract;

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ), -2 );
	}
}



BOOST_AUTO_TEST_CASE( operatorTimesEqual )
{
	using namespace finael::cuMath;
	constexpr unsigned int dimension = 3;
	Vector<dimension, int> vector( 5 );
	int factor { 7 };

	vector *= factor;

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ), 35 );
	}

	double doubleFactor {2.0};

	vector *= doubleFactor;

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ), 70 );
	}
}



BOOST_AUTO_TEST_CASE( operatorTimesEqualComplex )
{
	using namespace finael::cuMath;
	constexpr unsigned int dimension = 3;
	Vector<dimension, Complex<int>> vector( Complex<int>( 5, 1) );
	Complex<int> factor( 1, 5 );

	vector *= factor;

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ).real(), 0 );
		BOOST_CHECK_EQUAL( vector.at( i ).imag(), 26 );
	}

	double doubleFactor {2.0};

	vector *= doubleFactor;

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ).real(), 0 );
		BOOST_CHECK_EQUAL( vector.at( i ).imag(), 52 );
	}
}



BOOST_AUTO_TEST_CASE( operatorDivideEqual )
{
	using namespace finael::cuMath;
	constexpr unsigned int dimension = 3;
	Vector<dimension, int> vector( 1024 );
	int divide { 4 };

	vector /= divide;

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ), 256 );
	}

	double doubleDivide {2.0};

	vector /= doubleDivide;

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ), 128 );
	}
}



BOOST_AUTO_TEST_CASE( operatorDivideEqualComplex )
{
	using namespace finael::cuMath;
	constexpr unsigned int dimension = 3;
	Vector<dimension, Complex<int>> vector( Complex<int>( 1024, 512) );
	Complex<int> divide( 2, 0 );

	vector /= divide;

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ).real(), 512 );
		BOOST_CHECK_EQUAL( vector.at( i ).imag(), 256 );
	}

	double doubleDivide {16.0};

	vector /= doubleDivide;

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ).real(), 32 );
		BOOST_CHECK_EQUAL( vector.at( i ).imag(), 16 );
	}
}



BOOST_AUTO_TEST_CASE( operatorPlus )
{
	using namespace finael::cuMath;
	constexpr unsigned int dimension = 3;
	Vector<dimension, int> vector( 5 );
	Vector<dimension, int> summand( 7 );

	vector = vector + summand;

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ), 12 );
	}
}



BOOST_AUTO_TEST_CASE( operatorMinus )
{
	using namespace finael::cuMath;
	constexpr unsigned int dimension = 3;
	Vector<dimension, int> vector( 5 );
	Vector<dimension, int> subtract( 7 );

	vector = vector - subtract;

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ), -2 );
	}
}



BOOST_AUTO_TEST_CASE( operatorTimes )
{
	using namespace finael::cuMath;
	constexpr unsigned int dimension = 3;
	Vector<dimension, int> vector( 5 );
	int factor { 7 };

	vector = vector * factor;

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ), 35 );
	}

	vector = factor * vector;

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ), 245 );
	}

	double doubleFactor {2.0};

	vector = vector * doubleFactor;

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ), 490 );
	}

	vector = doubleFactor * vector;

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ), 980 );
	}
}



BOOST_AUTO_TEST_CASE( operatorTimesComplex )
{
	using namespace finael::cuMath;
	constexpr unsigned int dimension = 3;
	Vector<dimension, Complex<int>> vector( Complex<int>( 5, 1 ) );

	Complex<int> complexFactor( 2, 0 );
	vector = vector * complexFactor;

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ).real(), 10 );
		BOOST_CHECK_EQUAL( vector.at( i ).imag(), 2 );
	}

	vector = complexFactor * vector;

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ).real(), 20 );
		BOOST_CHECK_EQUAL( vector.at( i ).imag(), 4 );
	}

	int factor { 7 };
	vector = vector * factor;

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ).real(), 140 );
		BOOST_CHECK_EQUAL( vector.at( i ).imag(), 28 );
	}

	vector = factor * vector;

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ).real(), 980 );
		BOOST_CHECK_EQUAL( vector.at( i ).imag(), 196 );
	}

	double doubleFactor {2.0};
	vector = vector * doubleFactor;

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ).real(), 1960 );
		BOOST_CHECK_EQUAL( vector.at( i ).imag(), 392 );
	}

	vector = doubleFactor * vector;

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ).real(), 3920 );
		BOOST_CHECK_EQUAL( vector.at( i ).imag(), 784 );
	}
}



BOOST_AUTO_TEST_CASE( operatorDivide )
{
	using namespace finael::cuMath;
	constexpr unsigned int dimension = 3;
	Vector<dimension, int> vector( 1024 );
	int divide { 4 };

	vector = vector / divide;

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ), 256 );
	}

	double doubleDivide {2.0};

	vector = vector / doubleDivide;

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ), 128 );
	}
}



BOOST_AUTO_TEST_CASE( operatorDivideComplex )
{
	using namespace finael::cuMath;
	constexpr unsigned int dimension = 3;
	Vector<dimension, Complex<int>> vector( Complex<int>( 1024, 512 ) );

	Complex<int> complexDivisor( 2, 0 );
	vector = vector / complexDivisor;

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ).real(), 512 );
		BOOST_CHECK_EQUAL( vector.at( i ).imag(), 256 );
	}

	int divisor { 4 };
	vector = vector / divisor;

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ).real(), 128 );
		BOOST_CHECK_EQUAL( vector.at( i ).imag(), 64 );
	}

	double doubleDivisor {8.0};
	vector = vector / doubleDivisor;

	for (unsigned int i = 0; i < dimension; ++i)
	{
		BOOST_CHECK_EQUAL( vector.at( i ).real(), 16 );
		BOOST_CHECK_EQUAL( vector.at( i ).imag(), 8 );
	}
}



BOOST_AUTO_TEST_CASE( scalarProduct )
{
	using namespace finael::cuMath;
	constexpr unsigned int dimension = 3;
	Vector<dimension, int> first( {{1, 2, 3}} );

	Vector<dimension, int> second( {{4, 5, 6}} );

	BOOST_CHECK_EQUAL( first * second, 32 );
}



BOOST_AUTO_TEST_CASE( operatorIsEqual )
{
	using namespace finael::cuMath;
	constexpr unsigned int dimension = 3;
	Vector<dimension, int> first( {{1, 2, 3}} );
	Vector<dimension, int> second( {{1, 2, 4}} );
	Vector<dimension, int> third( {{2, 2, 3}} );
	Vector<dimension, int> fourth( {{3, 2, 1}} );

	BOOST_CHECK_EQUAL( first == first, true );
	BOOST_CHECK_EQUAL( first == second, false );
	BOOST_CHECK_EQUAL( first == third, false );
	BOOST_CHECK_EQUAL( first == fourth, false );
}



BOOST_AUTO_TEST_CASE( operatorIsNotEqual )
{
	using namespace finael::cuMath;
	constexpr unsigned int dimension = 3;
	Vector<dimension, int> first( {{1, 2, 3}} );
	Vector<dimension, int> second( {{1, 2, 4}} );
	Vector<dimension, int> third( {{2, 2, 3}} );
	Vector<dimension, int> fourth( {{3, 2, 1}} );

	BOOST_CHECK_EQUAL( first != first, false );
	BOOST_CHECK_EQUAL( first != second, true );
	BOOST_CHECK_EQUAL( first != third, true );
	BOOST_CHECK_EQUAL( first != fourth, true );
}



BOOST_AUTO_TEST_SUITE_END()
