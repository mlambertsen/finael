/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_SAMPLES_H_
#define FINAEL_CUVEGAS_SAMPLES_H_

#include "../common/cudaHostDevice.h"
#include "../tools/Array.h"
#include "../tools/SmartArray.h"

#include "increments/IncrementsIndicesContainer.h"
#include "optionFlags.h"


namespace finael {
namespace cuVegas {



template<typename T, CudaHostDeviceType hostDeviceType, ReductionFlag reductionMode>
class Samples
{
public:
	using type = T;



	friend class Samples<T, CudaHostDeviceType::HOST, reductionMode>;
	#ifdef __CUDACC__
	friend class Samples<T, CudaHostDeviceType::DEVICE, reductionMode>;
	#endif // __CUDACC__



	CUDA_HOST Samples( unsigned int numberSamples, unsigned int dim )
		: functionValues( numberSamples )
		, incrementIndices( dim*numberSamples )
	{}



	template<CudaHostDeviceType otherDevice>
	CUDA_HOST Samples( const Samples<T, otherDevice, reductionMode>& other )
		: functionValues ( other.functionValues )
		, incrementIndices ( other.incrementIndices )
	{}



	CUDA_HOST_DEVICE unsigned int size() const
	{
		return functionValues.size();
	}



	template<unsigned int dim, class Functor>
	CUDA_HOST_DEVICE void evaluate( unsigned int idx, const cuTool::Array<T, dim>& point, const Functor& functor, T inverseWeightFunction )
	{
		functionValues[idx] = inverseWeightFunction*functor( point );
	}



	CUDA_HOST_DEVICE void setIncrementIdx( unsigned int idx, unsigned int dimIdx, unsigned int incrementIdx )
	{
		incrementIndices.setIncrementIdx( dimIdx*size() + idx, incrementIdx );
	}



	CUDA_HOST_DEVICE T getFunctionValue( unsigned int idx ) const
	{
		return functionValues[idx];
	}



	CUDA_HOST_DEVICE unsigned int getIncrementIdx( unsigned int idx, unsigned int dimIdx ) const
	{
		return incrementIndices.getIncrementIdx( dimIdx*size() + idx );
	}



private:
	cuTool::SmartArray<T, hostDeviceType> functionValues;
	IncrementsIndicesContainer<hostDeviceType, reductionMode> incrementIndices;
};



} // namespace cuVegas
} // namespace finael


#endif // FINAEL_CUVEGAS_SAMPLES_H_
