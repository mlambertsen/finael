/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_TOOL_STOP_WATCH_H_
#define FINAEL_TOOL_STOP_WATCH_H_

#include <chrono>
#include <iostream>


namespace finael {
namespace tool {



template<class stdChronoDuration>
class StopWatch
{
public:
	using clock = std::chrono::steady_clock;


	StopWatch()
		: start( clock::now() )
	{}



	~StopWatch()
	{
		auto end = clock::now();
		auto duration = std::chrono::duration_cast<stdChronoDuration>(end - start);
		std::cout << duration.count() << std::endl;
	}



private:
	clock::time_point start;



	static_assert( clock::is_steady, "No reliable measurements possible." );
};



} // namespace tool
} // namespace finael




#endif // FINAEL_TOOL_STOP_WATCH_H_
