/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_QCD_PDF_INTERPOLATION_MMHT2014_SPLINE_H_
#define FINAEL_QCD_PDF_INTERPOLATION_MMHT2014_SPLINE_H_

#include <stdexcept>

#include "../../definitions.h"

#include "mmht2014_fortran.h"
#include "Mmht2014DefaultGrid.h"
#include "Mmht2014Functor.h"
#include "../PdfSplineBase.h"



namespace finael {
namespace qcd {
namespace pdfInterpolation {



// template just for interface reasons, to fit with other classes. In principle only T = double makes sense, because MMHT2014 provides only double
template<typename T = double>
class Mmht2014Spline : public PdfSplineBase<Mmht2014Functor, int, double, int, std::string>
{
private:
	using PdfSpline = PdfSplineBase<Mmht2014Functor, int, double, int, std::string>;



public:
	using SplineType = typename PdfSpline::SplineType;



	template<class PointIt>
	Mmht2014Spline( PointIt firstPoint, PointIt lastPoint, const SplineType type, const std::string& filePrefix, const int eigenvector, const PartonFlavour flavour, const T scale )
		: PdfSpline( firstPoint, lastPoint, type, convertPartonFlavour( flavour ), static_cast<double>( scale ), eigenvector, filePrefix )
	{}



	template<class PointIt>
	Mmht2014Spline( PointIt firstPoint, PointIt lastPoint, const std::string& filePrefix, const int eigenvector, const PartonFlavour flavour, const T scale )
		: Mmht2014Spline( firstPoint, lastPoint, SplineType::NATURAL, filePrefix, eigenvector, flavour, scale )
	{}



	Mmht2014Spline( const SplineType type, const std::string& filePrefix, const int eigenvector, const PartonFlavour flavour, const T scale )
		: Mmht2014Spline( Mmht2014DefaultGrid<T>::POINTS.begin(), Mmht2014DefaultGrid<T>::POINTS.end(), type, filePrefix, eigenvector, flavour, scale )
	{}



	Mmht2014Spline(const std::string& filePrefix, const int eigenvector, const PartonFlavour flavour, const T scale )
		: Mmht2014Spline( SplineType::NATURAL, filePrefix, eigenvector, flavour, scale )
	{}



	void setFilePrefix( const std::string& filePrefix )
	{
		std::strcpy( PdfSpline::function_.filePrefix_, filePrefix.c_str() );
		PdfSpline::function_.fileSize_ = filePrefix.length();
		PdfSpline::update();
	}



	void setEigenvector( const int eigenvector )
	{
		if (eigenvector != PdfSpline::function_.eigenvector_)
		{
			PdfSpline::function_.eigenvector_ = eigenvector;
			PdfSpline::update();
		}
	}



	void setDefaultPoints()
	{
		PdfSpline::set( Mmht2014DefaultGrid<T>::POINTS.begin(), Mmht2014DefaultGrid<T>::POINTS.end() );
	}



	void setScale( T scale )
	{
		double newScale = static_cast<double>( scale );
		if (newScale != PdfSpline::function_.scale_)
		{
			PdfSpline::function_.scale_ = newScale;
			PdfSpline::update();
		}
	}



	void setFlavour( const PartonFlavour flavour )
	{
		int newFlavour = convertPartonFlavour( flavour );
		if (newFlavour != PdfSpline::function_.flavour_)
		{
			PdfSpline::function_.flavour_ = newFlavour;
			PdfSpline::update();
		}
	}



private:
	int convertPartonFlavour( const PartonFlavour flavour ) const
	{
		switch (flavour)
		{
			case PartonFlavour::U:
				return 2;
			case PartonFlavour::UBAR:
				return -2;
			case PartonFlavour::D:
				return 1;
			case PartonFlavour::DBAR:
				return -1;
			case PartonFlavour::S:
				return 3;
			case PartonFlavour::SBAR:
				return -3;
			case PartonFlavour::C:
				return 4;
			case PartonFlavour::CBAR:
				return -4;
			case PartonFlavour::B:
				return 5;
			case PartonFlavour::BBAR:
				return -5;
			case PartonFlavour::T:
				return 6;
			case PartonFlavour::TBAR:
				return -6;
			case PartonFlavour::G:
				return 0;
			default:
				throw std::invalid_argument( "Required PartonFlavour is not availible for MMHT2014." );
		}
	}
};



} // namespace pdfInterpolation
} // namespace qcd
} // namespace finael



#endif // FINAEL_QCD_PDF_INTERPOLATION_MMHT2014_SPLINE_H_
