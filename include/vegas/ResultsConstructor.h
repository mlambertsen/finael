/*
Copyright (c) 2014-2018 Martin Lambertsen
License: MIT (https://opensource.org/licenses/MIT)
*/

#ifndef FINAEL_CUVEGAS_RESULTS_CONSTRUCTOR
#define FINAEL_CUVEGAS_RESULTS_CONSTRUCTOR

#include "../common/cudaHostDevice.h"

#include "Results.h"


namespace finael {
namespace cuVegas {



template<typename T>
class ResultsConstructor
{
public:
	CUDA_HOST static Results<T> constructResults( const AccumulatedSums<T>& accumulatedSums )
	{
		T variance = 1 / accumulatedSums.oneOverVariance;
		T estimate = variance * accumulatedSums.estimateOverVariance;
		T standardDeviation = std::sqrt( variance );
		T chiSquaredPerIteration = (accumulatedSums.cardinality == 1) ? 0 : (accumulatedSums.estimateSquaredOverVariance - estimate*accumulatedSums.estimateOverVariance)/(accumulatedSums.cardinality - 1);

		return Results<T>( estimate, standardDeviation, chiSquaredPerIteration );
	}
};



} // namespace cuVegas
} // namespace finael




#endif // FINAEL_CUVEGAS_RESULTS_CONSTRUCTOR
